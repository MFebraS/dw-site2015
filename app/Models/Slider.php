<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model {

	protected $table = 'sliders';

	protected $guarded = ['id'];

	public $timestamps = false;

	public static function getBySequence(){
		return SLider::orderBy('sequence', 'asc')->get();
	}
}
