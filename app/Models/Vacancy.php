<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vacancy extends Model {

	protected $table = "vacancies";

	protected $guarded = ['id'];

	public static function rules(){
		return [
			'title' => 'required|unique:vacancies,title',
		];
	}
}
