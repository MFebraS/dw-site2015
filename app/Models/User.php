<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	protected $table = 'users';

	protected $guarded = ['id'];

	protected $hidden = ['password', 'remember_token'];

	public $timestamps = false;

	public function news()
	{
		return $this->hasMany('App\Models\News', 'author_id', 'id');
	}

	public static function createRules(){
		return [
			'name' => 'required',
			'email' => 'required|email|unique:users,email',
			'employ' => 'required'
		];
	}
	public static function updateRules(){
		return [
			'name' => 'required',
			'email' => 'required|email|unique:users,email',
			'about' => 'max:120'
		];
	}

	public static function passwordRules(){
		return [
			'password' => 'required|confirmed|min:8',
		];
	}
}
