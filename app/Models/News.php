<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model {

	protected $table = 'news';

	protected $guarded = ['id'];

	public function category()
	{
		return $this->belongsTo('App\Models\Category');
	}

	public function news_picture()
	{
		return $this->hasMany('App\Models\NewsPicture');
	}

	public function user()
	{
		return $this->belongsTo('App\Models\User', 'author_id', 'id');
	}

	public static function rules(){
		return [
			'post_title' => 'required|unique:news,title',
			'permalink' => 'required|unique:news,permalink',
			'news_content_text' => 'required',
		];
	}
}