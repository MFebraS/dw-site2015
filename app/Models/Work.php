<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Work extends Model {

	protected $table = 'works';

	protected $guarded = ['id'];

	public function pictures(){
		return $this->hasMany('App\Models\WorkPicture')->orderBy('sequence', 'asc');
	}

	public function tags(){
		return $this->belongsToMany('App\Models\Tag', 'work_tag', 'work_id', 'tag_id');
	}

	public function client(){
		return $this->belongsTo('App\Models\Client');
	}

	public static function createRules(){
		return [
			'project_title' => 'required|unique:works,project_title',
			'client' => 'required',
			'description' => 'required|max:380',
			'tag_name' => 'required',
		];
	}

	public static function updateRules(){
		return [
			'project_title' => 'required|unique:works,project_title',
			'client' => 'required',
			'tag_name' => 'required',
			'description' => 'max:380'
		];
	}
}
