<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewsPicture extends Model {

	protected $table = 'news_pictures';

	protected $guaarded = ['id'];

	public function news()
	{
		return $this->belongsTo('App\Models\News');
	}

}
