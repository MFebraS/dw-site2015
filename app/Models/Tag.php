<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model {

	protected $table = 'tags';

	protected $guarded = ['id'];

	public $timestamps = false;

	public function works(){
		return $this->belongsToMany('App\Models\Work', 'work_tag', 'tag_id', 'work_id');
	}

}
