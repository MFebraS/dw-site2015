<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {

	protected $table = 'news_categories';

	protected $guarded = ['id'];

	public function news()
	{
		return $this->hasMany('App\Models\News');
	}

	public static function rules(){
		return [
			'category_name' => 'required|unique:news_categories,category_name',
			'slug' => 'required|unique:news_categories,slug'
		];
	}

}
