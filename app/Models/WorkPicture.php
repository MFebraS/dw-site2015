<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorkPicture extends Model {

	protected $table = 'work_pictures';

	protected $guarded = ['id'];

	public $timestamps = false;

	public function work(){
		return $this->belongsTo('App\Models\Work', 'id', 'work_id');
	}
}
