<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model {

	protected $table = 'clients';

	protected $guarded = ['id'];

	public $timestamps = false;

	public function works()
	{
		return $this->hasMany('App\Models\Work');
	}

	public static function rules(){
		return [
			'name' => 'required|unique:clients,name',
			
		];
	}

}
