<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorkTag extends Model {

	protected $table = 'work_tag';

	protected $guarded = ['id'];

	public $timestamps = false;

	
}
