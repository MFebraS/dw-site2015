<?php namespace App\Http\Controllers;

use App\Models\News;
use App\Models\Category;
use App\Models\NewsPicture;
use App\Models\User;

use DB;
use Illuminate\Http\Request;

class NewsController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$categories = new Category;
		$pictures = new NewsPicture;
		$news = new News;

		// jumlah recent dan popular news
		$limit = 5;
		$contents = [];
		$picture_names = [];
		$popular_post_titles = [];
		$recent_post_titles = [];
		$categories_count = [];

		$query = $request->q;
		$categories = $categories->get();
		// Menghitung jumlah kategori
		foreach ($categories as $key => $category) {
			$categories_count[] = $categories->find($category->id)->news()->where('status', '=', 'Published')->count();
		}

		$all_posts = $this->filter($news, $query);
		// $all_posts_titles = [];

		foreach ($all_posts as $post) {
			// $all_posts_titles[] = str_limit($post->title, 90);
			$content = htmlspecialchars_decode($post->html_content);
			// menghilangkan tag img dan br
			$no_image = preg_replace("/<img[^>]+\>/i", "", $content);
			$no_br = preg_replace("/<br \/>/i", " ", $no_image);
			$contents[] = str_limit($no_br, 350);

			$picture = $pictures->where('news_id', '=', $post->id)->where('status', '=', 'Feature')->pluck('file_name');
			$picture_names[] = $picture;
		}

		// Popular news
		$popular_posts = $news->where('status', '=', 'Published')->orderBy('hits', 'desc')->take($limit)->get();

		foreach ($popular_posts as $popular_post) {
			$popular_post_titles[] = str_limit($popular_post->title, 55);
		}

		// Recent news
		$recent_posts = $news->where('status', '=', 'Published')->orderBy('published_date', 'desc')->take($limit)->get();
		
		foreach ($recent_posts as $recent_post) {
			$recent_post_titles[] = str_limit($recent_post->title, 55);
		}


		$active = ["news"];
		return view('news', compact('all_posts', 'contents', 'picture_names', 'categories', 'recent_posts', 'recent_post_titles', 'popular_posts', 'popular_post_titles', 'query', 'categories_count', 'active'));
	}

	public function filter($all_posts, $query)
	{
		// Untuk jumlah news per halaman
		$limit = 5;
		
		$all_posts = $all_posts->where('status', '=', 'Published')->orderBy('published_date', 'desc');
		// Search
		if ($query != null) {
			$all_posts = $all_posts->where('title', 'like', '%'.$query.'%')->orWhere('content', 'like', '%'.$query.'%');
		}

		$all_posts = $all_posts->paginate($limit);

		if ($query != null) {
			$all_posts = $all_posts->appends(['q' => $query]);
		}

		return $all_posts;
	}

	public function category_filter($category_slug){
		$categories = new Category;
		$news = new News;
		$pictures = new NewsPicture;
		// untuk search box
		$query = null;
		// Jumlah news per halaman
		$limit = 5;
		$popular_post_titles = [];
		$recent_post_titles = [];

		$category = $categories->where('slug', '=', $category_slug)->first();
		$category_name = $category->category_name;
		$cat_id = $category->id;
		// Mengambil news dengan kategori tertentu
		$all_posts = $categories->find($cat_id)->news()->where('status', '=', 'Published')->orderBy('published_date', 'desc')->paginate($limit);
		
		$categories = $categories->get();
		// Menghitung jumlah kategori
		foreach ($categories as $key => $category) {
			$categories_count[] = $categories->find($category->id)->news()->where('status', '=', 'Published')->count();
		}

		$picture_names = [];
		foreach ($all_posts as $post) {
			$content = htmlspecialchars_decode($post->html_content);
			// menghilangkan tag img dan br
			$no_image = preg_replace("/<img[^>]+\>/i", "", $content);
			$no_br = preg_replace("/<br \/>/i", " ", $no_image);
			$contents[] = str_limit($no_br, 350);

			$picture = $pictures->where('news_id', '=', $post->id)->where('status', '=', 'Feature')->pluck('file_name');
			$picture_names[] = $picture;
		}

		$message = "Kategori <i>$category_name</i>";

		// Popular news
		$popular_posts = $news->where('status', '=', 'Published')->orderBy('hits', 'desc')->take($limit)->get();

		foreach ($popular_posts as $popular_post) {
			$popular_post_titles[] = str_limit($popular_post->title, 55);
		}

		// Recent news
		$recent_posts = $news->where('status', '=', 'Published')->orderBy('published_date', 'desc')->take($limit)->get();
		
		foreach ($recent_posts as $recent_post) {
			$recent_post_titles[] = str_limit($recent_post->title, 55);
		}

		$active = ["news"];
		return view('news', compact('all_posts', 'contents', 'picture_names', 'categories',
									'recent_posts', 'recent_post_titles', 'popular_posts',
									'popular_post_titles', 'query', 'message', 'categories_count', 'active')
		);
	}

	public function news_detail($news_permalink) {
		$categories = new Category;
		$news = new News;
		$picture = new NewsPicture;
		// Jumlah popular dan recent news
		$limit = 5;
		$popular_post_titles = [];
		$recent_post_titles = [];

		$categories = $categories->get();
		// Menghitung jumlah kategori
		foreach ($categories as $key => $category) {
			$categories_count[] = $categories->find($category->id)->news()->where('status', '=', 'Published')->count();
		}
		
		$post = $news->where('permalink', '=', $news_permalink)->first();

		$content = htmlspecialchars_decode($post->html_content);
		$picture_name = $picture->where('news_id', '=', $post->id)->where('status', '=', 'Feature')->pluck('file_name');

		// Update hits for popular news
		DB::beginTransaction();
		$hits = $post->hits+1;
		$post->hits = $post->update(['hits' => $hits]);
		DB::commit();
		
		// Popular news
		$popular_posts = $news->where('status', '=', 'Published')->orderBy('hits', 'desc')->take($limit)->get();
		foreach ($popular_posts as $popular_post) {
			$popular_post_titles[] = str_limit($popular_post->title, 55);
		}

		// Recent news
		$recent_posts = $news->where('status', '=', 'Published')->orderBy('published_date', 'desc')->take($limit)->get();
		foreach ($recent_posts as $recent_post) {
			$recent_post_titles[] = str_limit($recent_post->title, 55);
		}

		$active = ["news"];
		return view('news_detail', compact('categories', 'post','content', 'picture_name',
											'recent_posts', 'recent_post_titles', 'popular_posts',
											'popular_post_titles', 'categories_count', 'active'));
	}

}
