<?php namespace App\Http\Controllers;

use App\Models\User;

class AboutUsController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$users = User::whereActive('1')->where('picture', '!=', '')->get();
		
		$rows = [];
		$item = 0;
		for($i = 0; $i<(ceil($users->count()/4)); $i++){
			$remains = $users->count() - $item;
			$lastJ = $remains > 4 && $remains % 4 == 1 ? 3 : ($remains < 4 ? $remains : 4);
			for($j = 0; $j < $lastJ; $j++){
				$rows[$i][] = $item++;
			}
		}
		$active = ["about-us"];
		return view('about-us', compact('users', 'rows', 'active'));
	}

}
