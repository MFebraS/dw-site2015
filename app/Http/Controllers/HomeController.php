<?php namespace App\Http\Controllers;

use App\Models\Work;
use App\Models\Slider;
use App\Models\Client;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$sliders = Slider::getBySequence();

		$works = new Work();
		$works = $works->whereStatus('published')->orderBy('created_at', 'desc')->take(6)->get();
		$count = $works->count();
		$our_works = [];
		foreach ($works as $key => $work) {
			$tags = $work->tags;
			$work_tag = [];
			foreach ($tags as $key => $tag) {
				$work_tag[] = $tag->tag_name;
			}
			$our_works[] = [
				'slug' => $work->slug,
				'project_title' => $work->project_title,
				'tags' => implode(", ", $work_tag),
				'picture' => $work->pictures->first()->picture_name

			];
		}
		$clients = Client::where('logo', '!=', '')->orderBy('order', 'asc')->take(12)->get();

		$active = [""];
		return view('home', compact('sliders', 'our_works', 'count', 'clients', 'active'));
	}

}
