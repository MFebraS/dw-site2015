<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Mail;
use App\Models\User;

use Config;
use ReCaptcha\ReCaptcha;
use Validator;

class ContactUsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$active = ["contact-us"];
		return view('contact-us', compact('active'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function send(Request $request)
	{
		$recaptcha = new ReCaptcha(Config::get('recaptcha.secret_key'));
		$resp = $recaptcha->verify($request->input('g-recaptcha-response'), $request->ip());
		
		if ($resp->isSuccess()) {
			$email_address = $request->email;
			$name = $request->name;

			$validator = Validator::make($request->all(), ['name' => 'required', 'email' => 'required|email']);

			if($validator->fails()){
				return redirect()->back()->withErrors($validator->errors())->withInput();
			}

			$dataNotif = [
				'name' => $name,
				'email' => $email_address,
				'content' => $request->message
			];

			Mail::send('emails.notification', $dataNotif, function($message) {
				$message->to('contact@durenworks.com')->subject('Notification Message');
			});
			
			$success = "Message Has Been Sent";
			return redirect(action('ContactUsController@index').'#form-contact')->withSuccess($success);
		} else {
			$errors = $resp->getErrorCodes();
			$error = "reCAPTCHA failed, please try again.";
			return redirect(action('ContactUsController@index').'#form-contact')->withError($error)->withInput();
		}

		
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
