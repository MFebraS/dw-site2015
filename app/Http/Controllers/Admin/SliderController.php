<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Config;
use App\Models\Slider;
use Response;
use DB;
use File;

class SliderController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$slider_path = Config::get("custom_path.sliders");

		$last = 0;

		$latestRecord = Slider::orderBy("id", "desc")->first();

		$images = array_diff(scandir($slider_path), [".", ".."]);
		foreach($images as $key => $image){
			$images[$key] = explode("-", (explode(".", $image)[0]))[0];
		}
		rsort($images);


		if($latestRecord != null){
			$last = $latestRecord->id;
			if($images != null){
				$last = max($last, $images[0]);
			}
			//dd($last);
		}

		$sliders = Slider::getBySequence();

		$active = ["dashboard", "slider"];
		return view('admin.dashboard.slider_index', compact('sliders', 'last', 'active'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function upload(Request $request)
	{
		$file = $request->file('image');

		$slider = new Slider();
		$slider->image = $file->getClientOriginalExtension();
		$slider->save();

		$filename = $slider->id.'.'.$file->getClientOriginalExtension();

		$input = [
			'image' => $filename,
			'sequence' => $request->get('id')
		];
		$slider->fill($input)->save();

		$file_path = Config::get('custom_path.sliders');

		$file->move($file_path, $filename);

		$result = [
			'status' => 'ok',
			'old_id' => $request->get('id'),
			'new_id' => $slider->id,
			'filename' => $file_path."/".$filename
		];
		return Response::json($result);
	}

	public function saveSequence(Request $request){
		$images_order = $request->images_order;
		$images_order = explode(",", $images_order);
		//dd($images_order);
		$sliders = Slider::get();
		DB::beginTransaction();
		foreach($sliders as $slider){
			$sequence = array_search($slider->id, $images_order) + 1;
			$slider->sequence = $sequence;
			$slider->save();
		}
		DB::commit();
		return redirect()->route('slider_index');
	}

	public function saveDescription(Request $request){
		$subimage = $request->file('subimage');
		$description = trim($request->description);
		$description = e($request->description);

		$input = [
			'description_position' => $request->position,
			'description' => $description
		];
		if($subimage != null){
			$extension = $subimage->getClientOriginalExtension();
			$new_filename = $request->id."-sub.".$extension;
			$input = array_add($input, 'subimage', $new_filename);
			$subimage->move(Config::get('custom_path.sliders'), $new_filename);
		}
		$slider = Slider::whereId($request->id)->first();
		$slider->fill($input)->save();


		return redirect()->route('slider_index');
	}
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Slider $slider)
	{
		$image = $slider->image;

		$fullpath = Config::get('custom_path.sliders')."/".$image;
		File::delete($fullpath);

		$slider->delete();

		$result = [
			'status' => 'ok'
		];
		return Response::json($result);
	}

}
