<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Vacancy;
use Validator;
use Auth;
use League\CommonMark\CommonMarkConverter;

class VacanciesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if(Auth::user()->level != 1){
			return redirect()->route('dashboard');
		}
		$vacancies = Vacancy::where('detail', '!=', '')->orderBy('title', 'asc')->get();
		$empties = Vacancy::where(function($q){
			$q->orWhereNull('detail')->orWhere('detail', '=', '');
		})->orderBy('title','asc')->get();

		$active = ["vacancies", "all"];
		return view('admin.vacancies.vacancy_index', compact('vacancies', 'empties', 'active'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		if(Auth::user()->level != 1){
			return redirect()->route('dashboard');
		}

		$active = ["vacancies", "new"];
		return view('admin.vacancies.vacancy_create', compact('active'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		//dd($request->all());
		$validator = Validator::make($request->all(), Vacancy::rules());

		if($validator->fails()){
			return redirect()->back()->withErrors($validator->errors())->withInput();
		}

		$converter = new CommonMarkConverter();
		$html_detail = $converter->convertToHtml($request->detail);

		$input = [
			'title' => $request->title,
			'detail' => e($request->detail),
			'html_detail' => e($html_detail),
			'slug' => str_slug($request->title, '-'),
			'status' => $request->publish != "" ? 'published' : 'draft'
		];
		
		$vacancy = new Vacancy();
		$vacancy->fill($input)->save();

		$success = "Created Successfully";
		return redirect()->route('vacancy_index')->withSuccess($success);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(Vacancy $vacancy)
	{
		if(Auth::user()->level != 1){
			return redirect()->route('dashboard');
		}

		$active = ["vacancies", ""];
		return view('admin.vacancies.vacancy_show', compact('vacancy', 'active'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(Vacancy $vacancy)
	{
		if(Auth::user()->level != 1){
			return redirect()->route('dashboard');
		}

		$active = ["vacancies", ""];
		return view('admin.vacancies.vacancy_edit', compact('vacancy', 'active'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, Vacancy $vacancy)
	{
		$rules = Vacancy::rules();
		$rules['title'] .= ','.$vacancy->id;

		$validator = Validator::make($request->all(), $rules);

		if($validator->fails()){
			return redirect()->back()->withErrors($validator->errors())->withInput();
		}

		$converter = new CommonMarkConverter();
		$html_detail = $converter->convertToHtml($request->detail);

		$input = [
			'title' => $request->title,
			'detail' => e($request->detail),
			'html_detail' => e($html_detail),
			'slug' => str_slug($request->title, '-'),
			'status' => $request->publish != "" ? 'published' : 'draft'
		];

		$vacancy->fill($input)->save();

		$success = "Updated Successfully";
		return redirect()->route('vacancy_index')->withSuccess($success);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Vacancy $vacancy)
	{
		if(Auth::user()->level != 1){
			return redirect()->route('dashboard');
		}

		$vacancy->delete();

		$success = "Deleted Successfully";
		return redirect()->route('vacancy_index')->withSuccess($success);
	}

}
