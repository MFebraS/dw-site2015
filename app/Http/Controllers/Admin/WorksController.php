<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Work;
use App\Models\Tag;
use App\Models\WorkTag;
use App\Models\WorkPicture;
use App\Models\Client;
use Config;
use File;
use Validator;
use Response;
use Auth;

use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class WorksController extends Controller {


	public function __construct()
	{
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$filter = [
			'client' => $request->get('client'),
			'status' => $request->get('status'),
			'q' => $request->get('q')
		];

		$works = $this->selectWorks($filter);

		$all = $works['all'];
		$published = $works['published'];
		$draft = $works['draft'];
		$works = $works['works'];
		$clients = Client::get();

		$orders = Work::whereStatus('published')->orderBy('order', 'asc')->get();
		/*foreach($works as $work){
			$orders[] = [
				'id' => $work->id,
				'picture' => $work->pictures()->first()->picture_name
			];
		}*/

		$active = ["works", "all"];

		return view('admin.works.work_index', compact('works', 'clients', 'all', 'published', 'draft', 'filter', 'active', 'orders'));
		
	}

	

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{		
		$temp_path = Config::get("custom_path.works")."/temp";
		
		$pictures = array_diff(scandir($temp_path), [".", "..", ".gitignore"]);
		if($pictures == []){
			$pictures = false;
		}
		$last = 0;

		$latestWorkPicture = WorkPicture::orderBy("id", "desc")->first();
		if($latestWorkPicture != null){
			$last = $latestWorkPicture->id;
		}

		$clients = Client::get();

		$orders = Work::orderBy('order', 'asc')->get();
		
		$active = ["works", "new"];
		return view('admin.works.work_create', compact('pictures', 'last', 'clients', 'active', 'orders'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if($request->publish != ''){
			$status = 'published';
		}else if($request->save != ''){
			$status = 'draft';
		}
		
		$validator = Validator::make($request->all(), Work::createRules());

		if($validator->fails()){
			return redirect()->back()->withErrors($validator->errors())->withInput();
		}
		if($request->images_order == null){
			return redirect()->back()->withError("The pictures field is required.")->withInput();
		}

		if($request->client == "new"){
			$client_input = [
				'name' => $request->client_name,
				'website' => $request->client_website,
				'count' => '1'
			];
			
			$logo = $request->file('logo');
			if($logo != null){
				$extension = $logo->getClientOriginalExtension();
				$filename = str_replace(" ", "-", $request->client_name).".".$extension;
				$client_input = array_add($client_input, 'logo', $filename);
			}

			$validator = Validator::make($client_input, Client::rules());
			if($validator->fails()){
				return redirect()->back()->withErrors($validator->errors())->withInput();
			}
			
		}

		$work_input = [
			'project_title' => $request->project_title,
			'description' => $request->description,
			'status' => $status,
			'slug' => str_slug($request->project_title),
			'link' => $request->link,
			'order' => $request->order ? $request->order+1 : '0'
		];
		
		DB::beginTransaction();
		if(isset($client_input)){
			$client = new Client();
			$client->fill($client_input)->save();
		}else{
			$client = Client::whereId($request->client)->first();
			$client->count += 1;
			$client->save();
		}

		$works = Work::where('order', '>', $work_input['order'])->orderBy('order', 'asc')->get();
		foreach ($works as $item) {
			$item->order += 1;
			$item->save();
		}

		$work = new Work($work_input);
		$client->works()->save($work);
		//$work->fill($work_input)->save();
		
		$new_tag = [];
		$array_tags = explode(',', $request->get('tag_name'));
		foreach ($array_tags as $tag) {
			$tag = trim($tag);
			if($tag != ""){
				$search_tag = Tag::whereTag_name($tag);
				if(count($search_tag->first()) > 0){
					$work->tags()->attach($search_tag->first()->id);
					$counter = $search_tag->first()->counter + 1;
					$search_tag->update(['counter' => $counter]);
				}else{
					$new_tag[] = new Tag([
						'tag_name' => $tag
					]);
					
				}
			}
		}
		$work->tags()->saveMany($new_tag);

		$new_picture = [];
		$sequence = 0;
		$latestWorkPicture = WorkPicture::orderBy("id", "desc")->first();
		if($latestWorkPicture == null){
			$file_name = 0;
		}else{
			$file_name = $latestWorkPicture->id;
		}
		$images_order = explode(",", $request->images_order);
		$current_path = Config::get("custom_path.works")."/temp";
		$destination_path = Config::get("custom_path.works");
		foreach($images_order as $image_order){
			if($image_order != ""){
				++$sequence;
				++$file_name;
				$exploded = explode(".", $image_order);
				$extension = $exploded[sizeof($exploded)-1];
				$picture_name = $file_name.".".$extension;
				$new_picture[] = new WorkPicture([
					'picture_name' => $picture_name,
					'sequence' => $sequence,
					'work_id' => $work->id
				]);
				

			}
		}
		$work->pictures()->saveMany($new_picture);
		DB::commit();

		$destinationPath = Config::get('custom_path.clients');
		if(isset($client_input)){
			$logo->move($destinationPath, $filename);
		}

		foreach ($images_order as $key => $image_order) {
			$picture_name = $new_picture[$key]->picture_name;
			rename($current_path."/".$image_order, $destination_path."/".$picture_name);
		}
		
		$success = "Created Successfully";

		return redirect()->route('works_index')->withSuccess($success);
		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(Work $work)
	{
		$tag_results = $work->tags;
		$tags = "";
		foreach ($tag_results as $row) {
			$tag = $row->tag_name;
			$tags .= ", ".$tag;
		}
		$tags = trim($tags, ', ');

		$pictures = $work->pictures;
		$pictures_path = Config::get('custom_path.works');

		$active = ["works", ""];
		return view('admin.works.work_show', compact('work', 'tags', 'pictures_path', 'pictures', 'active'));
		
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(Request $request, Work $work)
	{
		$result = $work->find($work->id)->tags;
		$tags = "";
		foreach ($result as $row) {
			$tag = $row->tag_name;
			$tags .= ",".$tag;
		}
		$tags = trim($tags, ',');

		$pictures = $work->pictures;

		$temp_path = Config::get("custom_path.works")."/temp";
		
		
		$temp_pictures = array_diff(scandir($temp_path), [".", "..", ".gitignore"]);
		if($temp_pictures == []){
			$temp_pictures = false;
		}
		$last = 0;

		$latestWorkPicture = WorkPicture::orderBy("id", "desc")->first();
		if($latestWorkPicture != null){
			$last = $latestWorkPicture->id;
		}

		$clients = Client::get();

		$orders = Work::orderBy('order', 'asc')->get();

		$active = ["works", ""];
		return view('admin.works.work_edit', compact('work', 'tags', 'pictures', 'temp_pictures', 'clients', 'last', 'active', 'orders'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Work $work, Request $request)
	{
		$rules = Work::updateRules();
		$rules['project_title'] .= ',' .$work->id;

		$validator = Validator::make($request->all(), $rules);

		if($validator->fails()){
			return redirect()->back()->withErrors($validator->errors())->withInput();
		}
		if($request->images_order == null){
			return redirect()->back()->withError("The pictures field is required.")->withInput();
		}

		if($request->client == "new"){
			$client_input = [
				'name' => $request->client_name,
				'website' => $request->client_website,
				'count' => '1'
			];
			
			$logo = $request->file('logo');
			if($logo != null){
				$extension = $logo->getClientOriginalExtension();
				$filename = str_replace(" ", "-", $request->client_name).".".$extension;
				$client_input = array_add($client_input, 'logo', $filename);
			}

			$validator = Validator::make($client_input, Client::rules());
			if($validator->fails()){
				return redirect()->back()->withErrors($validator->errors())->withInput();
			}
			
		}

		if($request->publish != ''){
			$status = 'published';
		}else if($request->save != ''){
			$status = 'draft';
		}
		//dd('something');
		$work_input = [
			'project_title' => $request->project_title,
			'status' => $status,
			'description' => $request->description,
			'slug' => str_slug($request->project_title),
			'link' => $request->link,
			'order' => $request->order ? $request->order+1 : '0'
		];


		DB::beginTransaction();
		if(isset($client_input)){
			$client = new Client();
			$client->fill($client_input)->save();
		}else{
			$client = $work->client;
			$client->count -= 1;
			$client->save();

			$client = Client::whereId($request->client)->first();
			$client->count += 1;
			$client->save();
		}

		$works = Work::where('order', '>', $work_input['order'])->orderBy('order', 'asc')->get();
		foreach ($works as $item) {
			$item->order += 1;
			$item->save();
		}

		$work->fill($work_input);

		$work->client()->associate($client);
		$work->save();
		/*$client->works()->associate($work);
		$client->save();*/
		
		$array_tags = explode(',', $request->get('tag_name'));
		$array_tags = array_map('trim', $array_tags);
		$existing_tags = explode(',', $request->get('existing_tags'));
		$existing_tags = array_map('trim', $existing_tags);
		foreach ($existing_tags as $existing_tag) {
			if($existing_tag != ""){
				$key = array_search($existing_tag, $array_tags);
				if($key !== false){
					unset($array_tags[$key]);
				}else{
					$tag = $work->tags()->whereTag_name($existing_tag)->first();
					$tag_id = $tag->id;
					$work->tags()->detach($tag_id);

					$tag->counter -= 1;
					$tag->save();

					if($tag->counter == 0){
						$tag->delete();
					}
					
				}
			}
		}

		$new_tag = [];

		foreach ($array_tags as $tag) {
			if($tag != ""){
				$search_tag = Tag::whereTag_name($tag);
				if(count($search_tag->first()) > 0){
					$work->tags()->attach($search_tag->first()->id);
					$counter = $search_tag->first()->counter + 1;
					$search_tag->update(['counter' => $counter]);
				}else{
					$new_tag[] = new Tag([
						'tag_name' => $tag
					]);
				}
			}
		}

		$work->tags()->saveMany($new_tag);

		$images_delete = explode(",", $request->images_delete);

		if($images_delete != null){
			foreach($images_delete as $image_delete){
				$picture = $work->pictures()->wherePicture_name($image_delete)->delete();
			}
		}

		$new_picture = [];

		$sequence = 0;
		$file_name = WorkPicture::orderBy("id", "desc")->first()->id;
		$images_order = explode(",", $request->images_order);
		$current_path = Config::get("custom_path.works")."/temp";
		$destination_path = Config::get("custom_path.works");
		foreach($images_order as $key => $image_order){
			if($image_order != ""){
				++$sequence;
				++$file_name;
				$picture = $work->pictures()->wherePicture_name($image_order)->first();
				if($picture != null){
					$picture->sequence = $sequence;
					$picture->save();
					unset($images_order[$key]);
				}else{
					$exploded = explode(".", $image_order);
					$extension = $exploded[sizeof($exploded)-1];
					$picture_name = $file_name.".".$extension;
					$new_picture[$key] = new WorkPicture([
						'picture_name' => $picture_name,
						'sequence' => $sequence,
						'work_id' => $work->id
					]);
				}

			}
		}
		$work->pictures()->saveMany($new_picture);
		DB::commit();
		
		if($images_delete != null){
			foreach($images_delete as $image_delete){
				$file_path = Config::get('custom_path.works');
				$fullpath = $file_path.'/'.$image_delete;
				File::delete($fullpath);
			}
		}

		if($new_picture != []){
			foreach ($images_order as $key => $image_order) {
				$picture_name = $new_picture[$key]->picture_name;
				rename($current_path."/".$image_order, $destination_path."/".$picture_name);
			}
		}
		$success = "Updated Successfully";
		return redirect()->route('works_index')->withSuccess($success);
		
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Work $work)
	{
		$images_delete = [];
		DB::beginTransaction();
		$tags = $work->tags;
		foreach($tags as $tag){
			$tag->counter -= 1;
			$tag->save();
			$tag_id = $tag->id;
			$work->tags()->detach($tag_id);
			if($tag->counter == 0){
				$tag->delete();
			}
			
		}

		$picture_path = Config::get('custom_path.works');
		$pictures = $work->pictures;
		foreach($pictures as $picture){
			$images_delete[] = $picture->picture_name;
			$picture->delete();
		}
		
		$work->delete();
		DB::commit();

		if($images_delete != null){
			foreach($images_delete as $image_delete){
				$file_path = Config::get('custom_path.works');
				$fullpath = $file_path.'/'.$image_delete;
				File::delete($fullpath);
			}
		}
		
		$success = "Deleted Successfully";
		return redirect()->route('works_index')->withSuccess($success);
	}

	public function ajaxFilter(Request $request){
		$status = $request->get('status');
		if($status != 'all'){
			$works = Work::whereStatus($status)->get();
		}else{
			$works = Work::get();
		}
		

		$new_works[] = null;
		foreach ($works as $key => $work) {
			$work_tag = $work->tags;
			$tags = "";
			foreach ($work_tag as $row) {
				$tag = $row->tag_name;
				$tags .= ", ".$tag;
			}
			$tags = trim($tags, ', ');

			$work_pictures = $work->pictures->first();

			$picture = $work_pictures->picture_name;

			$new_works[$key] = [
				'id' => $work->id,
				'project_title' => $work->project_title,
				'client_name' => $work->client_name,
				'client_website' => $work->client_website,
				'description' => $work->description,
				'tags' => $tags,
				'picture' => $picture,
				'picture_path' => Config::get('custom_path.works')
			];

		}
		$works = $new_works;
		return Response::json($works);
	}

	public function uploadTemp(Request $request){
		$file = $request->file('0');
		$file_path = Config::get('custom_path.works').'/temp';
		$filename = $request->get('id').'.'.$file->getClientOriginalExtension();
		$file->move($file_path, $filename);
		$result = [
			'status' => 'ok',
			'id' => $request->get('id'),
			'filename' => $file_path."/".$filename
		];
		return Response::json($result);
	}

	public function removeTemp(Request $request){
		$work = $request->work_id;
		$file = explode("?", $request->file_delete)[0];
		
			$file_path = Config::get('custom_path.works').'/temp';
			$fullpath = $file_path.'/'.$file;
			File::delete($fullpath);
		
		$result = [
			'status' => 'ok',
		];
		return Response::json($result);
	}

	public function setFilter(Request $request){
		$diffArray = [
			'client' => 'all',
			'status' => 'all',
			'q' => null
		];
		$data = array_diff(array_filter([
			'client' => $request->get('client'),
			'status' => $request->get('status'),
			'q' => $request->get('q')
		]), $diffArray);

		$perpage = $request->get('perpage');
		
		if($perpage != null){
			session(['works_perpage' => $perpage]);
		}
		
		return redirect()->route('works_index', $data);
	}

	public function selectWorks($filter){
		$perpage = (session('works_perpage') != null ? session('works_perpage') : 10);
		
		$works = new Work();

		$all = new Work();
		$published = new Work();
		$draft = new Work();

		$client = $filter['client'];
		$status = $filter['status'];
		$q = $filter['q'];

		if($client != ""){
			$client_id = Client::whereSlug($client)->first()->id;
			$works = $works->whereClient_id($client_id);
			//$works = $client->works;

			$all = $all->whereClient_id($client_id);
			$published = $published->whereClient_id($client_id);
			$draft = $draft->whereClient_id($client_id);
			/*$all = $all->whereClient_name($client);
			$published = $published->whereClient_name($client);
			$draft = $draft->whereClient_name($client);*/
		}

		
		if($status != ""){
			$works = $works->whereStatus($status);
		}

		if($q != ""){
			$works_title = explode(" ", $q);
			$works = $works->where(function($queryResult) use ($works_title){
				foreach($works_title as $title){
					$queryResult->orWhere('project_title', 'like', '%'.$title.'%');
				}
				
			});
			$all = $all->where(function($queryResult) use ($works_title){
				foreach($works_title as $title){
					$queryResult->orWhere('project_title', 'like', '%'.$title.'%');
				}
			});
			$published = $published->where(function($queryResult) use ($works_title){
				foreach($works_title as $title){
					$queryResult->orWhere('project_title', 'like', '%'.$title.'%');
				}
			});
			$draft = $draft->where(function($queryResult) use ($works_title){
				foreach($works_title as $title){
					$queryResult->orWhere('project_title', 'like', '%'.$title.'%');
				}
			});
		}

		$all = $all->count();
		$published = $published->whereStatus('published')->count();
		$draft = $draft->whereStatus('draft')->count();
		if($perpage == "all"){
			$perpage = $all;
		}
		$works = $works->orderBy('order', 'asc')->paginate($perpage);
		$works->appends(['client' => $client]);
		$works->appends(['status' => $status]);
		$works->appends(['q' => $q]);

		$new_works = [
			'works' => $works,
			'all' => $all,
			'published' => $published,
			'draft' => $draft
		];
		return $new_works;
	}

	public function getPictures(Request $request){
		$work = new Work();
		$pictures = $work->find($request->get('id'))->pictures;

	}

	public function getReorder(){
		$works = Work::orderBy('order', 'asc')->get();

		$active = ["", ""];
		return view('admin.works.work_reorder', compact('works', 'active'));
	}
	public function postReorder(Request $request){
		$works = Work::get();
		$images_order = explode(",", $request->order);
		//dd($images_order);
		foreach ($works as $work) {
			$work->order = array_search($work->id, $images_order);
			$work->save();
		}

		$success = "Updated Successfully";
		return redirect()->route('works_index')->withSuccess($success);
	}
	
}
