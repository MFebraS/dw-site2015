<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Work;
use App\Models\News;

class DashboardController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$works = Work::orderBy('created_at', 'desc')->take(3)->get();

		$works_count = [
			'all' => Work::count(),
			'published' => Work::whereStatus("published")->count(),
			'draft' => Work::whereStatus("draft")->count()
		];


		$news = News::orderBy('updated_at', 'desc')->take(3)->get();

		$news_count = [
			'all' => News::count(),
			'published' => News::whereStatus("published")->count(),
			'draft' => News::whereStatus("draft")->count()
		];

		$active = ["dashboard", "dashboard"];

		return view('admin.dashboard.dashboard_index', compact('works', 'news', 'works_count', 'news_count', 'active'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
