<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\News;

use DB;
use Illuminate\Http\Request;
use Validator;

class CategoriesController extends Controller {

	public function __construct(Category $category)
	{
		$this->category = $category;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$categories = new Category;
		$limit = 10;

		$query = $request->q;
		if ($query != null) {
			$categories = $categories->where('category_name', 'like', '%'.$query.'%');
		}
		$categories = $categories->paginate($limit);

		$active = ["news", "category"];

		if ($query != null) {
			$categories = $categories->appends(['q' => $query]);
			$message = "$query category is not found";
			return view('admin.categories_index', compact('categories', 'message', 'active'));
		}
		if ($categories[0] == null){
			$message = "No Category";
			return view('admin.categories_index', compact('categories', 'message', 'active'));
		} else {
			return view('admin.categories_index', compact('categories', 'active'));
		}
	}

	

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Category $category, Request $request)
	{
		$validator = Validator::make($request->all(), Category::rules());
		if ($validator->fails()){
			return redirect()->back()->withErrors($validator->errors())->withInput();
		}
		else {
			DB::beginTransaction();
			$category->category_name = $request->category_name;
			$category->slug = $request->slug;
			$category->save();
			DB::commit();
			$success = "Created Successfully";
		}
		return redirect('admin/categories')->withSuccess($success);
	}

	public function edit(Category $category)
	{
		$edit_category = $category;
		$categories = $this->category->get();

		$active = ["news", "category"];
		return view('admin.categories_edit', compact('edit_category', 'categories', 'active'));
	}

	/*Save Edit*/
	public function update(Category $category, Request $request)
	{
		$rules = Category::rules();
		$rules['category_name'] .= ','.$category->id;
		$rules['slug'] .= ','.$category->id;
		$validator = Validator::make($request->all(), $rules);
		if ($validator->fails()){
			return redirect()->back()->withErrors($validator->errors())->withInput();
		}
		DB::beginTransaction();
		$category->category_name = $request->category_name;
		$category->slug = $request->slug;
		$category->save();
		DB::commit();

		$success = "Updated Successfully";
		return redirect('admin/categories')->withSuccess($success);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function confirm_delete(Category $category)
	{
		$news = new News;
		$news_categories = $this->category->where('id', '!=', $category->id)->get();
		
		/* Check category in news table*/
		$cat_id = $category->id;
		$check_in_news[] = $news->where('category_id', '=', $cat_id)->get();

		$active = ["news", "category"];
		return view('admin.categories_confirm_delete', compact('category', 'news_categories', 'check_in_news', 'active'));
	}

	public function destroy(Category $category)
	{
		DB::beginTransaction();
		$category_name = $category->category_name;
		$category->delete();
		DB::commit();
		$active = ["news", "category"];
		return view('admin.categories_deleted', compact('category_name', 'active'));
	}

	public function destroy_cat_only(Category $category, Request $request)
	{
		DB::beginTransaction();
		$category_name = $category->category_name;
		$new_cat = $request->select_new_category;
		$news_cat_id = News::where('category_id', '=', $category->id)->update(['category_id'=>$new_cat]);

		$category->delete();
		DB::commit();

		$active = ["news", "category"];
		return view('admin.categories_deleted', compact('category_name', 'active'));
	}

	public function destroy_cat_and_news(Category $category)
	{
		$news = new News;
		DB::beginTransaction();
		$category_name = $category->category_name;
		$news_cat_id = $news->where('category_id', '=', $category->id)->delete();

		$category->delete();
		DB::commit();

		$active = ["news", "category"];
		return view('admin.categories_deleted', compact('category_name', 'active'));
	}
}