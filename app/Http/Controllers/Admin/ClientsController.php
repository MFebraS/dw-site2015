<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\Client;
use Validator;
use DB;
use Config;
use File;

class ClientsController extends Controller {
	public function __construct()
	{
		
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$clients = new Client();
		if($request->get('q') != null){
			$q = explode(" ", $request->get('q'));
			
			$clients = $clients->where(function($queryResult) use ($q){
				foreach($q as $name){
					$queryResult->orWhere('name', 'like', '%'.$name.'%');
				}
				
			});
			$clients = $clients->orderBy('order', 'asc')->paginate(5);
			$q = $request->get('q');
			$clients->appends(['q' => $q]);
		}else{
			if($request->get('q') === ""){
				return redirect()->route('client_index');
			}
			$clients = $clients->orderBy('order', 'asc')->paginate(5);
			$q = "";
		}

		
		
		$clientEdit = null;

		$active = ["works", "clients"];
		$page = $clients->total() % 5 == 0 ? $clients->lastPage()+1 : $clients->lastPage();

		$logos = Client::where('logo', '!=', '')->orderBy('order', 'asc')->get();	

		return view('admin.works.client', compact('clients', 'clientEdit', 'active', 'page', 'q', 'logos'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		//dd($request->all());
		$input = [
			'name' => $request->name,
			'website' => $request->website,
			'slug' => str_slug($request->name),
			'order' => Client::orderBy('order', 'desc')->first()->order + 1,
		];

		$validator = Validator::make($input, Client::rules());

		if($validator->fails()){
			return redirect()->back()->withErrors($validator->errors())->withInput();
		}

		$logo = $request->file('logo');
		if($logo != null){
			$extension = $logo->getClientOriginalExtension();
			$filename = str_replace(" ", "-", $request->name).".".$extension;
			$input = array_add($input, 'logo', $filename);
		}

		$client = new Client();

		$client->fill($input)->save();

		$destinationPath = Config::get('custom_path.clients');
		if($logo != null){
			$logo->move($destinationPath, $filename);
		}

		$success = "Added Successfully";
		return redirect()->route('client_index', ['page' => $request->page])->withSuccess($success);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(Client $client, Request $request)
	{
		$clientEdit = $client;

		$clients = Client::orderBy('order', 'asc')->get();
		foreach($clients as $idx => $client){
			if($clientEdit->id == $client->id){
				$page = ceil(($idx+1)/5);
				break;
			}
		}
		$clients = Client::orderBy('order', 'asc')->paginate(5);

		$q = "";

		$logos = Client::where('logo', '!=', '')->orderBy('order', 'asc')->get();	

		$active = ["works", "clients"];
		return view('admin.works.client', compact('clients', 'clientEdit', 'active', 'page', 'q', 'logos'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, Client $client)
	{
		$input = [
			'name' => $request->name,
			'website' => $request->website,
			'slug' => str_slug($request->name),
		];

		$rules = Client::rules();
		$rules['name'] .= ','.$client->id;

		$validator = Validator::make($input, $rules);

		if($validator->fails()){
			return redirect()->back()->withErrors($validator->errors())->withInput();
		}

		$logo = $request->file('logo');
		if($logo != null){
			$extension = $logo->getClientOriginalExtension();
			$filename = str_replace(" ", "-", $request->name).".".$extension;
			$input = array_add($input, 'logo', $filename);
		}

		$client->fill($input)->save();

		$destinationPath = Config::get('custom_path.clients');
		if($logo != null){
			File::delete($destinationPath."/".$client->logo);
			$logo->move($destinationPath, $filename);
		}

		$success = "Updated Successfully";
		return redirect()->route('client_index', ['page' => $request->page])->withSuccess($success);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Client $client)
	{
		$works = $client->works;
		
		$images_delete = [];
		$logo_delete = null;
		DB::beginTransaction();
		foreach($works as $work){
			$tags = $work->tags;
			foreach($tags as $tag){
				$tag->counter -= 1;
				$tag->save();
				$tag_id = $tag->id;
				$work->tags()->detach($tag_id);
				if($tag->counter == 0){
					$tag->delete();
				}
				
			}

			$picture_path = Config::get('custom_path.works');
			$pictures = $work->pictures;
			foreach($pictures as $picture){
				$images_delete[] = $picture->picture_name;
				$picture->delete();
			}
			
			$work->delete();
		}
		$logo_delete = $client->logo;
		$client->delete();
		DB::commit();

		if($images_delete != null){
			foreach($images_delete as $image_delete){
				$file_path = Config::get('custom_path.works');
				$fullpath = $file_path.'/'.$image_delete;
				File::delete($fullpath);
			}
		}
		File::delete(Config::get('custom_path.clients').'/'.$logo_delete);
		
		$success = "Deleted Successfully";
		return redirect()->back()->withSuccess($success);
	}

	public function reorder(Request $request){
		$clients = Client::get();
		$images_order = explode(",", $request->order);
		//dd($images_order);
		foreach ($clients as $client) {
			$client->order = array_search($client->id, $images_order);
			$client->save();
		}

		$success = "Updated Successfully";
		return redirect()->route('client_index')->withSuccess($success);
	}	
}
