<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use Validator;
use Hash;
use App\Models\User;

use Illuminate\Http\Request;
use Mail;
use DB;
use Config;
use File;

class UserController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		//dd($request->get('status'));
		if(Auth::user()->level != 1){
			return redirect()->route('user_show', ['user' => Auth::user()]);
		}
		$active = $request->get('status');
		if($active != null){
			if($active == "inactive"){
				$users = User::whereActive('-1')->get();
			}else{
				$users = User::where('active', '!=', '-1')->get();
			}
		}else{
			$users = User::orderBy('active', 'desc')->get();
		}

		$count = [
			User::count(),
			User::where('active', '!=', '-1')->count(),
			User::whereActive('-1')->count(),
		];

		$active = ["users", "all", $active];
		return view('admin.users.user_index', compact('users', 'active', 'count'));
	}

	public function login(Request $request){
		if(Auth::check()){
			if($request->next != null){
				return redirect($request->next);
			}else{
				return redirect()->route('dashboard');
			}
    	}else{
    		return view('admin.users.user_login');
    	}
		
	}

	public function authenticate(Request $request)
    {
    	$rules = [
    		'email' => 'required|email',
    		'password' => 'required'
    	];

    	$validator = Validator::make($request->all(), $rules);
    	if($validator->fails()){
    		return redirect()->route('user_login')->withErrors($validator->errors())->withInput();
    	}else{
    		$credentials = [
    			'email' => $request->email,
    			'password' => $request->password,
    			'active' => '1'
    		];

    		$remember = ($request->remember == 'on' ? true : false);
    		if(Auth::attempt($credentials, $remember)){
    			
    			if($request->next != null){
    				return redirect($request->next);
    			}else{
    				return redirect()->route('dashboard');
    			}
    			
    		}else{
    			$user = User::whereEmail($request->email)->first();
    			if($user != null && $user->active == 0){
    				$error = "Please follow link in your email to activate this account.";
    			}else{
    				$error = "Username or Password incorrect";
    			}
    			return redirect()->route('user_login')->withInput()->withError($error);
    		}
    	}
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function logout()
	{
		Auth::logout();
		$success = session('success');
		$error = session('error');
		return redirect()->route('user_login')->withSuccess($success)->withError($error);
	}

	public function create(Request $request){
		//dd(User::whereEmail('erghbuer')->first());
		if(Auth::user()->level != 1){
			return redirect()->route('user_show', ['user' => Auth::user()]);
		}
		//dd($request->user);
		$reactive = null;
		if($request->get('user') != null){
			$user = User::whereId($request->get('user'))->first();
			if($user->active == -1){
				$reactive = [
					'name' => $user->name,
					'email' => $user->email,
					'employ'=> $user->employ,
					'level' => $user->level
				];
			}else{
				return redirect()->route('user_index');
			}
		}
		$active = ["users", "new"];
		return view('admin.users.user_create', compact('active'))->withReactive($reactive);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		
		$input = [
			'name' => $request->name,
			'email' => $request->email,
			'employ' => $request->employ,
			'level' => $request->level,
			'password' => "*",
			'active' => '1',
		];

		$rules = User::createRules();
		
		$user = User::whereEmail($request->email)->first();
		if($user != null && $user->active == -1){
			$rules['email'] .= ',' . $user->id;
		}

		$validator = Validator::make($input, $rules);

		if($validator->fails()){
    		return redirect()->back()->withErrors($validator->errors())->withInput();
    	}else{
    		$success = "Re-activated Successfully";
    		if($user == null){
				$user = new User();
				$success = "Created Successfully";
			}
			$user->fill($input)->save();

			$data = [
				'name' => $input['name'],
				'link' => route('user_activate', ['user' => $user->id, 'code' => $user->password])
			];

			/*$email_address = $user->email;
			Mail::send('emails.activation', $data, function($message) use($email_address){
			    $message->to($email_address)->subject('Activate your account!');
			});*/

			
            return redirect()->route('user_index')->with('success', $success);
		}
	}

	public function activate(User $user, $code)
	{
		if($user->password == $code && $user->active == 0){
			$post = route('user_setactive', ['user' => $user->id, 'code' => $code]);
			return view('admin.users.user_password', compact('user', 'code', 'post'));
		}else{
			$error = "Activation Failed. <br>Please check your email again.";
			return redirect()->route('user_login')->withError($error);
			//dd("else");
		}
		
		//return view('admin.users.user_password');
		//dd($user);
	}

	public function changePassword(User $user)
	{
		$code = null;
		if(Auth::user()->level != 1 && Auth::user() != $user){
			return redirect()->route('user_changePassword', ['user' => Auth::user()->id]);
			//$user = Auth::user();
		}
		$post = route('user_savePassword', ['user' => $user->id]);
		return view('admin.users.user_password', compact('user', 'code', 'post'));
		dd($code);
	}

	public function savePassword(Request $request, User $user)
	{
		$code = $request->code;
		$old_password = $request->old_password;
		$password = $request->password;

		$input = [
			'password' => $password,
			'password_confirmation' => $request->password_confirmation
		];

		$validator = Validator::make($input, User::passwordRules());
		if($validator->fails()){
			return redirect()->back()->withErrors($validator->errors())->withInput();
		}
		if($code != null){
			if($code == $user->password){
				$user->password = Hash::make($password);
				$user->active = 1;
				$user->save();

				$success = "Activated Successfully.";
				return redirect()->route('user_login')->withSuccess($success);
			}else{
				$error = "Activation Failed. <br>Please check your email again.";

				return redirect()->back()->withError($error);
			}
		}else if($old_password != null){
			if (Hash::check($old_password, $user->password)){
				
	            $user->password = Hash::make($password);
	            $user->save();

				$success = "Password Changed Successfully. <br>Please re-login.";
	            return redirect()->route('user_logout')->withSuccess($success);
	        }else{

				$error = 'Your old password is incorrect.';
				return redirect()->back()->withOld_password($error);
	        }
		}
		dd($code);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(User $user)
	{
		if(Auth::user()->level != 1 && $user != Auth::user()){
			return redirect()->route('user_show', ['user' => Auth::user()->id]);
			//$user = Auth::user();
		}
		$active = ["users", ""];
		return view('admin.users.user_show', compact('user', 'active'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(User $user)
	{
		if(Auth::user()->level != 1 && $user != Auth::user()){
			return redirect()->route('user_edit', ['user' => Auth::user()->id]);
			//$user = Auth::user();
		}
		$active = ["users",""];
		return view('admin.users.user_edit', compact('user', 'active'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(User $user, Request $request)
	{
		//dd($request->file('picture'));
		
		$rules = User::updateRules();
		$rules['email'] .= ','.$user->id;

		if(Auth::user()->level == 1){
			$rules = array_add($rules, 'employ', 'required');
		}

		$validator = Validator::make($request->all(), $rules);
		if($validator->fails()){
			return redirect()->back()->withErrors($validator->errors())->withInput();
		}
		if($request->file('picture') == null && $user->picture == null){
			$error = "Please select image file";
			return redirect()->back()->withError($error)->withInput();
		}

		$input = [
			'name' => $request->name,
			'email' => $request->email,
			'facebook' => $request->facebook,
			'twitter' => $request->twitter,
			'google' => $request->google,
			'about' => $request->about,
		];

		if(Auth::user()->level == 1){
			$input = array_add($input, 'employ', $request->employ);
		}

		$user->fill($input)->save();

		if($request->file('picture') != null){
			$path = Config::get('custom_path.employees');
			$picture = $request->file('picture');
			$extension = $picture->getClientOriginalExtension();
			$filename = str_replace(" ", "_", $request->name).".".$extension;

			$oldPicture = $user->picture;
			$fullpath = $path."/".$oldPicture;
			File::delete($fullpath);

			$user->picture = $filename;
			$user->save();
			$picture->move($path, $filename);
		}

		$success = "Updated Successfully";
        return redirect()->route('user_show', ['user' => $user->id])->with('success',$success);

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(User $user, Request $request)
	{
		if(Auth::user() != $user && Auth::user()->level != 1){
			return redirect()->route('user_show', ['user' => Auth::user()]);
		}

		$picture = null;
		if($request->get('delete')){
			$picture = $user->picture;

			DB::beginTransaction();

			$all_news = $user->news;
			foreach ($all_news as $news) {
				$news->author_id = Auth::user()->id;
				$news->status = "draft";
				$news->save();
			}
			
			//$user->news()->delete();
			$user->delete();

			DB::commit();

			if($picture != null){
				$fullpath = Config::get('custom_path.employees')."/".$picture;
				File::delete($fullpath);
			}
			$success = "Deleted Successfully";
		}else{
			$user->active = '-1';
			$user->save();
			$success = "Deactivated Successfully";
		}
		/**/

		
		return redirect()->route('user_index')->with("success", $success);
	}

}
