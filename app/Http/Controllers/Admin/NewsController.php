<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\News;
use App\Models\Category;
use App\Models\NewsPicture;
use App\Models\User;
use Validator;
use Auth;
use File;
use Config;
use Response;
use DB;

use Illuminate\Http\Request;
use App\Http\Requests\NewsFormRequest;
use Carbon\Carbon;
use League\CommonMark\CommonMarkConverter;

class NewsController extends Controller {

	public function __construct(News $news)
	{
		$this->news = $news;
	}

	public function index(Request $request)
	{
		$status = $request->get('status');
		$cat_id = $request->category;
		$limit = $request->per_page;
		$query = $request->q;
		$filter_selected = $request->filter;
		$limit_selected = $request->limit;

		$all_posts = $this->filter($status, $cat_id, $limit, $query, $filter_selected, $limit_selected);
		$categories = Category::get();
		$count_all_posts = News::count();
		$count_published = News::where('status', '=', 'Published')->count();
		$count_draft = News::where('status', '=', 'Draft')->count();

		$active = ["news", "all"];

		if ($query != null) {
			$message = "$query post is not found";
			return view('admin.news_index', compact('count_all_posts','count_published', 'count_draft', 'all_posts', 'categories', 'value', 'message', 'active'));
		}
		if ($all_posts[0] == null){
			$message = "No Post";
			return view('admin.news_index', compact('count_all_posts','count_published', 'count_draft', 'all_posts', 'categories', 'value', 'message', 'active'));
		} else {
			return view('admin.news_index', compact('count_all_posts','count_published', 'count_draft', 'all_posts', 'categories', 'value', 'active'));
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$news_pic = new NewsPicture;
		// cek folder upload
		$upload_path = Config::get("custom_path.upload-news-pictures");
		if(!file_exists($upload_path)){
			mkdir($upload_path);
		}

		$picture_names = [];
		$pictures = $news_pic->whereNull('news_id')->whereNull("status")->get();
		
		foreach ($pictures as $key => $picture) {
			$picture_names[] = $picture->file_name;
		}
		// mengambil gambar feature yang belum terpakai
		$feature_picture = $news_pic->whereNull('news_id')->where('status', '=', "Feature")->pluck('file_name');

		$post_category = Category::get();

		$users = User::get();

		$active = ["news", "new"];
		return view('admin.news_new_post', compact('post_category', 'picture_names', 'feature_picture', 'active', 'users'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(News $news, Request $request, NewsPicture $news_picture)
	{
		//dd($request->all());
		$rules = News::rules();
		$validator = Validator::make($request->all(), $rules);
		
		if ($validator->fails()){
			return redirect()->back()->withErrors($validator->errors())->withInput();
		}
		else {
			$converter = new CommonMarkConverter();
			DB::beginTransaction();
			$news->author_id = $request->contributed_by ? : Auth::user()->id;
			$news->title = $request->post_title;
			$news->permalink = $request->permalink;
			$news->category_id = $request->category_id;
			$news->status = $request->news_status;

			if ($news->status == "Published") {
				$news->published_date = Carbon::now('Asia/Jakarta')->toDateTimeString();
			} else {
				$news->published_date = null;
			}
			$news->save();

			// Mengubah path gambar
			$folder_name = $news->id;
			$content = $request->news_content_text;
			$change_path = str_replace("upload-news-pictures", "upload-news-pictures/$folder_name", $content);
			$news->content = $change_path;
			// Konversi commonmark
			$html_content = $converter->convertToHtml($change_path);
			$news->html_content = htmlspecialchars($html_content);
			$news->save();

			// Simpan gambar di DB dan memindah ke folder 
			// $status = "new_post";
			$pictures = $request->temp_pictures;
			$this->pictures($pictures, $news);
			DB::commit();
			
			$success = "Created Successfully";
			return redirect('admin/news')->withSuccess($success);
		}
	}

	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(News $news)
	{
		$category = new Category;
		$news_pic = new NewsPicture;
		$edit_news = $news;

		$edit_news_status = $news->status;
		$default_category_id = $news->category_id;
		$default_category_name = $news->category->category_name;
		$other_categories = $category->where('id', '!=', $default_category_id)->get();

		$old_pictures = $news_pic->where('news_id', '=', $news->id)->whereNull('status')->get();
		$feature_picture = $news_pic->where('news_id', '=', $news->id)->where('status', '=', "Feature")->pluck('file_name');

		$active = ["news", ""];

		$users = User::get();

		return view('admin.news_edit', compact('edit_news', 'old_pictures',
			'edit_news_status', 'other_categories', 'default_category_id','default_category_name',
			'feature_picture', 'users', 'active'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(News $news, Request $request)
	{
		$rules = News::rules();
		$rules['post_title'] .= ','.$news->id;
		$rules['permalink'] .= ','.$news->id;
		$validator = Validator::make($request->all(), $rules);
		
		if ($validator->fails()){
			return redirect()->back()->withErrors($validator->errors())->withInput();
		}
		$news_pic = new NewsPicture;
		$converter = new CommonMarkConverter();
		DB::beginTransaction();
		$news->author_id = $request->contributed_by ? : Auth::user()->id;
		$news->title = $request->post_title;
		if ($request->permalink != null) {
			$news->permalink = $request->permalink;
		}
		$news->category_id = $request->category_id;
		$news->content = $request->news_content_text;
		$html_content = $converter->convertToHtml($request->news_content_text);
		$news->html_content = htmlspecialchars($html_content);
		$news->status = $request->news_status;
		// Memberi tgl publish
		if($news->status == "Published"){
			$news->published_date = Carbon::now('Asia/Jakarta')->toDateTimeString();
		} 
		$news->save();

		// Simpan gambar di DB 
		// $status = "";
		$pictures = $request->temp_pictures;
		$this->pictures($pictures, $news);
		DB::commit();
		
		$success = "Updated Successfully";
		return redirect('admin/news')->withSuccess($success);
	
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(News $news, Request $request)
	{
		$news_pic = new NewsPicture;
		$file_path = Config::get('custom_path.upload-news-pictures');
		/* Delete picture files */
		$news_pictures = $news_pic->where('news_id', '=', $news->id)->get();
		foreach ($news_pictures as $news_picture)
		{
			$file_name = $news_picture->file_name;
			File::delete($file_path.'/'.$news->id.'/'.$file_name);
		}
		// Menghapus folder news_id
		$removed_folder = $file_path.'/'.$news->id;
		if (is_dir($removed_folder)){
			rmdir($removed_folder);
		}
		// Menghapus gambar di database
		DB::beginTransaction();
		$news_pic->where('news_id', '=', $news->id)->delete();
		$news->delete();
		DB::commit();

		$success = "Deleted Successfully";
		return redirect()->back()->withSuccess($success);
	}

	public function filter($status, $cat_id, $limit, $query, $filter_selected, $limit_selected)
	{
		session(['lim' => $limit,
					'status' => $status]);
		$limit_value = session('lim');
		$status_value = session('status');
		if ($limit_value == null) {
			$limit_value = 10;
		}

		$all_posts = new News;

		/*Search*/
		if ($query != null) {
			$all_posts = $all_posts->where('title', 'like', '%'.$query.'%')->orWhere('content', 'like', '%'.$query.'%');
		}

		/* Cek Filter Status */
		if ($status == "Published"){
			$all_posts = $all_posts->where('status', '=', 'Published');
		} else if ($status == "Draft"){
			$all_posts = $all_posts->where('status', '=', 'Draft');
		}
		
		if ($filter_selected != null) {
			if ($cat_id != "all") {
				$all_posts = $all_posts->where('category_id','=', $cat_id);
			}
		}

		// Mengambil post dengan urutan descending berdasarkan tanggal update
		$all_posts = $all_posts->orderBy('published_date', 'desc')->paginate($limit_value);

		// Menambahkan url ketika dilakukan seleksi
		if ($filter_selected != null) {
			$all_posts = $all_posts->appends(['category'=>$cat_id, 'per_page'=>$limit_value, 'filter'=>'Filter']);
		}
		else if ($limit_selected != null) {
			$all_posts = $all_posts->appends(['category'=>$cat_id, 'per_page'=>$limit_value, 'limit'=>'Apply']);
		}
		else if ($query != null) {
			$all_posts = $all_posts->appends(['q' => $query, 'per_page' => $limit_value]);
		}
		else if ($status != null){
			$all_posts = $all_posts->appends(['status' => $status_value]);
		}
		return $all_posts;
	}

	public function pictures($pictures, $news)
	{
		if ($pictures != null) {
			// Membuat folder baru jika belum ada -New Post-
			$upload_path = Config::get('custom_path.upload-news-pictures');
			$destination_path = $upload_path.'/'.$news->id;
			if(!is_dir($destination_path)){
				mkdir($destination_path);
			}
			// $pictures : array berisi nama gambar
			$exploded_pics = explode(',', $pictures);
			if (count($exploded_pics[0]) != null) {
				DB::beginTransaction();
				foreach ($exploded_pics as $picture) {
					$news_pic = new NewsPicture;
					$news_picture = $news_pic->where('file_name', '=', $picture);
					$news_picture->update(['news_id' => $news->id]);

					// Memindahkan file gambar ke folder news id					
					if(file_exists($upload_path."/".$picture)){
						rename($upload_path."/".$picture, $destination_path."/".$picture);
					}
				}
				DB::commit();
			}
		}
	}

	public function uploadTemp(Request $request)
	{
		$news_picture = new NewsPicture;
		$news_id = $request->news_id;

		$upload_path = Config::get('custom_path.upload-news-pictures');
		// Jika edit news
		if ($news_id) {
			$upload_path = $upload_path.'/'.$news_id;
		}
		// Jika akan upload gambar feature
		if ($request->picture_status) {
			// Jika edit news
			if ($request->news_id) {
				$feature_picture = $news_picture->where('news_id', '=', $request->news_id)->where('status', '=', 'Feature')->first();
			}
			else{
				$feature_picture = $news_picture->where('news_id', '=', '0')->where('status', '=', 'Feature')->first();
			}
			// Jika sebelumnya sudah ada gambar feature, maka id-nya akan digunakan untuk feature yang baru 
			if($feature_picture){
				$news_picture = $feature_picture;
			}
		}
		// dd($news_picture);

		$file = $request->file('0');
		// mengambil nama gambar
		$file_name = $file->getClientOriginalName();
		// mengambil ekstensi gambar
		$extension = $file->getClientOriginalExtension();

		$new_file_name = rand(11111,99999).'.'.$extension;
		while ($news_picture->where('file_name', '=', $new_file_name)->first() != null) {
			$new_file_name = rand(11111,99999).'.'.$extension;
		}

		DB::beginTransaction();
		$news_picture->name = $file_name;
		$news_picture->file_name = $new_file_name;
		$news_picture->format = $extension;
		// Jika gambar feature, maka statusnya diisi "Feature"
		if ($request->picture_status) {
			$news_picture->status = $request->picture_status;
		}
		$news_picture->save();
		DB::commit();

		$file->move($upload_path, $new_file_name);

		$result = [
			'status' => 'ok',
			'id' => $request->get('id'),
			'filename' => $new_file_name,
		];
		return Response::json($result);
	}

	public function removeTemp(Request $request){
		$news_picture = new NewsPicture;
		$file_name = $request->file_delete;
		$news_id = $request->news_id;

		$file_path = Config::get('custom_path.upload-news-pictures');
		// Jika edit news
		if ($news_id) {
			$file_path = $file_path.'/'.$news_id;
		}

		$full_path = $file_path.'/'.$file_name;
		File::delete($full_path);

		// Menghapus record pada DB
		DB::beginTransaction();
		// Jika gambar feature
		if ($request->picture_status != "Feature") {
			$news_picture->where('file_name', '=', $file_name)->delete();
		}
		DB::commit();

		$result = [
			'status' => 'ok',
		];
		return Response::json($result);
	}
}
