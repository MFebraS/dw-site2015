<?php namespace App\Http\Controllers;

use App\Models\Work;
use App\Models\Tag;
use App\Models\WorkTag;
use App\Models\WorkPicture;
use App\Models\Client;
use Config;
use Response;
use Request;

class WorksController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$works = $this->selectWorks(Request::get('tag'), Request::get('client'));
		//dd($our_works);
		$currentPage = $works['currentPage'];
		$lastPage = $works['lastPage'];
		$our_works = $works['works'];
		$all_tags = Tag::get();
		//dd(count($our_works));

		$active = ["works", Request::get('client') != null ? "-" : Request::get('tag')];
		return view('our-works', compact('our_works', 'all_tags', 'currentPage', 'lastPage', 'active'));
	}

	public function ajaxShow(){
		$id = Request::get('work_id');
		$work = Work::whereId($id)->first();
		
		$pictures = $work->pictures;

		return Response::json($pictures);
	}
	public function ajaxFilter(){
		$tag_name = Request::get('tag_name');
		$our_works = $this->selectWorks($tag_name);
		//dd(count($our_works[0]));
		//count($our_works[0]);
		//dd($our_works);
		return Response::json($our_works);
	}

	public function loadMore(Request $request){
		$works = $this->selectWorks(Request::get('tag'), Request::get('client'));
		//dd($works['works']);
		return Response::json($works);
	}

	public function selectWorks($tag_name, $client = null){
		//dd($client);
		if($tag_name != ''){
			$tags = Tag::where('tag_name', 'like', '%'.$tag_name.'%')->get();

			//dd($tags->works()->join());
			if($tags->count() != 0){
				$workTag = WorkTag::where(function($q) use($tags){
					foreach($tags as $tag){
						$q->orWhere('tag_id', '=', $tag->id);
					}
				})->get();

				$works = Work::whereStatus('published')->where(function($q) use($workTag){
					foreach($workTag as $tag){
						$q->orWhere('id', '=', $tag->work_id);
					}
				})->orderBy('order', 'asc')->paginate(9);
				
			}else{
				$works = null;
			}
		}else{
			if($client != null){
				$client = Client::whereSlug($client)->first();
				//dd($client);
				$works = $client->works()->whereStatus('published')->orderBy('updated_at', 'desc')->paginate(9);
			}else{
				$works = Work::whereStatus('published')->orderBy('order', 'asc')->paginate(9);
			}
		}
		//dd($works);
		
		$our_works[] = null;
		$currentPage = 1;
		$lastPage = 1;
		$total = 0;
		if($works != null){
			foreach ($works as $key => $work) {
				$work_tag = $work->tags;
				$tags = "";
				//dd($result);
				foreach ($work_tag as $row) {
					$tag = $row->tag_name;
					$tags .= ", ".$tag;
					//var_dump($row->id);
				}
				$tags = trim($tags, ', ');

				$work_pictures = $work->pictures->first();

				$picture = $work_pictures->picture_name;
				$our_works[$key] = [
					'id' => $work->id,
					'slug' => $work->slug,
					'project_title' => $work->project_title,
					'client_name' => $work->client->name,
					'client_website' => $work->link ? : '-',
					'description' => $work->description,
					'tags' => $tags,
					'picture' => $picture,
					'picture_path' => Config::get('custom_path.works')
				];
			}

			$currentPage = $works->currentPage();
			$lastPage = $works->lastPage();
			$total = $works->total();
		}

		$works = [
			'works' => $our_works,
			'currentPage' => $currentPage,
			'lastPage' => $lastPage,
			'total' => $total
		];

		return $works;
	}
}
