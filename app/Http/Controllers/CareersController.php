<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Vacancy;

class CareersController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$vacancies = Vacancy::whereStatus('published')->where('detail', '!=', '')->orderBy('title', 'asc')->get();
		$empties = Vacancy::whereStatus('published')->where(function($q){
			$q->orWhereNull('detail')->orWhere('detail', '=', '');
		})->orderBy('title','asc')->get();

		$active = ["careers", ""];
		//dd($empties);
		return view('careers_home', compact('vacancies', 'empties', 'active'));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(Vacancy $vacancy)
	{
		$vacancies = Vacancy::whereStatus('published')->where('detail', '!=', '')->orderBy('title', 'asc')->get();
		$empties = Vacancy::whereStatus('published')->whereDetail('')->orderBy('title','asc')->get();

		$active = ["careers", ""];
		return view('careers_detail', compact('vacancies', 'empties', 'vacancy', 'active'));
	}

	
}
