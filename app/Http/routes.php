<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', 'HomeController@index');

Route::get('/about-us', 'AboutUsController@index');

Route::get('/news', 'NewsController@index');
Route::get('/news/category/{category_slug}', 'NewsController@category_filter');
Route::get('/news/{news_permalink}', 'NewsController@news_detail');

Route::get('/our-works', 'WorksController@index');
Route::get('/ajax-slider', 'WorksController@ajaxShow');
Route::get('/ajax-filter', 'WorksController@ajaxFilter');
Route::get('/our-works/load-more', 'WorksController@loadMore');

Route::get('/products', 'ProductsController@index');

Route::get('/services', 'ServicesController@index');

Route::get('/careers', 'CareersController@index');

Route::get('/careers/junior-web-programmer-prev', 'CareersController@index_junior_web_programmer');
Route::get('/careers/senior-web-programmer-prev', 'CareersController@index_senior_web_programmer');
Route::get('/careers/{vacancy}', 'CareersController@show');

Route::get('/contact-us', 'ContactUsController@index');
Route::post('/contact-us', 'ContactUsController@send');


Route::get('/admin', ['as' => 'user_login', 'uses' => 'Admin\UserController@login']);
Route::post('/admin/login', ['as' => 'user_auth', 'uses' => 'Admin\UserController@authenticate']);

Route::get('/admin/user/{user}/activate/{code}', ['as' => 'user_activate', 'uses' => 'Admin\UserController@activate']);
Route::post('/admin/user/{user}/activate/{code}', ['as' => 'user_setactive', 'uses' => 'Admin\UserController@savePassword']);
Route::get('/admin/password/email', ['as' => 'user_getEmail', 'uses' => 'Auth\PasswordController@getEmail']);
Route::post('/admin/password/email', ['as' => 'user_postEmail', 'uses' => 'Auth\PasswordController@postEmail']);
Route::get('/admin/password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('/admin/password/reset/{token}', ['as' => 'user_postReset', 'uses' => 'Auth\PasswordController@postReset']);


Route::group(['middleware' => 'auth'], function(){
	Route::resource('/admin/categories', 'Admin\CategoriesController', [
		'except' => [
			'create', 'show', 'destroy'
		]
	]);
	Route::bind('categories', function($id){
		return \App\Models\Category::whereId($id)->first();
	});
	Route::get('/admin/categories/{categories}/edit', 'Admin\CategoriesController@edit');
	Route::post('/admin/categories/{categories}/update', 'Admin\CategoriesController@update');
	Route::get('/admin/categories/{categories}/confirm', 'Admin\CategoriesController@confirm_delete');
	Route::get('/admin/categories/{categories}/deleted', 'Admin\CategoriesController@destroy');
	Route::post('/admin/categories/{categories}/deleted', 'Admin\CategoriesController@destroy_cat_only');
	Route::get('/admin/categories/{categories}/deleted-category-and-news', 'Admin\CategoriesController@destroy_cat_and_news');

	Route::bind('news', function($id){
		return \App\Models\News::whereId($id)->first();
	});
	Route::get('/admin/news', 'Admin\NewsController@index');
	Route::get('/admin/news/filter', 'Admin\NewsController@filter');
	Route::get('/admin/news/new-post', 'Admin\NewsController@create');
	Route::post('/admin/news', 'Admin\NewsController@store');
	Route::get('/admin/news/{news}/edit', 'Admin\NewsController@edit');
	Route::post('/admin/news/{news}/update', 'Admin\NewsController@update');
	Route::get('/admin/news/{news}/delete', 'Admin\NewsController@destroy');
	Route::post('/admin/news/uploadTemp', 'Admin\NewsController@uploadTemp');
	Route::post('/admin/news/removeTemp', 'Admin\NewsController@removeTemp');

	Route::get('/admin/dashboard', ['as' => 'dashboard', 'uses' => 'Admin\DashboardController@index']);

	Route::bind('works', function($slug){
		return \App\Models\Work::whereSlug($slug)->firstOrFail();
	});
	Route::get('/admin/works/reorder', ['as' => 'works_getReorder', 'uses' => 'Admin\WorksController@getReorder']);
	Route::post('/admin/works/reorder', ['as' => 'works_postReorder', 'uses' => 'Admin\WorksController@postReorder']);
	Route::get('/admin/works', ['as' => 'works_index', 'uses' => 'Admin\WorksController@index']);
	Route::get('/admin/works/create', ['as' => 'works_create', 'uses' => 'Admin\WorksController@create']);
	Route::post('/admin/works/create', ['as' => 'works_store', 'uses' => 'Admin\WorksController@store']);
	Route::get('/admin/works/{works}', ['as' => 'works_show', 'uses' => 'Admin\WorksController@show']);
	Route::get('/admin/works/{works}/edit', ['as' => 'works_edit', 'uses' => 'Admin\WorksController@edit']);
	Route::post('/admin/works/{works}/edit/update', ['as' => 'works_update', 'uses' => 'Admin\WorksController@update']);
	Route::get('/admin/works/{works}/delete', ['as' => 'works_delete', 'uses' => 'Admin\WorksController@destroy']);
	Route::get('/admin/works-filter', ['as' => 'works_filter', 'uses' => 'Admin\WorksController@ajaxFilter']);
	Route::get('/admin/works-setFilter', ['as' => 'works_setfilter', 'uses' => 'Admin\WorksController@setFilter']);
	Route::get('/admin/works/getPictures', ['as' => 'works_getpicture', 'uses' => 'Admin\WorksController@getPictures']);
	Route::post('/admin/works/uploadTemp', ['as' => 'works_uploadtemp', 'uses' => 'Admin\WorksController@uploadTemp']);
	Route::post('/admin/works/removeTemp', ['as' => 'works_removetemp', 'uses' => 'Admin\WorksController@removeTemp']);

	Route::bind('client', function($slug){
		return \App\Models\Client::whereSlug($slug)->firstOrFail();
	});
	Route::get('/admin/clients', ['as' => 'client_index', 'uses' => 'Admin\ClientsController@index']);
	Route::post('/admin/clients/create', ['as' => 'client_store', 'uses' => 'Admin\ClientsController@store']);
	Route::get('/admin/clients/{client}/edit', ['as' => 'client_edit', 'uses' => 'Admin\ClientsController@edit']);
	Route::post('/admin/clients/{client}/edit/update', ['as' => 'client_update', 'uses' => 'Admin\ClientsController@update']);
	Route::get('/admin/clients/{client}/delete', ['as' => 'client_delete', 'uses' => 'Admin\ClientsController@destroy']);
	Route::post('/admin/clients/reorder', ['as' => 'client_reorder', 'uses' => 'Admin\ClientsController@reorder']);
	
	Route::bind('user', function($id){
		return \App\Models\User::whereId($id)->firstOrFail();
	});
	Route::get('/admin/user', ['as' => 'user_index', 'uses' => 'Admin\UserController@index']);
	Route::get('/admin/user/create', ['as' => 'user_create', 'uses' => 'Admin\UserController@create']);
	Route::post('/admin/user/create', ['as' => 'user_store', 'uses' => 'Admin\UserController@store']);
	Route::get('/admin/user/{user}/edit', ['as' => 'user_edit', 'uses' => 'Admin\UserController@edit']);
	Route::post('/admin/user/{user}/edit', ['as' => 'user_update', 'uses' => 'Admin\UserController@update']);
	Route::get('/admin/user/{user}/delete', ['as' => 'user_delete', 'uses' => 'Admin\UserController@destroy']);
	Route::get('/admin/logout', ['as' => 'user_logout', 'uses' => 'Admin\UserController@logout']);

	Route::get('/admin/user/{user}/change-password', ['as' => 'user_changePassword', 'uses' => 'Admin\UserController@changePassword']);
	Route::post('/admin/user/{user}/save-password', ['as' => 'user_savePassword', 'uses' => 'Admin\UserController@savePassword']);
	Route::get('/admin/user/{user}', ['as' => 'user_show', 'uses' => 'Admin\UserController@show']);


	Route::bind('employee', function($id){
		return \App\Models\Employee::whereId($id)->firstOrFail();
	});
	Route::get('/admin/employees', ['as' => 'employees_index', 'uses' => 'Admin\EmployeeController@index']);
	Route::get('/admin/employees/create', ['as' => 'employees_create', 'uses' => 'Admin\EmployeeController@create']);
	Route::post('/admin/employees/create', ['as' => 'employees_store', 'uses' => 'Admin\EmployeeController@store']);
	Route::get('/admin/employees/{employee}/show', ['as' => 'employees_show', 'uses' => 'Admin\EmployeeController@show']);
	Route::get('/admin/employees/{employee}/edit', ['as' => 'employees_edit', 'uses' => 'Admin\EmployeeController@edit']);
	Route::post('/admin/employees/{employee}/edit', ['as' => 'employees_update', 'uses' => 'Admin\EmployeeController@update']);
	Route::get('/admin/employees/{employee}/delete', ['as' => 'employees_delete', 'uses' => 'Admin\EmployeeController@destroy']);
	Route::post('/admin/employees/upload', 'Admin\EmployeeController@upload');

	Route::bind('slider', function($id){
		return \App\Models\Slider::whereId($id)->firstOrFail();
	});
	Route::get('/admin/slider', ['as' => 'slider_index', 'uses' => 'Admin\SliderController@index']);
	Route::post('/admin/slider/upload', ['as' => 'slider_upload', 'uses' => 'Admin\SliderController@upload']);
	Route::post('/admin/slider/saveSequence', ['as' => 'slider_saveSequence', 'uses' => 'Admin\SliderController@saveSequence']);
	Route::post('/admin/slider/saveDescription', ['as' => 'slider_saveDescription', 'uses' => 'Admin\SliderController@saveDescription']);
	Route::post('/admin/slider/create', ['as' => 'slider_store', 'uses' => 'Admin\SliderController@store']);
	Route::get('/admin/slider/{slider}', ['as' => 'slider_show', 'uses' => 'Admin\SliderController@show']);
	Route::get('/admin/slider/{slider}/edit', ['as' => 'slider_edit', 'uses' => 'Admin\SliderController@edit']);
	Route::post('/admin/slider/{slider}/edit', ['as' => 'slider_update', 'uses' => 'Admin\SliderController@store']);
	Route::get('/admin/slider/{slider}/delete', ['as' => 'slider_delete', 'uses' => 'Admin\SliderController@destroy']);


	Route::bind('vacancy', function($slug){
		return \App\Models\Vacancy::whereSlug($slug)->firstOrFail();
	});

	Route::get('/admin/vacancies', ['as' => 'vacancy_index', 'uses' => 'Admin\VacanciesController@index']);
	Route::get('/admin/vacancies/create', ['as' => 'vacancy_create', 'uses' => 'Admin\VacanciesController@create']);
	Route::post('/admin/vacancies/create', ['as' => 'vacancy_store', 'uses' => 'Admin\VacanciesController@store']);
	Route::get('/admin/vacancies/{vacancy}/show', ['as' => 'vacancy_show', 'uses' => 'Admin\VacanciesController@show']);
	Route::get('/admin/vacancies/{vacancy}/edit', ['as' => 'vacancy_edit', 'uses' => 'Admin\VacanciesController@edit']);
	Route::post('/admin/vacancies/{vacancy}/edit', ['as' => 'vacancy_update', 'uses' => 'Admin\VacanciesController@update']);
	Route::get('/admin/vacancies/{vacancy}/delete', ['as' => 'vacancy_delete', 'uses' => 'Admin\VacanciesController@destroy']);
});

