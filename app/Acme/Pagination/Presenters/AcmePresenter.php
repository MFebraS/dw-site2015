<?php namespace App\Acme\Pagination\Presenters;

use Illuminate\Pagination\BootstrapThreePresenter;

class AcmePresenter extends BootstrapThreePresenter {

    protected function getPreviousButton($text = '<span class="glyphicon glyphicon-triangle-left right-space"></span>Previous')
    {
        // If the current page is less than or equal to one, it means we can't go any
        // further back in the pages, so we will render a disabled previous button
        // when that is the case. Otherwise, we will give it an active "status".
        if ($this->paginator->currentPage() <= 1)
        {
            return $this->getDisabledTextWrapper($text);
        }

        $url = $this->paginator->url(
            $this->paginator->currentPage() - 1
        );

        return $this->getPageLinkWrapper($url, $text, 'prev');
    }

    protected function getNextButton($text = 'Next <span class="glyphicon glyphicon-triangle-right left-space"></span>')
    {
        // If the current page is greater than or equal to the last page, it means we
        // can't go any further into the pages, as we're already on this last page
        // that is available, so we will make it the "next" link style disabled.
        if ( ! $this->paginator->hasMorePages())
        {
            return $this->getDisabledTextWrapper($text);
        }

        $url = $this->paginator->url($this->paginator->currentPage() + 1);

        return $this->getPageLinkWrapper($url, $text, 'next');
    }

}