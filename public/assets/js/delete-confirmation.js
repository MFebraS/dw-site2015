$(function(){
	$('body').append('<div class="modal fade" id="delete-confirmation" tabindex="-1" role="dialog" aria-labelledby="delete-confirmation-label" aria-hidden="true">\
		<div class="modal-dialog">\
			<div class="modal-content">\
				<div class="modal-header">\
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\
					<h4 class="modal-title" id="delete-confirmation-label">Delete Confirmation</h4>\
				</div>\
				<div class="modal-body">\
					<h5>Are you sure want to delete <span id="delete-title"></span>?</h5>\
				</div>\
				<div class="modal-footer">\
					<button type="button" class="btn btn-default" data-dismiss="modal">NO</button>\
					<a><button type="button" class="btn btn-primary" >YES</button></a>\
				</div>\
			</div>\
		</div>\
	</div>');
})

$(document).on('show.bs.modal', '#delete-confirmation', function (event) {
	var button = $(event.relatedTarget); // Button that triggered the modal
	var delete_title = button.data('delete-title'); 
	var delete_target = button.data('delete-target');
	var modal = $(this);
	var href = delete_target;
	modal.find('#delete-title').text(delete_title);
	modal.find('a').attr('href', href);
});