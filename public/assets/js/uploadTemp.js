var processing;
var image_tools;
var temp_delete = [];

$('body').prepend('<div class="processing" id="processing"  style="display:none"><img class="loading" src="/assets/images/white-loader.gif"></div>').prepend('<div class="fail" id="fail"  style="display:none"><span>Failed</span></div>').prepend('<div id="image-tools" class="image-tools" style="display:none"><button type="button" class="delete-picture close"><span>&times;</span></button><button type="button" class="tools tools-left"><span class="glyphicon glyphicon-triangle-left"></span></button><button type="button" class="tools tools-right"><span class="glyphicon glyphicon-triangle-right"></span></button></div>');
processing = $('#processing').clone();
image_tools = $('#image-tools').clone();
fail = $('#fail').clone();


$('#work-pictures').on('show.bs.modal', function(){
	submit = true;
});
$('#save').on('click', function(e){
	if(!submit && $('#selected_files').children().length == 0){
		e.preventDefault();
		$('#work-pictures').modal("show");
	}
});
$('#submit').on('click', function(e){
	if(!submit && $('#selected_files').children().length == 0){
		e.preventDefault();
		$('#work-pictures').modal("show");
	}
});
$('#upload').on('change', function(){
	var files = $(this)[0].files;
	$.each(files, function(key,file){
		var type = file.type.toString();
		if(type.indexOf("image/") != -1){
			++id;
			var this_id = id;
			var li_id = this_id+"."+file.name.split(".").pop();
			$('#selected_files').append('<li id="'+ (li_id) +'" data-id="'+ this_id +'" class="list ui-state-default temp"><div class="image-container"><div class="processing" id="processing'+ id +'"><img class="loading" src="/assets/images/white-loader.gif"></div><img id="image'+ this_id +'" src="' + URL.createObjectURL(file) + '"></div></li>');
			
			var data = new FormData();
	        data.append('0', file);
	        
	        $.ajax({
	            url: '/admin/works/uploadTemp?id='+this_id,
	            headers: {
	                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	                },
	            type: 'POST',
	            data: data,
	            cache: false,
	            dataType: 'json',
	            processData: false, // Don't process the files
	            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
	            success: function(data, textStatus, jqXHR)
	            {
	                
	                if(data.status == 'ok'){
	                	$('#processing'+data.id).remove();
	                    $('#selected_files .temp #image'+data.id).attr('src', '/'+data.filename+'?'+Math.random()*Math.random());
	                    var image_order = $('#selected_files').sortable('toArray').toString();
						$("#images_order").val (image_order);
						$('#pictures-count').text($('#selected_files').sortable('toArray').length);
	                }
	                
	            },
	            error: function(jqXHR, textStatus, errorThrown)
	            {
	            	$('#processing'+this_id).remove();
	                $('#selected_files li[data-id='+this_id+']').prepend(fail.clone().show().attr('class', 'fail upload'));
	            }
	        });
		}else{
			alert('Please select image file!');
		}
	})
});
$('#selected_files').on('click', '.close', function(){
	var file_delete = $(this).parent().prev().children('img').attr('src').split('/').pop();
	var id_delete = file_delete.split(".")[0];
	var element_delete = $(this).parent().parent();
	var work_id = $(this).attr("data-id");

	file_delete = file_delete.split('?')[0];
	//console.log(id_delete);
	//console.log(file_delete);
	//console.log($('#selected_files li#'+id_delete));
	$(this).parent().prev().prepend(processing.clone().attr('id', 'processing'+id_delete).show());
	//console.log($('.pictures#'+id_delete));
	if(element_delete.hasClass('temp')){
		var data = new FormData();
		//data.append('work_id', work_id);
	    data.append('file_delete', file_delete);
		$.ajax({
	        url: '/admin/works/removeTemp?id='+id_delete,
	        headers: {
	                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	            },
	        type: 'POST',
	        data: data,
	        cache: false,
	        dataType: 'json',
	        processData: false, // Don't process the files
	        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
	        success: function(data, textStatus, jqXHR)
	        {
	            
	            if(data.status == 'ok'){
	            	element_delete.remove();
					var image_order = $('#selected_files').sortable('toArray').toString();
					$("#images_order").val (image_order);
					$('#pictures-count').text($('#selected_files').sortable('toArray').length);
	            }
	            
	        },
	        error: function(jqXHR, textStatus, errorThrown)
	        {
	        	$('#processing'+id_delete).remove();
	            $('#selected_files li[data-id='+id_delete+']').prepend(fail.clone().show().attr('class', 'fail delete'));
	        }
	    });
	}else{
		temp_delete.push(file_delete);
		$('#images_delete').val(temp_delete.toString());
		element_delete.remove();
		var image_order = $('#selected_files').sortable('toArray').toString();
		$("#images_order").val (image_order);
	}
});
$('#selected_files').on('mousedown', '.fail', function(){
	if($(this).hasClass('upload')){
		$(this).parent().remove();
	}else if($(this).hasClass('delete')){
		$(this).remove();
	}
	
});
$('#selected_files').on('mouseenter', 'li', function(){
	$(this).append(image_tools.show());
});
$('#selected_files').on('mouseleave', 'li', function(){
	$(this).find('.image-tools').remove();
});
$('#selected_files').on('click', '.tools-left', function(){
	var toMove = $(this).parent().parent();
	toMove.prev().before(toMove);
	var image_order = $('#selected_files').sortable('toArray').toString();
	$("#images_order").val (image_order);
})
$('#selected_files').on('click', '.tools-right', function(){
	var toMove = $(this).parent().parent();
	toMove.next().after(toMove);
	var image_order = $('#selected_files').sortable('toArray').toString();
	$("#images_order").val (image_order);
})
$("#selected_files").sortable({
	placeholder: "highlight",
	create: function (event, ui){
		var image_order = $(this).sortable('toArray').toString();
		$("#images_order").val(image_order);
		$('#pictures-count').text($(this).sortable('toArray').length);
	},
	start: function (event, ui) {
		ui.item.toggleClass("highlight");
	},
	stop: function (event, ui) {
		ui.item.toggleClass("highlight");
	},
	update: function(event, ui) {
		var image_order = $(this).sortable('toArray').toString();
		$("#images_order").val (image_order);
		$('#pictures-count').text($(this).sortable('toArray').length);
	}
});
