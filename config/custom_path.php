<?php

return [
	'favicon' => 'assets/images/favicon.ico',
	'works' => 'assets/uploads/works',
	'upload-news-pictures' => 'assets/uploads/upload-news-pictures',
	'employees' => 'assets/uploads/employees',
	'js' => 'assets/js',
	'sliders' => 'assets/uploads/sliders',
	'clients' => 'assets/uploads/clients'
];