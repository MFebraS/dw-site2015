<?PHP
	require "/template/header.php";
?>	
		
		<div class="title-page no-bottom">
			<div class="container">
				<h1>Our Works</h1>
			</div>
		</div>

		<div class="row text-center sub-title">
			<div class="container">
				<h3>Successfully Completed Projects</h3>
				<nav>
					<ul class="pager sub-nav">
						<li><a href="#">All</a></li>
						<li><a href="#">Web Design</a></li>
						<li><a href="#">Mobile App</a></li>
						<li><a href="#">Ilustration</a></li>
						<li><a href="#">Photography</a></li>
					</ul>
				</nav>
			</div>
		</div>
		

		<div class="collection">
			<div class="row row-collection">
				<div class="container">
					<div class="item-collection col-xs-6 col-sm-4 odd" data-title="Project 1" data-description="ini adalah project pertama" data-client="PT pertama" data-category="Web Design" data-link="www.pertama.com"></div>
					<div class="item-collection col-xs-6 col-sm-4"data-title="Project 2" data-description="ini adalah project kedua" data-client="PT kedua" data-category="Mobile Application" data-link="www.kedua.com"></div>
					<div class="item-collection col-xs-6 col-sm-4 odd"data-title="Project 3" data-description="ini adalah project ketiga" data-client="PT ketiga" data-category="Business Application" data-link="www.ketiga.com"></div>
					<div class="viewer col-sm-12"></div>

					<div class="item-collection col-xs-6 col-sm-4"data-title="Project 4" data-description="ini adalah project keempat" data-client="PT pertama" data-category="Digital Marketing" data-link="www.keempat.com"></div>
					<div class="item-collection col-xs-6 col-sm-4 odd"data-title="Project 5" data-description="ini adalah project kelima" data-client="PT kelima" data-category="Web Design" data-link="www.kelima.com"></div>
					<div class="item-collection col-xs-6 col-sm-4"data-title="Project 6" data-description="ini adalah project keenam" data-client="PT keenam" data-category="Business Application" data-link="www.keenam.com"></div>
					<div class="viewer col-sm-12"></div>
				
					<div class="item-collection col-xs-6 col-sm-4 odd"data-title="Project 7" data-description="ini adalah project ketujuh" data-client="PT ketujuh" data-category="Mobile Application" data-link="www.ketujuh.com"></div>
					<div class="item-collection col-xs-6 col-sm-4"data-title="Project 8" data-description="ini adalah project kedalapan" data-client="PT kedalapan" data-category="Digital Marketing" data-link="www.kedalapan.com"></div>
					<div class="item-collection col-xs-6 col-sm-4 odd"data-title="Project 9" data-description="ini adalah project kesembilan" data-client="PT kesembilan" data-category="Web Design" data-link="www.kesembilan.com"></div>
					<div class="viewer col-sm-12"></div>
				</div>
			</div>
			
			<div class="row see-all">
			<p class="text-center" style="color:white;"><a  href="#">LOAD MORE WORKS</a></p>
			</div>
		</div>

		<div class="purchase-now">
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<h4><b>Durenworks</b> is the Perfect Solution for Your Business</h4>
						<p>Let’s get started with us</p>
					</div>
					<div class="col-sm-3">
						<a href="#"><button type="button" class="btn btn-danger">GET STARTED</button></a>
					</div>
				</div>
				
				
			</div>
		</div>
		

		<div id="view-work" class="view-work collapse hidden-xs" section="view-work">
				<div class="container">
					<div class="row">
						<div class="col-sm-8">
							<div class="view-work-slider">
								<div id="view-work-slider" class="carousel slide" data-ride="carousel">
									<!-- Indicators -->
									<ol class="carousel-indicators">
										<li data-target="#view-work-slider" data-slide-to="0" class="active"></li>
										<li data-target="#view-work-slider" data-slide-to="1"></li>
										<li data-target="#view-work-slider" data-slide-to="2"></li>
										<li data-target="#view-work-slider" data-slide-to="3"></li>
										<li data-target="#view-work-slider" data-slide-to="4"></li>
									</ol>

									<!-- Wrapper for slides -->
									<div class="carousel-inner" role="listbox">
										<div class="item active" style="height:430px;background-color:#464646">
											<img src="" alt="...">
										</div>
										<div class="item" style="height:430px;background-color:#d42c0e">
											<img src="" alt="...">
										</div>
										<div class="item" style="height:430px;background-color:#363636">
											<img src="" alt="...">
										</div>
										<div class="item" style="height:430px;background-color:#ffffff">
											<img src="" alt="...">
										</div>
										<div class="item" style="height:430px;background-color:#8c8c8c">
											<img src="" alt="...">
										</div>
									</div>
									<!-- Controls -->
									<a class="left carousel-control" href="#view-work-slider" role="button" data-slide="prev">
									<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
									<span class="sr-only">Previous</span>
									</a>
									<a class="right carousel-control" href="#view-work-slider" role="button" data-slide="next">
									<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
									<span class="sr-only">Next</span>
									</a>
									
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<h4 id="view-work-title"></h4>
							<p>Description</p>
							<p id="view-work-description"></p>
							<div class="desc-botom">
								<div class="row">
									<div class="col-sm-4">
										<p>Client:</p>
									</div>
									<div class="col-sm-8">
										<p id="view-work-client"></p>
									</div>
								</div>
								<hr>
								<div class="row">
									<div class="col-sm-4">
										<p>Categories:</p>
									</div>
									<div class="col-sm-8">
										<p id="view-work-category"></p>
									</div>
								</div>
								<hr>
								<div class="row">
									<div class="col-sm-4">
										<p>Link:</p>
									</div>
									<div class="col-sm-8">
										<p id="view-work-link"></p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


		<?PHP
			require "/template/footer.php";
		?>


		<script type="text/javascript">
			$(document).ready(function() {
				$("#all").on('click', function(){
					$("#collection-all").collapse('toggle');
				});
				$("#web").on('click', function(){
					$("#collection-all").collapse('toggle');
				});
				$("#mobile").on('click', function(){
					$("#collection-all").collapse('toggle');
				});
				$("#ilustration").on('click', function(){
					$("#collection-all").collapse('toggle');
				});
				$("#photo").on('click', function(){
					$("#collection-all").collapse('toggle');
				});
				$('.sub-title a').on('click',function(){
					$('.sub-title a').each(function(){
						$(this).removeClass('active');
					})
					$(this).addClass('active');
				});
				$(window).resize(function(){
					if($(this).width() < 768){
						$('.viewer').each(function(){
							$(this).attr('style', 'display:none');
						});
						$('#view-work').each(function(){
							$(this).attr('style', 'display:none');
						});
					}else if($(this).width() >= 768){
						$('.viewer').each(function(){
							$(this).removeAttr('style');
						});
						$('#view-work').each(function(){
							$(this).removeAttr('style');
						});
					}
				});
				$(".col-sm-4").on('click', function(){
					if($(window).width() < 768){
						window.location.href = 'http://' + $(this).attr('data-link');
					}else if($(window).width() >= 768){
						var collection = $('.item-collection');
						var this_index = collection.index($(this));
						var row_index = Math.floor(this_index/3);
						var viewer = $('.viewer');
						var showing = $('#showing-row');
						var showed = $('#showed');
						var view_work = $("#view-work");
						view_work.find('.col-sm-4').removeClass('fade-out');
						view_work.find('.col-sm-8').removeClass('fade-out');
						var cloning = $("#view-work").clone();
						var title = $(this).attr('data-title');
						var description = $(this).attr('data-description');
						var client = $(this).attr('data-client');
						var category = $(this).attr('data-category');
						var link = $(this).attr('data-link');
						var arrow = '<div class="arrow-up gallery hidden-xs"></div>';
						var arrow_class = $('.arrow-up.gallery');
						var collection = $('.collection .row-collection .col-sm-4');
						var this_index = collection.index($(this));
						var showed_index = collection.index($('#showed'));
						if ((viewer.eq(row_index).attr('id') == 'showing-row') && ($(this).attr('id') == 'showed')){
							arrow_class.remove();
							view_work.collapse('hide');
							view_work.find('.col-sm-4').addClass('fade-out');
							view_work.find('.col-sm-8').addClass('fade-out');
							viewer.eq(row_index).removeAttr('id');
							$(this).removeAttr('id');

						}else if((viewer.eq(row_index).attr('id') == 'showing-row') && ($(this).attr('id') != 'showed')){
							arrow_class.remove();
							view_work.remove();
							viewer.eq(row_index).append(cloning);
							showed.removeAttr('id');
							$(this).attr('id', 'showed');
							$(this).append(arrow);
							$("html, body").animate({
								scrollTop: cloning.offset().top + 470 -$(window).height()
							}, 350);
						}else if((showing.length > 0)){
							arrow_class.remove();
							view_work.collapse('hide');
							view_work.find('.col-sm-4').addClass('fade-out');
							view_work.find('.col-sm-8').addClass('fade-out');
							cloning.attr('class','view-work collapse');
							viewer.eq(row_index).append(cloning);
							cloning.collapse('show');
							showing.removeAttr('id');
							showed.removeAttr('id');
							$(this).attr('id', 'showed');
							viewer.eq(row_index).attr('id', 'showing-row');
							$(this).append(arrow);
							if(this_index > showed_index){
								$("html, body").animate({
									scrollTop: cloning.offset().top -$(window).height()
								}, 350);
							}else{
								$("html, body").animate({
									scrollTop: cloning.offset().top + 470 -$(window).height()
								}, 350);
							}
							
							view_work.on('hidden.bs.collapse', function(){
								$(this).remove();
							});

						}else{
							$(this).append(arrow);
							viewer.eq(row_index).append(cloning);
							cloning.collapse('show');
							$(this).attr('id', 'showed');
							viewer.eq(row_index).attr('id', 'showing-row');
							view_work.remove();
							$("html, body").animate({
							scrollTop: cloning.offset().top + 470 -$(window).height()
						}, 350);

						}
						/*view_work.on('hide.bs.collapse', function(){
							view_work.find('.col-sm-4').addClass('fade-out');
							view_work.find('.col-sm-8').addClass('fade-out');
						});*/
						console.log('this index: ' +this_index);
						console.log('row index: ' +row_index);
						cloning.find('#view-work-title').html(title);
						cloning.find('#view-work-description').html(description);
						cloning.find('#view-work-client').html(client);
						cloning.find('#view-work-category').html(category);
						cloning.find('#view-work-link').html(link);
						cloning.find('.col-sm-4').addClass('fade-in');
						cloning.find('.col-sm-8').addClass('fade-in');
						cloning.find('.item').removeClass('active');
						cloning.find('.item:eq(0)').addClass('active');
						cloning.find('.carousel-indicators li').removeClass('active');
						cloning.find('.carousel-indicators li:eq(0)').addClass('active');
					}
					
				});
			});


			
		</script>
		
	</body>
</html>

                    
                    