	<footer>
		<hr class="footer-line">
		<div class="footer-top hidden-xs">
			<div class="container">
				<div class="row footer-top-head">
					<div class="col-md-3 col-sm-3 hidden-979 hidden-xs">
						<img src="assets/images/logo_durenworks_gray.png" height="100%">
					</div>
					<div class="col-md-3 col-sm-3">
						<h4>Services</h4>
					</div>
					<div class="col-md-3 col-sm-3">
						<h4>Products</h4>
					</div>
					<div class="col-md-3 col-sm-3">
						<h4>Contact Us</h4>
					</div>
				</div>
				<div class="row footer-top-content">
					<div class="col-md-3 col-sm-3 hidden-979 hidden-xs">
						<p>Nam liber tempor cum soluta nobis option congue nihil imperdiet doming id quod mazim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna.</p>
						<a href="">Read more <span class="glyphicon glyphicon-triangle-right"></span></a>
					</div>
					
					<div class="col-md-3 col-sm-3">
						<div class="row">
							<a href="#">
								<div class="col-md-10 col-sm-10">
									<p>Web Design &amp; Development</p>
								</div>
								<div class="col-md-2 col-sm-2">
									<span class="glyphicon glyphicon-triangle-right"></span>
								</div>
							</a>
						</div>
						<div class="row">
							<hr width="100%"></hr>
						</div>
						<div class="row" >
							<a href="#">
								<div class="col-md-10 col-sm-10">
									<p>Business Application</p>
								</div>
								<div class="col-md-2 col-sm-2">
									<span class="glyphicon glyphicon-triangle-right"></span>
								</div>
							</a>
						</div>
						<div class="row">
							<hr width="100%"></hr>
						</div>
						<div class="row">
							<a href="#">
								<div class="col-md-10 col-sm-10">
									<p>Mobile Application</p>
								</div>
								<div class="col-md-2 col-sm-2">
									<span class="glyphicon glyphicon-triangle-right"></span>
								</div>
							</a>
						</div>
						<div class="row">
							<hr width="100%"></hr>
						</div>
						<div class="row">
							<a href="#">
								<div class="col-md-10 col-sm-10">
									<p>Digital Marketing</p>
								</div>
								<div class="col-md-2 col-sm-2">
									<span class="glyphicon glyphicon-triangle-right"></span>
								</div>
							</a>
						</div>
						
					</div>
					<div class="col-md-3 col-sm-3">
						<div class="row">
							<a href="#">
								<div class="col-md-10 col-sm-10">
									<p>Pawons</p>
								</div>
								<div class="col-md-2 col-sm-2">
									<span class="glyphicon glyphicon-triangle-right"></span>
								</div>
							</a>
						</div>
						<div class="row">
							<hr width="100%"></hr>
						</div>
						<div class="row">
							<a href="#">
								<div class="col-md-10 col-sm-10">
									<p>Mondopad by Infocus</p>
								</div>
								<div class="col-md-2 col-sm-2">
									<span class="glyphicon glyphicon-triangle-right"></span>
								</div>
							</a>
						</div>
						<div class="row">
							<hr width="100%"></hr>
						</div>
						<div class="row">
							<a href="#">
								<div class="col-md-10 col-sm-10">
									<p>Avaya</p>
								</div>
								<div class="col-md-2 col-sm-2">
									<span class="glyphicon glyphicon-triangle-right"></span>
								</div>
							</a>
						</div>
					</div>
					<div class="col-md-3 col-sm-3">
						<table class="contact-us">
							<tr>
								<td class="contact-us-icon"><span class="glyphicon glyphicon-map-marker"></span></td>
								<td class="contact-us-atribute">Addres:</td>
								<td class="contact-us-content">Jalan Jatisari II/4, Tembalang</td>
							</tr>
							<tr>
								<td class="contact-us-icon"><span class="glyphicon glyphicon-earphone"></span></td>
								<td class="contact-us-atribute">Phone:</td>
								<td class="contact-us-content">+6224-7462189</td>
							</tr>
							<tr>
								<td class="contact-us-icon"><span class="glyphicon glyphicon-send"></span></td>
								<td class="contact-us-atribute">Email:</td>
								<td class="contact-us-content">contact@durenworks.com</td>
							</tr>
						</table>
						<div class="row">
							<div class="col-md-2 col-sm-2">
								<a href="">
									<div class="sprite facebook-icon"></div>
								</a>
							</div>
							<div class="col-md-2 col-sm-2">
								<a href="" >
									<div class="sprite twitter-icon"></div>
								</a>
							</div>
							<div class="col-md-2 col-sm-2">
								<a href="" >
									<div class="sprite google-icon"></div>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="footer-bottom">
			<div class="container">
				<div class="footer-bottom-left">
					Copyright &copy; 2014 <b>Durenworks</b><span class="fence"><a href="">Privacy Policy</a></span><span class="fence"><a href="">About Durenworks</a></span><span class="fence"><a href="">FAQ</a></span><span class="fence"><a href="">Contact Support</a></span>
				</div>
				<div class="footer-bottom-right hidden-xs">
					Designed by <a href=""><b>Durenworks</b></a> <span class="fence"></span>Assembled in <a href=""><b>Semarang</b></a>
				</div>
			</div>
		</div>

	</footer>
	<!-- Content END -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
	<script src="assets/js/jquery-1.11.2.min.js"></script>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="assets/js/bootstrap.min.js"></script>