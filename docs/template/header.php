<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Durenworks</title>

		<!-- Bootstrap -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet">
		<link href='http://fonts.googleapis.com/css?family=Raleway:400,500,700,600,800' rel='stylesheet' type='text/css'>
		<link href="assets/css/durenworks.css" rel="stylesheet">
		<link href="assets/css/responsive.css" rel="stylesheet">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<!-- Content START -->
		
		<!-- Static navbar -->
		<nav class="navbar navbar-default navbar-static-top">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<!-- Logo Brand-->
					<a class="navbar-brand" href="#"><img src="assets/images/logo_durenworks.png"/></a>
				</div>
				
				<div id="navbar" class="collapse">
					<ul class="nav navbar-nav"> <!--navbar-right-->
						<li><a href="about-us.php">About Us</a></li>
						<li><a href="services.php">Services</a></li>
						<li><a href="product.php">Products</a></li>
						<li><a href="our-works.php">Our Works</a></li>
						<li><a href="news.php">News</a></li>
						<li><a href="#">Careers</a></li>
						<li><a href="#">Contact Us</a></li>
					</ul>
				</div><!--/.nav-collapse -->
			</div>
		</nav>