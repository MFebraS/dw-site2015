<?PHP
	require "/template/header.php";
?>	
		
		
		<div class="title-page">
			<div class="container">
				<h1>Services</h1>
			</div>
		</div>
		
		
		<div class="services row"  style="background-color:#fafafa">
			<div class="container">
				<div class="row">
                    <div class="col-sm-6 hidden-xs services-image" style="background-color:#898989">
						
					</div>
					<div class="col-sm-6">
						<h4>Website Design &amp; Development</h4>
						<p>Durenworks is a full service ITConsultant &amp; Solution expertised in Website design &amp; Development, Business Solution Provider, Mobile Application Developer, and Digital Marketing Strategy. </p>
						<p>We Esteblished on October 2013 in Indonesia, as affiliated company of PT. Widya Solusi Utama for IT Services / Consulting in 2015, strive to fulfils all your business needs under one roof. Providing innovative, unique, and high quality services, based on requirements of clients.</p>
						<p>We understand that building great business, need efficiency to achieve your goal and result. that's why, we prefer to work continuously with a client. We act as not only an implementer but client's business consultant, giving them great advices and creative idea to solve their business problem.</p>
						<p>As a result, the best benefit from durenworks is "Sustainable Solution"</p>
					</div>
				</div>
			</div>
		</div>
		
		<div class="services row">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<h4>Business Application</h4>
						<p>Durenworks is a full service ITConsultant &amp; Solution expertised in Website design &amp; Development, Business Solution Provider, Mobile Application Developer, and Digital Marketing Strategy. </p>
						<p>We Esteblished on October 2013 in Indonesia, as affiliated company of PT. Widya Solusi Utama for IT Services / Consulting in 2015, strive to fulfils all your business needs under one roof. Providing innovative, unique, and high quality services, based on requirements of clients.</p>
						<p>We understand that building great business, need efficiency to achieve your goal and result. that's why, we prefer to work continuously with a client. We act as not only an implementer but client's business consultant, giving them great advices and creative idea to solve their business problem.</p>
						<p>As a result, the best benefit from durenworks is "Sustainable Solution"</p>
					</div>
                    <div class="col-sm-6 hidden-xs services-image" style="background-color:#898989">
						
					</div>
				</div>
			</div>
		</div>
		
		<div class="services row"  style="background-color:#fafafa">
			<div class="container">
				<div class="row">
                    <div class="col-sm-6 hidden-xs services-image" style="background-color:#898989">
						
					</div>
					<div class="col-sm-6">
						<h4>Mobile Application</h4>
						<p>Durenworks is a full service ITConsultant &amp; Solution expertised in Website design &amp; Development, Business Solution Provider, Mobile Application Developer, and Digital Marketing Strategy. </p>
						<p>We Esteblished on October 2013 in Indonesia, as affiliated company of PT. Widya Solusi Utama for IT Services / Consulting in 2015, strive to fulfils all your business needs under one roof. Providing innovative, unique, and high quality services, based on requirements of clients.</p>
						<p>We understand that building great business, need efficiency to achieve your goal and result. that's why, we prefer to work continuously with a client. We act as not only an implementer but client's business consultant, giving them great advices and creative idea to solve their business problem.</p>
						<p>As a result, the best benefit from durenworks is "Sustainable Solution"</p>
					</div>
				</div>
			</div>
		</div>
		
		<div class="services row">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<h4>Digital Marketing</h4>
						<p>Durenworks is a full service ITConsultant &amp; Solution expertised in Website design &amp; Development, Business Solution Provider, Mobile Application Developer, and Digital Marketing Strategy. </p>
						<p>We Esteblished on October 2013 in Indonesia, as affiliated company of PT. Widya Solusi Utama for IT Services / Consulting in 2015, strive to fulfils all your business needs under one roof. Providing innovative, unique, and high quality services, based on requirements of clients.</p>
						<p>We understand that building great business, need efficiency to achieve your goal and result. that's why, we prefer to work continuously with a client. We act as not only an implementer but client's business consultant, giving them great advices and creative idea to solve their business problem.</p>
						<p>As a result, the best benefit from durenworks is "Sustainable Solution"</p>
					</div>
                    <div class="col-sm-6 hidden-xs services-image" style="background-color:#898989">
						
					</div>
				</div>
			</div>
		</div>
		
		<?PHP
			require "/template/footer.php";
		?>
		
	</body>
</html>