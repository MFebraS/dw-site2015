<?PHP
	require "/template/header.php";
?>
    <!-- Judul Halaman -->
    <div class="title-page no-bottom">
      <div class="container">
        <h1>Products</h1>
      </div>
    </div>
	
	<div class="products">
	  <div class="products-background">
		<div class="container">
			<div id="sub-menu-products">
				<nav>
					<ul class="nav nav-justified">
						<li><a href="#avaya" id="left-sub-menu" data-toggle="tab">AVAYA</a><span class="arrow"></span></li>
						<li><a href="#mondopad" id="center-sub-menu" data-toggle="tab">Mondopad by Infocus</a><span class="arrow"></span></li>
						<li><a href="#pawons" id="right-sub-menu" data-toggle="tab">PAWONS System</a><span class="arrow"></span></li>
					</ul>
				</nav>
			</div>
		</div>
	  </div>
	  
	  <div class="tab-content">
		<!-- Tab Pane Avaya -->
		<div id="avaya" class="tab-pane">
			<div class="products-item products-background">
				<div class="image-products">
					<img src="assets/images/products/avaya.png" alt="Avaya"/>
				</div>
				<div class="products-item-description container">
					<h2>Avaya Product</h2>
					<p>The Giant touch PC to 
					present, annotate, and collaborate with people in
					the room and around the world</p>
				</div>
			</div>
			<div class="products-item-details">
				<div class="container">
					<div class="row">
						<div class="col-sm-6 hidden-xs">
							<img src="assets/images/products/avaya.png" width="100%" alt="Avaya"></img>
						</div>
						<div class="col-sm-6 col-xs-12">
							<p>Avaya..., collaborate and connect. meetings are more
							engaging, content is more immersive, and audiences are more engaged.</p>
							<div class="features">
								<h2>Avaya Key Features</h2>
								<ul class="nav">
									<li>Multi touch High Definition 55 & 70 inch</li>
									<li>Flexible and expandable with built-in Windows PC</li>
									<li>Digital Intercative Whiteboard and document annotation</li>
									<li>Business-class Video Conferencing</li>
									<li>Share, View and control from your Notebook, tablet or smartphone</li>
									<li>Full version of Microsoft Office</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="products-item-details products-background">
				<div class="container">
					<div class="row">
						<div class="col-sm-6 col-xs-12">
							<div class="features">
								<h2>AVAYA.... for Your Conference Room or Classroom</h2>
								<p>Avaya... in the room and around the world. An
								Infocus Mondopad puts everything you need to visually present,
								capture and share ideas at your fingertips.
								</p>
								<p>Avaya...
								device, Mondopad streamlines meetings and alows people to
								communicate clearly. You’ll bridge communication gaps, save
								time and money on travel, and never look back.
								</p>
							</div>
						</div>
						<div class="col-sm-6 hidden-xs">
							<iframe class="video-features" width="100%" height="315"
								src="https://www.youtube.com/embed/ARpz15baF_4" frameborder="0" allowfullscreen>
							</iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Tab Pane Mondopad -->
		<div id="mondopad" class="tab-pane active">
			<div class="products-item products-background">
				<div  class="image-products">
					<img class="hidden-xs" src="assets/images/products/mondopad.png" alt="Mondopad"/>
					<img class="hidden-sm hidden-md hidden-lg" src="assets/images/products/mondopad-phone.png" alt="Mondopad"/>
					<div class="video-products hidden-xs">
                        <video class="vertical-center" controls muted autoplay>
                            <source src="assets/videos/video.mp4" type="video/mp4">
                        </video>
					</div>
				</div>
				<div class="products-item-description container">
					<h2>Communicate and Collaborate better than ever</h2>
					<p>The Giant touch PC to 
					present, annotate, and collaborate with people in
					the room and around the world</p>
				</div>
			</div>
			<div class="products-item-details">
				<div class="container">
					<div class="row">
						<div class="col-sm-6 hidden-xs">
							<img src="assets/images/products/collaboration.png" width="100%"></img>
						</div>
						<div class="col-sm-6 col-xs-12">
							<p>Mondopad’s human, natural touch interface changes the way people
							interact with information, collaborate and connect. meetings are more
							engaging, content is more immersive, and audiences are more engaged.</p>
							<div class="features">
								<h2>Mondopad Key Features</h2>
								<ul class="nav">
									<li>Multi touch High Definition 55 & 70 inch</li>
									<li>Flexible and expandable with built-in Windows PC</li>
									<li>Digital Intercative Whiteboard and document annotation</li>
									<li>Business-class Video Conferencing</li>
									<li>Share, View and control from your Notebook, tablet or smartphone</li>
									<li>Full version of Microsoft Office</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="products-item-details products-background">
				<div class="container">
					<div class="row">
						<div class="col-sm-6 col-xs-12">
							<div class="features">
								<h2>Giant Touch Tablet for Your Conference Room or Classroom</h2>
								<p>Present, annotate and collaborate on a giant tablet with
								meeting participants in the room and around the world. An
								Infocus Mondopad puts everything you need to visually present,
								capture and share ideas at your fingertips.
								</p>
								<p>All beautifully integrated into a single, cost-effective
								device, Mondopad streamlines meetings and alows people to
								communicate clearly. You’ll bridge communication gaps, save
								time and money on travel, and never look back.
								</p>
							</div>
						</div>
						<div class="col-sm-6 hidden-xs">
							<iframe class="video-features" width="100%" height="315"
								src="https://www.youtube.com/embed/ARpz15baF_4" frameborder="0" allowfullscreen>
							</iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Tab Pane Pawons -->
		<div id="pawons" class="tab-pane">
			<div class="products-item products-background">
				<div class="image-products">
					<img src="assets/images/products/pawons.png" alt="Pawons"/>
				</div>
				<div class="products-item-description container">
					<h2>Pawons Product</h2>
					<p>The Giant touch PC to 
					present, annotate, and collaborate with people in
					the room and around the world</p>
				</div>
			</div>
			<div class="products-item-details">
				<div class="container">
					<div class="row">
						<div class="col-sm-6 hidden-xs">
							<img src="assets/images/products/collaboration.png" width="100%"></img>
						</div>
						<div class="col-sm-6 col-xs-12">
							<p>PAWONS changes the way people
							interact with information, collaborate and connect. meetings are more
							engaging, content is more immersive, and audiences are more engaged.</p>
							<div class="features">
								<h2>PAWONS Key Features</h2>
								<ul class="nav">
									<li>Multi touch High Definition 55 & 70 inch</li>
									<li>Flexible and expandable with built-in Windows PC</li>
									<li>Digital Intercative Whiteboard and document annotation</li>
									<li>Business-class Video Conferencing</li>
									<li>Share, View and control from your Notebook, tablet or smartphone</li>
									<li>Full version of Microsoft Office</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="products-item-details products-background">
				<div class="container">
					<div class="row">
						<div class="col-sm-6 col-xs-12">
							<div class="features">
								<h2>PAWONS.... for Your Conference Room or Classroom</h2>
								<p>Pawons... collaborate on a giant tablet with
								meeting participants in the room and around the world. An
								Infocus Mondopad puts everything you need to visually present,
								capture and share ideas at your fingertips.
								</p>
								<p>Pawons...
								device, Mondopad streamlines meetings and alows people to
								communicate clearly. You’ll bridge communication gaps, save
								time and money on travel, and never look back.
								</p>
							</div>
						</div>
						<div class="col-sm-6 hidden-xs">
							<iframe class="video-features" width="100%" height="315"
								src="https://www.youtube.com/embed/ARpz15baF_4" frameborder="0" allowfullscreen>
							</iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	  </div>
	</div>
<?PHP
	require "/template/footer.php";
?>
    <script type="text/javascript">
        function checkVideo()
        {
            if(!!document.createElement('iframe').canPlayType)
            {
                var vidTest=document.createElement("iframe");
                
                if (!vidTest)
                {
                    document.getElementById("checkVideoResult").HTML=;
                }
                else
                {
                    if (oggTest=="probably")
                    {
                        document.getElementById("checkVideoResult").innerHTML="Yeah! Full support!";
                    }
                    else
                    {
                        document.getElementById("checkVideoResult").innerHTML="Meh. Some support.";
                    }
                }
            }
            else
            {
                document.getElementById("checkVideoResult").innerHTML="Sorry. No video support."
            }
        }
    </script>
  </body>
</html>