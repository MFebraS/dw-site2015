<?PHP
	require "/template/header.php";
?>	
		
		
		<div class="title-page">
			<div class="container">
				<h1>About Us</h1>
			</div>
		</div>
		
        <div class="about-us">
            <div class="who-we-are"  style="background-color:#fafafa">
                <div class="container">
                    <h3 class="text-center">Who We are</h3>
                    <p class="text-center">DURENWORKS, Digital Utilities, Advertising and Enginering Works</p><br/>
                    <div class="row">
                        <div class="col-md-6 hidden-xs" style="background-color:#898989;height:300px">

                        </div>
                        <div class="col-md-6">
                            <h4>About Dreamworks</h4>
                            <p>Durenworks is a full service ITConsultant &amp; Solution expertised in Website design &amp; Development, Business Solution Provider, Mobile Application Developer, and Digital Marketing Strategy. </p>
                            <p>We Esteblished on October 2013 in Indonesia, as affiliated company of PT. Widya Solusi Utama for IT Services / Consulting in 2015, strive to fulfils all your business needs under one roof. Providing innovative, unique, and high quality services, based on requirements of clients.</p>
                            <p>We understand that building great business, need efficiency to achieve your goal and result. that's why, we prefer to work continuosly with a client. We act as not only an implementer but client's business consultant, giving them great advices and creative idea to solve their business problem.</p>
                            <p>As a result, the best benefit from durenworks is "Sustainable Solution"</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="our-team">
                <div class="container">
                    <h3 class="text-center">Our Profesional Team</h3>
                    <p class="text-center">Our secret ingredient for successful project</p><br/>
                    <div class="row">
                        <div class="col-xs-6 col-md-3">
                            <a href="">
                                <div class="img-wrap">
                                    <img src="...">
                                </div>
                                <h4>Andrie Widyastama</h4>
                            </a>
                            <small>Founder / CEO</small>
                            <p class="hidden-xs">Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim.</p>
                            <div class="row">
                                <div class="col-xs-1 col-md-2">
                                    <a href="">
                                        <div class="sprite facebook-icon"></div>
                                    </a>
                                </div>
                                <div class="col-xs-1 col-md-2">
                                    <a href="" >
                                        <div class="sprite twitter-icon"></div>
                                    </a>
                                </div>
                                <div class="col-xs-1 col-md-2">
                                    <a href="" >
                                        <div class="sprite google-icon"></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 col-md-3">
                            <a href="">
                                <div class="img-wrap">
                                    <img src="...">
                                </div>
                                <h4>Airlangga Cahya Utama</h4>
                            </a>
                            <small>Founder / CTO</small>
                            <p class="hidden-xs">Lorem ipsum dolor sit amet, adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet.</p>
                            <div class="row">
                                <div class="col-xs-1 col-md-2">
                                    <a href="">
                                        <div class="sprite facebook-icon"></div>
                                    </a>
                                </div>
                                <div class="col-xs-1 col-md-2">
                                    <a href="" >
                                        <div class="sprite twitter-icon"></div>
                                    </a>
                                </div>
                                <div class="col-xs-1 col-md-2">
                                    <a href="" >
                                        <div class="sprite google-icon"></div>
                                    </a>
                                </div>
                            </div>
                        </div><div class="col-xs-6 col-md-3">
                            <a href="">
                                <div class="img-wrap">
                                    <img src="...">
                                </div>
                                <h4>Max Darmawan</h4>
                            </a>
                            <small>Senior Developer</small>
                            <p class="hidden-xs">Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim.</p>
                            <div class="row">
                                <div class="col-xs-1 col-md-2">
                                    <a href="">
                                        <div class="sprite facebook-icon"></div>
                                    </a>
                                </div>
                                <div class="col-xs-1 col-md-2">
                                    <a href="" >
                                        <div class="sprite twitter-icon"></div>
                                    </a>
                                </div>
                                <div class="col-xs-1 col-md-2">
                                    <a href="" >
                                        <div class="sprite google-icon"></div>
                                    </a>
                                </div>
                            </div>
                        </div><div class="col-xs-6 col-md-3">
                            <a href="">
                                <div class="img-wrap">
                                    <img src="...">
                                </div>
                                <h4>Daniek Kusumaningsih</h4>
                            </a>
                            <small>Junior Developer</small>
                            <p class="hidden-xs">Lorem ipsum dolor sit amet, adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet.</p>
                            <div class="row">
                                <div class="col-xs-1 col-md-2">
                                    <a href="">
                                        <div class="sprite facebook-icon"></div>
                                    </a>
                                </div>
                                <div class="col-xs-1 col-md-2">
                                    <a href="" >
                                        <div class="sprite twitter-icon"></div>
                                    </a>
                                </div>
                                <div class="col-xs-1 col-md-2">
                                    <a href="" >
                                        <div class="sprite google-icon"></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="how-we-work" style="background-color:#fafafa">
                <div class="container">
                    <h3 class="text-center">How We Work</h3>
                    <p class="text-center">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </p>


                </div>
            </div>


            <div class="some-partner">
                <div class="container">
                    <h3 class="text-center">Some of Our Partners</h3>
                    <p class="text-center">Everything start with collaboration, we have our backup here</p><br/>
                    <!--<div>
                        <a href="#"><img src="assets/images/about-us/partners/avaya_logo.png" width="fixed"></a>
                        <a href="#"><img src="assets/images/about-us/partners/artapala_logo.png" width="fixed"></a>
                        <a href="#"><img src="assets/images/about-us/partners/veritrans_logo.png" width="fixed"></a>
                    </div>-->
                    <div class="row">
                        <div class="col-sm-4">
                            <a href="#"><img src="assets/images/about-us/partners/avaya_logo.png" width="100%"></a>
                        </div>
                        <div class="col-sm-4">
                            <a href="#"><img src="assets/images/about-us/partners/artapala_logo.png" width="100%"></a>
                        </div>
                        <div class="col-sm-4">
                            <a href="#"><img src="assets/images/about-us/partners/veritrans_logo.png" width="100%"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<?PHP
			require "/template/footer.php";
		?>
		
	</body>
</html>