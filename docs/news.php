<?PHP
	require "/template/header.php";
?>
    <!-- Judul Halaman -->
    <div class="title-page">
      <div class="container">
        <h1>News</h1>
      </div>
    </div>

    <div class="news">
	  <div class="container">
		<!-- News and Image -->
		<div class="col-md-9 col-sm-12">
		  <div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<h2><a href="">Nam Liber Tempor Cum Soluta Nobis Eleifend Option</a></h2>
				<div class="news-atribute">
					<span class="author">By <a href="">John Doe</a></span>	<span class="category">In: <a href="">Website Design</a></span>
				</div>
				<p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
				<div class="read-more"><a href="#">Read More <span class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span> </a></div>
			</div>
			<div class="col-md-6 col-sm-6">
			  <div class="media hidden-xs">
				<img class="media-object" data-src="holder.js/500x500/auto" alt=""/>
				<div class="news-tooltip">
					<div class="news-sprite news-tooltip-arrow"></div>
					<span>04 June, 2014</span>
				</div>
			  </div>
			</div>
		  </div>
		  <hr>
		  <div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<h2><a href="">Heading News 2</a></h2>
				<div class="news-atribute">
					<span class="author">By <a href="">John Doe</a></span>	<span class="category">In: <a href="">Website Design</a></span>
				</div>
				<p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
				<div class="read-more"><a href="#">Read More <span class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span> </a></div>
			</div>
			<div class="col-md-6 col-sm-6">
			  <div class="media hidden-xs">
				<img class="media-object" data-src="holder.js/500x500/auto" alt=""/>
				<div class="news-tooltip">
					<div class="news-sprite news-tooltip-arrow"></div>
					<span>04 June, 2014</span>
				</div>
			  </div>
			</div>
		  </div>
		  <hr>
		  <!-- Pagination -->
		  <div id="pagination">
			  <nav>
				<ul class="pager">
					<li class="previous">
					  <a href="#" aria-label="Previous">
						<span class="glyphicon glyphicon-triangle-left right-space" aria-hidden="true"></span>Previous
					  </a>
					</li>
					<li><a href="#">1</a></li>
					<li><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">4</a></li>
					<li><a href="#">5</a></li>
					<li><a href="#">6</a></li>
					<li><a href="#">7</a></li>
					<li><a href="#">8</a></li>
					<li><a href="#">9</a></li>
					<li><a href="#">10</a></li>
					<li class="next">
					  <a href="#" aria-label="Next">
						Next <span class="glyphicon glyphicon-triangle-right left-space" aria-hidden="true"></span>
					  </a>
					</li>
				</ul>
			  </nav>
		  </div>
	  </div> <!-- /News and Image -->
	  
	  <!-- Sidebar -->
	  <div class="col-md-3">
		<nav class="bs-docs-sidebar hidden-print hidden-xs hidden-sm">
		  <form>
			<input type="search" placeholder="Search..." />
			<input type="submit" value="" class="news-sprite" />
		  </form>
		  <div class="sidebar-module">
			<h2>Categories</h2>
			<ul class="nav bs-docs-sidenav">
			  <li><a href="#"><div class="news-sprite arrow-round"></div>Website Design<span class="pull-right">125</span></a></li>
			  <li><a href="#"><div class="news-sprite arrow-round"></div>Digital Marketing<span class="pull-right">97</span></a></li>
			  <li><a href="#"><div class="news-sprite arrow-round"></div>Mobile Application<span class="pull-right">56</span></a></li>
			  <li><a href="#"><div class="news-sprite arrow-round"></div>Business<span class="pull-right">24</span></a></li>
			  <li><a href="#"><div class="news-sprite arrow-round"></div>Miscellaneous<span class="pull-right">13</span></a></li>
			</ul>
			
			<!-- Menu Tab -->
			<div id="menu-tab" role="tabpanel" data-example-id="togglable-tabs">
				<ul id="sidebar-tab" class="nav nav-tabs" role="tablist">
				  <li role="presentation" class="active"><a href="#popular" id="popular-tab" role="tab" data-toggle="tab" aria-controls="" aria-expanded="true">Popular</a></li>
				  <li role="presentation"><a href="#recent" role="tab" id="recent-tab" data-toggle="tab" aria-controls="">Recent</a></li>
				</ul>
				<div id="sidebar-tab-content" class="tab-content">
				  <div role="tabpanel" class="tab-pane active" id="popular" aria-labelledBy="popular-tab">
					<div class="row">
						<ul class="list-unstyled">
						  <li><a href="#">Lorem ipsum dolor ...</a></li>
						  <li class="date">Date: <a href="#">05 June 2014</a></li>
						</ul>
					</div>
					<div class="row">
						<ul class="list-unstyled">
						  <li><a href="#">Diam nonummny nibh ...</a></li>
						  <li class="date">Date: <a href="#">05 June 2014</a></li>
						</ul>
					</div>
				  </div>
				  <div role="tabpanel" class="tab-pane" id="recent" aria-labelledBy="recent-tab">
					<div class="row">
						<ul class="list-unstyled">
						  <li><a href="#">Bagian Recent</a></li>
						  <li class="date">Date: <a href="#">05 June 2014</a></li>
						</ul>
					</div>
				  </div>
				</div>
			  </div>
			</div>
		  </nav>
		</div> <!-- /Sidebar -->
	  </div> <!-- /container -->
	</div>
<?PHP
	require "/template/footer.php";
?>
  </body>
</html>