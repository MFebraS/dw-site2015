<?PHP
	require "/template/header.php";
?>	
		<!-- SLIDER -->
		<div class="row slider">
			<div id="slider" class="carousel slide" data-ride="carousel">
				<!-- Indicators, Ini adalah Tombol BULET BULET dibawah. item ini dapat dihapus jika tidak diperlukan -->
				<ol class="carousel-indicators">
				<li data-target="#slider" data-slide-to="0" class="active"></li>
				<li data-target="#slider" data-slide-to="1"></li>
				<li data-target="#slider" data-slide-to="2"></li>
				<li data-target="#slider" data-slide-to="3"></li>
				</ol>

				<!-- Wrapper for slides, Ini adalah Tempat Gambar-->
				<div class="carousel-inner" >
					<div class="item active">
						<div class="bg-slider" style="background-image:linear-gradient(to right,rgb(242,201,80) 0,rgb(220,99,78) 100%)">
							<img class="bg-slider2" src="assets/images/index/slider/bg-slider.png">
							<div class="text-slider">
								<div class="container">
									<div class="row">
										<div class="col-sm-6 vertical-center">
											<div class="text-content">
												<h3>Unique and Modern Technology</h3>
												<h2>YOUR WEBSITE</h2>
												<p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</p>
												<br/>
												<a href="#"><button type="button" class="btn btn-danger">PURCHASE NOW</button></a>
											</div>
										</div>
									</div>
								</div>
							</div>
							
						</div>
						
					</div><div class="item">
						<div class="bg-slider" style="background-image:url(assets/images/index/slider/slider1.jpg)">
							<img class="bg-slider2" src="assets/images/index/slider/bg-slider.png">
							
						</div>
						
					</div>
					<div class="item">
						<div class="bg-slider" style="background-image:url(assets/images/index/slider/slider2.jpg)">
							<img class="bg-slider2" src="assets/images/index/slider/bg-slider.png">
						</div>
						
					</div>
					<div class="item">
						<div class="bg-slider" style="background-image:url(assets/images/index/slider/slider3.jpg)">
							<img class="bg-slider2" src="assets/images/index/slider/bg-slider.png">
						</div>
						
					</div>
					

				</div>

				<!-- Slider Controls -->
				<a class="left carousel-control" href="#slider" data-slide="prev">
				<!--<span class="glyphicon glyphicon-chevron-left"></span>-->
				<span><img src="assets/images/index/slider/arrow_left.png"></span>
				</a>
				<a class="right carousel-control" href="#slider" data-slide="next">
				<!--<span class="glyphicon glyphicon-chevron-right"></span>-->
				<span><img src="assets/images/index/slider/arrow_right.png"></span>
				</a>
			</div>
			<div class="visible-xs xs-carousel">
				<img src="assets/images/index/slider/small-display.png" width="100%">
			</div>
		</div>
		<!-- END SLIDER -->
		
		<div class="welcome">
			<div class="container">
				<div class="row">
					<div class="col-sm-8">
						<h3>Welcome to Durenworks</h3>
						<p>Durenworks, Digital Utilities, Advertising, and Engineering Works, is a full service IT Consultant &amp; Solution expertised in Webside design &amp; development, Business Solution Provider, Mobile Application Developer, and Digital Marketing Strategy.</p>
						<p>We Established on October 2013 in Semarang, Indonesia. As affiliated company of PT. Widya Solusi Utama for IT Services / Consulting in 2015, strive to fulfils all your business needs under one roof. providing innovative, unique, and high quality services, based on requirements of clients.</p><br/>
						<a href="#"><button type="button" class="btn btn-danger">Learn More</button></a>
					</div>
					<div class="col-sm-4">
						<img src="assets/images/index/welcome/mobile.png" width="95%">
					</div>
				</div>
			</div>
		</div>
		
		
		<div class="what-we-do" style="background-image: url(assets/images/index/what-we-do/back-whatwedo.png)">
			<div class="container">
				<h3>What we do</h3>
				<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </p>
				<div class="row">
					<div class="col-xs-6 col-sm-3 web">
						<a href="#">
							<div class="title">
								<div class="icon-place">
                                    <div class="index-sprite icon"></div>
                                </div>
                                <h4>Web &amp; App Design</h4>
							</div>
							<p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim.</p>
							<div class="read-more">
								Read more <span class="glyphicon glyphicon-triangle-right"></span>
							</div>
						</a>
					</div>
					<div class="col-xs-6 col-sm-3 marketing">
						<a href="#">
							<div class="title">
								<div class="icon-place">
                                    <div class="index-sprite icon"></div>
                                </div>
                                <h4>Digital Marketing Strategy</h4>
							</div>
							<p>Lorem ipsum dolor sit amet, adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet.</p>
							<div class="read-more">
								Read more <span class="glyphicon glyphicon-triangle-right"></span>
							</div>
						</a>
					</div>
					<div class="col-xs-6 col-sm-3 development">
						<a href="#">
							<div class="title">
								<div class="icon-place">
                                    <div class="index-sprite icon"></div>
                                </div>
                                <h4>Development</h4>
							</div>
							<p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim.</p>
							<div class="read-more">
								Read more <span class="glyphicon glyphicon-triangle-right"></span>
							</div>
						</a>
					</div>
					<div class="col-xs-6 col-sm-3 collaboration">
						<a href="#">
							<div class="title">
								<div class="icon-place">
                                    <div class="index-sprite icon"></div>
                                </div>
                                <h4>Collaborate Sollution</h4>
							</div>
							<p>Lorem ipsum dolor sit amet, adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet.</p>
							<div class="read-more">
								Read more <span class="glyphicon glyphicon-triangle-right"></span>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
		
		<div class="some-work">
			<div class="container">
				<h3 class="text-center">Some of Our Works</h3>
				<p class="text-center">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </p><br/>
				
				<div class="row">
					<div class="col-sm-6 col-md-4 odd">
						<div class="img-wrap"></div>
						<div class="tool-place">
							<a href="#"><img class="lupa" src="assets/images/index/icons/lupa.png"/></a>
						</div>
						<div class="caption-place">
							<div class="caption-arrow"></div>
							<div class="caption">
								<a href=""><h5>Project title here</h5></a>
								<p>Category</p>
							</div>
						</div>
					</div>

					<div class="col-sm-6 col-md-4">
						<div class="img-wrap"></div>
						<div class="tool-place">
							<a href="#"><img class="lupa" src="assets/images/index/icons/lupa.png"></a>
						</div>
						<div class="caption-place">
							<div class="caption-arrow"></div>
							<div class="caption">
								<a href=""><h5>Project title here</h5></a>
								<p>Category</p>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-4 odd">
						<div class="img-wrap"></div>
						<div class="tool-place">
							<a href="#"><img class="lupa" src="assets/images/index/icons/lupa.png"></a>
						</div>
						<div class="caption-place">
							<div class="caption-arrow"></div>
							<div class="caption">
								<a href=""><h5>Project title here</h5></a>
								<p>Category</p>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-4">
						<div class="img-wrap"></div>
						<div class="tool-place">
							<a href="#"><img class="lupa" src="assets/images/index/icons/lupa.png"></a>
						</div>
						<div class="caption-place">
							<div class="caption-arrow"></div>
							<div class="caption">
								<a href=""><h5>Project title here</h5></a>
								<p>Category</p>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-4 odd">
						<div class="img-wrap"></div>
						<div class="tool-place">
							<a href="#"><img class="lupa" src="assets/images/index/icons/lupa.png"></a>
						</div>
						<div class="caption-place">
							<div class="caption-arrow"></div>
							<div class="caption">
								<a href=""><h5>Project title here</h5></a>
								<p>Category</p>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-4">
						<div class="img-wrap"></div>
						<div class="tool-place">
							<a href="#"><img class="lupa" src="assets/images/index/icons/lupa.png"></a>
						</div>
						<div class="caption-place">
							<div class="caption-arrow"></div>
							<div class="caption">
								<a href=""><h5>Project title here</h5></a>
								<p>Category</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row see-all">
			<p class="text-center"><a  href="#">SEE ALL OUR WORK</a></p>
			</div>
		</div>
		
		
		<div class="some-client" >
			<div class="container">
				<div class="row text-center">
					<h3>Some of Our Client</h3>
					<p class="container">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </p>
					<br/>
					<div class="col-sm-6 col-md-3 col-xs-12">
						<a href="#"><img src="assets/images/index/clients/explora.png" width="100%"></a>
					</div>
					<div class="col-sm-6 col-md-3 col-xs-12">
						<a href="#"><img src="assets/images/index/clients/KPKNL-sorong.png" width="100%"></a>
					</div>
					<div class="col-sm-6 col-md-3 col-xs-12">
						<a href="#"><img src="assets/images/index/clients/cendana-print.png" width="100%"></a>
					</div>
					<div class="col-sm-6 col-md-3 col-xs-12">
						<a href="#"><img src="assets/images/index/clients/denata-cafe.png" width="100%"></a>
					</div>
				</div>
			</div>
		</div>

		<?PHP
			require "/template/footer.php";
		?>

		<script type="text/javascript">
			$(document).ready(function(){
				$('.navbar-static-top').addClass('index');
			});
		</script>
	</body>
</html>