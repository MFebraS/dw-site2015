<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkPicturesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('work_pictures', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('picture_name');
			$table->integer('sequence')->default(1);
			$table->integer('work_id')->unsigned();
			$table->foreign('work_id')->references('id')->on('works');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('work_pictures');
	}

}
