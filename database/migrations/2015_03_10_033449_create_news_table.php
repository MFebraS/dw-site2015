<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('news', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('author_id');
			$table->string('title')->unique();
			$table->string('permalink')->unique();
			$table->integer('category_id');
			$table->string('status');
			$table->datetime('published_date')->nullable();
			$table->integer('hits')->nullable();
			$table->text('content')->nullable();
			$table->text('html_content')->nullable();
			$table->timestamps();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('news');
	}

}
