<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Models\Client;
use App\Models\User;
use App\Models\Vacancy;
use App\Models\Slider;
use App\Models\Category;
use Carbon\Carbon;

class SeederInMigration extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		$client = new Client();
		$client->insert([
			['name' => 'Explora Digital Printing', 'logo' => 'explora.jpg', 'slug' => str_slug('Explora Digital Printing')],
			['name' => 'KPKNL Sorong', 'logo' => 'KPKNL-sorong.jpg', 'slug' => str_slug('KPKNL Sorong')],
			['name' => 'CENDANA Digital Printing Station', 'logo' => 'cendana-print.jpg', 'slug' => str_slug('CENDANA Digital Printing Station')],
			['name' => 'Denata Cafe', 'logo' => 'denata-cafe.jpg', 'slug' => str_slug('Denata Cafe')],
			['name' => 'ISA Grafika', 'logo' => 'isa-grafika.jpg', 'slug' => str_slug('ISA Grafika')],
			['name' => 'RED Indonesia Desain', 'logo' => 'red-ind.jpg', 'slug' => str_slug('RED Indonesia Desain')],
			['name' => 'klick', 'logo' => 'klick.jpg', 'slug' => str_slug('klick')],
			['name' => 'Baju--Anak.com', 'logo' => 'baju-anak.jpg', 'slug' => str_slug('Baju--Anak.com')],
		]);

		$user = new User;
		$user->insert([
			'name' => 'Andrie Widyastama',
			'email' => 'andrie@durenworks.com',
			'employ' => 'Founder / CEO',
			'password' => Hash::make('durenworks'),
			'level' => '1',
			'picture' => 'andrie_face.jpg',
			'about' => 'Bachelor degree of IT from Binus University Jakarta, over 5 years experience as sales and advertising in IT industry',
			'active' => '1'
		]);
		$user->insert([
			'name' => 'Airlangga Cahya Utama',
			'email' => 'airlangga@durenworks.com',
			'employ' => 'Founder / CTO',
			'password' => Hash::make('durenworks'),
			'level' => '1',
			'picture' => 'airlangga_face.jpg',
			'about' => 'Profesional Programmer that graduated from ITS Surabaya. Familiar with computer science kindergarten and pationed',
			'active' => '1'
		]);
		$user->insert([
			'name' => 'Max Darmawan',
			'email' => 'max@durenworks.com',
			'employ' => 'Senior Developer',
			'password' => '*',
			'level' => '0',
			'picture' => 'max_face.jpg',
			'about' => 'Senior Programmer & Developer who have expertise in Finance & accounting module.',
			'active' => '1'
		]);
		$user->insert([
			'name' => 'Daniek Kusumaningsih',
			'email' => 'daniek@durenworks.com',
			'employ' => 'Software Developer',
			'password' => '*',
			'level' => '0',
			'picture' => 'daniek_face.jpg',
			'about' => 'Graduate from Accounting Computerize from POLINES Semarang, the only woman on team',
			'active' => '1'
		]);
		$user->insert([
			'name' => 'Jati Juli Susanto',
			'email' => 'jati@durenworks.com',
			'employ' => 'Designer',
			'password' => '*',
			'level' => '0',
			'picture' => 'jati_face.jpg',
			'about' => '...',
			'active' => '1'
		]);


		Vacancy::insert([
			'title' => 'HRD',
			'status' => 'published',
			'slug' => 'hrd',
			'created_at' => Carbon::now()->subHours(72),
			'updated_at' => Carbon::now()->subHours(72)
		]);

		Vacancy::insert([
			'title' => 'UI Web Designer',
			'status' => 'published',
			'slug' => 'ui-web-designer',
			'created_at' => Carbon::now()->subHours(60),
			'updated_at' => Carbon::now()->subHours(60)
		]);

		Vacancy::insert([
			'title' => 'Designer',
			'status' => 'published',
			'slug' => 'designer',
			'created_at' => Carbon::now()->subHours(48),
			'updated_at' => Carbon::now()->subHours(48)
		]);

		Vacancy::insert([
			'title' => 'Account Business Consultant',
			'status' => 'published',
			'slug' => 'account-business-consultant',
			'created_at' => Carbon::now()->subHours(36),
			'updated_at' => Carbon::now()->subHours(36)
		]);

		Vacancy::insert([
			'title' => 'Senior Web Programmer',
			'detail' => '### Qualification :
- Male / Female Max 30 Years Old
- Candidate must possess at least Diploma, Bachelor&rsquo;s degree, Computer science / Information technology or equivalent
- Full-time position(s) available
- Willing to work with deadline
- Willing to work as a team or individual
- Have a good sense of humor (don&#039;t be uncommunicative)
### Requirement :
- Minimal already handle 5 live projects
- Strong knowledge of CSS, HTML5, PHP, Javascript /AJAX, Net, VB. Net / Delphi/Java etc
- Having experience min 2 years using php Framework (Laravel, CI, Symfony, zend, etc)
- Having experience in Relational Data Base (SQL server / MySql)
- Having experience in SQL Language and Storage Procedure Programming
- Good logical skill / minded
- Good Leadership skill

&lt;br&gt;
Please sent your complete Resume and CV attached by email to &lt;contact@durenworks.com&gt; with subject **[Senior Web Programmer]**.',
			'html_detail' => '&lt;h3&gt;Qualification :&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;Male / Female Max 30 Years Old&lt;/li&gt;
&lt;li&gt;Candidate must possess at least Diploma, Bachelor&rsquo;s degree, Computer science / Information technology or equivalent&lt;/li&gt;
&lt;li&gt;Full-time position(s) available&lt;/li&gt;
&lt;li&gt;Willing to work with deadline&lt;/li&gt;
&lt;li&gt;Willing to work as a team or individual&lt;/li&gt;
&lt;li&gt;Have a good sense of humor (don&#039;t be uncommunicative)&lt;/li&gt;
&lt;/ul&gt;
&lt;h3&gt;Requirement :&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;Minimal already handle 5 live projects&lt;/li&gt;
&lt;li&gt;Strong knowledge of CSS, HTML5, PHP, Javascript /AJAX, Net, VB. Net / Delphi/Java etc&lt;/li&gt;
&lt;li&gt;Having experience min 2 years using php Framework (Laravel, CI, Symfony, zend, etc)&lt;/li&gt;
&lt;li&gt;Having experience in Relational Data Base (SQL server / MySql)&lt;/li&gt;
&lt;li&gt;Having experience in SQL Language and Storage Procedure Programming&lt;/li&gt;
&lt;li&gt;Good logical skill / minded&lt;/li&gt;
&lt;li&gt;Good Leadership skill&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;&lt;br&gt;
Please sent your complete Resume and CV attached by email to &lt;a href=&quot;mailto:contact@durenworks.com&quot;&gt;contact@durenworks.com&lt;/a&gt; with subject &lt;strong&gt;[Senior Web Programmer]&lt;/strong&gt;.&lt;/p&gt;
',
			'status' => 'published',
			'slug' => 'senior-web-programmer',
			'created_at' => Carbon::now()->subHours(24),
			'updated_at' => Carbon::now()->subHours(24)
		]);

		Vacancy::insert([
			'title' => 'Junior Web Programmer',
			'detail' => '### Qualification :
- Male / Female Max 27 Years Old
- Candidate must possess at least a SMK, Diploma, Bachelor&rsquo;s degree, Computer science / Information technology or equivalent
- Fresh graduate are welcome
- Full-time position(s) available
- Willing to work with deadline
- Willing to work as a team or individual
- Have a good sense of humor (don&#039;t be uncommunicative)
### Requirement :
- Minimal already handle 2 live projects
- Strong knowledge of CSS, HTML5, PHP, Javascript /AJAX, Net, VB. Net / Delphi/Java etc
- Good Knowledge and can use of PHP Framework (Laravel, CI, Symfony, zend, etc)
- Having experience in Relational Data Base (SQL server / MySql)
- Having experience in SQL Language and Storage Procedure Programming
- Good logical skill / minded

&lt;br&gt;
Please sent your complete Resume and CV attached by email to &lt;contact@durenworks.com&gt; with subject **[Junior Web Programmer]**.',
			'html_detail' => '&lt;h3&gt;Qualification :&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;Male / Female Max 27 Years Old&lt;/li&gt;
&lt;li&gt;Candidate must possess at least a SMK, Diploma, Bachelor&rsquo;s degree, Computer science / Information technology or equivalent&lt;/li&gt;
&lt;li&gt;Fresh graduate are welcome&lt;/li&gt;
&lt;li&gt;Full-time position(s) available&lt;/li&gt;
&lt;li&gt;Willing to work with deadline&lt;/li&gt;
&lt;li&gt;Willing to work as a team or individual&lt;/li&gt;
&lt;li&gt;Have a good sense of humor (don&#039;t be uncommunicative)&lt;/li&gt;
&lt;/ul&gt;
&lt;h3&gt;Requirement :&lt;/h3&gt;
&lt;ul&gt;
&lt;li&gt;Minimal already handle 2 live projects&lt;/li&gt;
&lt;li&gt;Strong knowledge of CSS, HTML5, PHP, Javascript /AJAX, Net, VB. Net / Delphi/Java etc&lt;/li&gt;
&lt;li&gt;Good Knowledge and can use of PHP Framework (Laravel, CI, Symfony, zend, etc)&lt;/li&gt;
&lt;li&gt;Having experience in Relational Data Base (SQL server / MySql)&lt;/li&gt;
&lt;li&gt;Having experience in SQL Language and Storage Procedure Programming&lt;/li&gt;
&lt;li&gt;Good logical skill / minded&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;&lt;br&gt;
Please sent your complete Resume and CV attached by email to &lt;a href=&quot;mailto:contact@durenworks.com&quot;&gt;contact@durenworks.com&lt;/a&gt; with subject &lt;strong&gt;[Junior Web Programmer]&lt;/strong&gt;.&lt;/p&gt;
',
			'status' => 'published',
			'slug' => 'junior-web-programmer',
			'created_at' => Carbon::now()->subHours(12),
			'updated_at' => Carbon::now()->subHours(12)
		]);




		$slider = new Slider;
		$slider->insert([
			'image' => '1.jpg',
			'subimage' => '',
			'description' => e('<h1><b>CREATIVE DIGITAL SOLUTIONS</b></h1><p class="hidden-sm">We are digital consultant company that present creativity of IT technology. Delivering our best value to provide digital business solution</p><div class="button-danger-place"><a href="/services#business-application"><button type="button" class="btn btn-danger">LEARN MORE</button></a></div>'),
			'description_position' => 'center',
			'sequence' => '1'
			]);
		$slider->insert([
			'image' => '2.jpg',
			'subimage' => '2-sub.png',
			'description' => e('<div class="text-right" style="margin-right: 50px;margin-left: -50px;"><h2>Unique and Modern Technology for</h2><h1>RESPONSIVE <b>WEBSITE</b></h1><p class="hidden-sm">Our experience team always giving their best capability to deliver sophiscated design and technology. That give Unique experience for your website visitor or customers in any devices</p><div class="button-danger-place"><a href="/services"><button type="button" class="btn btn-danger">LEARN MORE</button></a></div></div>'),
			'description_position' => 'left',
			'sequence' => '2'
			]);
		$slider->insert([
			'image' => '3.jpg',
			'subimage' => '3-sub.png',
			'description' => e('<div class="text-left" style="margin-right: -50px;margin-left: 50px;"><h2>Fully custom with simple design</h2><h1>MOBILE <b>APPS</b></h1><p class="hidden-sm">Simple doesn\'t meant you have limited experience using application, our technology allows you to create fully custom apps that fit your business even with simple layout and UI</p><div class="button-danger-place"><a href="/services#mobile-application"><button type="button" class="btn btn-danger">LEARN MORE</button></a></div></div>'),
			'description_position' => 'right',
			'sequence' => '3'
			]);
		$slider->insert([
			'image' => '4.jpg',
			'subimage' => '',
			'description' => e('<div><h2>Fully custom with simple design</h2><h1>DIGITAL <b>MARKETING</b></h1><p class="hidden-sm">Simple doesn\'t meant you have limited experience using application, our technology allows you to create fully custom apps that fit your business even with simple layout and UI</p><div class="button-danger-place"><a href="/services#digital-marketing"><button type="button" class="btn btn-danger">LEARN MORE</button></a></div></div>'),
			'description_position' => 'center',
			'sequence' => '3'
			]);



		$category = new Category;
		$category->insert([
			'category_name' => 'Web Design',
			'slug' => 'web-design',
			'created_at' => '2015-03-28',
			'updated_at' => '2015-03-28'
		]);
		$category->insert([
			'category_name' => 'Mobile Application',
			'slug' => 'mobile-application',
			'created_at' => '2015-03-28',
			'updated_at' => '2015-03-28'
		]);$category->insert([
			'category_name' => 'Digital Marketing',
			'slug' => 'digital-marketing',
			'created_at' => '2015-03-28',
			'updated_at' => '2015-03-28'
		]);$category->insert([
			'category_name' => 'Business Application',
			'slug' => 'business-application',
			'created_at' => '2015-03-28',
			'updated_at' => '2015-03-28'
		]);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		$users = User::get();

		$clients = Client::get();

		$vacancies = Vacancy::get();

		$sliders = Slider::get();

		$categories = Category::get();

		foreach($users as $user){
			$user->delete();
		}

		foreach ($clients as $client) {
			$client->delete();
		}

		foreach ($vacancies as $vacancy) {
			$vacancy->delete();
		}

		foreach ($sliders as $slider) {
			$slider->delete();
		}

		foreach($categories as $category){
			$category->delete();
		}
	}

}
