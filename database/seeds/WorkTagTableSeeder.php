<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\WorkTag;

class WorkTagTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$workTag = new WorkTag;
		$workTag->insert([
			'work_id' => '1', 
			'tag_id' => '1'
			]);
		$workTag->insert([
			'work_id' => '1', 
			'tag_id' => '2'
			]);
		$workTag->insert([
			'work_id' => '2', 
			'tag_id' => '1'
			]);
		$workTag->insert([
			'work_id' => '2', 
			'tag_id' => '3'
			]);
		$workTag->insert([
			'work_id' => '2', 
			'tag_id' => '4'
			]);
		$workTag->insert([
			'work_id' => '3', 
			'tag_id' => '3'
			]);
		$workTag->insert([
			'work_id' => '3', 
			'tag_id' => '4'
			]);
		$workTag->insert([
			'work_id' => '4', 
			'tag_id' => '1'
			]);
		$workTag->insert([
			'work_id' => '4', 
			'tag_id' => '2'
			]);
		$workTag->insert([
			'work_id' => '4', 
			'tag_id' => '3'
			]);
		$workTag->insert([
			'work_id' => '5', 
			'tag_id' => '1'
			]);
		$workTag->insert([
			'work_id' => '6', 
			'tag_id' => '1'
			]);
		$workTag->insert([
			'work_id' => '6', 
			'tag_id' => '2'
			]);
		$workTag->insert([
			'work_id' => '7', 
			'tag_id' => '1'
			]);
		$workTag->insert([
			'work_id' => '7', 
			'tag_id' => '3'
			]);
		$workTag->insert([
			'work_id' => '7', 
			'tag_id' => '4'
			]);
		$workTag->insert([
			'work_id' => '8', 
			'tag_id' => '3'
			]);
		$workTag->insert([
			'work_id' => '8', 
			'tag_id' => '4'
			]);
		$workTag->insert([
			'work_id' => '9', 
			'tag_id' => '1'
			]);
		$workTag->insert([
			'work_id' => '9', 
			'tag_id' => '2'
			]);
		$workTag->insert([
			'work_id' => '9', 
			'tag_id' => '3'
			]);
		$workTag->insert([
			'work_id' => '10', 
			'tag_id' => '1'
			]);
		$workTag->insert([
			'work_id' => '11', 
			'tag_id' => '1'
			]);
		$workTag->insert([
			'work_id' => '11', 
			'tag_id' => '2'
			]);
		$workTag->insert([
			'work_id' => '12', 
			'tag_id' => '1'
			]);
		$workTag->insert([
			'work_id' => '12', 
			'tag_id' => '3'
			]);
		$workTag->insert([
			'work_id' => '12', 
			'tag_id' => '4'
			]);
		$workTag->insert([
			'work_id' => '13', 
			'tag_id' => '3'
			]);
		$workTag->insert([
			'work_id' => '13', 
			'tag_id' => '4'
			]);
		$workTag->insert([
			'work_id' => '14', 
			'tag_id' => '1'
			]);
		$workTag->insert([
			'work_id' => '14', 
			'tag_id' => '2'
			]);
		$workTag->insert([
			'work_id' => '14', 
			'tag_id' => '3'
			]);
		$workTag->insert([
			'work_id' => '15', 
			'tag_id' => '1'
			]);
		$workTag->insert([
			'work_id' => '16', 
			'tag_id' => '1'
			]);
		$workTag->insert([
			'work_id' => '16', 
			'tag_id' => '2'
			]);
		$workTag->insert([
			'work_id' => '17', 
			'tag_id' => '1'
			]);
		$workTag->insert([
			'work_id' => '17', 
			'tag_id' => '3'
			]);
		$workTag->insert([
			'work_id' => '17', 
			'tag_id' => '4'
			]);
		$workTag->insert([
			'work_id' => '18', 
			'tag_id' => '3'
			]);
		$workTag->insert([
			'work_id' => '18', 
			'tag_id' => '4'
			]);
		$workTag->insert([
			'work_id' => '19', 
			'tag_id' => '1'
			]);
		$workTag->insert([
			'work_id' => '19', 
			'tag_id' => '2'
			]);
		$workTag->insert([
			'work_id' => '19', 
			'tag_id' => '3'
			]);
		$workTag->insert([
			'work_id' => '20', 
			'tag_id' => '1'
			]);
		$workTag->insert([
			'work_id' => '21', 
			'tag_id' => '1'
			]);
		$workTag->insert([
			'work_id' => '21', 
			'tag_id' => '2'
			]);
		$workTag->insert([
			'work_id' => '22', 
			'tag_id' => '1'
			]);
		$workTag->insert([
			'work_id' => '22', 
			'tag_id' => '3'
			]);
		$workTag->insert([
			'work_id' => '22', 
			'tag_id' => '4'
			]);
		$workTag->insert([
			'work_id' => '23', 
			'tag_id' => '3'
			]);
		$workTag->insert([
			'work_id' => '23', 
			'tag_id' => '4'
			]);
		$workTag->insert([
			'work_id' => '24', 
			'tag_id' => '1'
			]);
		$workTag->insert([
			'work_id' => '24', 
			'tag_id' => '2'
			]);
		$workTag->insert([
			'work_id' => '24', 
			'tag_id' => '3'
			]);
		$workTag->insert([
			'work_id' => '25', 
			'tag_id' => '1'
			]);
		$workTag->insert([
			'work_id' => '26', 
			'tag_id' => '1'
			]);
		$workTag->insert([
			'work_id' => '26', 
			'tag_id' => '2'
			]);
		$workTag->insert([
			'work_id' => '27', 
			'tag_id' => '1'
			]);
		$workTag->insert([
			'work_id' => '27', 
			'tag_id' => '3'
			]);
		$workTag->insert([
			'work_id' => '27', 
			'tag_id' => '4'
			]);
		$workTag->insert([
			'work_id' => '28', 
			'tag_id' => '3'
			]);
		$workTag->insert([
			'work_id' => '28', 
			'tag_id' => '4'
			]);
		$workTag->insert([
			'work_id' => '29', 
			'tag_id' => '1'
			]);
		$workTag->insert([
			'work_id' => '29', 
			'tag_id' => '2'
			]);
		$workTag->insert([
			'work_id' => '29', 
			'tag_id' => '3'
			]);
		$workTag->insert([
			'work_id' => '30', 
			'tag_id' => '1'
			]);
		$workTag->insert([
			'work_id' => '31', 
			'tag_id' => '1'
			]);
		$workTag->insert([
			'work_id' => '31', 
			'tag_id' => '2'
			]);
		$workTag->insert([
			'work_id' => '32', 
			'tag_id' => '1'
			]);
		$workTag->insert([
			'work_id' => '32', 
			'tag_id' => '3'
			]);
		$workTag->insert([
			'work_id' => '32', 
			'tag_id' => '4'
			]);
		$workTag->insert([
			'work_id' => '33', 
			'tag_id' => '3'
			]);
		$workTag->insert([
			'work_id' => '33', 
			'tag_id' => '4'
			]);
		$workTag->insert([
			'work_id' => '34', 
			'tag_id' => '1'
			]);
		$workTag->insert([
			'work_id' => '34', 
			'tag_id' => '2'
			]);
		$workTag->insert([
			'work_id' => '34', 
			'tag_id' => '3'
			]);
		$workTag->insert([
			'work_id' => '35', 
			'tag_id' => '1'
			]);
		$workTag->insert([
			'work_id' => '36', 
			'tag_id' => '1'
			]);
		$workTag->insert([
			'work_id' => '36', 
			'tag_id' => '2'
			]);
		$workTag->insert([
			'work_id' => '37', 
			'tag_id' => '1'
			]);
		$workTag->insert([
			'work_id' => '37', 
			'tag_id' => '3'
			]);
		$workTag->insert([
			'work_id' => '37', 
			'tag_id' => '4'
			]);
		$workTag->insert([
			'work_id' => '38', 
			'tag_id' => '3'
			]);
		$workTag->insert([
			'work_id' => '38', 
			'tag_id' => '4'
			]);
		$workTag->insert([
			'work_id' => '39', 
			'tag_id' => '1'
			]);
		$workTag->insert([
			'work_id' => '39', 
			'tag_id' => '2'
			]);
		$workTag->insert([
			'work_id' => '39', 
			'tag_id' => '3'
			]);
		$workTag->insert([
			'work_id' => '40', 
			'tag_id' => '1'
			]);
		$workTag->insert([
			'work_id' => '41', 
			'tag_id' => '1'
			]);
		$workTag->insert([
			'work_id' => '41', 
			'tag_id' => '2'
			]);
		$workTag->insert([
			'work_id' => '42', 
			'tag_id' => '1'
			]);
		$workTag->insert([
			'work_id' => '42', 
			'tag_id' => '3'
			]);
		$workTag->insert([
			'work_id' => '42', 
			'tag_id' => '4'
			]);
		$workTag->insert([
			'work_id' => '43', 
			'tag_id' => '3'
			]);
		$workTag->insert([
			'work_id' => '43', 
			'tag_id' => '4'
			]);
		$workTag->insert([
			'work_id' => '44', 
			'tag_id' => '1'
			]);
		$workTag->insert([
			'work_id' => '44', 
			'tag_id' => '2'
			]);
		$workTag->insert([
			'work_id' => '44', 
			'tag_id' => '3'
			]);
		$workTag->insert([
			'work_id' => '45', 
			'tag_id' => '1'
			]);
		$workTag->insert([
			'work_id' => '46', 
			'tag_id' => '1'
			]);
		$workTag->insert([
			'work_id' => '46', 
			'tag_id' => '2'
			]);
		$workTag->insert([
			'work_id' => '47', 
			'tag_id' => '1'
			]);
		$workTag->insert([
			'work_id' => '47', 
			'tag_id' => '3'
			]);
		$workTag->insert([
			'work_id' => '47', 
			'tag_id' => '4'
			]);
		$workTag->insert([
			'work_id' => '48', 
			'tag_id' => '3'
			]);
		$workTag->insert([
			'work_id' => '48', 
			'tag_id' => '4'
			]);
		$workTag->insert([
			'work_id' => '49', 
			'tag_id' => '1'
			]);
		$workTag->insert([
			'work_id' => '49', 
			'tag_id' => '2'
			]);
		$workTag->insert([
			'work_id' => '49', 
			'tag_id' => '3'
			]);
		$workTag->insert([
			'work_id' => '50', 
			'tag_id' => '1'
			]);
		
	}
}
