<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\WorkPicture;

class WorkPictureTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$WorkPicture = new WorkPicture;
		$WorkPicture->insert([
			'picture_name' => '1.jpg', 
			'sequence' => '5', 
			'work_id' => '1'
			]);
		$WorkPicture->insert([
			'picture_name' => '2.jpg', 
			'sequence' => '3', 
			'work_id' => '1'
			]);
		$WorkPicture->insert([
			'picture_name' => '3.jpg', 
			'sequence' => '1', 
			'work_id' => '1'
			]);
		$WorkPicture->insert([
			'picture_name' => '4.jpg', 
			'sequence' => '2', 
			'work_id' => '1'
			]);
		$WorkPicture->insert([
			'picture_name' => '5.jpg', 
			'sequence' => '4', 
			'work_id' => '1'
			]);
		$WorkPicture->insert([
			'picture_name' => '6.jpg', 
			'sequence' => '1', 
			'work_id' => '2'
			]);
		$WorkPicture->insert([
			'picture_name' => '7.jpg', 
			'sequence' => '3', 
			'work_id' => '2'
			]);
		$WorkPicture->insert([
			'picture_name' => '8.jpg', 
			'sequence' => '2', 
			'work_id' => '2'
			]);
		$WorkPicture->insert([
			'picture_name' => '9.jpg', 
			'sequence' => '3', 
			'work_id' => '3'
			]);
		$WorkPicture->insert([
			'picture_name' => '10.jpg', 
			'sequence' => '1', 
			'work_id' => '3'
			]);
		$WorkPicture->insert([
			'picture_name' => '11.jpg', 
			'sequence' => '4', 
			'work_id' => '3'
			]);
		$WorkPicture->insert([
			'picture_name' => '12.jpg', 
			'sequence' => '2', 
			'work_id' => '3'
			]);
		$WorkPicture->insert([
			'picture_name' => '13.jpg', 
			'sequence' => '2', 
			'work_id' => '4'
			]);
		$WorkPicture->insert([
			'picture_name' => '14.jpg', 
			'sequence' => '3', 
			'work_id' => '4'
			]);
		$WorkPicture->insert([
			'picture_name' => '15.jpg', 
			'sequence' => '4', 
			'work_id' => '4'
			]);
		$WorkPicture->insert([
			'picture_name' => '16.jpg', 
			'sequence' => '5', 
			'work_id' => '4'
			]);
		$WorkPicture->insert([
			'picture_name' => '17.jpg', 
			'sequence' => '1', 
			'work_id' => '4'
			]);
		$WorkPicture->insert([
			'picture_name' => '18.jpg', 
			'sequence' => '4', 
			'work_id' => '5'
			]);
		$WorkPicture->insert([
			'picture_name' => '19.jpg', 
			'sequence' => '5', 
			'work_id' => '5'
			]);
		$WorkPicture->insert([
			'picture_name' => '20.jpg', 
			'sequence' => '3', 
			'work_id' => '5'
			]);
		$WorkPicture->insert([
			'picture_name' => '21.jpg', 
			'sequence' => '6', 
			'work_id' => '5'
			]);
		$WorkPicture->insert([
			'picture_name' => '22.jpg', 
			'sequence' => '2', 
			'work_id' => '5'
			]);
		$WorkPicture->insert([
			'picture_name' => '23.jpg', 
			'sequence' => '1', 
			'work_id' => '5'
			]);
		$WorkPicture->insert([
			'picture_name' => '24.jpg', 
			'sequence' => '5', 
			'work_id' => '6'
			]);
		$WorkPicture->insert([
			'picture_name' => '25.jpg', 
			'sequence' => '3', 
			'work_id' => '6'
			]);
		$WorkPicture->insert([
			'picture_name' => '26.jpg', 
			'sequence' => '1', 
			'work_id' => '6'
			]);
		$WorkPicture->insert([
			'picture_name' => '27.jpg', 
			'sequence' => '2', 
			'work_id' => '6'
			]);
		$WorkPicture->insert([
			'picture_name' => '28.jpg', 
			'sequence' => '4', 
			'work_id' => '6'
			]);
		$WorkPicture->insert([
			'picture_name' => '29.jpg', 
			'sequence' => '1', 
			'work_id' => '7'
			]);
		$WorkPicture->insert([
			'picture_name' => '30.jpg', 
			'sequence' => '3', 
			'work_id' => '7'
			]);
		$WorkPicture->insert([
			'picture_name' => '31.jpg', 
			'sequence' => '2', 
			'work_id' => '7'
			]);
		$WorkPicture->insert([
			'picture_name' => '32.jpg', 
			'sequence' => '3', 
			'work_id' => '8'
			]);
		$WorkPicture->insert([
			'picture_name' => '33.jpg', 
			'sequence' => '1', 
			'work_id' => '8'
			]);
		$WorkPicture->insert([
			'picture_name' => '34.jpg', 
			'sequence' => '4', 
			'work_id' => '8'
			]);
		$WorkPicture->insert([
			'picture_name' => '35.jpg', 
			'sequence' => '2', 
			'work_id' => '8'
			]);
		$WorkPicture->insert([
			'picture_name' => '36.jpg', 
			'sequence' => '2', 
			'work_id' => '9'
			]);
		$WorkPicture->insert([
			'picture_name' => '37.jpg', 
			'sequence' => '3', 
			'work_id' => '9'
			]);
		$WorkPicture->insert([
			'picture_name' => '38.jpg', 
			'sequence' => '4', 
			'work_id' => '9'
			]);
		$WorkPicture->insert([
			'picture_name' => '39.jpg', 
			'sequence' => '5', 
			'work_id' => '9'
			]);
		$WorkPicture->insert([
			'picture_name' => '40.jpg', 
			'sequence' => '1', 
			'work_id' => '9'
			]);
		$WorkPicture->insert([
			'picture_name' => '41.jpg', 
			'sequence' => '4', 
			'work_id' => '10'
			]);
		$WorkPicture->insert([
			'picture_name' => '42.jpg', 
			'sequence' => '5', 
			'work_id' => '10'
			]);
		$WorkPicture->insert([
			'picture_name' => '43.jpg', 
			'sequence' => '3', 
			'work_id' => '10'
			]);
		$WorkPicture->insert([
			'picture_name' => '44.jpg', 
			'sequence' => '6', 
			'work_id' => '10'
			]);
		$WorkPicture->insert([
			'picture_name' => '45.jpg', 
			'sequence' => '2', 
			'work_id' => '10'
			]);
		$WorkPicture->insert([
			'picture_name' => '46.jpg', 
			'sequence' => '1', 
			'work_id' => '10'
			]);
		$WorkPicture->insert([
			'picture_name' => '47.jpg', 
			'sequence' => '5', 
			'work_id' => '11'
			]);
		$WorkPicture->insert([
			'picture_name' => '48.jpg', 
			'sequence' => '3', 
			'work_id' => '11'
			]);
		$WorkPicture->insert([
			'picture_name' => '49.jpg', 
			'sequence' => '1', 
			'work_id' => '11'
			]);
		$WorkPicture->insert([
			'picture_name' => '50.jpg', 
			'sequence' => '2', 
			'work_id' => '11'
			]);
		$WorkPicture->insert([
			'picture_name' => '51.jpg', 
			'sequence' => '4', 
			'work_id' => '11'
			]);
		$WorkPicture->insert([
			'picture_name' => '52.jpg', 
			'sequence' => '1', 
			'work_id' => '12'
			]);
		$WorkPicture->insert([
			'picture_name' => '53.jpg', 
			'sequence' => '3', 
			'work_id' => '12'
			]);
		$WorkPicture->insert([
			'picture_name' => '54.jpg', 
			'sequence' => '2', 
			'work_id' => '12'
			]);
		$WorkPicture->insert([
			'picture_name' => '55.jpg', 
			'sequence' => '3', 
			'work_id' => '13'
			]);
		$WorkPicture->insert([
			'picture_name' => '56.jpg', 
			'sequence' => '1', 
			'work_id' => '13'
			]);
		$WorkPicture->insert([
			'picture_name' => '57.jpg', 
			'sequence' => '4', 
			'work_id' => '13'
			]);
		$WorkPicture->insert([
			'picture_name' => '58.jpg', 
			'sequence' => '2', 
			'work_id' => '13'
			]);
		$WorkPicture->insert([
			'picture_name' => '59.jpg', 
			'sequence' => '2', 
			'work_id' => '14'
			]);
		$WorkPicture->insert([
			'picture_name' => '60.jpg', 
			'sequence' => '3', 
			'work_id' => '14'
			]);
		$WorkPicture->insert([
			'picture_name' => '61.jpg', 
			'sequence' => '4', 
			'work_id' => '14'
			]);
		$WorkPicture->insert([
			'picture_name' => '62.jpg', 
			'sequence' => '5', 
			'work_id' => '14'
			]);
		$WorkPicture->insert([
			'picture_name' => '63.jpg', 
			'sequence' => '1', 
			'work_id' => '14'
			]);
		$WorkPicture->insert([
			'picture_name' => '64.jpg', 
			'sequence' => '4', 
			'work_id' => '15'
			]);
		$WorkPicture->insert([
			'picture_name' => '65.jpg', 
			'sequence' => '5', 
			'work_id' => '15'
			]);
		$WorkPicture->insert([
			'picture_name' => '66.jpg', 
			'sequence' => '3', 
			'work_id' => '15'
			]);
		$WorkPicture->insert([
			'picture_name' => '67.jpg', 
			'sequence' => '6', 
			'work_id' => '15'
			]);
		$WorkPicture->insert([
			'picture_name' => '68.jpg', 
			'sequence' => '2', 
			'work_id' => '15'
			]);
		$WorkPicture->insert([
			'picture_name' => '69.jpg', 
			'sequence' => '1', 
			'work_id' => '15'
			]);
		$WorkPicture->insert([
			'picture_name' => '70.jpg', 
			'sequence' => '5', 
			'work_id' => '16'
			]);
		$WorkPicture->insert([
			'picture_name' => '71.jpg', 
			'sequence' => '3', 
			'work_id' => '16'
			]);
		$WorkPicture->insert([
			'picture_name' => '72.jpg', 
			'sequence' => '1', 
			'work_id' => '16'
			]);
		$WorkPicture->insert([
			'picture_name' => '73.jpg', 
			'sequence' => '2', 
			'work_id' => '16'
			]);
		$WorkPicture->insert([
			'picture_name' => '74.jpg', 
			'sequence' => '4', 
			'work_id' => '16'
			]);
		$WorkPicture->insert([
			'picture_name' => '75.jpg', 
			'sequence' => '1', 
			'work_id' => '17'
			]);
		$WorkPicture->insert([
			'picture_name' => '76.jpg', 
			'sequence' => '3', 
			'work_id' => '17'
			]);
		$WorkPicture->insert([
			'picture_name' => '77.jpg', 
			'sequence' => '2', 
			'work_id' => '17'
			]);
		$WorkPicture->insert([
			'picture_name' => '78.jpg', 
			'sequence' => '3', 
			'work_id' => '18'
			]);
		$WorkPicture->insert([
			'picture_name' => '79.jpg', 
			'sequence' => '1', 
			'work_id' => '18'
			]);
		$WorkPicture->insert([
			'picture_name' => '80.jpg', 
			'sequence' => '4', 
			'work_id' => '18'
			]);
		$WorkPicture->insert([
			'picture_name' => '81.jpg', 
			'sequence' => '2', 
			'work_id' => '18'
			]);
		$WorkPicture->insert([
			'picture_name' => '82.jpg', 
			'sequence' => '2', 
			'work_id' => '19'
			]);
		$WorkPicture->insert([
			'picture_name' => '83.jpg', 
			'sequence' => '3', 
			'work_id' => '19'
			]);
		$WorkPicture->insert([
			'picture_name' => '84.jpg', 
			'sequence' => '4', 
			'work_id' => '19'
			]);
		$WorkPicture->insert([
			'picture_name' => '85.jpg', 
			'sequence' => '5', 
			'work_id' => '19'
			]);
		$WorkPicture->insert([
			'picture_name' => '86.jpg', 
			'sequence' => '1', 
			'work_id' => '19'
			]);
		$WorkPicture->insert([
			'picture_name' => '87.jpg', 
			'sequence' => '4', 
			'work_id' => '20'
			]);
		$WorkPicture->insert([
			'picture_name' => '88.jpg', 
			'sequence' => '5', 
			'work_id' => '20'
			]);
		$WorkPicture->insert([
			'picture_name' => '89.jpg', 
			'sequence' => '3', 
			'work_id' => '20'
			]);
		$WorkPicture->insert([
			'picture_name' => '90.jpg', 
			'sequence' => '6', 
			'work_id' => '20'
			]);
		$WorkPicture->insert([
			'picture_name' => '91.jpg', 
			'sequence' => '2', 
			'work_id' => '20'
			]);
		$WorkPicture->insert([
			'picture_name' => '92.jpg', 
			'sequence' => '1', 
			'work_id' => '20'
			]);
		$WorkPicture->insert([
			'picture_name' => '93.jpg', 
			'sequence' => '5', 
			'work_id' => '21'
			]);
		$WorkPicture->insert([
			'picture_name' => '94.jpg', 
			'sequence' => '3', 
			'work_id' => '21'
			]);
		$WorkPicture->insert([
			'picture_name' => '95.jpg', 
			'sequence' => '1', 
			'work_id' => '21'
			]);
		$WorkPicture->insert([
			'picture_name' => '96.jpg', 
			'sequence' => '2', 
			'work_id' => '21'
			]);
		$WorkPicture->insert([
			'picture_name' => '97.jpg', 
			'sequence' => '4', 
			'work_id' => '21'
			]);
		$WorkPicture->insert([
			'picture_name' => '98.jpg', 
			'sequence' => '1', 
			'work_id' => '22'
			]);
		$WorkPicture->insert([
			'picture_name' => '99.jpg', 
			'sequence' => '3', 
			'work_id' => '22'
			]);
		$WorkPicture->insert([
			'picture_name' => '100.jpg', 
			'sequence' => '2', 
			'work_id' => '22'
			]);
		$WorkPicture->insert([
			'picture_name' => '101.jpg', 
			'sequence' => '3', 
			'work_id' => '23'
			]);
		$WorkPicture->insert([
			'picture_name' => '102.jpg', 
			'sequence' => '1', 
			'work_id' => '23'
			]);
		$WorkPicture->insert([
			'picture_name' => '103.jpg', 
			'sequence' => '4', 
			'work_id' => '23'
			]);
		$WorkPicture->insert([
			'picture_name' => '104.jpg', 
			'sequence' => '2', 
			'work_id' => '23'
			]);
		$WorkPicture->insert([
			'picture_name' => '105.jpg', 
			'sequence' => '2', 
			'work_id' => '24'
			]);
		$WorkPicture->insert([
			'picture_name' => '106.jpg', 
			'sequence' => '3', 
			'work_id' => '24'
			]);
		$WorkPicture->insert([
			'picture_name' => '107.jpg', 
			'sequence' => '4', 
			'work_id' => '24'
			]);
		$WorkPicture->insert([
			'picture_name' => '108.jpg', 
			'sequence' => '5', 
			'work_id' => '24'
			]);
		$WorkPicture->insert([
			'picture_name' => '109.jpg', 
			'sequence' => '1', 
			'work_id' => '24'
			]);
		$WorkPicture->insert([
			'picture_name' => '110.jpg', 
			'sequence' => '4', 
			'work_id' => '25'
			]);
		$WorkPicture->insert([
			'picture_name' => '111.jpg', 
			'sequence' => '5', 
			'work_id' => '25'
			]);
		$WorkPicture->insert([
			'picture_name' => '112.jpg', 
			'sequence' => '3', 
			'work_id' => '25'
			]);
		$WorkPicture->insert([
			'picture_name' => '113.jpg', 
			'sequence' => '6', 
			'work_id' => '25'
			]);
		$WorkPicture->insert([
			'picture_name' => '114.jpg', 
			'sequence' => '2', 
			'work_id' => '25'
			]);
		$WorkPicture->insert([
			'picture_name' => '115.jpg', 
			'sequence' => '1', 
			'work_id' => '25'
			]);
		$WorkPicture->insert([
			'picture_name' => '116.jpg', 
			'sequence' => '5', 
			'work_id' => '26'
			]);
		$WorkPicture->insert([
			'picture_name' => '117.jpg', 
			'sequence' => '3', 
			'work_id' => '26'
			]);
		$WorkPicture->insert([
			'picture_name' => '118.jpg', 
			'sequence' => '1', 
			'work_id' => '26'
			]);
		$WorkPicture->insert([
			'picture_name' => '119.jpg', 
			'sequence' => '2', 
			'work_id' => '26'
			]);
		$WorkPicture->insert([
			'picture_name' => '120.jpg', 
			'sequence' => '4', 
			'work_id' => '26'
			]);
		$WorkPicture->insert([
			'picture_name' => '121.jpg', 
			'sequence' => '1', 
			'work_id' => '27'
			]);
		$WorkPicture->insert([
			'picture_name' => '122.jpg', 
			'sequence' => '3', 
			'work_id' => '27'
			]);
		$WorkPicture->insert([
			'picture_name' => '123.jpg', 
			'sequence' => '2', 
			'work_id' => '27'
			]);
		$WorkPicture->insert([
			'picture_name' => '124.jpg', 
			'sequence' => '3', 
			'work_id' => '28'
			]);
		$WorkPicture->insert([
			'picture_name' => '125.jpg', 
			'sequence' => '1', 
			'work_id' => '28'
			]);
		$WorkPicture->insert([
			'picture_name' => '126.jpg', 
			'sequence' => '4', 
			'work_id' => '28'
			]);
		$WorkPicture->insert([
			'picture_name' => '127.jpg', 
			'sequence' => '2', 
			'work_id' => '28'
			]);
		$WorkPicture->insert([
			'picture_name' => '128.jpg', 
			'sequence' => '2', 
			'work_id' => '29'
			]);
		$WorkPicture->insert([
			'picture_name' => '129.jpg', 
			'sequence' => '3', 
			'work_id' => '29'
			]);
		$WorkPicture->insert([
			'picture_name' => '130.jpg', 
			'sequence' => '4', 
			'work_id' => '29'
			]);
		$WorkPicture->insert([
			'picture_name' => '131.jpg', 
			'sequence' => '5', 
			'work_id' => '29'
			]);
		$WorkPicture->insert([
			'picture_name' => '132.jpg', 
			'sequence' => '1', 
			'work_id' => '29'
			]);
		$WorkPicture->insert([
			'picture_name' => '133.jpg', 
			'sequence' => '4', 
			'work_id' => '30'
			]);
		$WorkPicture->insert([
			'picture_name' => '134.jpg', 
			'sequence' => '5', 
			'work_id' => '30'
			]);
		$WorkPicture->insert([
			'picture_name' => '135.jpg', 
			'sequence' => '3', 
			'work_id' => '30'
			]);
		$WorkPicture->insert([
			'picture_name' => '136.jpg', 
			'sequence' => '6', 
			'work_id' => '30'
			]);
		$WorkPicture->insert([
			'picture_name' => '137.jpg', 
			'sequence' => '2', 
			'work_id' => '30'
			]);
		$WorkPicture->insert([
			'picture_name' => '138.jpg', 
			'sequence' => '1', 
			'work_id' => '30'
			]);
		$WorkPicture->insert([
			'picture_name' => '139.jpg', 
			'sequence' => '5', 
			'work_id' => '31'
			]);
		$WorkPicture->insert([
			'picture_name' => '140.jpg', 
			'sequence' => '3', 
			'work_id' => '31'
			]);
		$WorkPicture->insert([
			'picture_name' => '141.jpg', 
			'sequence' => '1', 
			'work_id' => '31'
			]);
		$WorkPicture->insert([
			'picture_name' => '142.jpg', 
			'sequence' => '2', 
			'work_id' => '31'
			]);
		$WorkPicture->insert([
			'picture_name' => '143.jpg', 
			'sequence' => '4', 
			'work_id' => '31'
			]);
		$WorkPicture->insert([
			'picture_name' => '144.jpg', 
			'sequence' => '1', 
			'work_id' => '32'
			]);
		$WorkPicture->insert([
			'picture_name' => '145.jpg', 
			'sequence' => '3', 
			'work_id' => '32'
			]);
		$WorkPicture->insert([
			'picture_name' => '146.jpg', 
			'sequence' => '2', 
			'work_id' => '32'
			]);
		$WorkPicture->insert([
			'picture_name' => '147.jpg', 
			'sequence' => '3', 
			'work_id' => '33'
			]);
		$WorkPicture->insert([
			'picture_name' => '148.jpg', 
			'sequence' => '1', 
			'work_id' => '33'
			]);
		$WorkPicture->insert([
			'picture_name' => '149.jpg', 
			'sequence' => '4', 
			'work_id' => '33'
			]);
		$WorkPicture->insert([
			'picture_name' => '150.jpg', 
			'sequence' => '2', 
			'work_id' => '33'
			]);
		$WorkPicture->insert([
			'picture_name' => '151.jpg', 
			'sequence' => '2', 
			'work_id' => '34'
			]);
		$WorkPicture->insert([
			'picture_name' => '152.jpg', 
			'sequence' => '3', 
			'work_id' => '34'
			]);
		$WorkPicture->insert([
			'picture_name' => '153.jpg', 
			'sequence' => '4', 
			'work_id' => '34'
			]);
		$WorkPicture->insert([
			'picture_name' => '154.jpg', 
			'sequence' => '5', 
			'work_id' => '34'
			]);
		$WorkPicture->insert([
			'picture_name' => '155.jpg', 
			'sequence' => '1', 
			'work_id' => '34'
			]);
		$WorkPicture->insert([
			'picture_name' => '156.jpg', 
			'sequence' => '4', 
			'work_id' => '35'
			]);
		$WorkPicture->insert([
			'picture_name' => '157.jpg', 
			'sequence' => '5', 
			'work_id' => '35'
			]);
		$WorkPicture->insert([
			'picture_name' => '158.jpg', 
			'sequence' => '3', 
			'work_id' => '35'
			]);
		$WorkPicture->insert([
			'picture_name' => '159.jpg', 
			'sequence' => '6', 
			'work_id' => '35'
			]);
		$WorkPicture->insert([
			'picture_name' => '160.jpg', 
			'sequence' => '2', 
			'work_id' => '35'
			]);
		$WorkPicture->insert([
			'picture_name' => '161.jpg', 
			'sequence' => '1', 
			'work_id' => '35'
			]);
		$WorkPicture->insert([
			'picture_name' => '162.jpg', 
			'sequence' => '5', 
			'work_id' => '36'
			]);
		$WorkPicture->insert([
			'picture_name' => '163.jpg', 
			'sequence' => '3', 
			'work_id' => '36'
			]);
		$WorkPicture->insert([
			'picture_name' => '164.jpg', 
			'sequence' => '1', 
			'work_id' => '36'
			]);
		$WorkPicture->insert([
			'picture_name' => '165.jpg', 
			'sequence' => '2', 
			'work_id' => '36'
			]);
		$WorkPicture->insert([
			'picture_name' => '166.jpg', 
			'sequence' => '4', 
			'work_id' => '36'
			]);
		$WorkPicture->insert([
			'picture_name' => '167.jpg', 
			'sequence' => '1', 
			'work_id' => '37'
			]);
		$WorkPicture->insert([
			'picture_name' => '168.jpg', 
			'sequence' => '3', 
			'work_id' => '37'
			]);
		$WorkPicture->insert([
			'picture_name' => '169.jpg', 
			'sequence' => '2', 
			'work_id' => '37'
			]);
		$WorkPicture->insert([
			'picture_name' => '170.jpg', 
			'sequence' => '3', 
			'work_id' => '38'
			]);
		$WorkPicture->insert([
			'picture_name' => '171.jpg', 
			'sequence' => '1', 
			'work_id' => '38'
			]);
		$WorkPicture->insert([
			'picture_name' => '172.jpg', 
			'sequence' => '4', 
			'work_id' => '38'
			]);
		$WorkPicture->insert([
			'picture_name' => '173.jpg', 
			'sequence' => '2', 
			'work_id' => '38'
			]);
		$WorkPicture->insert([
			'picture_name' => '174.jpg', 
			'sequence' => '2', 
			'work_id' => '39'
			]);
		$WorkPicture->insert([
			'picture_name' => '175.jpg', 
			'sequence' => '3', 
			'work_id' => '39'
			]);
		$WorkPicture->insert([
			'picture_name' => '176.jpg', 
			'sequence' => '4', 
			'work_id' => '39'
			]);
		$WorkPicture->insert([
			'picture_name' => '177.jpg', 
			'sequence' => '5', 
			'work_id' => '39'
			]);
		$WorkPicture->insert([
			'picture_name' => '178.jpg', 
			'sequence' => '1', 
			'work_id' => '39'
			]);
		$WorkPicture->insert([
			'picture_name' => '179.jpg', 
			'sequence' => '4', 
			'work_id' => '40'
			]);
		$WorkPicture->insert([
			'picture_name' => '180.jpg', 
			'sequence' => '5', 
			'work_id' => '40'
			]);
		$WorkPicture->insert([
			'picture_name' => '181.jpg', 
			'sequence' => '3', 
			'work_id' => '40'
			]);
		$WorkPicture->insert([
			'picture_name' => '182.jpg', 
			'sequence' => '6', 
			'work_id' => '40'
			]);
		$WorkPicture->insert([
			'picture_name' => '183.jpg', 
			'sequence' => '2', 
			'work_id' => '40'
			]);
		$WorkPicture->insert([
			'picture_name' => '184.jpg', 
			'sequence' => '1', 
			'work_id' => '40'
			]);
		$WorkPicture->insert([
			'picture_name' => '185.jpg', 
			'sequence' => '5', 
			'work_id' => '41'
			]);
		$WorkPicture->insert([
			'picture_name' => '186.jpg', 
			'sequence' => '3', 
			'work_id' => '41'
			]);
		$WorkPicture->insert([
			'picture_name' => '187.jpg', 
			'sequence' => '1', 
			'work_id' => '41'
			]);
		$WorkPicture->insert([
			'picture_name' => '188.jpg', 
			'sequence' => '2', 
			'work_id' => '41'
			]);
		$WorkPicture->insert([
			'picture_name' => '189.jpg', 
			'sequence' => '4', 
			'work_id' => '41'
			]);
		$WorkPicture->insert([
			'picture_name' => '190.jpg', 
			'sequence' => '1', 
			'work_id' => '42'
			]);
		$WorkPicture->insert([
			'picture_name' => '191.jpg', 
			'sequence' => '3', 
			'work_id' => '42'
			]);
		$WorkPicture->insert([
			'picture_name' => '192.jpg', 
			'sequence' => '2', 
			'work_id' => '42'
			]);
		$WorkPicture->insert([
			'picture_name' => '193.jpg', 
			'sequence' => '3', 
			'work_id' => '43'
			]);
		$WorkPicture->insert([
			'picture_name' => '194.jpg', 
			'sequence' => '1', 
			'work_id' => '43'
			]);
		$WorkPicture->insert([
			'picture_name' => '195.jpg', 
			'sequence' => '4', 
			'work_id' => '43'
			]);
		$WorkPicture->insert([
			'picture_name' => '196.jpg', 
			'sequence' => '2', 
			'work_id' => '43'
			]);
		$WorkPicture->insert([
			'picture_name' => '197.jpg', 
			'sequence' => '2', 
			'work_id' => '44'
			]);
		$WorkPicture->insert([
			'picture_name' => '198.jpg', 
			'sequence' => '3', 
			'work_id' => '44'
			]);
		$WorkPicture->insert([
			'picture_name' => '199.jpg', 
			'sequence' => '4', 
			'work_id' => '44'
			]);
		$WorkPicture->insert([
			'picture_name' => '200.jpg', 
			'sequence' => '5', 
			'work_id' => '44'
			]);
		$WorkPicture->insert([
			'picture_name' => '201.jpg', 
			'sequence' => '1', 
			'work_id' => '44'
			]);
		$WorkPicture->insert([
			'picture_name' => '202.jpg', 
			'sequence' => '4', 
			'work_id' => '45'
			]);
		$WorkPicture->insert([
			'picture_name' => '203.jpg', 
			'sequence' => '5', 
			'work_id' => '45'
			]);
		$WorkPicture->insert([
			'picture_name' => '204.jpg', 
			'sequence' => '3', 
			'work_id' => '45'
			]);
		$WorkPicture->insert([
			'picture_name' => '205.jpg', 
			'sequence' => '6', 
			'work_id' => '45'
			]);
		$WorkPicture->insert([
			'picture_name' => '206.jpg', 
			'sequence' => '2', 
			'work_id' => '45'
			]);
		$WorkPicture->insert([
			'picture_name' => '207.jpg', 
			'sequence' => '1', 
			'work_id' => '45'
			]);
		$WorkPicture->insert([
			'picture_name' => '208.jpg', 
			'sequence' => '5', 
			'work_id' => '46'
			]);
		$WorkPicture->insert([
			'picture_name' => '209.jpg', 
			'sequence' => '3', 
			'work_id' => '46'
			]);
		$WorkPicture->insert([
			'picture_name' => '210.jpg', 
			'sequence' => '1', 
			'work_id' => '46'
			]);
		$WorkPicture->insert([
			'picture_name' => '211.jpg', 
			'sequence' => '2', 
			'work_id' => '46'
			]);
		$WorkPicture->insert([
			'picture_name' => '212.jpg', 
			'sequence' => '4', 
			'work_id' => '46'
			]);
		$WorkPicture->insert([
			'picture_name' => '213.jpg', 
			'sequence' => '1', 
			'work_id' => '47'
			]);
		$WorkPicture->insert([
			'picture_name' => '214.jpg', 
			'sequence' => '3', 
			'work_id' => '47'
			]);
		$WorkPicture->insert([
			'picture_name' => '215.jpg', 
			'sequence' => '2', 
			'work_id' => '47'
			]);
		$WorkPicture->insert([
			'picture_name' => '216.jpg', 
			'sequence' => '3', 
			'work_id' => '48'
			]);
		$WorkPicture->insert([
			'picture_name' => '217.jpg', 
			'sequence' => '1', 
			'work_id' => '48'
			]);
		$WorkPicture->insert([
			'picture_name' => '218.jpg', 
			'sequence' => '4', 
			'work_id' => '48'
			]);
		$WorkPicture->insert([
			'picture_name' => '219.jpg', 
			'sequence' => '2', 
			'work_id' => '48'
			]);
		$WorkPicture->insert([
			'picture_name' => '220.jpg', 
			'sequence' => '2', 
			'work_id' => '49'
			]);
		$WorkPicture->insert([
			'picture_name' => '221.jpg', 
			'sequence' => '3', 
			'work_id' => '49'
			]);
		$WorkPicture->insert([
			'picture_name' => '222.jpg', 
			'sequence' => '4', 
			'work_id' => '49'
			]);
		$WorkPicture->insert([
			'picture_name' => '223.jpg', 
			'sequence' => '5', 
			'work_id' => '49'
			]);
		$WorkPicture->insert([
			'picture_name' => '224.jpg', 
			'sequence' => '1', 
			'work_id' => '49'
			]);
		$WorkPicture->insert([
			'picture_name' => '225.jpg', 
			'sequence' => '4', 
			'work_id' => '50'
			]);
		$WorkPicture->insert([
			'picture_name' => '226.jpg', 
			'sequence' => '5', 
			'work_id' => '50'
			]);
		$WorkPicture->insert([
			'picture_name' => '227.jpg', 
			'sequence' => '3', 
			'work_id' => '50'
			]);
		$WorkPicture->insert([
			'picture_name' => '228.jpg', 
			'sequence' => '6', 
			'work_id' => '50'
			]);
		$WorkPicture->insert([
			'picture_name' => '229.jpg', 
			'sequence' => '2', 
			'work_id' => '50'
			]);
		$WorkPicture->insert([
			'picture_name' => '230.jpg', 
			'sequence' => '1', 
			'work_id' => '50'
			]);

		
	}
}
