<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use Carbon\Carbon;

class UserTableSeeder extends Seeder {

	public function run()
	{
		$user = new User();
		$user->insert([
			'name' => 'Febra S',
			'email' => 'm.febras@yahoo.com',
			'employ' => 'Front-End Developer',
			'password' => Hash::make('muslim'),
			'level' => '1',
			'picture' => '',
			'about' => '',
			'active' => '1'
		]);
		$user->insert([
			'name' => 'Faqih Arifian Ajipradana',
			'email' => 'faqiharifianaji@gmail.com',
			'employ' => 'Back-End Developer',
			'password' => Hash::make('fb100210'),
			'level' => '1',
			'active' => '1'
		]);
	}
}