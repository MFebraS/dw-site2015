<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\NewsPicture;
use Carbon\Carbon;

class NewsPictureTableSeeder extends Seeder {

	public function run()
	{
		for ($i=1; $i<=30; $i++) {
			
			// $news_id = $i+10;

			$picture = new NewsPicture;
			$picture->insert([
			'name' => 'Gambar'.$i.'.jpg',
			'file_name' => $i.'.jpg',
			'format' => 'jpg',
			'news_id' => $i,
			'status' => 'Feature',
			'created_at' => Carbon::now()->subMonth()->addDays($i),
			'updated_at' => Carbon::now()->subMonth()->addDays($i)
			]);
		}
	}
}