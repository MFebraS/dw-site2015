<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\News;

class NewsTableSeeder extends Seeder {

	public function run()
	{
		for ($i=1; $i<=30; $i++)
		{
			if ($i % 4 == 2)
			{
				$cat_id = 2;
			} else if ($i % 4 == 3)
			{
				$cat_id = 3;
			} else if ($i % 4 == 0)
			{
				$cat_id = 4;
			} /*else if ($i % 5 == 0)
			{
				$cat_id = 5;
			}*/ else
			{
				$cat_id = 1;
			}

			if ($i < 10)
			{
				$date = '2015-02-'.$i;
			} else
			{
				$date = '2015-03-'.$i;
			}
			$news = new News;
			$news->insert([
			'author_id' => 2,
			'title' => 'Title '.$i,
			'permalink' => 'title-'.$i,
			'category_id' => $cat_id,
			'content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
			'html_content' => '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&lt;/p&gt;
',
			'status' => 'Published',
			'published_date' => $date,
			'created_at' => $date,
			'updated_at' => $date
			]);
		}

	}

}