<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->call('UserTableSeeder');

		//$this->call('CategoryTableSeeder');

		//$this->call('ClientTableSeeder');

		/*$this->call('SliderTableSeeder');

		$this->call('VacancyTableSeeder');*/





		$this->call('NewsTableSeeder');

		$this->call('NewsPictureTableSeeder');

		//$this->call('EmployeeTableSeeder');

		$this->call('WorkTableSeeder');

		$this->call('WorkPictureTableSeeder');

		$this->call('TagTableSeeder');

		$this->call('WorkTagTableSeeder');

	}

}

