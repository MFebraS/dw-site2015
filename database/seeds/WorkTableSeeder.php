<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Work;
use Carbon\Carbon;
use App\Models\Client;

class WorkTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$work = new Work;
		$work->insert([
			'project_title' => 'Project 1', 
			'client_id' => '1', 
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'published',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 1'),
			'order' => '1',
			]);
		$work->insert([
			'project_title' => 'Project 2', 
			'client_id' => '2', 
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'draft',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 2'),
			'order' => '2',
			]);
		$work->insert([
			'project_title' => 'Project 3', 
			'client_id' => '3', 
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'draft',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 3'),
			'order' => '3',
			]);
		$work->insert([
			'project_title' => 'Project 4', 
			'client_id' => '4', 
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'published',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 4'),
			'order' => '4',
			]);
		$work->insert([
			'project_title' => 'Project 5', 
			'client_id' => '5', 
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'published',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 5'),
			'order' => '5',
			]);
		$work->insert([
			'project_title' => 'Project 6', 
			'client_id' => '1', 
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'published',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 6'),
			'order' => '6',
			]);
		$work->insert([
			'project_title' => 'Project 7', 
			'client_id' => '2', 
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'draft',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 7'),
			'order' => '7',
			]);
		$work->insert([
			'project_title' => 'Project 8', 
			'client_id' => '3', 
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'draft',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 8'),
			'order' => '8',
			]);
		$work->insert([
			'project_title' => 'Project 9', 
			'client_id' => '4', 
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'published',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 9'),
			'order' => '9',
			]);
		$work->insert([
			'project_title' => 'Project 10',
			'client_id' => '5',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'published',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 10'),
			'order' => '10',
			]);
		$work->insert([
			'project_title' => 'Project 11',
			'client_id' => '1',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'published',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 11'),
			'order' => '11',
			]);
		$work->insert([
			'project_title' => 'Project 12',
			'client_id' => '2',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'draft',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 12'),
			'order' => '12',
			]);
		$work->insert([
			'project_title' => 'Project 13',
			'client_id' => '3',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'draft',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 13'),
			'order' => '13',
			]);
		$work->insert([
			'project_title' => 'Project 14',
			'client_id' => '4',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'published',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 14'),
			'order' => '14',
			]);
		$work->insert([
			'project_title' => 'Project 15',
			'client_id' => '5',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'published',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 15'),
			'order' => '15',
			]);
		$work->insert([
			'project_title' => 'Project 16',
			'client_id' => '1',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'published',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 16'),
			'order' => '16',
			]);
		$work->insert([
			'project_title' => 'Project 17',
			'client_id' => '2',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'draft',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 17'),
			'order' => '17',
			]);
		$work->insert([
			'project_title' => 'Project 18',
			'client_id' => '3',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'draft',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 18'),
			'order' => '18',
			]);
		$work->insert([
			'project_title' => 'Project 19',
			'client_id' => '4',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'published',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 19'),
			'order' => '19',
			]);
		$work->insert([
			'project_title' => 'Project 20',
			'client_id' => '5',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'published',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 20'),
			'order' => '20',
			]);
		$work->insert([
			'project_title' => 'Project 21',
			'client_id' => '1',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'published',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 21'),
			'order' => '21',
			]);
		$work->insert([
			'project_title' => 'Project 22',
			'client_id' => '2',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'draft',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 22'),
			'order' => '22',
			]);
		$work->insert([
			'project_title' => 'Project 23',
			'client_id' => '3',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'draft',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 23'),
			'order' => '23',
			]);
		$work->insert([
			'project_title' => 'Project 24',
			'client_id' => '4',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'published',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 24'),
			'order' => '24',
			]);
		$work->insert([
			'project_title' => 'Project 25',
			'client_id' => '5',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'published',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 25'),
			'order' => '25',
			]);
		$work->insert([
			'project_title' => 'Project 26',
			'client_id' => '1',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'published',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 26'),
			'order' => '26',
			]);
		$work->insert([
			'project_title' => 'Project 27',
			'client_id' => '2',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'draft',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 27'),
			'order' => '27',
			]);
		$work->insert([
			'project_title' => 'Project 28',
			'client_id' => '3',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'draft',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 28'),
			'order' => '28',
			]);
		$work->insert([
			'project_title' => 'Project 29',
			'client_id' => '4',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'published',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 29'),
			'order' => '29',
			]);
		$work->insert([
			'project_title' => 'Project 30',
			'client_id' => '5',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'published',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 30'),
			'order' => '30',
			]);
		$work->insert([
			'project_title' => 'Project 31',
			'client_id' => '1',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'published',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 31'),
			'order' => '31',
			]);
		$work->insert([
			'project_title' => 'Project 32',
			'client_id' => '2',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'draft',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 32'),
			'order' => '32',
			]);
		$work->insert([
			'project_title' => 'Project 33',
			'client_id' => '3',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'draft',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 33'),
			'order' => '33',
			]);
		$work->insert([
			'project_title' => 'Project 34',
			'client_id' => '4',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'published',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 34'),
			'order' => '34',
			]);
		$work->insert([
			'project_title' => 'Project 35',
			'client_id' => '5',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'published',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 35'),
			'order' => '35',
			]);
		$work->insert([
			'project_title' => 'Project 36',
			'client_id' => '1',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'published',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 36'),
			'order' => '36',
			]);
		$work->insert([
			'project_title' => 'Project 37',
			'client_id' => '2',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'draft',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 37'),
			'order' => '37',
			]);
		$work->insert([
			'project_title' => 'Project 38',
			'client_id' => '3',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'draft',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 38'),
			'order' => '38',
			]);
		$work->insert([
			'project_title' => 'Project 39',
			'client_id' => '4',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'published',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 39'),
			'order' => '39',
			]);
		$work->insert([
			'project_title' => 'Project 40',
			'client_id' => '5',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'published',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 40'),
			'order' => '40',
			]);
		$work->insert([
			'project_title' => 'Project 41',
			'client_id' => '1',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'published',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 41'),
			'order' => '41',
			]);
		$work->insert([
			'project_title' => 'Project 42',
			'client_id' => '2',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'draft',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 42'),
			'order' => '42',
			]);
		$work->insert([
			'project_title' => 'Project 43',
			'client_id' => '3',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'draft',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 43'),
			'order' => '43',
			]);
		$work->insert([
			'project_title' => 'Project 44',
			'client_id' => '4',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'published',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 44'),
			'order' => '44',
			]);
		$work->insert([
			'project_title' => 'Project 45',
			'client_id' => '5',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'published',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 45'),
			'order' => '45',
			]);
		$work->insert([
			'project_title' => 'Project 46',
			'client_id' => '1',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'published',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 46'),
			'order' => '46',
			]);
		$work->insert([
			'project_title' => 'Project 47',
			'client_id' => '2',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'draft',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 47'),
			'order' => '47',
			]);
		$work->insert([
			'project_title' => 'Project 48',
			'client_id' => '3',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'draft',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 48'),
			'order' => '48',
			]);
		$work->insert([
			'project_title' => 'Project 49',
			'client_id' => '4',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'published',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 49'),
			'order' => '49',
			]);
		$work->insert([
			'project_title' => 'Project 50',
			'client_id' => '5',
			'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu consectetur ipsum, at mollis tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent lectus nisi, facilisis non elit a, pharetra maximus arcu. Pellentesque tristique nulla vitae mauris viverra, at pulvinar dui efficitur. Etiam luctus mi orci, a consectetur sapien lobortis id.',
			'status' => 'published',
			'created_at' => Carbon::today(),
			'updated_at' => Carbon::today(),
			'slug' => str_slug('Project 50'),
			'order' => '50',
			]);

		$client = Client::whereId('1')->first();
		$client->count = 5;
		$client->save();

		$client = Client::whereId('2')->first();
		$client->count = 5;
		$client->save();

		$client = Client::whereId('3')->first();
		$client->count = 5;
		$client->save();

		$client = Client::whereId('4')->first();
		$client->count = 5;
		$client->save();

		$client = Client::whereId('5')->first();
		$client->count = 5;
		$client->save();
	}
}
