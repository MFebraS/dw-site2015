<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Tag;

class TagTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$tag = new Tag;
		$tag->insert([
			'tag_name' => 'web', 
			'counter' => '40'
			]);
		$tag->insert([
			'tag_name' => 'mobile', 
			'counter' => '20'
			]);
		$tag->insert([
			'tag_name' => 'ilustration', 
			'counter' => '30'
			]);
		$tag->insert([
			'tag_name' => 'photography', 
			'counter' => '20'
			]);

		
	}
}
