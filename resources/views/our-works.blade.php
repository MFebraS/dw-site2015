@extends('layout')

@section('content_title')
<title>Durenworks - Our Works</title>
@stop

@section('content')
<div class="title-page">
            <div class="container">
                <h1>Our Works</h1>
            </div>
        </div>

        <div class="row text-center sub-title">
            <div class="container">
                <h3>Successfully Completed Projects</h3>
                <nav>
                    <ul class="pager sub-nav">
                        <li><a {{ $active[1] == '' ? "class=active" : "" }} href="{{ action('WorksController@index') }}">All</a></li>
                        <li><a {{ $active[1] == 'web' ? "class=active" : "" }} href="{{ action('WorksController@index', ['tag' => 'web']) }}">Web Design &amp; Dev</a></li>
                        <li><a {{ $active[1] == 'business' ? "class=active" : "" }} href="{{ action('WorksController@index', ['tag' => 'business']) }}">Business Application</a></li>
                        <li><a {{ $active[1] == 'marketing' ? "class=active" : "" }} href="{{ action('WorksController@index', ['tag' => 'marketing']) }}">Digital Marketing</a></li>
                        <li><a {{ $active[1] == 'mobile' ? "class=active" : "" }} href="{{ action('WorksController@index', ['tag' => 'mobile']) }}">Mobile Application</a></li>
                    </ul>
                </nav>
            </div>
        </div>
        

        <div class="collection">
            <div class="loading-container">
                <img class="loading" src="/assets/images/black-loader.gif" alt="">
            </div>
            <div class="row row-collection">
                <div id="collection-container" class="container">
                @if($our_works[0] == "")
                    <h5 class="text-center">No Record Found</h5>
                @else
                    @foreach($our_works as $i => $work)
                        <div class="item-collection col-xs-12 col-sm-4" data-id="{{ $work['id'] }}" data-slug="{{ $work['slug'] }}" data-path="{{ $work['picture_path'] }}" data-title="{{ $work['project_title'] }}" data-description="{{ $work['description'] }}" data-client="{{ $work['client_name'] }}" data-category="{{ $work['tags'] }}" data-link="{{ $work ['client_website'] }}">
                            <img src="/{{ Config::get('custom_path.works') }}/{{ $work['picture'] }}" alt="{{ $work['project_title'] }}'s image">
                            <div class="caption-place hidden-sm hidden-md hidden-lg">
                                <div class="caption-arrow"></div>
                                <div class="caption">
                                    <h5>{{ str_limit($work['project_title'], 40) }}</h5>
                                    <p>{{ $work['tags'] }}</p>
                                </div>
                            </div>
                        </div>
                        @if ((($i + 1) == count($our_works)) || ((($i + 1) % 3) == 0))
                            <div class="viewer col-sm-12"></div>
                        @endif
                    @endforeach
                @endif
                </div>
            </div>
            
            @if($currentPage < $lastPage)
                <div class="row see-all">
                    <p class="text-center" style="color:white;"><a href="#">LOAD MORE WORKS</a></p>
                </div>
            @endif
        </div>

        <div class="purchase-now">
            <div class="container">
                <div class="row">
                    <div class="col-sm-9">
                        <h4><b>Durenworks</b> is the Perfect Solution for Your Business</h4>
                        <p>Let’s get started with us</p>
                    </div>
                    <div class="col-sm-3">
                        <div class="button-danger-place">
                            <a href="{{ action('ContactUsController@index') }}"><button type="button" class="btn btn-danger">GET STARTED</button></a>
                        </div>
                    </div>
                </div>
                
                
            </div>
        </div>
        

        <div id="view-work" class="view-work collapse hidden-xs" section="view-work">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="loading-container">
                                <img class="loading" src="/assets/images/white-loader.gif">
                            </div>
                            <div class="view-work-slider">
                                <div id="view-work-slider" class="carousel slide" data-ride="carousel">
                                    <!-- Indicators -->
                                    <ol id="slider-indicator" class="carousel-indicators">
                                        
                                    </ol>

                                    <!-- Wrapper for slides -->
                                    <div id="slider-inner" class="carousel-inner" role="listbox">
                                        
                                    </div>
                                    <!-- Controls -->
                                    <a class="left carousel-control" href="#view-work-slider" role="button" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="right carousel-control" href="#view-work-slider" role="button" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                    </a>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 description-work">
                            <h4 id="view-work-title"></h4>
                            <p>Description</p>
                            <p id="view-work-description"></p>
                            <div class="desc-bottom">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <p>Client:</p>
                                    </div>
                                    <div class="col-sm-8">
                                        <p id="view-work-client"></p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <p>Categories:</p>
                                    </div>
                                    <div class="col-sm-8">
                                        <p id="view-work-category"></p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <p>Link:</p>
                                    </div>
                                    <div class="col-sm-8">
                                        <p id="view-work-link"></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection
@section('content_js')
<script type="text/javascript">
    var currentPage = 1;
    var tag_name = 'all';
    $(function() {

        if(window.location.hash != ""){
            $('.item-collection[data-slug='+window.location.hash.split('#')[1]+']').click();
        }

        $(window).resize(function(){
            if($(this).width() < 751){
                $('#view-work').each(function(){
                    $('.arrow-up.gallery').remove();
                    $(this).collapse('hide');
                    $('#showing-row').removeAttr('id');
                    $('#showed').removeAttr('id');
                });
            }
        });
        
        
        $.fn.ajax_filter = function(filter){
            if(!($(this).hasClass('active'))){
                currentPage = 1;
                $('.sub-title a').each(function(){
                    $(this).removeClass('active');
                })
                $(this).addClass('active');
                $('.collection .loading-container').show();
                $('.see-all').addClass('hide');

                tag_name = filter;
                var view_work_clone = $('#view-work').clone();
                view_work_clone.attr('class','view-work hidden-xs collapse');
                $('#view-work').remove();
                $('#collection-container').empty();
                $('#collection-container').append(view_work_clone);
                $.get("/ajax-filter?tag_name=" +tag_name, function(data){
                    $('.collection .loading-container').hide();
                    //success data
                    if(data.total == 0){
                        $('#collection-container').append('<h5 class="text-center">No Record Found</h5>');
                    }else{
                        $.each(data.works, function(i, work){

                                $('#collection-container').append('<div class="item-collection col-xs-6 col-sm-4 '+(i % 2 == 0 ? "odd" : "")+'" data-id="'+work.id+'" data-path="'+work.picture_path+'" data-title="'+work.project_title+'" data-description="'+work.description+'" data-client="'+work.client_name+'" data-category="'+work.tags+'" data-link="'+work.client_website+'" onclick="$(this).show_function()"><img src="/'+work.picture_path+'/'+work.picture+'"></div>');
                           
                                
                            if (((i + 1) == data.works.length) || (((i + 1) % 3) == 0)){
                                $('#collection-container').append('<div class="viewer col-sm-12"></div>');
                            }
                        });
                    }
                    if(data.currentPage < data.lastPage){
                        $('.see-all').removeClass('hide');

                    }
                });
            }
        };
        
    });
    
    $(document).on('click', '.see-all a', function(e){
        e.preventDefault();
        var currentElmt = $(this).parent().parent();
        var insertPlace = currentElmt.prev().find('#collection-container');
        var loadingMore = '<div class="loading-more"><img class="loading" src="/assets/images/black-loader.gif" alt=""></div>';
        currentElmt.addClass('hide');
        currentElmt.before(loadingMore).show();

        var url = '/our-works/load-more';
        if(window.location.search == ''){
            url += '?page='+(++currentPage);
        }else{
            url += window.location.search+'&page='+(++currentPage);
        }
        $.get(url, function(data){

            $.each(data.works, function(i, work){
                insertPlace.append('<div class="item-collection col-xs-6 col-sm-4 '+((i + currentPage) % 2 != 0 ? "odd" : "")+'" data-id="'+work.id+'" data-path="'+work.picture_path+'" data-title="'+work.project_title+'" data-description="'+work.description+'" data-client="'+work.client_name+'" data-category="'+work.tags+'" data-link="'+work.client_website+'" onclick="$(this).show_function()"><img src="/assets/uploads/works/'+work.picture+'" alt="'+work.project_title+'\'s image"></div>');

                if ((i + 1) == data.works.length || (((i + 1) % 3) == 0)){
                    insertPlace.append('<div class="viewer col-sm-12"></div>');
                }
            });
            $('.loading-more').remove();

            if(data.currentPage < data.lastPage){
                currentElmt.removeClass('hide');
            }

        });
    });

    $('.collection').on('click', '.item-collection', function(){
        window.location.hash = $(this).attr('data-slug');
        if($(window).width() < 751){
        }else if($(window).width() >= 751){
            var collection = $('.item-collection');
            var this_index = collection.index($(this));
            var row_index = Math.floor(this_index/3);
            var viewer = $('.viewer');
            var showing = $('#showing-row');
            var showed = $('#showed');
            var view_work = $("#view-work");
            view_work.find('.col-sm-4').removeClass('fade-out');
            view_work.find('.col-sm-8').removeClass('fade-out');
            var cloning = $("#view-work").clone();
            var title = $(this).attr('data-title');
            var description = $(this).attr('data-description');
            var client = $(this).attr('data-client');
            var category = $(this).attr('data-category');
            var link = $(this).attr('data-link');
            var arrow = '<div class="arrow-up gallery hidden-xs"></div>';
            var arrow_class = $('.arrow-up.gallery');
            var collection = $('.collection .row-collection .col-sm-4');
            var this_index = collection.index($(this));
            var showed_index = collection.index($('#showed'));
            if ((viewer.eq(row_index).attr('id') == 'showing-row') && ($(this).attr('id') == 'showed')){
                arrow_class.remove();
                view_work.collapse('hide');
                view_work.find('.col-sm-4').addClass('fade-out');
                view_work.find('.col-sm-8').addClass('fade-out');
                viewer.eq(row_index).removeAttr('id');
                $(this).removeAttr('id');

            }else if((viewer.eq(row_index).attr('id') == 'showing-row') && ($(this).attr('id') != 'showed')){
                arrow_class.remove();
                view_work.remove();
                viewer.eq(row_index).append(cloning);
                showed.removeAttr('id');
                $(this).attr('id', 'showed');
                $(this).append(arrow);
                $("html, body").animate({
                    scrollTop: cloning.offset().top + 470 -$(window).height()
                }, 350);
                $(this).ajax_show(cloning);
            }else if((showing.length > 0)){
                arrow_class.remove();
                view_work.collapse('hide');
                view_work.find('.col-sm-4').addClass('fade-out');
                view_work.find('.col-sm-8').addClass('fade-out');
                cloning.attr('class','view-work hidden-xs collapse');
                viewer.eq(row_index).append(cloning);
                cloning.collapse('show');
                showing.removeAttr('id');
                showed.removeAttr('id');
                $(this).attr('id', 'showed');
                viewer.eq(row_index).attr('id', 'showing-row');
                $(this).append(arrow);
                if(this_index > showed_index){
                    $("html, body").animate({
                        scrollTop: cloning.offset().top -$(window).height()
                    }, 350);
                }else{
                    $("html, body").animate({
                        scrollTop: cloning.offset().top + 470 -$(window).height()
                    }, 350);
                }
                
                view_work.on('hidden.bs.collapse', function(){
                    $(this).remove();
                });
                $(this).ajax_show(cloning);
            }else{
                $(this).append(arrow);
                viewer.eq(row_index).append(cloning);
                cloning.collapse('show');
                $(this).attr('id', 'showed');
                viewer.eq(row_index).attr('id', 'showing-row');
                view_work.remove();
                $("html, body").animate({
                    scrollTop: cloning.offset().top + 470 -$(window).height()
                }, 350);
                $(this).ajax_show(cloning);
            }

            if(link != '-'){
                link = '<a href="'+ link +'" target="_blank">' + link + '</a>'
            }

            cloning.find('#view-work-title').html(title);
            cloning.find('#view-work-description').html(description);
            cloning.find('#view-work-client').html(client);
            cloning.find('#view-work-category').html(category);
            cloning.find('#view-work-link').html(link);
            cloning.find('.col-sm-4').addClass('fade-in');
            cloning.find('.col-sm-8').addClass('fade-in');
            cloning.find('.item').attr('class', 'item');
            cloning.find('.item:eq(0)').addClass('active');
            cloning.find('.carousel-indicators li').removeClass('active');
            cloning.find('.carousel-indicators li:eq(0)').addClass('active');
        }
        
    });    

    $.fn.ajax_show = function(cloning){
        cloning.find('.loading-container').show();
        var work_id = $(this).attr('data-id');
        var path = $(this).attr('data-path');
        cloning.find('#slider-indicator').empty();
        cloning.find('#slider-inner').empty();
        $.ajax();
        $.get("/ajax-slider?work_id=" +work_id, function(data){
            cloning.find('.loading-container').hide();
            //success data
            $.each(data, function(index, slider){
                if(index == 0){
                    cloning.find('#slider-indicator').append('<li data-target="#view-work-slider" data-slide-to="'+index+'" class="active"></li>');
                    cloning.find('#slider-inner').append('<div class="item active"><img src="/'+path+'/'+slider.picture_name+'" alt="..."></div>');
                }else{
                    cloning.find('#slider-indicator').append('<li data-target="#view-work-slider" data-slide-to="'+index+'"></li>');
                    cloning.find('#slider-inner').append('<div class="item"><img src="/'+path+'/'+slider.picture_name+'" alt="..."></div>');
                }
                


            });
            $('#view-work-slider').carousel('cycle');
        });
    };
</script>
@endsection

