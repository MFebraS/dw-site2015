@extends('layout')

@section('content_title')
<title>Durenworks - News</title>
@stop

@section('content')
<!-- Judul Halaman -->
    <div class="news-title title-page">
      <div class="container">
        <h1>News</h1>
      </div>
    </div>

    <div class="news">
      <div class="container">
        <!-- News and Image -->
        <div class="col-md-9 col-sm-12">
          @forelse($all_posts as $key => $post)
            <div class="row">
            <!-- Jika gambar tidak tersedia -->
            @if($picture_names[$key] == null)
              <div class="col-xs-12">
                  <h2><a href="{{ action('NewsController@news_detail', [$post->permalink]) }}">{{ $post->title }}</a></h2>
                  <div class="news-attribute">
                      <span>By <span class="yellow">{{ $post->user->name }}</span></span>
                      <span class="attribute-fence">In: <a href="{{ action('NewsController@category_filter'), [$post->category->slug] }}">{{ $post->category->category_name }}</a></span>
                  </div>
                  <div>{!! $contents[$key] !!}</div>
                  <div class="read-more"><a href="{{ action('NewsController@news_detail', [$post->permalink]) }}">Read More <span class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span> </a></div>
              </div>
            @else
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <h2><a href="{{ action('NewsController@news_detail', [$post->permalink]) }}">{{ $post->title }}</a></h2>
                  <div class="news-attribute">
                      <span>By <span class="yellow">{{ $post->user->name }}</span></span>
                      <span class="attribute-fence">In: <a href="{{ action('NewsController@category_filter', ['category_slug' => $post->category->slug]) }}">{{ $post->category->category_name }}</a></span>
                  </div>
                  <div>{!! $contents[$key] !!}</div>
                  <div class="read-more"><a href="{{ action('NewsController@news_detail', [$post->permalink]) }}">Read More <span class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span> </a></div>
              </div>
              <div class="col-md-6 col-sm-6">
                <div class="media hidden-xs">
                  <a href="{{ action('NewsController@news_detail', [$post->permalink]) }}">
                    <img class="media-object" src="/assets/images/upload-news-pictures/{{ $post->id }}/{{ $picture_names[$key]}}"/>
                    <div class="image-hover"></div>
                    <div class="news-tooltip">
                        <div class="news-sprite news-tooltip-arrow"></div>
                        <span> {{ date("j M Y", strtotime($post->published_date)) }} </span> <!-- 04 June, 2014 -->
                    </div>
                  </a>
                </div>
              </div>
            @endif
            </div>
            <hr>
          @empty
            <div class="row">
              <p class="text-center"><strong>No Record Found</strong></p>
            </div>
          @endforelse

          <!-- Pagination -->
          <div id="pagination">
            @include('pagination', ['object' => $all_posts])
          </div>
      </div> <!-- /News and Image -->
      
      <!-- Sidebar -->
      <div class="col-md-3">
        <div class="bs-docs-sidebar hidden-print hidden-xs hidden-sm">
          <div id="search-box">
            <form action="{{ action('NewsController@index') }}" method="GET">
              <input type="search" placeholder="Search..." name="q" value="{{ $query }}" />
              <input type="submit" value="" class="news-sprite" />
            </form>
          </div>
          <div class="sidebar-module">
            <h2>Categories</h2>
            <ul class="nav bs-docs-sidenav">
            @foreach($categories as $key => $category)
              <li><a href="{{ action('NewsController@category_filter', [$category->slug]) }}"><div class="news-sprite arrow-round"></div>{{ $category->category_name }}<span class="pull-right"> {{ $categories_count[$key] }} </span></a></li>
            @endforeach
            </ul>
            
            <!-- Menu Tab -->
            <div id="menu-tab" role="tabpanel" data-example-id="togglable-tabs">
                <ul id="sidebar-tab" class="nav nav-tabs" role="tablist">
                  <li role="presentation" class="active"><a href="#popular" id="popular-tab" role="tab" data-toggle="tab" aria-controls="" aria-expanded="true">Popular</a></li>
                  <li role="presentation"><a href="#recent" role="tab" id="recent-tab" data-toggle="tab" aria-controls="">Recent</a></li>
                </ul>
                <!-- Tab Content -->
                <div id="sidebar-tab-content" class="tab-content">
                  <div role="tabpanel" class="tab-pane active" id="popular" aria-labelledBy="popular-tab">
                    @forelse ($popular_posts as $key => $popular_post)
                      <div class="row">
                          <ul class="list-unstyled">
                            <li><a href="{{ action('NewsController@news_detail', [$popular_post->permalink]) }}">{{ $popular_post_titles[$key] }}</a></li>
                            <li class="date">Date: <span class="yellow">{{ date("j F Y", strtotime($popular_post->published_date)) }}</span></li>
                          </ul>
                      </div>
                    @empty
                      <div class="row">
                        <p class="text-center"><strong>No Record Found</strong></p>
                      </div>
                    @endforelse
                  </div>
                  <div role="tabpanel" class="tab-pane" id="recent" aria-labelledBy="recent-tab">
                    @foreach ($recent_posts as $key => $recent_post)
                      <div class="row">
                          <ul class="list-unstyled">
                            <li><a href="{{ action('NewsController@news_detail', [$recent_post->permalink]) }}">{{ $recent_post_titles[$key] }}</a></li>
                            <li class="date">Date: <span class="yellow">{{ date("j F Y", strtotime($recent_post->published_date)) }}</span></li>
                          </ul>
                      </div>
                    @endforeach
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div> <!-- /Sidebar -->
      </div> <!-- /container -->
    </div>
@endsection
@section('content_js')
   <script type="text/javascript">
        $(document).ready(function(){

            var window_size = $(window).width();
            if(window_size < 516){
              for(var i=1; i<=6; i++){
                $('#pagination li:eq('+i+')').attr('class', 'hidden-xs');
              }
            }
        });
    </script>
@endsection
