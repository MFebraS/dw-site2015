@extends('layout')

@section('content_title')
<title>Durenworks - Contact Us</title>
<script src='https://www.google.com/recaptcha/api.js'></script>
@stop

@section('content')
  <div id="contact-us">
  <!-- Judul Halaman -->
    <div class="title-page">
      <div class="container">
        <h1>Contact Us</h1>
      </div>
    </div>
    <div class="container">
      <!-- Menampilkan peta Durenworks di Google Map -->
      <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7919.1963447677435!2d110.43509628465581!3d-7.056411002172294!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e708ea9cd326539%3A0xa1a94b6225752435!2sJl.+Jatisari+II%2C+Tembalang%2C+Kota+Semarang%2C+Jawa+Tengah+50275%2C+Indonesia!5e0!3m2!1sid!2s!4v1430921674074" width="100%" height="350" frameborder="0" style="border:0"></iframe>

      <div id="contact-content">
      <!-- LEFT -->
        <div class="col-sm-6 col-xs-12">
          <h2> Contact Info </h2>
          <p>
              Durenworks will escort your business achieve goals and effectiveness. Do not waiting too 
              long for any of your business needs. Eitherwise, you will miss any opportunity or left 
              behind by your competitor Contact us right now!
          </p>
          <div id="contact-info">
            <div class="row">
              <div class="col-sm-6">
                <h3> Address: </h3>
                <p>
                  Jalan Jatisari II no. 4 Tembalang
                  Semarang, Central Java,<br>
                  Indonesia
                </p>
              </div>
              <div class="col-sm-6">
                <h3> Email: </h3>
                <p>
                  contact@durenworks.com
                </p>
              </div>
            </div>
            
            <div class="row">
              <div class="col-sm-6">
                <h3> Phone &amp; Fax: </h3>
                <p>
                  +6224-7462189
                </p>
              </div>
              <div id="sos-med" class="col-sm-6">
                <h3> Social Network: </h3>
                <div class="row">
                  <div class="col-md-1 col-xs-1">
                    <a href="https://www.facebook.com/pages/Durenworks/669509426431580" target="_blank">
                      <div id="facebook-icon" class="contact-sprite"></div>
                    </a>
                  </div>
                  <div class="col-md-1 col-xs-1">
                    <a href="https://twitter.com/durenworks" target="_blank">
                      <div id="twitter-icon" class="contact-sprite"></div>
                    </a>
                  </div>
                  <div class="col-md-1 col-xs-1">
                    <a href="https://plus.google.com/101815098485465188722" target="_blank">
                      <div id="google-icon" class="contact-sprite"></div>
                    </a></div>
                </div>
              </div> <!-- End sos-med -->
            </div>
          </div> <!-- Contact Info -->
        </div>
        <!-- RIGHT -->
        <div id="form-contact" class="col-sm-6 col-xs-12">
          <div class="row">
            <h2> Leave us a Message </h2>
            @if(session('success') != null)
              <p class="bg-success"><b>{{ session('success') }}</b></p>
            @endif
            @if(session('error') != null)
              <p class="bg-danger"><b>{{ session('error') }}</b></p>
            @endif
            <form action="{{ action('ContactUsController@send') }}" method="post">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input class="input-message" type="input" name="name" placeholder="Your Name..." value="{{ old('name') }}" required/>
              {!! $errors->first('name', '<p class="text-danger">:message</p>') !!}
              <input class="input-message" type="email" name="email" placeholder="Your Email Address..." value="{{ old('email') }}" required/>
              {!! $errors->first('email', '<p class="text-danger">:message</p>') !!}
              <textarea id="text-area" class="input-message" name="message" placeholder="Your Message...">{{ old('message') }}</textarea>
              <div class="g-recaptcha" data-sitekey="{{ Config::get('recaptcha.site_key') }}"></div>
              <input id="send-message" class="btn" type="submit" name="send_message" value="SEND MESSAGE"/>
            </form>
          </div>
        </div>  <!-- Right -->
      </div>
    </div>
  </div>
    
@endsection
@section('content_js')
  <script type="text/javascript">
    /* Mengaktifkan Menu Contact Us */
    $(document).ready(function(){
        $('.place-card').parent().parent().parent().remove();
    });
  </script>
@endsection
