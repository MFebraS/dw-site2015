@extends('layout')

@section('content_title')
<title>Durenworks - Services</title>
@stop

@section('content')
  <div id="services">
  <!-- Judul Halaman -->
    <div class="services-title title-page">
      <div class="container">
        <h1>Services</h1>
      </div>
    </div>

    <div class="container">
    	<!-- Menu Services -->
    	<div id="menu-services">
            <nav>
                <ul class="nav nav-justified">
                	<li class="active"><a href="#web-design" id="left-menu" data-toggle="tab">Web Design &amp; Dev</a><span class="arrow"></span></li>
                    <li class=""><a href="#business-application" id="center-left-menu" data-toggle="tab" aria-expanded="true">Business Application</a><span class="arrow"></span></li>
                    <li class=""><a href="#digital-marketing" id="center-right-menu" data-toggle="tab">Digital Marketing</a><span class="arrow"></span></li>
                    <li class=""><a href="#mobile-application" id="right-menu" data-toggle="tab">Mobile Application</a><span class="arrow"></span></li>
                </ul>
            </nav>
        </div> <!-- End of Menu Services -->

        <!-- Content dari Menu -->
        <div class="tab-content">
        	<!-- Konten Web -->
        	<div id="web-design" class="tab-pane fade in active">
        		<div class="row">
	                <div class="col-sm-6 services-image hidden-xs">
	                    <img src="/assets/images/services/website_design_dev.jpg" />
	                </div>
	                <div class="col-sm-6">
	                    <h4>Website Design &amp; Development</h4>
	                    <p>
	                    	We have a passion for building well-architected web 
	                    	applications that streamline processes, extend capabilities, 
	                    	and enable business, consumers, and organizations to 
	                    	connect in new ways.
	                    </p>
	                    <p>
	                    	When commercial, off-the-shelf software (COTS) doesn't 
	                    	fit the bill, we help business create the tools they 
	                    	need to complete.
	                    </p>
	                    <p>
	                    	Please explore our site and contact us to learn how 
	                    	we can partner to build the next web application 
	                    	for your enterprise.
	                    </p>
	                </div>
	            </div>
        	</div>
        	<!-- Konten Bisnis App -->
        	<div id="business-application" class="tab-pane fade">
        		<div class="row">
	                <div class="col-sm-6 services-image hidden-xs">
	                    <img src="/assets/images/services/business_application.jpg" />
	                </div>
	                <div class="col-sm-6">
	                    <h4>Business Application</h4>
	                    <p>
	                    	Surely there's a better way. We've created hundreds 
	                    	of productivity applications, content management systems, 
	                    	process tools, databases, and more. Every one tailored 
	                    	to the unique needs of each business we encounter. We're 
	                    	never one-size-fit-all, we work with your team to create 
	                    	databases, websites, and mobile apps that fit exactly 
	                    	what your team needs.
	                    </p>
	                    <p>
	                    	"Work smarter, not harder" - In today's world that means 
	                    	having the right software tools in the hands of your very
	                    	busy team. We can cut hours from data entry tasks by 
	                    	perfectly tailoring software. We can eliminate error-
	                    	prone duplication by integrating systems. We can improve 
	                    	decision making by more quickly synthesizing business 
	                    	data. We give your customers a means of communicating 
	                    	with you.
	                    </p>
	                    <p>
	                    	We can manufacture a competitive edge.
	                    </p>
	                </div>
	            </div>
        	</div>
        	<!-- Konten Digital -->
        	<div id="digital-marketing" class="tab-pane fade">
        		<div class="row">
	                <div class="col-sm-6 services-image hidden-xs">
	                    <img src="/assets/images/services/digital_marketing.jpg" />
	                </div>
	                <div class="col-sm-6">
	                    <h4>Digital Marketing</h4>
	                    <p>
	                    	Social Media, SEO, SEM, and Virtual Marketing. All 
	                    	of it has to come together as one cohesive unit 
	                    	to drive results. We also refer to this as the 
	                    	"Squeezing all the juice out of an orange" philosophy
	                    </p>
	                    <p>
	                    	We don't believe in creating empty websites that don't 
	                    	generate leads or in designing meaningless campaign. 
	                    	Instead, we believe in creating comprehensive digital 
	                    	marketing strategies an implementing them - all while 
	                    	keeping our client's goals in mind.
	                    </p>
	                    <p>
	                    	Our formula in digital marketing is attract, convert, 
	                    	and transform.
	                    </p>
	                </div>
	            </div>
        	</div>
        	<!-- Konten Mobile -->
        	<div id="mobile-application" class="tab-pane fade">
        		<div class="row">
	                <div class="col-sm-6 services-image hidden-xs">
	                    <img src="/assets/images/services/mobile_application.jpg" />
	                </div>
	                <div class="col-sm-6">
	                    <h4>Mobile Application</h4>
	                    <p>
	                    	Durenworks approaches custom mobile application development 
	                    	with the same focus on utility and value that we have for 
	                    	all of our development practices. Whether creating a mobile 
	                    	app for your sales reps to generate quotes on the go, a tool 
	                    	for your customers to check on the status of their order, or 
	                    	a registration system for your offsite events. We have the 
	                    	experience, methodologies, and the know-how to create effective apps.
	                    </p>
	                    <p>
	                    	We focus on extending our clients business systems rather than 
	                    	taking costly approach of reinventingthe wheel. We deliver 
	                    	mobile databases --both server based and standalone.
	                    </p>
	                    <p>
	                    	In some circumstances a client needs a native application: 
	                    	we've build both iOS and Android apps. Durenworks believes 
	                    	in bringing the right technology to bear that meets business needs.
	                    </p>
	                </div>
	            </div>
        	</div>
        </div> <!-- End of tab content -->
    </div>
    <div id="exellent-business">
    	<div id="center">
    		<h2>Excellent for YOUR BUSINESS</h2>
    		<div id="hp" class="hidden-xs">
    			<img src="/assets/images/services/hp.png">
    		</div>
    	</div>
    	<!-- Left Excellent Business-->
    	<div class="col-xs-6 left">
    		<div class="col-md-3 col-sm-1 hidden-xs"></div>

   			<div class="col-md-9 col-sm-11 col-xs-12">
	   			<div class="middle-content">
	   				<div class="row">
			    		<div class="col-sm-7 col-xs-12">
			    			<h5>Fast Process</h5>
			    			<p>Simple and neat coding technology. Manage to consume less memory</p>
			    		</div>
			    		<div class="col-sm-2 hidden-xs">
			    			<img src="/assets/images/services/sand-clock.png">
			    		</div>
			    		<div class="col-sm-3 hidden-xs"></div>
		    		</div>
		    		<div class="row">
			    		<div class="col-sm-7 col-xs-12">
			    			<h5>User Friendly Interface</h5>
			    			<p>Easy User Experience. Simple and elegant UI Design</p>
			    		</div>
			    		<div class="col-sm-2 hidden-xs">
			    			<img src="/assets/images/services/mobile.png">
			    		</div>
			    		<div class="col-sm-3 hidden-xs"></div>
		    		</div>
		    		<div class="row">
			    		<div class="col-sm-7 col-xs-12">
			    			<h5>Easy to Update</h5>
			    			<p>We have our own server. No need to contact 3rd party</p>
			    		</div>
			    		<div class="col-sm-2 hidden-xs">
			    			<img src="/assets/images/services/cloud.png">
			    		</div>
			    		<div class="col-sm-3 hidden-xs"></div>
		    		</div>
	   			</div>
    		</div> <!-- End vertical-center -->
    	</div>

    	<!-- Right Excellent Business-->
    	<div class="col-xs-6 right">
   			<div class="col-md-9 col-sm-11 col-xs-12">
	   			<div class="middle-content">
		    		<div class="row">
			    		<div class="col-sm-3 hidden-xs"></div>
			    		<div class="col-sm-2 hidden-xs">
			    			<img src="/assets/images/services/toga.png">
			    		</div>
			    		<div class="col-sm-7 col-xs-12">
			    			<h5>Universal Structure</h5>
			    			<p>Acceptable with any technology. Easy to migrate</p>
			    		</div>
			    	</div>
			    	<div class="row">
			    		<div class="col-sm-3 hidden-xs"></div>
			    		<div class="col-sm-2 hidden-xs">
			    			<img src="/assets/images/services/flexibility.png">
			    		</div>
			    		<div class="col-sm-7 col-xs-12">
			    			<h5>Change Flexibility</h5>
			    			<p>Flexible developing. Giving you very personalize app</p>
			    		</div>
			    	</div>
			    	<div class="row">
			    		<div class="col-sm-3 hidden-xs"></div>
			    		<div class="col-sm-2 hidden-xs">
			    			<img src="/assets/images/services/flag.png">
			    		</div>
			    		<div class="col-sm-7 col-xs-12">
			    			<h5>Awesome Support</h5>
			    			<p>Fast response technical support by email and on call</p>
			    		</div>
			    	</div>
	   			</div>
   			</div>
			<div class="col-md-3 col-sm-1 hidden-xs"></div>
    	</div>
    </div>

    <div id="ideas" class="container">
        <h3>Do You Need Some Ideas?</h3>
        <div class="col-sm-8 col-xs-12">
        	<p>
        		Have no idea how to increase your business profit and effectiveness? 
        		Frustated by your plan and strategy not manage to your goals? 
        		Lets give us a clue about your business problem. We will try our 
        		best to come up with solution.
        	</p>
        </div>
        
        <div class="col-sm-1 hidden-xs"></div>

        <div class="col-sm-3 col-xs-12">
        	<a id="order-btn" href="{{ action('ContactUsController@index') }}">ORDER NOW!</a>
        </div>
    </div>
  </div>
    
@endsection
@section('content_js')
 <script type="text/javascript">
		window.onhashchange = function(){
            var hash = window.location.hash;
            $('.nav.nav-justified a[href=#'+hash.split('#')[1]+']').tab('show') ;
        }

        /* Menambah hashtag avaya dan pawons */
        window.onload = function(){
            var url = document.location.toString();
            if (url.match('#')) {
                $('.nav.nav-justified a[href=#'+url.split('#')[1]+']').tab('show') ;
                $(window).scrollTop('0');
            } 
        }
        $('.nav.nav-justified a[data-toggle="tab"]').on('show.bs.tab', function (e) {
            window.location.hash = e.target.hash;
        })
        $('.nav.nav-justified a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
             $('html, body').animate({
                scrollTop: '0'
            }, 300);
        })
  </script>
@endsection
