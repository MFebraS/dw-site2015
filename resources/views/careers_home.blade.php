@extends('careers_layout')

@section('sub-content')
        <h2> Why Join Durenworks ? </h2>
        <div id="reason">
          <div class="image-wrap">
            <img src="/assets/images/careers/careers.jpg">
          </div>
          <p>
            We still growing and need to grow more. That's why we always seek 
            best candidate in every departement. We believe, the most precious 
            things in a company is not how much they making the money, but the 
            team and the people in the company it self.
          </p>
        </div>
        <div id="advantage">
          <!-- TOP -->
          <div class="row">
            <div class="col-sm-6 col-xs-12">
              <div class="col-sm-4 col-xs-4">
                <img class="image-wrap" src="/assets/images/careers/income.png">
              </div>
              <div class="col-sm-8 col-xs-8">
                <h3> Great Income </h3>
                <p>
                  Not only basic salary, you always come up with any extra 
                  profit with every work you do.
                </p>
              </div>
            </div>
            <div class="col-sm-6 col-xs-12">
              <div class="col-sm-4 col-xs-4">
                <img class="image-wrap" src="/assets/images/careers/fun.png">
              </div>
              <div class="col-sm-8 col-xs-8">
                <h3> Fun Environment </h3>
                <p>
                  Are you working hard for playing or playing hard for working?
                </p>
              </div>
            </div>
          </div>
          <!-- BOTTOM -->
          <div class="row">
            <div class="col-sm-6 col-xs-12">
              <div class="col-sm-4 col-xs-4">
                <img class="image-wrap" src="/assets/images/careers/learning.png">
              </div>
              <div class="col-sm-8 col-xs-8">
                <h3> Learning Something New </h3>
                <p>
                  Always get something new every day. That's our motto. You can't 
                  do boring stuff in our office.
                </p>
              </div>
            </div>
            <div class="col-sm-6 col-xs-12">
              <div class="col-sm-4 col-xs-4">
                <img class="image-wrap" src="/assets/images/careers/benefits.png">
              </div>
              <div class="col-sm-8 col-xs-8">
                <h3> Benefits </h3>
                <p>
                  Extra bonus, health insurance, and flexible work hours.
                </p>
              </div>
            </div>
          </div>
        </div>
@stop

@section('content_js')
@endsection
