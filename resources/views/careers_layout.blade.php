@extends('layout')

@section('content_title')
<title>Durenworks - Careers</title>
<style type="text/css">
  ul{
    padding-left: 15px;
  }
  h3{
    margin-top: 20px;
  }
</style>
@stop

@section('content')
  <div id="careers">
  <!-- Judul Halaman -->
    <div class="title-page">
      <div class="container">
        <h1>Careers</h1>
      </div>
    </div>

    <div class="container">
      <div class="col-sm-4 col-xs-12">
        <div id="looking-for">
          <h2> What we looking for : </h2>
          <ul class="nav">
            @forelse($vacancies as $value)
                <a href="{{ action('CareersController@show', ['vacancy' => $value->slug]) }}"> <li {{ isset($vacancy) && $vacancy->slug == $value->slug ? "class=active" : "" }}> {{ $value->title }} </li> </a>
            @empty
              <p class="text-center">No Record Found</p>
            @endforelse

            @foreach($empties as $empty)
              <li> {{ $empty->title }} </li>
            @endforeach
          </ul>
          </div>  <!-- End of looking-for -->
      </div>

      <div class="col-sm-8 col-xs-12 join">
        @yield('sub-content')
      </div>

    </div>
  </div>
    
@endsection
