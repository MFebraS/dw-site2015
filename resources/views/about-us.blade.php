@extends('layout')

@section('content_title')
<title>Durenworks - About Us</title>
@stop

@section('content')
		
		<div class="title-page">
			<div class="container">
				<h1>About Us</h1>
			</div>
		</div>
		
        <div class="about-us">
            <div class="who-we-are"  style="background-color:#fafafa">
                <div class="container">
                    <h3 class="text-center">Who We are</h3>
                    <p class="text-center subtitle-h3">DURENWORKS, Digital Utilities, Advertising and Enginering Works</p>
                    <div class="row">
                        <div class="col-md-6 hidden-xs">
                            <img src="assets/images/about-us/durenworks_logo.png">
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <h4>About Durenworks</h4>
                            <p>Durenworks is a full service ITConsultant &amp; Solution expertised in Website design &amp; Development, Business Solution Provider, Mobile Application Developer, and Digital Marketing Strategy. </p>
                            <p>We Esteblished on October 2013 in Indonesia, as affiliated company of PT. Widya Solusi Utama for IT Services / Consulting in 2015, strive to fulfils all your business needs under one roof. Providing innovative, unique, and high quality services, based on requirements of clients.</p>
                            <p>We understand that building great business, need efficiency to achieve your goal and result. that's why, we prefer to work continuosly with a client. We act as not only an implementer but client's business consultant, giving them great advices and creative idea to solve their business problem.</p>
                            <p>As a result, the best benefit from durenworks is "Sustainable Solution"</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="our-team">
                <div class="container">
                    <h3 class="text-center">Our Profesional Team</h3>
                    <p class="text-center subtitle-h3">Our secret ingredient for successful project</p><br/>

                    @forelse($rows as $row)
                        <div id="team" class="row row-centered">
                        @foreach($row as $item)
                            {{--*/ $user = $users[$item] /*--}}
                            <div class="col-xs-6 col-md-3 col-centered team">
                                <div class="primary-detail">
                                    <div class="img-wrap">
                                        <img src="/{{ Config::get('custom_path.employees') }}/{{ $user->picture }}">
                                    </div>
                                    <h4>{{ $user->name }}</h4>
                                    <small>{{ $user->employ }}</small>
                                </div>
                                <p class="hidden-xs">{{ $user->about }}</p>
                                <div class="row">
                                    @if($user->facebook != null)
                                    <div class="col-xs-1 col-md-2">
                                        <a href="{{ $user->facebook }}">
                                            <div class="sprite facebook-icon"></div>
                                        </a>
                                    </div>
                                    @endif
                                    @if($user->twitter != null)
                                    <div class="col-xs-1 col-md-2">
                                        <a href="{{ $user->twitter }}" >
                                            <div class="sprite twitter-icon"></div>
                                        </a>
                                    </div>
                                    @endif
                                    @if($user->google != null)
                                    <div class="col-xs-1 col-md-2">
                                        <a href="{{ $user->google }}" >
                                            <div class="sprite google-icon"></div>
                                        </a>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                        </div>
                    @empty
                        <div id="team" class="row row-centered">
                            <h5 class="text-center">No Record Found</h5>
                        </div>
                    @endforelse
                    


                </div>
            </div>

            <div class="how-we-work" style="background-color:#fafafa">
                <div class="container">
                    <h3 class="text-center">How We Work</h3>
                    <p class="text-center">Our way and believe how a project is treated. Each project is special and need special way to deliver it.</p>
                    <div class="graph">
                        <div id="ani-0" class="start-node hidden-xs" style="opacity:0">
                            <span class="node-inner text-center">START</span>
                        </div>

                        <div id="ani-1" class="row">
                            <div class="col-sm-6 picture-area hidden-xs">
                                <div class="picture-frame">
                                    <img class="picture" src="/assets/images/about-us/how-we-work/1.jpg "/>
                                    <div class="picture-cover"></div>
                                </div>
                            </div>
                            <div class="col-sm-6 text-area">
                                <div class="col-sm-3 graph-container hidden-xs">
                                    <div class="row text-area-graph">
                                        <div class="col-sm-8 hr-area">
                                            <div class="hr-container">
                                                <hr/>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 node-area">
                                            <div class="node-container">
                                                <div class="node">1</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-9 text-area-content">
                                    <div class="content">
                                        <h4 style="font-size:20px">Listen to Discover</h4>
                                        <p>
                                            Listening is the best job in the world. Listening 
                                            giving you any most updated information. And listening 
                                            also giving us an opportunity to discover your business 
                                            problem that can lead us a strategy for solution.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="ani-2" class="row">
                            
                            <div class="col-sm-6 text-area">
                                
                                <div class="col-sm-9 text-area-content">
                                    <div class="content">
                                        <h4 style="font-size:20px">Research the Concept</h4>
                                        <p>
                                            We understand that each business is unique. Every 
                                            business come up with different solution and priority 
                                            level of work. That's why our team always come up 
                                            with creative consept for each customer that we handled.
                                        </p>
                                    </div>
                                </div>
                                <div class="col-sm-3 graph-container hidden-xs">
                                    <div class="row text-area-graph">
                                        
                                        <div class="col-sm-4 node-area">
                                            <div class="node-container">
                                                <div class="node">2</div>
                                            </div>
                                        </div>
                                        <div class="col-sm-8 hr-area">
                                            <div class="hr-container">
                                                <hr style="width:0%"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 picture-area hidden-xs">
                                <div class="picture-frame col-sm-offset-2 col-md-offset-1">
                                    <img class="picture" src="/assets/images/about-us/how-we-work/2.jpg"/>
                                </div>
                            </div>
                        </div>

                        <div id="ani-3" class="row">
                            <div class="col-sm-6 picture-area hidden-xs">
                                <div class="picture-frame">
                                    <img class="picture" src="/assets/images/about-us/how-we-work/3.jpg"/>
                                    <div class="picture-cover"></div>
                                </div>
                            </div>
                            <div class="col-sm-6 text-area">
                                <div class="col-sm-3 graph-container hidden-xs">
                                    <div class="row text-area-graph ">
                                        <div class="col-sm-8 hr-area">
                                            <div class="hr-container">
                                                <hr style="width:0%; opacity:0"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 node-area">
                                            <div class="node-container">
                                                <div class="node">3</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-9 text-area-content">
                                    <div class="content">
                                        <h4 style="font-size:20px">Planning the Strategy</h4>
                                        <p>
                                            A great consept need a great strategy. Come up 
                                            with SWOT analytic. We calculate every Strength, 
                                            Weakness, Opportunity, and Threat that come up 
                                            from your business for giving us a layout and 
                                            "how to" process for execute the concept.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="ani-4" class="row">
                            
                            <div class="col-sm-6 text-area">
                                
                                <div class="col-sm-9 text-area-content">
                                    <div class="content">
                                        <h4 style="font-size:20px">Execute and Develop</h4>
                                        <p>
                                            At this step, we giving our best skill to deliver 
                                            a clear and neat design, best specification, and 
                                            sophisticated technology into your project. Our team 
                                            will develop a precise coding and structure with
                                            full attention down to detailed.
                                        </p>
                                    </div>
                                </div>
                                <div class="col-sm-3 graph-container hidden-xs">
                                    <div class="row text-area-graph">
                                        
                                        <div class="col-sm-4 node-area">
                                            <div class="node-container">
                                                <div class="node">4</div>
                                            </div>
                                        </div>
                                        <div class="col-sm-8 hr-area">
                                            <div class="hr-container">
                                                <hr/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 picture-area hidden-xs">
                                <div class="picture-frame col-sm-offset-2 col-md-offset-1">
                                    <img class="picture" src="/assets/images/about-us/how-we-work/4.jpg"/>
                                </div>
                            </div>
                        </div>

                        <div id="ani-5" class="row">
                            <div class="col-sm-6 picture-area hidden-xs">
                                <div class="picture-frame">
                                    <img class="picture" src="/assets/images/about-us/how-we-work/5.jpg"/>
                                    <div class="picture-cover"></div>
                                </div>
                            </div>
                            <div class="col-sm-6 text-area">
                                <div class="col-sm-3 graph-container hidden-xs">
                                    <div class="row text-area-graph">
                                        <div class="col-sm-8 hr-area">
                                            <div class="hr-container">
                                                <hr/>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 node-area">
                                            <div class="node-container">
                                                <div class="node">5</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-9 text-area-content">
                                    <div class="content">
                                        <h4 style="font-size:20px">Deploy and Implementation</h4>
                                        <p>
                                            Before your project can be launch, we do some quality 
                                            test and bug checking. It will ensure that every 
                                            design, function, and structure is ready and fit by
                                            your requirement. When the stages are clear, we also 
                                            giving technical knowledge transfer.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="ani-6" class="row">
                            
                            <div class="col-sm-6 text-area">
                                
                                <div class="col-sm-9 text-area-content">
                                    <div class="content">
                                        <h4 style="font-size:20px">Manage and Develop</h4>
                                        <p>
                                            When the project is launch, our journey is not over 
                                            yet. In fact, the great challenge facing us to 
                                            maintenance and develop your new web/ application 
                                            manage to fulfill and achieve your business target and 
                                            our responsibility to bring your business to success.
                                        </p>
                                    </div>
                                </div>
                                <div class="col-sm-3 graph-container hidden-xs">
                                    <div class="row text-area-graph">
                                        
                                        <div class="col-sm-4 node-area">
                                            <div class="node-container">
                                                <div class="node">6</div>
                                            </div>
                                        </div>
                                        <div class="col-sm-8 hr-area">
                                            <div class="hr-container">
                                                <hr/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 picture-area hidden-xs">
                                <div class="picture-frame col-sm-offset-2 col-md-offset-1">
                                    <img class="picture" src="/assets/images/about-us/how-we-work/6.jpg"/>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>  <!-- End of how we work -->

            <div class="some-partner">
                <div class="container">
                    <h3 class="text-center">Some of Our Partners</h3>
                    <p class="text-center subtitle-h3">Everything start with collaboration, we have our backup here</p>
                    <div class="row" style="margin-bottom: 30px;">
                        <div class="col-sm-6 col-xs-12">
                            <a class="right" href="http://www.avaya.com/apac/" target="_blank"><img src="/assets/images/about-us/partners/avaya_logo.jpg"></a>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <a class="left" href="https://veritrans.co.id/" target="_blank"><img src="/assets/images/about-us/partners/veritrans_logo.jpg"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection
@section('content_js')
    <script type="text/javascript">
        /* Cek lebar device */
        var window_width = $(window).width();
        if(window_width >= 755){

            if(window_width >= 1034){
                var picture_frame = '100%';
            }
            else if( (window_width <= 1033) && (window_width >= 968) ){
                var picture_frame = '85%';
            }
            else{
                var picture_frame = '75%';
            }

            var scrollPosition = [];
            var ani = [];
            $(function(){
                $('.graph').children().each(function(index){
                    scrollPosition[index] = $(this).offset().top - 690;
                    ani[index] = true;
                    if(index <= 1){
                        scrollPosition[index] = $(this).offset().top - 690;
                    }else{
                        scrollPosition[index] = scrollPosition[index-1] + 690;
                    }
                })
            });
            $(window).scroll(function(){
                if  ( $(window).scrollTop() >= scrollPosition[0] && ani[0])
                {
                    ani[0] = false;
                    $('#ani-0').animate({
                        'opacity': 1
                    }, 800);
                }
                if  ( $(window).scrollTop() >= scrollPosition[1] && ani[1])
                {
                    ani[1] = false;
                    $('#ani-1').animate({
                        'height': '690px',
                        'opacity': '1'
                    }, 1400);
                    $('#ani-1 .picture-cover').delay(1900).animate({
                        'width': '0%'
                    }, 1000);
                    $('#ani-1 .picture').delay(1900).animate({
                        'opacity': '1'
                    }, 1000);
                    $('#ani-1 hr').delay(1200).animate({
                        'width': '100%',
                        'opacity': '1'
                    }, 500);
                    $('#ani-1 .node-container').delay(1600).animate({
                        'opacity': '1'
                    }, 500);
                    $('#ani-1 .text-area-content').delay(1600).animate({
                        'opacity': '1'
                    }, 800);
                }
                if  ( $(window).scrollTop() >= scrollPosition[2] && ani[2])
                {
                    ani[2] = false;
                    $('#ani-2').animate({
                       'height': '690px',
                        'opacity': '1'
                    }, 1400);
                    $('#ani-2 .picture-frame').delay(1900).animate({
                        'width': picture_frame
                    }, 1000);
                    $('#ani-2 .picture').delay(1900).animate({
                        'opacity': '1'
                    }, 1200);
                    $('#ani-2 .hr-container').delay(1400).animate({
                        'left': '0%'
                    }, 500);
                    $('#ani-2 hr').delay(1400).animate({
                        'width': '100%'
                    }, 500);
                    $('#ani-2 .node-container').delay(1700).animate({
                        'opacity': '1'
                    }, 500);
                    $('#ani-2 .text-area-content').delay(1700).animate({
                        'opacity': '1'
                    }, 500);
                }
                if  ( $(window).scrollTop() >= scrollPosition[3] && ani[3])
                {
                    ani[3] = false;
                    $('#ani-3').animate({
                        'height': '690px',
                        'opacity': '1'
                    }, 1400);
                    $('#ani-3 .picture-cover').delay(1900).animate({
                        'width': '0%'
                    }, 1000);
                    $('#ani-3 .picture').delay(1900).animate({
                        'opacity': '1'
                    }, 1000);
                    $('#ani-3 hr').delay(1200).animate({
                        'width': '100%',
                        'opacity': '1'
                    }, 500);
                    $('#ani-3 .node-container').delay(1600).animate({
                        'opacity': '1'
                    }, 500);
                    $('#ani-3 .text-area-content').delay(1600).animate({
                        'opacity': '1'
                    }, 800);
                }
                if  ( $(window).scrollTop() >= scrollPosition[4] && ani[4])
                {
                    ani[4] = false;
                    $('#ani-4').animate({
                       'height': '690px',
                        'opacity': 1
                    }, 1400);
                    $('#ani-4 .picture-frame').delay(1900).animate({
                        'width': picture_frame
                    }, 1000);
                    $('#ani-4 .picture').delay(1900).animate({
                        'opacity': '1'
                    }, 1200);
                    $('#ani-4 .hr-container').delay(1400).animate({
                        'left': '0%'
                    }, 500);
                    $('#ani-4 hr').delay(1400).animate({
                        'width': '100%'
                    }, 500);
                    $('#ani-4 .node-container').delay(1700).animate({
                        'opacity': '1'
                    }, 500);
                    $('#ani-4 .text-area-content').delay(1700).animate({
                        'opacity': '1'
                    }, 500);
                }
                if  ( $(window).scrollTop() >= scrollPosition[5] && ani[5])
                {
                    ani[5] = false;
                    $('#ani-5').animate({
                        'height': '690px',
                        'opacity': '1'
                    }, 1400);
                    $('#ani-5 .picture-cover').delay(1900).animate({
                        'width': '0%'
                    }, 1000);
                    $('#ani-5 .picture').delay(1900).animate({
                        'opacity': '1'
                    }, 1000);
                    $('#ani-5 hr').delay(1200).animate({
                        'width': '100%',
                        'opacity': '1'
                    }, 500);
                    $('#ani-5 .node-container').delay(1600).animate({
                        'opacity': '1'
                    }, 500);
                    $('#ani-5 .text-area-content').delay(1600).animate({
                        'opacity': '1'
                    }, 800);
                }
                if  ( $(window).scrollTop() >= scrollPosition[6] && ani[6])
                {
                    ani[6] = false;
                    $('#ani-6').animate({
                       'height': '690px',
                        'opacity': '1'
                    }, 1400);
                    $('#ani-6 .picture-frame').delay(1900).animate({
                        'width': picture_frame
                    }, 1000);
                    $('#ani-6 .picture').delay(1900).animate({
                        'opacity': '1'
                    }, 1200);
                    $('#ani-6 .hr-container').delay(1400).animate({
                        'left': '0%'
                    }, 500);
                    $('#ani-6 hr').delay(1400).animate({
                        'width': '100%'
                    }, 500);
                    $('#ani-6 .node-container').delay(1700).animate({
                        'opacity': '1'
                    }, 500);
                    $('#ani-6 .text-area-content').delay(1700).animate({
                        'opacity': '1'
                    }, 500);
                }
            });
        }
    </script>
    
@endsection
