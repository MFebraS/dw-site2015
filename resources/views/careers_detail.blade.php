@extends('careers_layout')

@section('sub-content')
	<h2>{{ $vacancy->title }}</h2>
	{!! html_entity_decode($vacancy->html_detail) !!}
@stop

@section('content_js')
@endsection

