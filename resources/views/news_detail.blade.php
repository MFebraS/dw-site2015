@extends('layout')

@section('content_title')
<title>Durenworks - News</title>
@stop

@section('content')
<!-- Judul Halaman -->
    <div class="news-title title-page">
      <div class="container">
        <h1>News</h1>
      </div>
    </div>

    <div class="news news-detail">
      <div class="container">
        <!-- News -->
        <div class="col-md-9 col-sm-12">
          <div class="row">
          <!-- Title and Image -->
            <div class="col-md-11 col-sm-11 news-header">
              <h2> {{ $post->title }} </h2>
              <br>
              <div class="news-attribute">
                  <span > By <span class="yellow">{{ $post->user->name }}</span> </span>
                  <span class="attribute-fence"> In: <a href="{{ action('NewsController@category_filter', [$post->category->slug]) }}">{{ $post->category->category_name }}</a> </span>
                  <span class="attribute-fence"> Post on: <span class="yellow">{{ date("j F Y", strtotime($post->published_date)) }}</span> </span>
              </div>
              <div class="media hidden-xs">
              <!-- Feauture Picture -->
                @if( $picture_name != null )
                  <img id="media-object-full" src="/assets/images/upload-news-pictures/{{ $post->id }}/{{ $picture_name }}"/>
                @endif
              </div>
            </div>  <!-- End Title and Image -->
            
            <div class="col-md-11 col-sm-11 col-xs-12">
                <div id="news-content">{!! $content !!}</div>
            </div>
          </div>
        </div> <!-- End News -->

        <!-- Sidebar -->
        <div class="col-md-3">
          <div class="bs-docs-sidebar hidden-print hidden-xs hidden-sm">
            <!-- Search Box -->
            <div id="search-box">
              <form action="{{ action('NewsController@index') }}" method="GET">
                <input type="search" placeholder="Search..." name="q" />
                <input type="submit" value="" class="news-sprite" />
              </form>
            </div> <!-- End Search Box -->

            <div class="sidebar-module">
              <h2>Categories</h2>
              <ul class="nav bs-docs-sidenav">
              @foreach($categories as $key => $category)
                <li><a href="{{ action('NewsController@category_filter', [$category->slug]) }}"><div class="news-sprite arrow-round"></div>{{ $category->category_name }}<span class="pull-right"> {{ $categories_count[$key] }} </span></a></li>
              @endforeach
              </ul>
              
              <!-- Menu Tab -->
              <div id="menu-tab" role="tabpanel" data-example-id="togglable-tabs">
                <ul id="sidebar-tab" class="nav nav-tabs" role="tablist">
                  <li role="presentation" class="active"><a href="#popular" id="popular-tab" role="tab" data-toggle="tab" aria-controls="" aria-expanded="true">Popular</a></li>
                  <li role="presentation"><a href="#recent" role="tab" id="recent-tab" data-toggle="tab" aria-controls="">Recent</a></li>
                </ul>
                <!-- Tab Content -->
                <div id="sidebar-tab-content" class="tab-content">
                  <div role="tabpanel" class="tab-pane active" id="popular" aria-labelledBy="popular-tab">
                    @foreach ($popular_posts as $key => $popular_post)
                      <div class="row">
                          <ul class="list-unstyled">
                            <li><a href="{{ action('NewsController@news_detail', [$popular_post->permalink]) }}">{{ $popular_post_titles[$key] }}</a></li>
                            <li class="date">Date: <span class="yellow">{{ date("j F Y", strtotime($popular_post->published_date)) }}</span></li>
                          </ul>
                      </div>
                    @endforeach
                  </div>
                  <div role="tabpanel" class="tab-pane" id="recent" aria-labelledBy="recent-tab">
                    @foreach ($recent_posts as $key => $recent_post)
                      <div class="row">
                          <ul class="list-unstyled">
                            <li><a href="{{ action('NewsController@news_detail', [$recent_post->permalink]) }}">{{ $recent_post_titles[$key] }}</a></li>
                            <li class="date">Date: <span class="yellow">{{ date("j F Y", strtotime($recent_post->published_date)) }}</span></li>
                          </ul>
                      </div>
                    @endforeach
                  </div>
                </div>
              </div>  <!-- End Menu Tab -->
            </div>  <!-- Sidebar Modul -->
          </div>
        </div>  <!-- /Sidebar -->
      </div> <!-- /container -->
    </div>
@endsection
@section('content_js')
@endsection
