@extends('layout')

@section('content_title')
<title>Durenworks - Products</title>
@stop

@section('content')
<!-- Judul Halaman -->
    <div class="title-page no-bottom">
      <div class="container">
        <h1>Products</h1>
      </div>
    </div>
    
    <div class="products">
      <div class="products-background">
        <div class="container">
            <div id="menu-products">
                <nav>
                    <ul class="nav nav-justified">
                        <li><a href="#avaya" id="left-menu" data-toggle="tab">AVAYA</a><span class="arrow"></span></li>
                        <li class="active"><a href="#mondopad" id="center-menu" data-toggle="tab" aria-expanded="true">Mondopad by Infocus</a><span class="arrow"></span></li>
                        <li><a href="#pawons" id="right-menu" data-toggle="tab">PAWONS System</a><span class="arrow"></span></li>
                    </ul>
                </nav>
            </div>
        </div>
      </div>
      
      <div class="tab-content">
        <!-- Tab Pane Avaya -->
        <div id="avaya" class="tab-pane fade">
            <div class="products-item products-background">
                <div class="image-products">
                    <img src="/assets/images/products/avaya/ip-office-infographic.jpg" alt="Avaya"/>
                </div>
                <div class="products-item-description container">
                    <h2>A Simple, Powerfull Midmarket Collaboration Solution</h2>
                    <p>
                        Seamless midmarket collaboration - Voice, Video, Mobile - on any device
                    </p>
                </div>
            </div>
            <div class="products-item-details">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 hidden-xs">
                            <img src="/assets/images/products/avaya/ip-office-feature.jpg" width="100%" alt="Avaya">
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <p>
                                Change the way your mobile, distributed workforce collaborates.
                                Deliver and engaging experience for voice, video, and mobility on
                                virtually any device. Simple enough to run on an appliance,
                                powerfull enough to support 2,000 users with virtuallized

                            </p>
                            <div class="features">
                                <h2>Avaya Key Features</h2>
                                <ul class="nav">
                                    <li>Flexible Configurations</li>
                                    <li>A Complete Midmarket Collaboration Solution</li>
                                    <li>Lower Collaboration TCO Business-class Video Conferencing</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="products-item-details products-background">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <div class="features description">
                                <h2>A COMPLETE ENGAGEMENT SOLUTION FOR MIDSIZE BUSINESS</h2>
                                <p>
                                    Everything you need from a single source: a simple, powerfull 
                                    core -Avaya IP Office&trade; Platform- surrounded by contact server, 
                                    video, networking, and support. Reduced integration risk. 
                                    Faster return on investment.
                                </p>
                                <p>
                                    Optimized for midsize businesses, an Avaya engagement solution 
                                    is flexible, yet complete. It's simple to use and manage, 
                                    delivering real value and low total cost of ownership.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="products-item-download" style="background-color: #E0E0E0;">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-9">
                            <h4><b>More Info</b> about Avaya and specification</h4>
                            <p>Please download PDF content by clicking download button</p>
                        </div>
                        <div class="col-sm-3">
                            <div class="button-danger-place">
                                <a href="{{ asset('/assets/downloadable/Avaya.pdf')}}"><button type="button" class="btn btn-danger">DOWNLOAD</button></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Tab Pane Mondopad -->
        <div id="mondopad" class="tab-pane fade in active">
            <div class="products-item products-background">
                <div  class="image-products">
                    <img class="hidden-xs" src="/assets/images/products/mondopad/mondopad.jpg" alt="Mondopad"/>
                    <img class="hidden-sm hidden-md hidden-lg" src="/assets/images/products/mondopad/mondopad-phone.jpg" alt="Mondopad"/>
                    <div class="video-products hidden-xs">
                        <!-- Gambar atau video -->
                    </div>
                </div>
                <div class="products-item-description container">
                    <h2>Communicate and Collaborate better than ever</h2>
                    <p>The Giant touch PC to 
                    present, annotate, and collaborate with people in
                    the room and around the world</p>
                </div>
            </div>
            <div class="products-item-details">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 hidden-xs">
                            <img src="/assets/images/products/mondopad/collaboration.jpg" width="100%">
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <p>
                                Mondopad’s human, natural touch interface changes the way people
                                interact with information, collaborate and connect. meetings are more
                                engaging, content is more immersive, and audiences are more engaged.
                            </p>
                            <div class="features">
                                <h2>Mondopad Key Features</h2>
                                <ul class="nav">
                                    <li>Multi Touch High Definition 55 amp; 70 Inch</li>
                                    <li>Flexible and Expandable with Built-in Windows PC</li>
                                    <li>Digital Intercative Whiteboard and Document Annotation</li>
                                    <li>Business-class Video Conferencing</li>
                                    <li>Share, View and Control from Your Notebook, Tablet, or Smartphone</li>
                                    <li>Full Version of Microsoft Office</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="products-item-details products-background">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <div class="features description">
                                <h2>Giant Touch Tablet for Your Conference Room or Classroom</h2>
                                <p>
                                    Present, annotate and collaborate on a giant tablet with
                                    meeting participants in the room and around the world. An
                                    Infocus Mondopad puts everything you need to visually present,
                                    capture and share ideas at your fingertips.
                                </p>
                                <p>
                                    All beautifully integrated into a single, cost-effective
                                    device, Mondopad streamlines meetings and alows people to
                                    communicate clearly. You’ll bridge communication gaps, save
                                    time and money on travel, and never look back.
                                </p>
                            </div>
                        </div>
                        <div class="col-sm-6 hidden-xs">
                            <iframe width="100%" height="315px"
                                src="https://www.youtube.com/embed/wMbxkvcaeXo?feature=player_detailpage" frameborder="0" allowfullscreen>
                            </iframe>
                        </div>
                    </div>
                </div>
            </div>
            <div class="products-item-download" style="background-color: #E0E0E0;">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-9">
                            <h4><b>More Info</b> about Mondopad and specification</h4>
                            <p>Please download PDF content by clicking download button</p>
                        </div>
                        <div class="col-sm-3">
                            <div class="button-danger-place">
                                <a href="/assets/downloadable/Mondopad.pdf"><button type="button" class="btn btn-danger">DOWNLOAD</button></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Tab Pane Pawons -->
        <div id="pawons" class="tab-pane fade">
            <div class="products-item products-background">
                <div class="image-products">
                    <img src="/assets/images/products/pawons/infographic.jpg" alt="Pawons"/>
                </div>
                <div class="products-item-description container">
                    <h2>A Simple, Powerfull Midmarket Collaboration Solution</h2>
                    <p>
                        Modern small to mid market resto and cafe platform
                    </p>
                </div>
            </div>
            <div class="products-item-details">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 hidden-xs">
                            <img src="/assets/images/products/pawons/hardware.jpg" width="100%">
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <p>
                                Pawons System is integrated cafe and resto system. This is Durenworks 
                                original product that help cafe and restaurant business to grow. 
                                Consist of Point of Sales (POS), Website, Accountuing, and Mobile 
                                Apps Menu Board. Become total solution for your small cafe or
                                exclusive restaurant.
                            </p>
                            <p>
                                In the future, it will integrated with 
                                Reserve system that can access via 
                                website or mobile apps.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>  <!-- End of Pawons -->
      </div>
    </div>
@endsection
@section('content_js')
    <script type="text/javascript" src="/assets/js/modernizr.custom.js"></script>
    <script type="text/javascript">
        $(function(){

            /* Mengecek apakah browser support dengan video */
            if (Modernizr.video) {  //jika tidak support
              $(".video-products").html('<img id="img-default" src="/assets/images/products/mondopad/img-default.jpg"/>');
            }
            else {
              $(".video-products").html('<video class="vertical-center" id="video-default" controls muted autoplay><source src="/assets/videos/mondopad.mp4" type="video/mp4"></video>');
            }
            
            /* Pause video mondopad ketika pindah tab */
            $("#left-menu, #right-menu").on('click', function() {
                $("#video-default").trigger("pause");
            });

            
        });

        window.onhashchange = function(){
            var hash = window.location.hash;
            $('.nav.nav-justified a[href=#'+hash.split('#')[1]+']').tab('show') ;
        }

        /* Menambah hashtag avaya dan pawons */
        window.onload = function(){
            var url = document.location.toString();
            if (url.match('#')) {
                $('.nav.nav-justified a[href=#'+url.split('#')[1]+']').tab('show') ;
                $(window).scrollTop('0');
            } 
        }
        $('.nav.nav-justified a[data-toggle="tab"]').on('show.bs.tab', function (e) {
            window.location.hash = e.target.hash;
        })
        $('.nav.nav-justified a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
             $('html, body').animate({
                scrollTop: '0'
            }, 300);
        })

    </script>
@endsection
