@extends('admin/layout-news')

@section('content_title')
    <title>Durenworks - Admin/News/Edit Category</title>
@endsection

@section('content')
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <div id="news-admin">
                <div id="categories">
                    <h1 class="page-header">Categories</h1>
                    <div class="search">
                        <form>
                            <input type="search" name="search-category" placeholder="Search Category"/>
                        </form>
                    </div>

                    <div id="categories-content">
                        <div class="col-sm-4 no-padding">

                            <h2 id="category-subtitle">Edit <em> {{$edit_category->category_name}} </em> Category</h2>

                            <div class="category-news">
                                <form action="{{ action('Admin\CategoriesController@update', [$edit_category->id]) }}" method="POST">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="form-group {{ $errors->first('category_name') != "" ? "has-error" : "" }}">
                                        <label for="new-category" class="control-label"><b>Name</b></label>
                                        <input type="text" id="new-category" class="form-control" name="category_name" value="{{ old('category_name') ? : $edit_category->category_name }}" autofocus required/>
                                        <p class="text-muted"><i>The name is how it appears on your site.</i></p>
                                        {!! $errors->first('category_name', '<p class="text-danger">:message</p>') !!}
                                    </div>
                                    <div id="slug" class="form-group {{ $errors->first('slug') != "" ? "has-error" : "" }}">
                                        <label for="slug" class="control-label"><b>Slug :</b></label>
                                        <span id="slug-result" {{ $errors->first('slug') != "" ? "style=color:red" : "" }}>{{ old('slug') ? : $edit_category->slug }}
                                        </span>
                                        <input type="hidden" id="duplicate-slug" name="slug" value="{{ old('slug') ? : $edit_category->slug }}"/>
                                        <input class="mybtn" id="edit-slug-category" type="button" name="edit-slug-category" value="Edit"/>
                                        <div class="row">
                                            <input class="btn btn-info" id="btn-slug-save" type="button" name="btn-slug-save" value="Save Changes">
                                        </div>
                                        <p class="text-muted"><i>The “slug” is the URL-friendly version of the name. It is usually all lowercase and contains only letters, numbers, and hyphens.</i></p>
                                    </div>
                                    
                                    <br>
                                    <input class="btn btn-primary" type="submit" name="save-changes" value="Save Changes"/>
                                </form>
                            </div>
                        </div>

                        <div class="col-sm-8 no-padding">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th class="check"><input type="checkbox" name="select-all-news"/></th>
                                            <th class="number">No</th>
                                            <th class="category-name">Name</th>
                                            <th class="category-slug">Slug</th>
                                            <th class="category-last-modified">Last Modified</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($categories as $index => $category)
                                        <tr>
                                            <td><input type="checkbox" name="select-categories" data-id="{{ $category->id }}"/></td>
                                            <td class="number">{{ $index+1 }}</td>
                                            <td>
                                                <div class="news-title"><a href="">{{ $category->category_name }}</a></div>
                                                <div class="news-action">
                                                    <a href="{{ action('Admin\CategoriesController@edit', [$category->id]) }}">Edit</a> | 
                                                    <a class="news-delete" href="{{ action('Admin\CategoriesController@confirm_delete', [$category->id]) }}">Delete</a></div>
                                            </td>
                                            <td>{{ $category->slug }}</td>
                                            <td class="category-last-modified">{{ date("d F y", strtotime($category->updated_at)) }}
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content_js')
    <script type="text/javascript">
        $(function(){
            $('.nav.main-menu li:eq(1)').addClass('active');
            $('.nav.nav-sidebar li:eq(2)').addClass('active');
        });
        // From validation
        $('#new-category').on('change', function(){
            $(this).val($.trim($(this).val()));
        });
        $("#slug").on('change','#field-slug', function(){
            $(this).val($.trim($(this).val()));
        });
        $('form :submit').on('click', function(){
            if($("#new-category").val() == '') {
                $("#new-category").after('<br><span class="error">This field is may not be empty.</span>');
            }
            if($("#slug-result").val() == '') {
                $("#edit-slug-category").after('<br><span class="error">This field is may not be empty.</span>');
            }
        });
        // Hapus pesan error ketika field input diklik
        $('#new-category, #edit-slug-category').on('click', function(){
            $(this).nextAll('.error:first').remove();
        });
        /* Konvesi Teks ke Slug */
        function text_slug(str){
            str = str.trim(); // trim
            str = str.toLowerCase();
            
            str = str.replace(/[^a-z0-9 -]/g, '-') // remove invalid chars
            .replace(/\s+/g, '-') // collapse whitespace and replace by -
            .replace(/-+/g, '-') // collapse dashes
            .replace(/^-+/g, '')  // collapse characters in the start
            .replace(/-+$/g, '');  // collapse characters in the end
            return str;
        }
        /* Fungsi untuk menampilkan Slug ketika load */
        $(function(){
            var text = $("#new-category").val();
            var slug = text_slug(text);
            $("#slug-result").text(slug);
            $("#duplicate-slug").attr('value', slug);
        });
        /* Fungsi untuk Slug */
        $("#new-category").on('change keyup paste click', function(){
            var text = $(this).val();
            var slug = text_slug(text);
            $("#slug-result").text(slug);
            $("#duplicate-slug").attr('value', slug);
        });
        /* Menampilkan Edit */
        $(function(){
            $("#edit-slug-category").css("display", "inline-block");
        });
        /* Edit Slug */
        $("#edit-slug-category").on('click', function(){
            var text = $("#slug-result").text();
            $("#slug-result").replaceWith("<input id='field-slug' class='form-control' type='text' name='field-slug' required value='" + text + "' autofocus />" );
            $("#edit-slug-category").css("display", "none");
            $("#btn-slug-save").css("display", "block");
        });
        /* Save Slug */
        $("#btn-slug-save").on('click', function(){
            var text = $("#field-slug").val();
            var slug = text_slug(text);
            $("#field-slug").replaceWith("<span id='slug-result'>" + slug + "</span>");
            $("#btn-slug-save").css("display", "none");
            $("#edit-slug-category").css("display", "inline-block");
            $("#duplicate-slug").attr('value', slug);
        });

    </script>
@endsection