<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <!--<link rel="icon" href="../../favicon.ico">-->

        <!-- Bootstrap core CSS -->
        <link href="/assets/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
	    <title>Durenworks - Admin/User/{{ $user->name }}/{{ $code == null ? "Change Password" : "Activate" }}</title>
        <style type="text/css">
            body#login {
                padding-top: 40px;
                padding-bottom: 40px;
                background-color: #eee;
            }

            #login .form-signin {
                max-width: 330px;
                padding: 15px;
                margin: 0 auto;
            }
            #login .form-signin .form-signin-heading,
            .form-signin .checkbox {
                margin-bottom: 10px;
            }
            #login .form-signin .checkbox {
                font-weight: normal;
            }
            #login .form-signin .form-control {
                position: relative;
                height: auto;
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                box-sizing: border-box;
                padding: 10px;
                font-size: 16px;
            }
            #login .form-signin .form-control:focus {
                z-index: 2;
            }
            #login .form-signin input[type="email"] {
                margin-bottom: -1px;
                border-bottom-right-radius: 0;
                border-bottom-left-radius: 0;
            }
            #login .form-signin input {
                margin-bottom: 10px;
                border-top-left-radius: 0;
                border-top-right-radius: 0;
            }
            .btn.btn-lg.btn-primary.btn-block{
                width: 100%;
                height: 45px;
                margin-bottom: 10px;
            }
        </style>
	</head>

    <body id="login">
		<div class="container">

			<form class="form-signin" action="{{ $post }}" method="post">

				<h2 class="form-signin-heading">Change Password</h2>
                @if(session('error') != null)
                    <p class="bg-danger"><b>{{ session('error') }}</b></p>
                @endif

                <input type="text" class="form-control" value="{{ $user->name }}" disabled>
                @if($code == null)
                    <div class="form-group {{ session('old_password') != "" ? "has-error" : "" }}">
                        <label for="old_password" class="sr-only">Old Password</label>
                        <input type="password" id="old_password" class="form-control" placeholder="Old Password" name="old_password" required autofocus>
                    </div>
                    <p class="text-danger">{{ session('old_password') }}</p>
                @else
                    <input type="hidden" name="code" value="{{ $code }}">
                @endif

                <div class="form-group {{ $errors->first('password') != "" ? "has-error" : "" }}">
    				<label for="password" class="sr-only">Password</label>
    				<input type="password" id="password" class="form-control" placeholder="Password Min.8 characters" name="password" required autofocus>
                </div>
                {!! $errors->first('password', '<p class="text-danger">:message</p>') !!}

                <div class="form-group">
    				<label for="password_confirmation" class="sr-only">Password Confirmation</label>
    				<input type="password" id="password_confirmation" class="form-control" name="password_confirmation" placeholder="Password Confirmation" required>
                </div>

                
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<button class="btn btn-lg btn-primary btn-block" type="submit">Save</button>
                @if(Auth::user() != null)
                    <a href="{{ route('dashboard') }}" title=""><button class="btn btn-lg btn-danger btn-block" type="button">Cancel</button></a>
                @endif
			</form>

		</div>
 <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="/assets/js/jquery-1.11.2.min.js"></script>
        <script src="/assets/js/bootstrap.min.js"></script>
        <!--<script src="../../assets/js/docs.min.js"></script>-->
    </body>
</html>
