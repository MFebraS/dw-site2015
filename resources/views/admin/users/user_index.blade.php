@extends('admin/users/layout_user')

@section('content_title')
	<title>Durenworks - Admin/Users</title>
@stop

@section('content_user')
	<div class="col-sm-12">
		<div id="page_title">
			<h1>Users <a class="new-post" href="{{ route('user_create') }}">New User</a></h1>
		</div>
		
		
		
		
		<div class="status_filter {{ $active[2] == '' || $active[2] == 'all' ? "active" : "" }}" data-status="all">
			All <span class="badge">{{ $count[0] }}</span>
		</div> | 
		<div class="status_filter {{ $active[2] == 'active' ? "active" : "" }}" data-status="active">
			Active <span class="badge">{{ $count[1] }}</span>
		</div> | 
		<div class="status_filter {{ $active[2] == 'inactive' ? "active" : "" }}" data-status="inactive">
			Inactive <span class="badge">{{ $count[2] }}</span>
		</div>
		@if(session('success') != null)
			<p class="bg-success text-center"><strong>{{ $success = session('success') }}</strong></p>
		@endif
		<table class="table table-hover">
			<thead>
				<th>No</th>
				<th>Name</th>
				<th>Email</th>
				<th>Level</th>
				<th>Employ</th>
				<th>Action</th>
			</thead>
			<tbody>
				@foreach($users as $key => $user)
					<tr data-href="{{ route('user_show', ['user' => $user->id]) }}" {{ $user->active == '-1' ? "style=background-color:#bfbfbf" : "" }}>
						<td>{{ $key+1 }}</td>
						<td>{{ $user->name }}<br>
							{!! $user->active == '1' ? "<span class=\"label label-success\">Active</span>" : ($user->active == '-1' ? "<span class=\"label label-danger\">Inactive</span>" : "<span class=\"label label-warning\">Pending</span>") !!}
						</td>
						<td>{{ $user->email }}</td>
						<td>{{ $user->level == 1 ? "Admin" : "Employee" }}</td>
						<td>{{ $user->employ }}</td>
						
						<td>
							@if($user->active == -1)
								<a href="{{ route('user_create', ['user' => $user->id]) }}"><button class="btn btn-success btn-sm">Re-active</button> </a>
								<button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#delete-confirmation" data-delete-target="{{ route('user_delete', ['user' => $user->id, 'delete' => 'true']) }}" data-delete-title="{{ $user->name }}">Delete</button>
							@else
							<a href="{{ route('user_edit', ['user' => $user->id]) }}"><button class="btn btn-primary btn-sm">Edit</button> </a> 
							@if($user->id != Auth::user()->id)
								<button class="btn btn-warning btn-sm" data-toggle="modal" data-target="#deactivate-confirmation" data-delete-target="{{ route('user_delete', ['user' => $user->id]) }}" data-delete-title="{{ $user->name }}">Deactivate</button>
							@endif
							@endif
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	<div class="modal fade" id="deactivate-confirmation" tabindex="-1" role="dialog" aria-labelledby="deactivate-confirmation-label" aria-hidden="true">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                <h4 class="modal-title" id="deactivate-confirmation-label">Deactivate Confirmation</h4>
	            </div>
	            <div class="modal-body">
	                <h5>Are you sure want to deactivate <span id="delete-title"></span>?</h5>
	            </div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
	                <a><button type="button" class="btn btn-primary" >YES</button></a>
	            </div>
	        </div>
	    </div>
	</div>
@stop

@section('content_user_js')
<script src="/{{ Config::get('custom_path.js') }}/delete-confirmation.js" type="text/javascript"></script>
<script type="text/javascript">
	$('tr[data-href]').on('click', function(e){

		if(!$(e.target).hasClass('btn')){
			window.location = $(this).attr('data-href');
		}
	});

	$('.status_filter').on('mouseenter', function(){
		if($(this).find('.badge').text() == 0){
			$(this).css('cursor', 'auto');
		}
	}).on('click', function(e){
		e.preventDefault();

		if($(this).find('.badge').text() != 0){
			window.location = window.location.pathname + "?status=" + $(this).attr('data-status');
		}
		
	});
	$(document).on('show.bs.modal', '#deactivate-confirmation', function (event) {
		var button = $(event.relatedTarget); // Button that triggered the modal
		var delete_title = button.data('delete-title'); 
		var delete_target = button.data('delete-target');
		var modal = $(this);
		var href = delete_target;
		modal.find('#delete-title').text(delete_title);
		modal.find('a').attr('href', href);
	});
</script>
@stop