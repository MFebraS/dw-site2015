@extends('admin/users/layout_user')

@section('content_title')
	<title>Durenworks - Admin/User/{{ $user->name }}/Edit</title>
@stop

@section('content_user')
<div class="row employees">
	
    <h2>Edit Profile</h2>
    <form action="{{ route('user_update', ['user' => $user->id]) }}" method="POST" enctype="multipart/form-data">
        <div class="col-sm-6 col-md-4">
            <div class="form-group {{ $errors->first('name') != "" ? "has-error" : "" }}">
            	<label class="control-label" for="name">Name<span class="required">*</span>:</label>
                <input type="text" class="form-control" id="name" name="name" value="{{ old('name') ? : $user->name }}" placeholder="Name" required>
                {!! $errors->first('name', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="form-group {{ $errors->first('email') != "" ? "has-error" : "" }}">
            	<label class="control-label" for="email">Email<span class="required">*</span>:</label>
                <input type="email" class="form-control" id="email" name="email" value="{{ old('email') ? : $user->email }}" placeholder="Email" required>
                {!! $errors->first('email', '<p class="text-danger">:message</p>') !!}
            </div>
            @if(Auth::user()->level == 1)
            <div class="form-group {{ $errors->first('employ') != "" ? "has-error" : "" }}">
            	<label class="control-label" for="employ">Employ<span class="required">*</span>:</label>
                <input type="text" class="form-control" id="employ" name="employ" value="{{ old('employ') ? : $user->employ }}" placeholder="Employ" required>
                {!! $errors->first('employ', '<p class="text-danger">:message</p>') !!}
            </div>
            @endif
            <div class="form-group {{ $errors->first('facebook') != "" ? "has-error" : "" }}">
                <label class="control-label" for="facebook">Facebook:</label>
                <input type="text" class="form-control" id="facebook" name="facebook" value="{{ old('facebook') ? : $user->facebook }}" placeholder="Facebook">
                {!! $errors->first('facebook', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="form-group {{ $errors->first('twitter') != "" ? "has-error" : "" }}">
                <label class="control-label" for="twitter">Twitter:</label>
                <input type="text" class="form-control" id="twitter" name="twitter" value="{{ old('twitter') ? : $user->twitter }}" placeholder="Twitter">
                {!! $errors->first('twitter', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="form-group {{ $errors->first('google') != "" ? "has-error" : "" }}">
                <label class="control-label" for="google">Google+:</label>
                <input type="text" class="form-control" id="google" name="google" value="{{ old('google') ? : $user->google }}" placeholder="Google+">
                {!! $errors->first('google', '<p class="text-danger">:message</p>') !!}
            </div>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            
        </div>

        <div class="col-sm-6 col-md-4">
            <div class="form-group {{ $errors->first('picture') != "" ? "has-error" : "" }}">
                <div class="container-picture">
                    <div id="processing" style="display:none;">
                        <img class="loading" src="/assets/images/white-loader.gif">
                    </div>
                    <img class="profile-picture" src="/{{ Config::get('custom_path.employees') }}/{{ $user->picture ? : "default.jpg" }}">

                </div>
                <small class="text-muted"><em>300px x 300px (1 : 1) for best result</em></small><br>
                <div class="btn btn-primary btn-file">
                    Select Picture<input id="upload" type="file" name="picture" accept="image/*"/>
                </div>
                <p class="text-danger">{{ session('error') }}</p>
            </div>

            <div class="form-group {{ $errors->first('about') != "" ? "has-error" : "" }}">
                <div class="pull-right">
                    <span id="char-left">380</span> characters left
                </div>
                <label class="control-label" for="about">About:</label>
                <textarea id="about" class="form-control" rows="3" id="about" name="about" placeholder="Max 120 characters" maxlength="120">{{ old('about') ? : $user->about }}</textarea>
                {!! $errors->first('about', '<p class="text-danger">:message</p>') !!}
            </div>
        </div>
        <div class="col-sm-12">
            <input type="submit" class="btn btn-primary" value="Update">
            <a href="{{ route('user_show', ['user' => $user->id]) }}" title=""><button type="button" class="btn btn-danger">Cancel</button></a>
            
        </div>
        
    </form>
    
</div>
@stop

@section('content_user_js')
<script src="/assets/js/trim.js" type="text/javascript"></script>
<script type="text/javascript">
    $('#upload').on('change', function(){
        var file = $(this)[0].files[0];
        var type = file.type.toString();
        if(type.indexOf("image/") != -1){
            $('.profile-picture').attr('src', URL.createObjectURL(file));
        }else{
            alert("Please select image file!");
            $('#upload').val('');
        }
    });

    $(function(){
        $('#char-left').text($('#about').attr('maxlength') - $('#about').val().length);
    });

    $('#about').on('paste keyup change', function(){
        $('#char-left').text($('#about').attr('maxlength') - $('#about').val().length);
    });
</script>
@stop