@extends('admin/users/layout_user')

@section('content_title')
	<title>Durenworks - Admin/User/{{ $user->name }}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
@stop

@section('content_user')
<div class="row employees">
    @if(session('success') != null)
        <p class="bg-success text-center"><strong>{{ $success = session('success') }}</strong></p>
    @endif
	<div class="col-md-offset-2 col-md-10 col-sm-12">
        <h1>
            <div class="container-picture">
                <div id="processing"  style="display:none;">
                    <img class="loading" src="/assets/images/white-loader.gif">
                </div>
                <img class="profile-picture" src="/{{ Config::get('custom_path.employees') }}/{{ $user->picture ? : "default.jpg" }}">
                
            </div>
            {{ $user->name }}
            <!-- <form action="{{ action('Admin\EmployeeController@upload') }}" method="post" enctype="multipart/form-data">
                <div class="btn btn-primary btn-file">
                    Change Picture<input id="upload" type="file" name="picture" accept="image/*" required/>
                </div>
            </form> -->
        </h1>
        
        
        <div class="col-sm-12 employee-detail">
            <div class="row row-detail">
                <div class="col-sm-12 col-md-7">
                    <div class="col-sm-4 text-right"><strong>Email:</strong></div>
                    <div class="col-sm-8">{{ $user->email }}</div>
                </div>
            </div>
            <div class="row row-detail">
                <div class="col-sm-12 col-md-7">
                    <div class="col-sm-4 text-right"><strong>Employ:</strong></div>
                    <div class="col-sm-8">{{ $user->employ ? : '-' }}</div>
                </div>
            </div>
            <div class="row row-detail">
                <div class="col-sm-12 col-md-7">
                    <div class="col-sm-4 text-right"><strong>Facebook:</strong></div>
                    <div class="col-sm-8">{{ $user->facebook ? : '-' }}</div>
                </div>
            </div>
            <div class="row row-detail">
                <div class="col-sm-12 col-md-7">
                    <div class="col-sm-4 text-right"><strong>Twitter:</strong></div>
                    <div class="col-sm-8">{{ $user->twitter ? : '-' }}</div>
                </div>
            </div>
            <div class="row row-detail">
                <div class="col-sm-12 col-md-7">
                    <div class="col-sm-4 text-right"><strong>Google+:</strong></div>
                    <div class="col-sm-8">{{ $user->google ? : '-' }}</div>
                </div>
            </div>
            <div class="row row-detail">
                <div class="col-sm-12 col-md-7">
                    <div class="col-sm-4 text-right"><strong>About:</strong></div>
                    <div class="col-sm-8">{{ $user->about ? : '-' }}</div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-7 employee-action">
            @if($user->active == -1)
            <div class="col-sm-4">
                <a href="{{ route('user_create', ['user' => $user->id]) }}"><button class="btn btn-success btn-sm">Re-active</button> </a>
            </div>
            <div class="col-sm-3">
                <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#delete-confirmation" data-delete-target="{{ route('user_delete', ['user' => $user->id, 'delete' => 'true']) }}" data-delete-title="{{ $user->name }}">Delete</button>
            </div>
            @else
            <div class="col-sm-4">
                <a class="btn btn-primary" href="{{ route('user_edit', ['user' => $user->id]) }}" title="">EDIT</a>
            </div>
            @if($user->id != Auth::user()->id)
            <div class="col-sm-3">
                <button class="btn btn-warning btn-sm" data-toggle="modal" data-target="#deactivate-confirmation" data-delete-target="{{ route('user_delete', ['user' => $user->id]) }}" data-delete-title="{{ $user->name }}">Deactivate</button>
            </div>
            @endif
            @endif
            
        </div>
    </div>
</div>
<div class="modal fade" id="deactivate-confirmation" tabindex="-1" role="dialog" aria-labelledby="deactivate-confirmation-label" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="deactivate-confirmation-label">Deactivate Confirmation</h4>
                </div>
                <div class="modal-body">
                    <h5>Are you sure want to deactivate <span id="delete-title"></span>?</h5>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
                    <a><button type="button" class="btn btn-primary" >YES</button></a>
                </div>
            </div>
        </div>
    </div>
@stop

@section('content_user_js')
<script src="/{{ Config::get('custom_path.js') }}/delete-confirmation.js" type="text/javascript"></script>
<script src="/assets/js/trim.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).on('show.bs.modal', '#deactivate-confirmation', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var delete_title = button.data('delete-title'); 
        var delete_target = button.data('delete-target');
        var modal = $(this);
        var href = delete_target;
        modal.find('#delete-title').text(delete_title);
        modal.find('a').attr('href', href);
    });
   /*{{-- $('#upload').on('change', function(){
        var files = $(this)[0].files;
        var data = new FormData();
        $.each(files, function(key, value){
            data.append(key, value);
        })
        
        //console.log(data);
        $('#processing').show();
        $.ajax({
            url: '/admin/employees/upload?id={{ $employee->id }}',
            headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'json',
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success: function(data, textStatus, jqXHR)
            {
                $('#processing').hide();
                if(data.status == 'ok'){
                    $('.profile-picture').attr('src', '/'+data.filename+'?'+Math.random()*Math.random());
                }
                
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                
            }
        });
        $.each($(this)[0].files, function(key, value){
            console.log(value.name);
        })
    });--}}*/
</script>
@stop