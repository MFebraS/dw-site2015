<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <!--<link rel="icon" href="../../favicon.ico">-->

        <!-- Bootstrap core CSS -->
        <link href="/assets/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
	    <title>Login</title>
        <style type="text/css">
            body#login {
                padding-top: 40px;
                padding-bottom: 40px;
                background-color: #eee;
            }

            #login .form-signin {
                max-width: 330px;
                padding: 15px;
                margin: 0 auto;
            }
            #login .form-signin .form-signin-heading,
            .form-signin .checkbox {
                margin-bottom: 10px;
            }
            #login .form-signin .checkbox {
                font-weight: normal;
            }
            #login .form-signin .form-control {
                position: relative;
                height: auto;
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                box-sizing: border-box;
                padding: 10px;
                font-size: 16px;
            }
            #login .form-signin .form-control:focus {
                z-index: 2;
            }
            #login .form-signin input[type="email"] {
                margin-bottom: -1px;
                border-bottom-right-radius: 0;
                border-bottom-left-radius: 0;
            }
            #login .form-signin input[type="password"] {
                margin-bottom: 10px;
                border-top-left-radius: 0;
                border-top-right-radius: 0;
            }
            .btn.btn-lg.btn-primary.btn-block{
                width: 100%;
                height: 45px;
            }
            .bg-danger{
                padding: 10px;
                border: 1px solid red;
                border-radius: 10px;
                color: red;
            }
            .bg-success{
                padding: 10px;
                border: 1px solid #3dce00;
                border-radius: 10px;
                color: #3dce00;
            }
        </style>
	</head>

    <body id="login">
		<div class="container">

			<form class="form-signin" action="{{ route('user_auth') }}" method="post">

				<h2 class="form-signin-heading">Please sign in</h2>
                @if(session('error') != null)
                    <p class="bg-danger"><b>{!! session('error') !!}</b></p>
                @endif
                @if(session('success') != null)
                    <p class="bg-success"><b>{!! session('success') !!}</b></p>
                @endif
				<label for="inputEmail" class="sr-only">Email address</label>
				<input type="email" id="inputEmail" class="form-control" placeholder="Email address" name="email" value="{{ old('email') }}" required autofocus>
				<label for="inputPassword" class="sr-only">Password</label>
				<input type="password" id="inputPassword" class="form-control" name="password" placeholder="Password" required>
                <a href="{{ route('user_getEmail') }}" style="float:right">Forgot Your Password?</a>

                <div class="checkbox" style="width:50%">
                    <label>
                    <input type="checkbox" id="remember" name="remember"> Remember me
                    </label>
                </div>
                    
				
                <input type="hidden" name="next" value="{{ Request::get('next') }}">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
                <a href="{{ action('HomeController@index') }}" title=""><span class="glyphicon glyphicon-arrow-left"></span>  Go to <strong>durenworks.com</strong></a>
			</form>

		</div>
 <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <!--<script src="../../assets/js/docs.min.js"></script>-->
    </body>
</html>
