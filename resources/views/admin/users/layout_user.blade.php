@extends('admin/layout')

@section('layout_2')
<div class="user">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3 col-md-2 sidebar">
                <ul class="nav nav-sidebar">
                    <li id="new-user" {{ $active[1] == 'new' ? "class=active" : "" }}><a href="{{ route('user_create') }}">New User</a></li>
                    <li id="all-users" {{ $active[1] == 'all' ? "class=active" : "" }}><a href="{{ route('user_index') }}">All Users</a></li>
                    <!-- <li id="employees" {{ $active[1] == '' ? "class=active" : "" }}><a href="{{ route('employees_index') }}">Employees</a></li> -->
                </ul>
            </div>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                @yield('content_user')
            </div>
        </div>
    </div>
</div>
@stop

@section('content_js')
    @yield('content_user_js')
@stop