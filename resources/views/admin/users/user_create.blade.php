@extends('admin/users/layout_user')

@section('content_title')
	<title>Durenworks - Admin/User/Register</title>
@stop

@section('content_user')
<div class="row">
	<div class="col-sm-8 col-md-6">
        <h2>Register here</h2>
        <form action="{{ route('user_store') }}" method="POST">
            <div class="form-group {{ $errors->first('name') != "" ? "has-error" : "" }}">
            	<label class="control-label" for="name">Name<span class="required">*</span>:</label>
                <input type="text" class="form-control" id="name" name="name" value="{{ $reactive != null ? $reactive['name'] : old('name') }}" placeholder="Name" required>
                {!! $errors->first('name', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="form-group {{ $errors->first('email') != "" ? "has-error" : "" }}">
            	<label class="control-label" for="email">Email<span class="required">*</span>:</label>
                <input type="email" class="form-control" id="email" name="email" value="{{ $reactive != null ? $reactive['email'] : old('email') }}" placeholder="Email" required>
                {!! $errors->first('email', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="form-group {{ $errors->first('employ') != "" ? "has-error" : "" }}">
                <label class="control-label" for="employ">Employ<span class="required">*</span>:</label>
                <input type="text" class="form-control" id="employ" name="employ" value="{{ $reactive != null ? $reactive['employ'] : old('employ') }}" placeholder="Employ" required>
                {!! $errors->first('employ', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="form-group {{ $errors->first('level') != "" ? "has-error" : "" }}">
                <label class="control-label" for="level" required>Level<span class="required">*</span>:</label>
                <select class="form-control" id="level" name="level">
                    <option value="">-- Select User's Level --</option>
                    <option value="1" {{ $reactive != null ? ($reactive['level'] == '1' ? "selected" : "") : (old('level') == '1' ? "selected" : "") }}>Admin</option>
                    <option value="0" {{ $reactive != null ? ($reactive['level'] == '0' ? "selected" : "") : (old('level') == '0' ? "selected" : "") }}>Employee</option>
                </select>
                {!! $errors->first('level', '<p class="text-danger">:message</p>') !!}
            </div>
            <!-- <div class="form-group">
            	<label for="password">Password</label>
                <input type="password" class="form-control" name="password" placeholder="Password. Min 8 character" required>
                {!! $errors->first('password', '<p class="text-danger">:message</p>') !!}
            </div>
            <div class="form-group">
            	<label for="password">Password Confirmation</label>
                <input type="password" class="form-control" name="password_confirmation" placeholder="Password Confirmation" required>
                {!! $errors->first('password_confirmation', '<p class="text-danger">:message</p>') !!}
            </div> -->
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="submit" class="btn btn-primary" value="{{ $reactive == null ? "Add" : "Re-active" }}">
        </form>
    </div>
</div>

@stop

@section('content_user_js')
<script src="/assets/js/trim.js" type="text/javascript"></script>
<script type="text/javascript">
	$('#new-user').addClass('active');
</script>
@stop