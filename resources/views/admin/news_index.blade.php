@extends('admin/layout-news')

@section('content_title')
    <title>Durenworks - Admin/News</title>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3 col-md-2 sidebar">
                <ul class="nav nav-sidebar">
                <li><a href="{{ action('Admin\NewsController@create') }}">New Post</a></li>
                <li><a href="{{ action('Admin\NewsController@index') }}">All Post</a></li>
                <li><a href="{{ action('Admin\CategoriesController@index') }}">Categories</a></li>
                </ul>
            </div>

            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                <div id="news-admin">
                    <div id="all-post">
                        <h1 class="page-header">Posts
                            <a class="new-post" href="{{ action('Admin\NewsController@create') }}">New Post</a>
                        </h1> 
                        <div class="post-status">
                            <ul class="nav">
                                <li class=" {{ (Request::get('status') == '' || Request::get('status') == 'All') ? "active" : "" }}"><a href="{{ action('Admin\NewsController@index') }}?status=All">All <span class="badge"> {{ $count_all_posts }} </span></a></li>|
                                <li class="right-left-fence {{ Request::get('status') == 'Published' ? "active" : "" }}"><a href="{{ action('Admin\NewsController@index') }}?status=Published">Published <span class="badge"> {{ $count_published }} </span></a></li>|
                                <li class=" {{ Request::get('status') == 'Draft' ? "active" : "" }}"><a href="{{ action('Admin\NewsController@index') }}?status=Draft">Drafts <span class="badge"> {{ $count_draft }} </span></a></li>
                            </ul>
                            <div class="search">
                                <form action="{{ action('Admin\NewsController@index') }}" method="GET">
                                    <input  id="search_post" type="search" name="search_post" placeholder="Search Post"/>
                                </form>
                            </div>
                        </div>
                        
                        <div class="filter">
                            <form action="{{ action('Admin\NewsController@index') }}" method="GET">
                                <select name="category">
                                    <option value="all">All categories</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}"> {{ $category->category_name }} </option>
                                    @endforeach
                                </select>
                                <input class="mybtn" type="submit" value="Filter" name="filter"/>
                                <span class="space"></span>
                                
                                {!! Form::select('per_page', [10=>10, 25=>25, 50=>50], session('lim')) !!}

                                <span>Posts</span>
                                <input class="mybtn" type="submit" value="Apply" name="limit"/>
                            </form>
                        </div>
                        @if(session('success') != null)
                        <div class="row">
                            <p class="bg-success col-sm-12 text-center"><strong>{{ $success = session('success') }}</strong></p>
                        </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th class="number">No</th>
                                        <th class="title">Title</th>
                                        <th class="status">Status</th>
                                        <th class="author">Author</th>
                                        <th class="categories">Category</th>
                                        <th >Published</th>
                                        <th >Last Update</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if($all_posts[0] == null)
                                    <table id="null-table">
                                        <tr>
                                            <td> <em>{{ $message }}</em> </td>
                                        </tr>
                                    </table>
                                @else
                                    @foreach($all_posts as $index => $post)
                                    <tr>
                                        <td>{{ $index+1+(($all_posts->currentPage()-1)*10) }}</td>
                                        <td>
                                            <div class="news-title"><a href="{{ action('Admin\NewsController@edit', [$post->id]) }}">{{ $post->title }}</a></div>
                                            <div class="news-action">
                                                <a href="{{ action('NewsController@news_detail', [$post->permalink]) }}">Preview</a> | 
                                                <a href="{{ action('Admin\NewsController@edit', [$post->id]) }}">Edit</a> | 
                                                <span class="news-delete" data-toggle="modal" data-target="#delete-confirmation" data-id="{{ $post->id }}" data-delete-title="{{ $post->title }}">Delete</span>
                                            </div>
                                        </td>
                                        <td> {{ $post->status }} </td>
                                        <td><a href="">{{ $post->user->name }}</a></td>
                                        <td> {{ $post->category->category_name }} </td>
                                        @if($post->published_date == null)
                                            <td > - </td>
                                        @else
                                            <td> {{ date("j M Y", strtotime($post->published_date)) }} </td>
                                        @endif
                                        
                                        <td>{{ date("j F Y", strtotime($post->updated_at)) }}</td>
                                    </tr>
                                    @endforeach
                                @endif
                                </tbody>
                                <div class="modal fade" id="delete-confirmation" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="delete-confirmation-label">Delete Confirmation</h4>
                                            </div>
                                            <div class="modal-body">
                                                <h5>Are you sure want to delete <span id="delete-title"></span>?</h5>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
                                                <a> <button type="button" class="btn btn-primary">YES</button></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- </div> -->
                            </table>

                            {!! $all_posts->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content_js')
<script type="text/javascript">
    $(function(){
        $('.nav.main-menu li:eq(1)').addClass('active');
        $('.nav.nav-sidebar li:eq(1)').addClass('active');
        $('#delete-confirmation').on('show.bs.modal', function(event){
            var button = $(event.relatedTarget);
            var delete_target = button.data('id');
            var delete_title = button.data('delete-title');
            var url = $(location).attr('href');
            var route = "/admin/news/"+ delete_target + "/delete";
            $(this).find('a').attr('href', route);
            $(this).find('#delete-title').text(delete_title);
        });
    });

    $('#search_post').on('keypress',function(e){
        if(e.which==13){
            e.preventDefault();
            var q = $(this).val();
            q = q.trim(q);
            q = q.replace(/\s+/,'+');
            window.location = window.location.pathname + '?q=' + q;
        }
    })
</script>

@endsection