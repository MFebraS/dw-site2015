@extends('admin/layout-news')

@section('content_title')
    <title>Durenworks - Admin/News/Confirm Delete Category</title>
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <ul class="nav nav-sidebar">
                <li><a href="{{ action('Admin\NewsController@create') }}">New Post</a></li>
                <li><a href="{{ action('Admin\NewsController@index') }}">All Post</a></li>
                <li><a href="{{ action('Admin\CategoriesController@index') }}">Categories</a></li>
            </ul>
        </div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <div id="news-admin">
                <div id="categories">
                    <h1 class="page-header">Delete Category</h1>

                    <div id="categories-content">
                        <div class="row">
    							        <div class="col-sm-12">
                            <p class="attention">You will delete <em>{{ $category->category_name }}</em> category. There are news with this category.</p>
    							          @if(count($check_in_news[0]) <= 0 )
                              <p>There is no News with this category</p>
                              <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete-confirmation">Delete</button>
                              <div class="modal fade" id="delete-confirmation" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                  <div class="modal-dialog">
                                      <div class="modal-content">
                                          <div class="modal-header">
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                              <h4 class="modal-title" id="delete-confirmation-label">Delete Confirmation</h4>
                                          </div>
                                          <div class="modal-body">
                                              <h5>Are you sure want to delete this category?</h5>
                                          </div>
                                          <div class="modal-footer">
                                              <a href="{{ action('Admin\CategoriesController@destroy', [$category->id]) }}"> <button type="button" class="btn btn-primary">YES</button></a>
                                              <button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                            @else
                              <p>Please select 1 option below :</p>
      							
                							<div class="col-md-3">
                							<p>Select a new category for its News and delete the category</p>
              								<form action="{{ action('Admin\CategoriesController@destroy_cat_only', [$category->id]) }}" method="POST">
              									<input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <select name="select_new_category">
              										@foreach ($news_categories as $categories)
                                      <option value="{{ $categories->id }}"> {{ $categories->category_name }} </option>
                                  @endforeach
              									</select>
                                <br><br>
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete-confirmation">Select and Delete</button>
                                <div class="modal fade" id="delete-confirmation" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                  <div class="modal-dialog">
                                      <div class="modal-content">
                                          <div class="modal-header">
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                              <h4 class="modal-title" id="delete-confirmation-label">Delete Confirmation</h4>
                                          </div>
                                          <div class="modal-body">
                                              <h5>Are you sure want to delete this category?</h5>
                                          </div>
                                          <div class="modal-footer">
                                              <button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
                                              <input type="submit" class="btn btn-primary" value="YES"/>
                                          </div>
                                      </div>
                                  </div>
                                </div> <!-- End Modal -->
                              </form>
              							</div>

              							<div class="col-md-1">
              								<p>or</p>
              							</div>
              							<div class="col-md-3">
              								<p>Delete all News with this category.</p>
                              <input type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete-all-confirmation" value="Delete Category and News">
                              <div class="modal fade" id="delete-all-confirmation" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                  <div class="modal-dialog">
                                      <div class="modal-content">
                                          <div class="modal-header">
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                              <h4 class="modal-title" id="delete-confirmation-label">Delete All Confirmation</h4>
                                          </div>
                                          <div class="modal-body">
                                              <h5>Are you sure want to delete this category?</h5>
                                          </div>
                                          <div class="modal-footer">
                                              <button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
                                              <a href="{{ action('Admin\CategoriesController@destroy_cat_and_news', [$category->id]) }}"> <button type="button" class="btn btn-primary">YES</button></a>
                                          </div>
                                      </div>
                                  </div>
                              </div> <!-- End Modal -->
              							</div>
              						</div>
                        </div>
                          
                          @endif
              						<br><br>
              						<div class"row">
                            <div class="col-sm-12" style="padding: 0;">
              							 <a class="btn btn-primary back-left" href="{{ action('Admin\CategoriesController@index') }}"> Back </a>
                            </div>
              						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content_js')
    <script>
        $(function(){
            $('.nav.main-menu li:eq(1)').addClass('active');
            $('.nav.nav-sidebar li:eq(2)').addClass('active');
        });
    </script>
@endsection