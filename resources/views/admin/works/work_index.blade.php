@extends('admin/works/layout_work')

@section('content_title')
	<title>Durenworks - Admin/Works</title>
@stop

@section('content_work')
	<div class="search">
        <form>
            <input id="search" type="search" class="pull-right" value="{{ $filter['q'] }}" placeholder="Search Works"/>
        </form>
    </div>
	<h1>Works <a class="new-post" href="{{ route('works_create') }}">New Work</a></h1>
	
	<div class="status_filter {{ $filter['status'] == '' || $filter['status'] == 'all' ? "active" : "" }}" data-status="all">
		All <span class="badge">{{ $all }}</span>
	</div> | 
	<div class="status_filter {{ $filter['status'] == 'published' ? "active" : "" }}" data-status="published">
		Publish <span class="badge">{{ $published }}</span>
	</div> | 
	<div class="status_filter {{ $filter['status'] == 'draft' ? "active" : "" }}" data-status="draft">
		Draft <span class="badge">{{ $draft }}</span>
	</div>

	<div id="per_page">
		<form class="form-inline" action="{{ route('works_setfilter') }}" method="get">
			<select class="form-control" name="client" style="margin-right:20px;width:200px">
				<option value="all">All Client</option>
				@foreach($clients as $client)
					<option value="{{ $client->name }}" {{ Request::get('client') == $client->name ? "selected" : "" }}>{{ $client->name }}</option>
				@endforeach
			</select>
			<label for="perpage">Show: </label>
			<select class="form-control" name="perpage">
				<option value="10" {{ $works->perPage() == '10' ? "selected" : "" }}>10</option>
				<option value="25" {{ $works->perPage() == '25' ? "selected" : "" }}>25</option>
				<option value="50" {{ $works->perPage() == '50' ? "selected" : "" }}>50</option>
				<option value="100" {{ $works->perPage() == '100' ? "selected" : "" }}>100</option>
				<option value="all" {{ $works->perPage() == $works->total() ? "selected" : ""}}>All</option>
			</select>
			<input class="mybtn" type="submit" value="Apply"/>
			<a href="{{ route('works_getReorder') }}" class="btn btn-default pull-right">Reorder Works</a>
			{{-- <button type="button" class="btn btn-default pull-right" data-toggle="modal" data-target="#work-reorder">Reorder Client</button> --}}
		</form>
		
	</div>
	@if(session('success') != null)
	<div class="row">
		<p class="bg-success col-sm-12 text-center"><strong>{{ $success = session('success') }}</strong></p>
	</div>
	@endif
	<div class="table-responsive">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th class="number">No</th>
                    <th class="image">Image</th>
                    <th class="project">Project Title</th>
                    <th class="client">Client</th>
                    <th class="action">Action</th>
                </tr>
            </thead>
            <tbody id="table-body">
            	<tr class="loading-container" style="display:none">
        			<td colspan="6" class="text-center"><img src="/assets/images/black-loader.gif" alt=""></td>
        		</tr>
            	@if($works->total() == 0)
            		<tr>
            			<td colspan="6" class="text-center"><b>No Record</b></td>
            		</tr>
            	@else
            	@foreach($works as $key => $work)
            		<tr data-href="{{ route('works_show', ['works' => $work->slug]) }}">
	            		<td>{{ $works->perPage()*($works->currentPage()-1)+($key+1) }}</td>
	            		<td class="image">
	            			<img src="/{{ Config::get('custom_path.works') }}/{{ $work->pictures->where('sequence', '1')->first()->picture_name }}?{{ rand()*rand() }}">
	            		</td>
	            		<td class="project">
	            			<b>{{ $work->project_title }}</b> {!! $work->status == "published" ? "<span class=\"label label-success\">Published</span>" : "<span class=\"label label-danger\">Draft</span>" !!}<br>
	            			<small>
	            				@foreach($work->tags as $i => $tag)
	            					@if($i >= 3)
	            						...
	            					@else
	            						{{ $tag->tag_name.($i < $work->tags->count()-1 ? ", " : " ") }}
	            					@endif

	            				@endforeach
	            			</small><br>
	            			<small>{{ $work->link ? : "-" }}</small>
	            		</td>
	            		<td class="client">
	            			<p><b>{{ $work->client->name }}</b></p>
	            			<small>{{ $work->client->website ? : "-" }}</small>
	            		</td>
	            		<td class="action">
	            			<a href="{{ route('works_edit', ['works' => $work->slug]) }}"><button class="btn btn-primary btn-sm">Edit</button></a>
	            			<button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#delete-confirmation" data-delete-target="{{ route('works_delete', ['works' => $work->slug]) }}" data-delete-title="{{ $work->project_title }}">Delete</button>
	            		</td>
	            	</tr>
            	@endforeach               
            	@endif
            </tbody>
        </table>
        @if($works->total() > 0)
        	{!! $works->render() !!}
        @endif
    </div>
	
	<div class="modal fade" id="view_pictures" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel"></h4>
				</div>
				<div class="modal-body text-center">

				</div>
			</div>
		</div>
	</div>
	

	{{-- <div class="modal fade" id="work-reorder" tabindex="-1" role="dialog" aria-labelledby="add-picture" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Reorder Work</h4>
                </div>

                <div class="modal-body">
                    <ul id="selected_files">
						@foreach($orders as $key => $order)
							<li id="{{ $order->id }}" class="list ui-state-default">
								<div class="image-container">
									
									<img src="/{{ Config::get('custom_path.works') }}/{{ $order->pictures->first()->picture_name }}">
								</div>
								
                        	</li>
						@endforeach
					</ul>
                    
                </div>
                <div class="modal-footer">
                    <form action="{{ route('works_reorder') }}" method="post" accept-charset="utf-8">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input id="images_order" type="hidden" name="order">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </form>
                    
                </div>
            </div>
        </div>
    </div> --}}
@stop

@section('content_work_js')
	<script type="text/javascript" src="https://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
	<script src="/{{ Config::get('custom_path.js') }}/delete-confirmation.js" type="text/javascript"></script>
	<script type="text/javascript">
	
	
	$('tr[data-href]').on('click', function(e){

		if(!$(e.target).hasClass('btn')){
			window.location = $(this).attr('data-href');
		}
	});
	$('#search').on('keypress', function(e){
		if(e.which == 13){
			e.preventDefault();
			var q = $.trim($(this).val());
			q = q.replace(/ /g, "+");

			var keySearch = ["q"];
			var valueSearch = [q];

			addSearch(keySearch, valueSearch);
			/*var q = $.trim($(this).val());
			//console.log(window.location.pathname);
			if(q == ""){
				window.location = window.location.pathname;
			}else{
				var q = q.replace(" ", "+");
				window.location = window.location.pathname + "?q=" + q;
			}*/
		}
	});
	$(function(){
		$('#view_pictures').on('show.bs.modal', function(e){
			var image = $(e.relatedTarget);
			$(this).find('img').attr('src', image.attr('src'));
			$(this).find('.modal-title').text(image.data('title'));
		});
		/*
		$.fn.showFiltered = function(filter){
			if((!$(this).hasClass('active') && $(this).find('.badge').text() != 0)){
				var status = filter;
	            var table_body = $('#table-body');
	            var loader = table_body.find('.loading-container');
	            $('.status_filter').each(function(){
	            	$(this).removeClass('active');
	            });
	            $(this).addClass('active');
	            table_body.empty();
	            loader.show();
	            table_body.append(loader);
	            $.get("/admin/works-filter?status=" +status, function(data){
	                //success data
	                loader.hide();
	                console.log(data);
	                if(data[0] == null){
	                	table_body.append('<tr><td colspan="6" class="text-center"><b>No Record</b></td></tr>');
	                }else{
		                $.each(data, function(index, works){
		                    table_body.append('<tr><td class="check"><input type="checkbox" name="select-works" data-id="'+works.id+'"/></td><td>'+(index+1)+'</td><td class="image"><img src="/'+works.picture_path+'/'+works.picture+'"></td><td class="project"><a href="/admin/works/'+works.id+'"><b>'+works.project_title+'</b></a><br><small>'+works.tags+'</small></td><td class="client"><p><b>'+works.client_name+'</b></p><small>'+works.client_website+'</small></td><td class="action"><a href="/admin/works/'+works.id+'/edit"><button class="btn btn-primary btn-sm">Edit</button></a><button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#delete-confirmation" data-delete-target="'+works.id+'" data-delete-title="'+works.project_title+'">Delete</button></td></tr>');
		                        
		                });
	            	}
	            });
	        }
		};*/
	});
	$('.status_filter').on('mouseenter', function(){
		if($(this).find('.badge').text() == 0){
			$(this).css('cursor', 'auto');
		}
	}).on('click', function(e){
		e.preventDefault();

		if($(this).find('.badge').text() != 0){
			var keySearch = ["status"];
			var valueSearch = [$(this).attr('data-status')];
			addSearch(keySearch, valueSearch);
		}
		
	});
	$('form input[type=submit]').on('click', function(e){
		e.preventDefault();
		var keySearch = ["client", "perpage"];
		var valueSearch = [
			$('select[name=client]').val(),
			$('select[name=perpage]').val()
		];
		addSearch(keySearch, valueSearch);
	});
	function addSearch(keySearch, valueSearch){
		var connector = window.location.search == "" ? "?" : "&";
		var newLocation = "{{ route('works_setfilter') }}" + window.location.search;
		for(var i = 0; i<keySearch.length; i++){
			if(i == 0){
				newLocation += connector + keySearch[i] + "=" + valueSearch[i];
			}else{
				newLocation += "&" + keySearch[i] + "=" + valueSearch[i];
			}
		};
		window.location = newLocation;
		//console.log(newLocation);
	}

	$("#selected_files").sortable({
    placeholder: "highlight",
    create: function (event, ui){
        var image_order = $(this).sortable('toArray').toString();
        $("#images_order").val(image_order);
    },
    start: function (event, ui) {
        ui.item.toggleClass("highlight");
    },
    stop: function (event, ui) {
        ui.item.toggleClass("highlight");
    },
    update: function(event, ui) {
        var image_order = $(this).sortable('toArray').toString();
        $("#images_order").val (image_order);
    }
});
	</script>
@stop