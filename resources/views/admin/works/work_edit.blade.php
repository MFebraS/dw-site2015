@extends('admin/works/layout_work')

@section('content_title')
	<title>Durenworks - Admin/Work/{{ $work->project_title }}/Edit</title>
	<link href="/assets/css/bootstrap-tagsinput.css" rel="stylesheet">
	<meta name="csrf-token" content="{{ csrf_token() }}" />
@stop

@section('content_work')
	<h1>Edit {{ $work->project_title }}</h1>

	<form method="POST" class="work-form" action="{{ route('works_update', ['works' => $work->slug]) }}"  enctype="multipart/form-data">
		<div class="row">
			<div class="col-md-8">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" value="{{ $tags }}" name="existing_tags">
				<input id="images_order" type="hidden" name="images_order">
				<input id="images_delete" type="hidden" name="images_delete">
				
				<div class="col-md-12">
					<div class="form-group {{ $errors->first('project_title') != "" ? "has-error" : "" }}">
						<label class="control-label" for="project_title">Project Title<span class="required">*</span>:</label>
						<input type="text" id="project_title" class="form-control" name="project_title" value="{{ old('project_title') ? : $work->project_title }}" placeholder="Enter Project Title Here"/>
						{!! $errors->first('project_title', '<p class="text-danger">:message</p>') !!}
					</div>
					<div class="form-group {{ $errors->first('tag_name') != "" ? "has-error" : "" }}">
						<label class="control-label" for="tags">Tags<span class="required">*</span>: </label> <input id="tags" type="text" class="btn btn-default" name="tag_name" data-role="tagsinput" value="{{ old('tag_name') ? : $tags }}"/>
						{!! $errors->first('tag_name', '<p class="text-danger">:message</p>') !!}
					</div>
					<div class="form-group">
						<div class="form-inline {{ $errors->first('link') != "" ? "has-error" : "" }}">
							<label class="control-label" for="link">Link: </label>  <input id="link" class="form-control" type="text" name="link"value="{{ old('link') ? : $work->link }}" placeholder="Project's Link"/>
							{!! $errors->first('link', '<p class="text-danger">:message</p>') !!}
						</div>
					</div>
					<div class="form-group">
						<div class="form-inline {{ $errors->first('order') != "" ? "has-error" : "" }}">
							<label class="control-label" for="order">Order: </label>  
							<select id="order" class="form-control" name="order">
								<option>-- Place After --</option>
								@foreach($orders as $order)
									<option value="{{ $order->order }}" {{ old('order') == $order->order ? "selected" : ($work->order == $order->order ? "selected" : "") }}>{{ $order->project_title }}</option>
								@endforeach
							</select>
							{!! $errors->first('order', '<p class="text-danger">:message</p>') !!}
						</div>
					</div>
				</div>
				<!-- <div class="col-sm-6">
					<div class="form-group">
						<label for="client_name">Client's Name:</label>
						<input type="text" id="client_name" class="form-control" name="client_name" value="{{ old('client_name') ? : $work->client_name }}" placeholder="Enter client's name here">
						{!! $errors->first('client_name', '<p class="text-danger">:message</p>') !!}
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<label for="client_website">Client's Website:</label>
						<input type="text" id="client_website" class="form-control" name="client_website" value="{{ old('client_website') ? : $work->client_website }}"  placeholder="Enter client's website here">
						{!! $errors->first('client_website', '<p class="text-danger">:message</p>') !!}
					</div>
				</div> -->
				<div class="client col-sm-6" style="height:{{ old('client') == 'new' ? "auto" : "100px" }}">
					<div class="form-group">
						<div class="form-group {{ $errors->first('client') != "" ? "has-error" : "" }}">
							<label  class="control-form" for="client">Client<span class="required">*</span>:</label>
							<select id="client" name="client" class="form-control" required>
								<option value="">-- Select Client --</option>
								<option value="new" {{ old('client') == "new" ? "selected" : "" }}>-- Add New Client --</option>
								{{ $old_logo = null }}
								@foreach($clients as $client)
									<option value="{{ $client->id }}" data-logo="/{{Config::get('custom_path.clients')}}/{{ $client->logo }}" {{ old('client') != null ? (old('client') == $client->id ? "selected" : "") : ($work->client->id == $client->id ? "selected" : "") }}>{{ $client->name }}</option>
									@if(old('client') == $client->id)
										{{ $old_logo = $client->logo }}
									@endif
								@endforeach
							</select>
							{!! $errors->first('client', '<p class="text-danger">:message</p>') !!}
						</div>

						<div id="new-client" style="opacity:{{ old('client') == 'new' ? "1" : "0" }}">
							<div class="form-inline {{ $errors->first('name') != "" ? "has-error" : "" }}">
		                        <label  class="control-form" for="name">Name<span class="required">*</span>:</label>
		                        <input type="text" id="name" class="form-control pull-right" name="client_name" value="{{ old('client_name') }}"/>
		                        {!! $errors->first('name', '<p class="text-danger">:message</p>') !!}
		                    </div>
		                    <div class="form-inline">
		                        <label  class="control-form" for="website">Website:</label>
		                        <input type="text" id="website" class="form-control pull-right" name="client_website" value="{{ old('client_website') }}"/>
		                    </div>
						</div>
					</div>
				</div>
				<div class="client col-sm-6" style="height:100px;margin-top:25px">
					<div class="form-group">
						<div class="container-logo">
                            <img class="logo-preview" src="/{{ Config::get('custom_path.clients') }}/{{ old('client') != null ? ($old_logo ? : "default.jpg") : ($work->client->logo ? : "default.jpg") }}">
                        </div>
                        <small id="nb" class="text-muted" style="opacity:{{ old('client') == 'new' ? "1" : "0" }}"><em>450px x 150px (9 : 3) for best result</em></small><br>
                        <div class="btn btn-primary btn-file" style="opacity:{{ old('client') == 'new' ? "1" : "0" }}">
                            Upload Logo<input id="logo" type="file" name="logo" accept="image/*">
                        </div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label>Work's Pictures<span class="required">*</span>:</label><br>
						<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#work-pictures">Select Pictures</button>
						<small class="text-muted"><em><span id="pictures-count">0</span> pictures selected</em></small><br>
						{!! session('error') != null ? '<p class="text-danger">'.session('error').'</p>' : '' !!}
					</div>
					<div class="form-group {{ $errors->first('description') != "" ? "has-error" : "" }}">
						<div class="pull-right">
							<span id="char-left">380</span> characters left
						</div>
						<label  class="control-form" for="description">Description<span class="required">*</span>:</label>
						<textarea id="description" class="form-control" name="description" rows="5" placeholder="Project's Description. Max 380 characters" maxlength="380">{{ old('description') ? : $work->description }}</textarea>
						{!! $errors->first('description', '<p class="text-danger">:message</p>') !!}
					</div>
					<div class="form-group">
						<span class="btn btn-danger pull-right" data-toggle="modal" data-target="#delete-confirmation">Delete</span>
						<input id="save" class="btn btn-primary" name="save" type="submit" value="Save As Draft">
						<input id="submit" class="btn btn-success" name="publish" type="submit" value="PUBLISH"/>	
					</div>
				</div>
			</div>
		</div>
	</form>
	<div class="modal fade" id="work-pictures" tabindex="-1" role="dialog" aria-labelledby="add-picture" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Work's Pictures</h4>
                    </div>

                    <div class="modal-body">
                        <!-- Tab Contents -->
                        <div class="btn btn-primary btn-file">
							Upload Images<input id="upload" type="file" accept="image/*" multiple/>
						</div>
						<small id="nb" class="text-muted"><em>600px x 450px (4 : 3) for best result</em></small><br>
						<ul id="selected_files">
							@if($pictures != false)
								@foreach($pictures as $key => $picture)
									<li id="{{ $picture->picture_name }}" data-id="{{ explode(".", $picture->picture_name)[0] }}" class="list">
										<div class="image-container">
											<img id="image{{ explode(".", $picture->picture_name)[0] }}" src="/{{ Config::get('custom_path.works') }}/{{ $picture->picture_name }}?{{ rand()*rand() }}">
										</div>
										
                                	</li>
								@endforeach
							@endif
							@if($temp_pictures != false)
								@foreach($temp_pictures as $key => $picture)
									<li id="{{ $picture }}" data-id="{{ explode(".", $picture)[0] }}" class="list temp">
										<div class="image-container">
											<img id="image{{ explode(".", $picture)[0] }}" src="/{{ Config::get('custom_path.works') }}/temp/{{ $picture }}?{{ rand()*rand() }}">
										</div>
                                	</li>
								@endforeach
							@endif
						</ul>
                        
                    </div>
                    <div class="modal-footer">
                    	<button type="button" class="btn btn-primary" data-dismiss="modal">Finish</button>
                    </div>
                </div>
            </div>
        </div>
	<div class="modal fade" id="delete-confirmation" tabindex="-1" role="dialog" aria-labelledby="delete-confirmation-label" aria-hidden="true">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="delete-confirmation-label">Delete Confirmation</h4>
				</div>
				<div class="modal-body">
					<h5>Are you sure want to delete {{ $work->project_title }}?</h5>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
					<a href="{{ route('works_delete', ['works' => $work->slug]) }}"><button type="button" class="btn btn-primary">YES</button></a>
				</div>
			</div>
		</div>
	</div>
@stop

@section('content_work_js')
	<script type="text/javascript" src="https://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
	
	<script type="text/javascript" src="/assets/js/bootstrap-tagsinput.min.js"></script>
	<script type="text/javascript" src="/{{ Config::get('custom_path.js') }}/uploadTemp.js"></script>
	<script type="text/javascript">
		var submit = false;
		var id = parseInt({{ $last }});
		$('#tags').tagsinput({
			onTagExists: function(item, $tag) {
				$tag.hide.fadeIn();
			}
		});

		$('#client').on('change', function(){
			if($('option:selected', this).data('logo') != undefined){
				$('.logo-preview').attr('src', $('option:selected', this).data('logo'));
			}else{
				$('.logo-preview').attr('src', "/{{ Config::get('custom_path.clients') }}/default.jpg");
			}
			if($('#client').val() == "new"){
				$('#new-client').animate({'opacity': 1}, 400);
				$('#logo').parent().animate({'opacity': 1}, 400);
				$('.client').animate({"height": "150px"}, 400);
			}else{
				$('#logo').val('');
				$('#new-client').animate({'opacity': 0}, 400);
				$('#logo').parent().animate({'opacity': 0}, 400);
				$('.client').animate({"height": "100px"}, 400);
			}
		});

		$('#logo').on('change', function(){
		    var file = $(this)[0].files[0];
		    var type = file.type.toString();
		    if(type.indexOf("image/") != -1){
		        $('.logo-preview').attr('src', URL.createObjectURL(file));
		    }else{
		    	$('.logo-preview').attr('src', "/{{ Config::get('custom_path.clients') }}/default.jpg");
		        $('#logo').val('');
		        alert('Please select image file!');
		    }
		});

		$(function(){
			$('#char-left').text($('#description').attr('maxlength') - $('#description').val().length);
		});

		$('#description').on('paste keyup change', function(){
			$('#char-left').text($('#description').attr('maxlength') - $('#description').val().length);
		});
	</script>
@stop