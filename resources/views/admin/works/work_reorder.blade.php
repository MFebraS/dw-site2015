@extends('admin/works/layout_work')

@section('content_title')
	<title>Durenworks - Admin/Works/Reorder</title>
@stop

@section('content_work')
	<h1>Reorder Works</h1>
	
	<form action="{{ route('works_postReorder') }}" method="post" accept-charset="utf-8">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input id="images_order" type="hidden" name="order">

		<div id="reorder-files" class="text-center">
			<button type="submit" class="btn btn-primary pull-right">Save</button>
			<ul id="selected_files" class="reorder">
				@foreach($works as $key => $work)
					<li id="{{ $work->id }}" class="list ui-state-default {{ $work->status }}">
						<div class="image-container">
							
							<img src="/{{ Config::get('custom_path.works') }}/{{ $work->pictures->first()->picture_name }}">
						</div>
						
	            	</li>
				@endforeach
			</ul>
			<button type="submit" class="btn btn-primary pull-right">Save</button>
		</div>

    </form>
                   
@stop

@section('content_work_js')
	<script type="text/javascript" src="https://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
	<script type="text/javascript">

	$("#selected_files").sortable({
	    placeholder: "highlight",
	    create: function (event, ui){
	        var image_order = $(this).sortable('toArray').toString();
	        $("#images_order").val(image_order);
	    },
	    start: function (event, ui) {
	        ui.item.toggleClass("highlight");
	    },
	    stop: function (event, ui) {
	        ui.item.toggleClass("highlight");
	    },
	    update: function(event, ui) {
	        var image_order = $(this).sortable('toArray').toString();
	        $("#images_order").val (image_order);
	    }
	});
	</script>
@stop