@extends('admin/works/layout_work')

@section('content_title')
	<title>Durenworks - Admin/Work/{{ $work->project_title }}</title>
@stop

@section('content_work')
<div class="viewer">
	<div id="view-work" class="view-work hidden-xs" section="view-work">

            <div class="row">
                <div class="col-sm-8">
                    <div class="view-work-slider">
                        <div id="view-work-slider" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol id="slider-indicator" class="carousel-indicators">
                                @foreach($pictures as $index => $picture)
                                	@if($index == 0)
                                		<li data-target="#view-work-slider" data-slide-to="{{ $index }}" class="active"></li>
                                	@else
                                		<li data-target="#view-work-slider" data-slide-to="{{ $index }}"></li>
                                	@endif
                                @endforeach
                            </ol>

                            <!-- Wrapper for slides -->
                            <div id="slider-inner" class="carousel-inner" role="listbox">
                                @foreach($pictures as $index => $picture)
                                	@if($index == 0)
                                		<div class="item active"><img src="/{{ $pictures_path }}/{{$picture->picture_name}}?{{ rand()*rand() }}" alt="..."></div>
                                	@else
                                		<div class="item"><img src="/{{ $pictures_path }}/{{$picture->picture_name}}?{{ rand()*rand() }}" alt="..."></div>
                                	@endif
                                @endforeach
                            </div>
                            <!-- Controls -->
                            <a class="left carousel-control" href="#view-work-slider" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#view-work-slider" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                            </a>
                            
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <h3 id="view-work-title">{{ $work->project_title }}</h3> 
                    <p>Description</p>
                    <p id="view-work-description">{{ str_limit($work->description, 550) }}</p>
                    <div class="desc-botom">
                        <div class="row">
                            <div class="col-sm-5">
                                <p>Client:</p>
                            </div>
                            <div class="col-sm-7">
                                <p id="view-work-client">{{ $work->client->name }}</p>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-5">
                                <p>Categories:</p>
                            </div>
                            <div class="col-sm-7">
                                <p id="view-work-category">{{ $tags }}</p>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-5">
                                <p>Link:</p>
                            </div>
                            <div class="col-sm-7">
                                <p id="view-work-link">{{ $work->link ? : "-" }}</p>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-danger btn-sm pull-right" data-toggle="modal" data-target="#delete-confirmation" data-delete-target="{{ route('works_delete', ['works' => $work->slug]) }}" data-delete-title="{{ $work->project_title }}">Delete</button>
                    <a href="{{ route('works_edit', ['works' => $work->slug]) }}" ><button class="btn btn-primary btn-sm">Edit</button></a>
                </div>
            </div>

    </div>
</div>

	
@stop

@section('content_work_js')
<script src="/{{ Config::get('custom_path.js') }}/delete-confirmation.js" type="text/javascript"></script>
<script type="text/javascript">
	
</script>
@stop