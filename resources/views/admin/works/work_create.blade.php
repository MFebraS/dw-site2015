@extends('admin/works/layout_work')

@section('content_title')
	<title>Durenworks - Admin/Work/Create</title>
	<link href="/assets/css/bootstrap-tagsinput.css" rel="stylesheet">
	<meta name="csrf-token" content="{{ csrf_token() }}" />
@stop

@section('content_work')
	<h1>Add New Work</h1>

	<form id="add-work" class="work-form" method="POST" action="{{ route('works_store') }}" enctype="multipart/form-data">
		<div class="row">
			<div class="col-md-8">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input id="images_order" type="hidden" name="images_order">
				
				<div class="col-md-12">
					<div class="form-group {{ $errors->first('project_title') != "" ? "has-error" : "" }}">
						<label class="control-label" for="project_title">Project Title<span class="required">*</span>:</label>
						<input type="text" id="project_title" class="form-control" name="project_title" value="{{ old('project_title') }}" placeholder="Enter Project Title Here" required/>
						{!! $errors->first('project_title', '<p class="text-danger">:message</p>') !!}
					</div>
					<div class="form-group {{ $errors->first('tag_name') != "" ? "has-error" : "" }}">
						<label class="control-label" for="tags">Tags<span class="required">*</span>: </label>  <input id="tags" type="text" class="btn btn-default" name="tag_name" data-role="tagsinput" value="{{ old('tag_name') }}"/>
						{!! $errors->first('tag_name', '<p class="text-danger">:message</p>') !!}
					</div>
					<div class="form-group">
						<div class="form-inline {{ $errors->first('link') != "" ? "has-error" : "" }}">
							<label class="control-label" for="link">Link: </label>  <input id="link" class="form-control" type="text" name="link"value="{{ old('link') }}" placeholder="Project's Link"/>
							{!! $errors->first('link', '<p class="text-danger">:message</p>') !!}
						</div>
					</div>
					<div class="form-group">
						<div class="form-inline {{ $errors->first('order') != "" ? "has-error" : "" }}">
							<label class="control-label" for="order">Order: </label>  
							<select id="order" class="form-control" name="order">
								<option>-- Place After --</option>
								@foreach($orders as $order)
									<option value="{{ $order->order }}" {{ old('order') == $order->order ? "selected" : "" }}>{{ $order->project_title }}</option>
								@endforeach
							</select>
							{!! $errors->first('order', '<p class="text-danger">:message</p>') !!}
						</div>
					</div>
				</div>
				
				<div class="client col-sm-6" style="height:{{ old('client') == 'new' ? "auto" : "100px" }}">
					<div class="form-group">
						<div class="form-group {{ $errors->first('client') != "" ? "has-error" : "" }}">
							<label class="control-label" for="client">Client<span class="required">*</span>:</label>
							<select id="client" name="client" class="form-control" required>
								<option value="">-- Select Client --</option>
								<option value="new" {{ old('client') == "new" ? "selected" : "" }}>-- Add New Client --</option>
								{{ $old_logo = null }}
								@foreach($clients as $client)
									<option value="{{ $client->id }}" data-logo="/{{Config::get('custom_path.clients')}}/{{ $client->logo }}" {{ old('client') == $client->id ? "selected" : "" }}>{{ $client->name }}</option>
									@if(old('client') == $client->id)
										{{ $old_logo = '/'.Config::get('custom_path.clients').'/'.$client->logo }}
									@endif
								@endforeach
							</select>
							{!! $errors->first('client', '<p class="text-danger">:message</p>') !!}
						</div>

						<div id="new-client" style="opacity:{{ old('client') == 'new' ? "1" : "0" }}">
							<div class="form-inline {{ $errors->first('name') != "" ? "has-error" : "" }}">
		                        <label class="control-label" for="name">Name<span class="required">*</span>:</label>
		                        <input type="text" id="name" class="form-control pull-right" name="client_name" value="{{ old('client_name') }}"/>
		                        {!! $errors->first('name', '<p class="text-danger">:message</p>') !!}
		                    </div>
		                    <div class="form-inline">
		                        <label class="control-label" for="website">Website:</label>
		                        <input type="text" id="website" class="form-control pull-right" name="client_website" value="{{ old('client_website') }}"/>
		                    </div>
						</div>
					</div>
				</div>
				<div class="client col-sm-6" style="height:100px;margin-top:25px">
					<div class="form-group">
						<div class="container-logo">
                            <img class="logo-preview" src="{{ $old_logo != null ? $old_logo : '/'.Config::get('custom_path.clients').'/default.jpg' }}">
                        </div>
                        <small id="nb" class="text-muted" style="opacity:{{ old('client') == 'new' ? "1" : "0" }}"><em>450px x 150px (9 : 3) for best result</em></small><br>
                        <div class="btn btn-primary btn-file" style="opacity:{{ old('client') == 'new' ? "1" : "0" }}">
                            Upload Logo<input id="logo" type="file" name="logo" accept="image/*">
                        </div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label>Work's Pictures<span class="required">*</span>:</label><br>
						<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#work-pictures">Select Pictures</button> 
						    <small class="text-muted"><em><span id="pictures-count">0</span> pictures selected</em></small><br>
						{!! session('error') != null ? '<p class="text-danger">'.session('error').'</p>' : '' !!}
					</div>
					
					<div class="form-group {{ $errors->first('description') != "" ? "has-error" : "" }}">
						<div class="pull-right">
							<span id="char-left">380</span> characters left
						</div>
						<label class="control-label" for="description">Description<span class="required">*</span>:</label>{!! $errors->first('description', '<p class="text-danger">:message</p>') !!}
						<textarea id="description" class="form-control" name="description" rows="5" placeholder="Project's Description. Max 380 characters" maxlength="380">{{ old('description') }}</textarea>
						
					</div>
					<input id="save" class="btn btn-primary" name="save" type="submit" value="Save As Draft">
					<input id="submit" class="btn btn-success" name="publish" type="submit" value="PUBLISH"/>	
				</div>
			</div>
		</div>
		<div class="modal fade" id="work-pictures" tabindex="-1" role="dialog" aria-labelledby="add-picture" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Work's Pictures</h4>
                    </div>

                    <div class="modal-body">
                        <!-- Tab Contents -->
                        <div class="btn btn-primary btn-file">
							Upload Images<input id="upload" type="file" accept="image/*" multiple/>
						</div>
						<small id="nb" class="text-muted"><em>600px x 450px (4 : 3) for best result</em></small><br>
						<ul id="selected_files">
							@if($pictures != false)
								@foreach($pictures as $key => $picture)
									<li id="{{ $picture }}" data-id="{{ explode(".", $picture)[0] }}" class="list ui-state-default">
										<div class="image-container">
											
											<img id="image{{ explode(".", $picture)[0] }}" src="/{{ Config::get('custom_path.works') }}/temp/{{ $picture }}?{{ rand()*rand() }}">
										</div>
										
                                	</li>
								@endforeach
							@endif
						</ul>
                        
                    </div>
                    <div class="modal-footer">
                    	<button type="button" class="btn btn-primary" data-dismiss="modal">Finish</button>
                    </div>
                </div>
            </div>
        </div>
		
	</form>
	<div class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Work's Pictures</h4>
				</div>
				<div class="modal-body">
					<p>One fine body&hellip;</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Save changes</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
@stop

@section('content_work_js')
	<script type="text/javascript" src="https://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
	<script type="text/javascript" src="/assets/js/bootstrap-tagsinput.min.js"></script>
	<script type="text/javascript" src="/{{ Config::get('custom_path.js') }}/uploadTemp.js"></script>
	<script type="text/javascript">
		var submit = false;
		var id = parseInt({{ $last }});
		$('#tags').tagsinput({
			onTagExists: function(item, $tag) {
				$tag.hide.fadeIn();
			}
		});

		$('#client').on('change', function(){
			if($('option:selected', this).data('logo') != undefined){
				$('.logo-preview').attr('src', $('option:selected', this).data('logo'));
			}else{
				$('.logo-preview').attr('src', "/{{ Config::get('custom_path.clients') }}/default.jpg");
			}
			if($('#client').val() == "new"){
				$('#new-client').animate({'opacity': 1}, 200);
				$('#logo').parent().animate({'opacity': 1}, 200);
				$('#logo').parent().prev().animate({'opacity': 1}, 200);
				$('.client').animate({"height": "200px"}, 200);
			}else{
				$('#logo').val('');
				$('#new-client').animate({'opacity': 0}, 200);
				$('#logo').parent().animate({'opacity': 0}, 200);
				$('#logo').parent().prev().animate({'opacity': 0}, 200);
				$('.client').animate({"height": "100px"}, 200);
				$('.client .text-danger').hide();

			}
		});

		$('#logo').on('change', function(){
		    var file = $(this)[0].files[0];
		    var type = file.type.toString();
		    if(type.indexOf("image/") != -1){
		        $('.logo-preview').attr('src', URL.createObjectURL(file));
		    }else{
		    	$('.logo-preview').attr('src', "/{{ Config::get('custom_path.clients') }}/default.jpg");
		        $('#logo').val('');
		        alert('Please select image file!');
		    }
		});

		$(function(){
			$('#char-left').text($('#description').attr('maxlength') - $('#description').val().length);
		});

		$('#description').on('paste keyup change', function(){
			$('#char-left').text($('#description').attr('maxlength') - $('#description').val().length);
		});
	</script>
@stop
