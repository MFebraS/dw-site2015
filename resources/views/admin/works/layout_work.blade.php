@extends('admin/layout')

@section('layout_2')
<div class="work">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3 col-md-2 sidebar">
                <ul class="nav nav-sidebar">
                    <li id="new-work" class="{{ $active[1] == 'new' ? "active" : "" }}"><a href="{{ route('works_create') }}">New Work</a></li>
                    <li id="all-works" class="{{ $active[1] == 'all' ? "active" : "" }}"><a href="{{ route('works_index') }}">All Works</a></li>
                    <li id="clients" class="{{ $active[1] == 'clients' ? "active" : "" }}"><a href="{{ route('client_index') }}">Clients</a></li>
                </ul>
            </div>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                @yield('content_work')
            </div>
        </div>
    </div>
</div>
@stop

@section('content_js')
    <script type="text/javascript">
        $('.nav.main-menu li:eq(2)').addClass('active');
    </script>
    @yield('content_work_js')
@stop