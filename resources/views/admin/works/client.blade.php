@extends('admin/works/layout_work')

@section('content_title')
<title>Durenworks - Admin/Clients</title>
@stop

@section('content_work')
    <h1 class="page-header">Clients</h1>
    <div id="clients-content">
        <div class="col-sm-4">

            <h2 id="client-subtitle">{{ $clientEdit != null ? "Edit" : "Add New" }} Client</h2> <a href="" title=""></a>
            <div class="client-news">
                <form action="{{ $clientEdit != null ? route('client_update', ['client' => $clientEdit->slug, 'page' => Request::get('page')]) : route('client_store') }}" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="page" value="{{ old('page') ? : $page }}">
                    <div class="form-group {{ $errors->first('name') != null ? "has-error" : "" }}">
                        <label class="control-label" for="name">Name<span class="required">*</span>:</label>
                        <input type="text" id="name" class="form-control" name="name" value="{{ $clientEdit != null ? (old('name') ? : $clientEdit->name) : old('name') }}" autofocus required/>
                        {!! $errors->first('name', '<p class="text-danger">:message</p>') !!}
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="website">Website</label>
                        <input type="text" id="website" class="form-control" name="website" value="{{ $clientEdit != null ? (old('website') ? : $clientEdit->website) : old('website') }}"/>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="logo">Logo</label>
                        <div class="container-logo">
                            <img class="logo-preview" src="/{{ Config::get('custom_path.clients') }}/{{ $clientEdit != null ? ($clientEdit->logo ? : "default.jpg") : "default.jpg" }}">
                        </div>
                        <small class="text-muted"><em>450px x 150px (9 : 3) for best result</em></small><br>
                        <div class="btn btn-primary btn-file">
                            Upload Logo<input id="logo" type="file" name="logo" accept="image/*">
                        </div>
                        
                        <p class="text-muted"><small><em>Clients without logo will not be displayed on home page.</em></small></p>
                    </div>
                    <br>
                    @if($clientEdit != null)
                        <a href="{{ route('client_index') }}" title=""><button type="button" class="btn btn-danger pull-right">Cancel</button></a>
                    @endif
                    <input class="btn btn-primary" type="submit" name="client" value="{{ $clientEdit != null ? "Update" : "Add"}} Client"/>
                </form>
            </div>
        </div>

        <div class="col-sm-8">
            <div style="height:50px">
                <div class="search">
                    <form action="{{ route('client_index') }}" method="GET">
                        <input type="search" name="q" placeholder="Search Client" value="{{ $q }}"/>
                    </form>
                </div>
            </div>
            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#client-reorder">Reorder Client</button>
            @if(session('success') != null)
            <div class="row">
                <p class="bg-success col-sm-12 text-center"><strong>{{ $success = session('success') }}</strong></p>
            </div>
            @endif
            
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th class="number">No</th>
                            <th class="client-name">Name</th>
                            <th class="client-website">Works</th>
                            <th class="client-last-modified">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                            @forelse($clients as $index => $client)
                            @if($clientEdit != null && $clientEdit->name == $client->name)
                            @else
                            <tr data-href="{{ route('works_index', ['client' => $client->slug]) }}">
                                <td>{{ $clients->perPage()*($clients->currentPage()-1)+($index+1) }}</td>
                                <td>
                                    <b>{{ $client->name }}</b><br>
                                    <small>{{ $client->website ? : "-" }}</small><br>
                                    <img src="/{{ Config::get('custom_path.clients') }}/{{ $client->logo ? : "default.jpg" }}" alt="" style="width:150px">
                                </td>
                                <td style="vertical-align:middle">{{ $client->count }}</td>
                                <td style="vertical-align:middle">
                                    <a href="{{ route('client_edit', ['client' => $client->slug, 'page' => $clients->currentPage()]) }}">
                                        <button type="button" class="btn btn-primary">EDIT</button>
                                    </a>
                                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete-confirmation" data-delete-target="{{ route('client_delete', ['client' => $client->slug]) }}" data-delete-title="{{ $client->name }}, all of works with this client will be deleted">DELETE</button>
                                </td>
                            </tr>
                            @endif
                            @empty
                                <tr>
                                    <td colspan="4" class="text-center"><b>No Record Found</b></td>
                                </tr>
                            @endforelse
                            
                    </tbody>
                </table>
            </div>
            {!! $clients->render() !!}
        </div>
    </div>




    <div class="modal fade" id="client-reorder" tabindex="-1" role="dialog" aria-labelledby="add-picture" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Reorder Client</h4>
                    </div>

                    <div class="modal-body">
                        <!-- Tab Contents -->
                        <ul id="logos">
                            @foreach($logos as $key => $logo)
                                @if($logo->logo != null)
                                    <li id="{{ $logo->id }}" class="list ui-state-default" style="width:130px;height:50px;{{ $key < 12 ? "border-color:green;border-width:2px" : "" }}">
                                        <div class="image-container">
                                            <img src="/{{ Config::get('custom_path.clients') }}/{{ $logo->logo }}">
                                        </div>
                                        
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                        
                    </div>
                    <div class="modal-footer">
                        <form action="{{ route('client_reorder') }}" method="post" accept-charset="utf-8">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input id="images_order" type="hidden" name="order">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </form>
                        
                    </div>
                </div>
            </div>
        </div>
@stop

@section('content_work_js')
<script type="text/javascript" src="https://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
<script src="/{{ Config::get('custom_path.js') }}/delete-confirmation.js" type="text/javascript"></script>
<script type="text/javascript">
    $('tr[data-href]').on('click', function(e){
        if(!$(e.target).hasClass('btn')){
            window.location = $(this).attr('data-href');
        }
    });
    $('#logo').on('change', function(){
        var file = $(this)[0].files[0];
        var type = file.type.toString();
        if(type.indexOf("image/") != -1){
            $('.logo-preview').attr('src', URL.createObjectURL(file));
        }else{
            $('.logo-preview').attr('src', "/{{ Config::get('custom_path.clients') }}/default.jpg");
            $('#logo').val('');
            alert('Please select image file!');
        }
    })
</script>
<script type="text/javascript">

$("#logos").sortable({
    placeholder: "highlight-client",
    create: function (event, ui){
        var image_order = $(this).sortable('toArray').toString();
        $("#images_order").val(image_order);
    },
    start: function (event, ui) {
        ui.item.toggleClass("highlight-client");
    },
    stop: function (event, ui) {
        ui.item.toggleClass("highlight-client");
    },
    update: function(event, ui) {
        var image_order = $(this).sortable('toArray').toString();
        $("#images_order").val (image_order);
    }
});
</script>
@stop