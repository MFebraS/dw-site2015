@extends('admin/dashboard/layout_dashboard')

@section('content_title')
	<title>Slider</title>
	<link href='https://fonts.googleapis.com/css?family=Raleway:400,500,700,600,800' rel='stylesheet' type='text/css'>
	<meta name="csrf-token" content="{{ csrf_token() }}" />
@stop

@section('content_dashboard')
	<div id="slider">
		<h1>Slider</h1>

		<div class="row slider-container" style="padding-top:20px">
			<div class="col-md-7">
				<div class="btn btn-primary btn-file">
					Add Picture<input id="upload" type="file" accept="image/*" multiple/>
				</div>
				<small class="text-muted"><em>1300px x 600px (13 : 6) for best result</em></small><br>
				<ul id="selected_files"  style="background:white">
					@if($sliders->first() != null)
						@foreach($sliders as $slider)
							<li id="{{ $slider->id }}" class="list ui-state-default" data-position="{{ $slider->description_position }}" data-desc="{{ $slider->description }}" data-path="{{ $slider->subimage != '' ? Config::get('custom_path.sliders') : '' }}" data-subimage="{{ $slider->subimage }}">
								<div class="image-container">
									<img class="image" src="/{{ Config::get('custom_path.sliders') }}/{{ $slider->image }}?{{ rand()*rand() }}">
								</div>
							</li>
						@endforeach
					@endif
				</ul>
				<form action="{{ route('slider_saveSequence') }}" method="post">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input id="images_order" type="hidden" name="images_order">
					<input id="save" class="btn btn-primary pull-right" name="save" type="submit" value="SAVE" style="display:none">
				</form>
				
			</div>
			<div id="slider_desc" class="col-md-5" style="display:none">
				<h3>Slider <span class="slider_name"></span>'s Text</h3>
				<div class="form-group">
					<span class="position-title">Position: </span> <span class="position left"></span> <span class="position center"></span> <span class="position right"></span>
					<textarea id="slider_text" class="form-control" rows="10" name="slider_text"></textarea>
				</div>
				
				<form action="{{ route('slider_saveDescription') }}" method="post" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input id="slider_id" type="hidden" name="id">
					<input id="slider_description_position" type="hidden" name="position">
					<input id="slider_description" type="hidden" name="description">
					<div class="form-group">
						<label for="subimage">Subimage</label>
						<div class="input-group">
			                <span class="input-group-btn">
			                    <span class="btn btn-primary btn-file">
			                        Browse&hellip; <input type="file" name="subimage" accept="image/*">
			                    </span>
			                </span>
			                <input type="text" class="form-control" placeholder="No file chosen" readonly>
			            </div>
			            <small class="text-muted"><em>400px x 400px (1 : 1) for best result</em></small><br>
					</div>
					<button id="button_preview" type="button" class="btn btn-primary" data-toggle="modal" data-target="#preview_slider">Preview</button>
					<button type="submit" class="btn btn-primary pull-right">Apply</button>
				</form>

			</div>
			
		</div>

			
	</div>

	<div id="preview_slider" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="preview_slider" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Slider <span class="slider_name"></span></h4>
				</div>
				<div class="modal-body">
					<div class="bg-slider" style="">
						<img class="bg-slider2" src="/assets/images/index/slider/bg-slider.png">
						<div class="text-slider">
							<div class="container">
								<div class="row">
									<div id="text-content-align" class="col-md-6 vertical-center">
										<div class="text-content">
											
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
				
		</div>
	</div>
@stop

@section('content_dashboard_js')
	<script type="text/javascript" src="https://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
	<script src="/assets/js/commonmark.js"></script>
	<script src="/{{ Config::get('custom_path.js') }}/delete-confirmation.js" type="text/javascript"></script>
	<script type="text/javascript">
		$('body').prepend('<div class="processing" id="processing"  style="display:none"><img class="loading" src="/assets/images/white-loader.gif"></div>').prepend('<div class="fail" id="fail"  style="display:none"><span>Failed</span></div>').prepend('<div id="image-tools" class="image-tools" style="display:none"><button type="button" class="delete-picture close"><span class="tool">&times;</span></button><button type="button" class="tools tools-left"><span class="tool glyphicon glyphicon-triangle-left"></span></button><button type="button" class="tools tools-right"><span class="tool glyphicon glyphicon-triangle-right"></span></button></div>');
		var subimage_container = '<div class="col-sm-6 vertical-center"><div class="subimage-content"><img src="" alt=""></div></div>';
		var processing = $('#processing').clone();
		var image_tools = $('#image-tools').clone();
		var fail = $('#fail').clone();
		var id = parseInt({{ $last }});
		$('#upload').on('change', function(){
			var files = $(this)[0].files;
			$.each(files, function(key,file){
				var type = file.type.toString();
				if(type.indexOf("image/") != -1){
					++id;
					var this_id = id;
					var li_id = this_id+"."+file.name.split(".").pop();
					$('#selected_files').append('<li data-id="'+ this_id +'" class="list ui-state-default"><div class="image-container"><div id="processing'+this_id+'" class="processing"><img class="loading" src="/assets/images/white-loader.gif"></div><img class="image" src="' + URL.createObjectURL(file) + '"></div></li>');
					
					var data = new FormData();
			        data.append('image', file);
			        
			        $.ajax({
			            url: '/admin/slider/upload?id='+this_id,
			            headers: {
			                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			                },
			            type: 'POST',
			            data: data,
			            cache: false,
			            dataType: 'json',
			            processData: false, // Don't process the files
			            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
			            success: function(data, textStatus, jqXHR)
			            {
			                
			                if(data.status == 'ok'){
			                	var currentElmt = $('#selected_files li[data-id='+data.old_id+']');
			                	currentElmt.attr('id', data.new_id);
			                	currentElmt.find('.processing').remove();
			                	currentElmt.find('.image').attr('src', '/'+data.filename+'?'+Math.random()*Math.random());
			                    var image_order = $('#selected_files').sortable('toArray').toString();
								$("#images_order").val (image_order);
			                }
			                
			            },
			            error: function(jqXHR, textStatus, errorThrown)
			            {
			            	$('#processing'+this_id).remove();
			                $('#selected_files li[data-id='+this_id+']').prepend(fail.clone().show().attr('class', 'fail upload'));
			            }
			        });
				}
			})
		});
		$('#selected_files').on('click', '.close', function(){
			var element_delete = $(this).parent().parent();
			var id_delete = element_delete.attr('id');
			
			$(this).parent().prev().prepend(processing.clone().show());
			//console.log($('.pictures#'+id_delete));
			$.ajax({
		        url: '/admin/slider/'+id_delete+'/delete',
		        headers: {
		                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		            },
		        type: 'get',
		        cache: false,
		        dataType: 'json',
		        processData: false, // Don't process the files
		        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
		        success: function(data, textStatus, jqXHR)
		        {
		            
		            if(data.status == 'ok'){
		            	element_delete.remove();
						var image_order = $('#selected_files').sortable('toArray').toString();
						$("#images_order").val (image_order);
		            }
		            
		        },
		        error: function(jqXHR, textStatus, errorThrown)
		        {
		        	$('#processing'+id_delete).remove();
		            $('#selected_files li[data-id='+id_delete+']').prepend(fail.clone().show().attr('class', 'fail delete'));
		        }
		    });
		});
		$('#selected_files').on('mousedown', '.fail', function(){
			if($(this).hasClass('upload')){
				$(this).parent().remove();
			}else if($(this).hasClass('delete')){
				$(this).remove();
			}
			
		});
		$('#selected_files').on('mouseenter', 'li', function(){
			$(this).append(image_tools.show());
		});
		$('#selected_files').on('mouseleave', 'li', function(){
			$(this).find('.image-tools').remove();
		});
		$('#selected_files').on('click', '.tools-left', function(){
			var toMove = $(this).parent().parent();
			toMove.prev().before(toMove);
			var image_order = $('#selected_files').sortable('toArray').toString();
			$("#images_order").val (image_order);
			$('#save').show();
		})
		$('#selected_files').on('click', '.tools-right', function(){
			var toMove = $(this).parent().parent();
			toMove.next().after(toMove);
			var image_order = $('#selected_files').sortable('toArray').toString();
			$("#images_order").val (image_order);
			$('#save').show();
		})
		$("#selected_files").sortable({
			placeholder: "highlight",
			create: function (event, ui){
				var image_order = $(this).sortable('toArray').toString();
				$("#images_order").val(image_order);
			},
			start: function (event, ui) {
				ui.item.toggleClass("highlight");
			},
			stop: function (event, ui) {
				ui.item.toggleClass("highlight");
			},
			update: function(event, ui) {
				var image_order = $(this).sortable('toArray').toString();
				$("#images_order").val (image_order);
				$('#save').show();
			}
		});
		$('#selected_files').on('click', 'li', function(e){
			if(!($(e.target).hasClass('tool') || $(e.target).hasClass('tools') || $(e.target).hasClass('close'))){
				if($(this).find('.processing').length <= 0){
					$('#slider_desc').show();
					$('.slider_name').html($(this).attr('id'));
					$('.bg-slider2').attr('src', $(this).find('img').attr('src'));
					$('#slider_text').val($(this).attr('data-desc'));
					$('#slider_id').val($(this).attr('id'));
					$('.btn-file :file ').parents('.input-group').find(':text').val($(this).attr('data-subimage'));
					marker();
					setPosition($(this).attr('data-position'), $(this).attr('data-path'), $(this).attr('data-subimage'));
				}
			}
		});
		function setPosition(position, path, subimage){
			$('.subimage-content').parent().remove();
			if(position == 'right'){
				$('.position').removeClass('active');
				$('.position.right').addClass('active');
				$('#text-content-align').removeClass('col-md-offset-3').removeClass('col-md-offset-6');
				if(subimage != ''){
					$('#text-content-align').before(subimage_container);
					if(path != ''){
						$('.subimage-content img').attr('src', '/'+path+'/'+subimage);
					}else{
						$('.subimage-content img').attr('src', subimage);
					}
				}else{
					$('#text-content-align').addClass('col-md-offset-6');
				}
			}else if(position == 'center'){
				$('.position').removeClass('active')
				$('.position.center').addClass('active')
				$('#text-content-align').addClass('col-md-offset-3').removeClass('col-md-offset-6');
			}else{
				$('.position').removeClass('active')
				$('.position.left').addClass('active')
				$('#text-content-align').removeClass('col-md-offset-3').removeClass('col-md-offset-6');
				if(subimage != ''){
					$('#text-content-align').after(subimage_container);
					if(path != ''){
						$('.subimage-content img').attr('src', '/'+path+'/'+subimage);
					}else{
						$('.subimage-content img').attr('src', subimage);
					}
				}
			}
			$('#slider_description_position').val(position);
		}
		function marker() {
            var text = $("#slider_text").val();
            var reader = new commonmark.Parser();
            var writer = new commonmark.HtmlRenderer();
            var parsed = reader.parse(text);
            var result = writer.render(parsed);
            $("#preview_slider .text-content").html(result);
            $('#slider_description').val(result);
        }
        $('#slider_text').on('change', function(){
        	marker();
        })
        $('.position').on('click', function(){
        	var position;
        	if($(this).hasClass('left')){
        		position = 'left';
        	}else if($(this).hasClass('center')){
        		position = 'center';
        	}else{
        		position = 'right';
        	}
        	setPosition(position, $('#'+$('#slider_id').val()).attr('data-path'), $('#'+$('#slider_id').val()).attr('data-subimage'));
        })
        $('.btn-file :file').on('change', function() {
        	var file = $(this)[0].files[0];
        	var input = $(this).parents('.input-group').find(':text');

        	if(file == undefined){
        		input.val('');
        	}else{
	        	var type = file.type.toString();
				if(type.indexOf("image/") != -1){
					
					input.val(file.name);
				}
				setPosition( $('#'+$('#slider_id').val()).attr('data-position'), '', URL.createObjectURL(file));
				$('#'+$('#slider_id').val()).attr('data-subimage', URL.createObjectURL(file));
			}
			
		});
	</script>
@stop