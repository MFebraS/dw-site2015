@extends('admin/dashboard/layout_dashboard')

@section('content_title')
	<title>Durenworks - Admin/Dashboard</title>
@stop

@section('content_dashboard')
	<h1>Dashboard</h1>
	
	<div class="row">
		<div class="col-md-6 news">
			<h2>News <a class="new-post" href="{{ action('Admin\NewsController@create') }}">New Post</a></h2>

			<div class="row">
				<a href="{{ action('Admin\NewsController@index') }}">
					<div class="col-sm-3 count-container all">
						<span class="count-number">{{ $news_count['all'] }}</span>
						<span class="count-desc">Total Post</span>
					</div>
				</a>
				<a href="{{ action('Admin\NewsController@index', ['status' => 'published']) }}">
					<div class="col-sm-3 count-container published">
						<span class="count-number">{{ $news_count['published'] }}</span>
						<span class="count-desc">Published</span>
					</div>
				</a>
				<a href="{{ action('Admin\NewsController@index', ['status' => 'draft']) }}">
					<div class="col-sm-3 count-container draft">
						<span class="count-number">{{ $news_count['draft'] }}</span>
						<span class="count-desc">Draft</span>
					</div>
				</a>

			</div>

			<div class="row recent-post">
				<h3>Recent Posts</h3>

				<div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th class="number">No</th>
                                <th class="title">Title</th>
                                <th class="author">Author</th>
                                <th class="categories">Category</th>
                            </tr>
                        </thead>
                        <tbody>
                        	@if($news_count['all'] == 0)
			            		<tr>
			            			<td colspan="4" class="text-center"><b>No Record</b></td>
			            		</tr>
			            	@else
                                @foreach($news as $index => $post)
                                <tr>
                                    <td>{{ $index+1 }}</td>
                                    <td>
                                        <div class="news-title"><a href="">{{ $post->title }}</a></div>
                                        <div class="news-action">
                                            <a href="">Preview</a> | 
                                            <a href="{{ action('Admin\NewsController@edit', [$post->id], [$post->featured_picture_id]) }}">Edit</a> | 
                                            <span class="news-delete" data-toggle="modal" data-target="#modal-delete-confirm" data-id="{{ $post->id }}">Delete</span>
                                        </div>
                                    </td>
                                    <td><a href="">{{ $post->user->name }}</a></td>
                                    <td> {{ $post->category->category_name }} </td>
                                </tr>
                                @endforeach
                            @endif
                        </tbody>
                        @if($news_count['all'] > 3)
			            	<tfoot>
			            		<tr>
		            				<td class="text-center" colspan="4">
		            					<a href="{{ action('Admin\NewsController@index') }}">Show All</a>
		            				</td>
			            		</tr>
			            	</tfoot>
			            @endif
                    </table>
                </div>
			</div>
		</div>
		<div class="col-md-6 works">
			<h2>Works <a class="new-post" href="{{ route('works_create') }}">New Work</a></h2>

			<div class="row">
				<a href="{{ route('works_index') }}">
					<div class="col-sm-3 count-container all">
						<span class="count-number">{{ $works_count['all'] }}</span>
						<span class="count-desc">Total Post</span>
					</div>
				</a>
				<a href="{{ route('works_index') }}?status=published">
					<div class="col-sm-3 count-container published">
						<span class="count-number">{{ $works_count['published'] }}</span>
						<span class="count-desc">Published</span>
					</div>
				</a>
				<a href="{{ route('works_index') }}?status=draft">
					<div class="col-sm-3 count-container draft">
						<span class="count-number">{{ $works_count['draft'] }}</span>
						<span class="count-desc">Draft</span>
					</div>
				</a>
			</div>

			<div class="row recent-post">
				<h3>Recent Posts</h3>

				<div class="table-responsive">
			        <table class="table table-striped">
			            <thead>
			                <tr>
			                    <!-- <th class="check"><input type="checkbox" name="select-all-news"/></th> -->
			                    <th class="number">No</th>
			                    <!-- <th class="image">Image</th> -->
			                    <th class="project">Project Title</th>
			                    <th class="client">Client</th>
			                </tr>
			            </thead>
			            <tbody id="table-body">
			            	@if($works_count['all'] == 0)
			            		<tr>
			            			<td colspan="4" class="text-center"><b>No Record</b></td>
			            		</tr>
			            	@else
				            	@foreach($works as $key => $work)
					            	<tr data-href="{{ route('works_show', ['works' => $work->id]) }}">
					            		<td>{{ $key+1 }}</td>
					            		<!-- <td class="image">	            			
					            			<img src="/{{ Config::get('custom_path.uploads') }}/{{ $work->pictures->where('sequence', '1')->first()->file_name }}?{{ rand()*rand() }}" data-toggle="modal" data-target="#view_pictures" data-id="{{ $work->id }}" data-title="{{ $work->project_title }}">
					            		</td> -->
					            		<td class="project" >
					            			<b>{{ str_limit($work->project_title, 20) }}</b>
					            			{!! $work->status == "published" ? "<span class=\"label label-success\">Published</span>" : "<span class=\"label label-danger\">Draft</span>" !!}<br>
					            			<small>
					            				@foreach($work->tags as $tag)
					            					{{ $tag->tag_name }} 
					            				@endforeach
					            			</small>
					            			<!-- <span class="label label-primary read-more"><a href="{{ action('Admin\WorksController@show', ['works' => $work->id]) }}">Read more<span class="glyphicon glyphicon-menu-right"></span></a></span> -->
					            		</td>
					            		<td class="client">
					            			<p><b>{{ $work->client->name }}</b></p>
	            							<small>{{ $work->client->website ? : "-" }}</small>
					            		</td>
					            	</tr>
					            @endforeach
					        @endif
			            </tbody>
			            @if($works_count['all'] > 3)
			            	<tfoot>
			            		<tr>
		            				<td class="text-center" colspan="4">
		            					<a href="{{ route('works_index') }}">Show All</a>
		            				</td>
			            		</tr>
			            	</tfoot>
			            @endif
        			</table>
        		</div>
			</div>
		</div>
	</div>
@stop

@section('content_dashboard_js')
	<script type="text/javascript">
		$('tr[data-href]').on('click', function(e){

			if(!$(e.target).hasClass('btn')){
				window.location = $(this).attr('data-href');
			}
		});
	</script>
@stop