@extends('admin/layout')

@section('layout_2')
<div class="dashboard">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3 col-md-2 sidebar">
                <ul class="nav nav-sidebar">
                    <li id="dashboard" {{ $active[1] == 'dashboard' ? "class=active" : "" }}><a href="{{ route('dashboard') }}">Dashboard</a></li>
                    <li id="slider" {{ $active[1] == 'slider' ? "class=active" : "" }}>
                        <a href="{{ route('slider_index') }}">Slider</a>
                    </li>
                </ul>
            </div>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                @yield('content_dashboard')
            </div>
        </div>
    </div>
</div>
@stop

@section('content_js')
    @yield('content_dashboard_js')
@stop