@extends('admin/vacancies/layout_vacancy')

@section('vacancy_title')
	<title>Durenworks - Admin/Vacancy/{{ $vacancy->title }}</title>
@stop

@section('content_vacancy')
    <a href="{{ route('vacancy_index') }}" title=""><span class="glyphicon glyphicon-arrow-left"></span> Back</a>
    <a href="{{ route('vacancy_edit', ['vacancy' => $vacancy->slug]) }}" title=""><button type="button" class="btn btn-primary pull-right">Edit</button></a>
    <h2>{{ $vacancy->title }}</h2>
    {!! html_entity_decode($vacancy->html_detail) !!}
@stop

@section('content_vacancy_js')
@stop