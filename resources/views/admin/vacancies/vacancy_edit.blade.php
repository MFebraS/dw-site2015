@extends('admin/vacancies/layout_vacancy')

@section('vacancy_title')
	<title>Durenworks - Admin/Vacancy/{{ $vacancy->title }}/Edit</title>
@stop

@section('content_vacancy')
<div class="row">
	<div class="col-sm-12 col-md-8">
        <h2>Edit Vacancy - {{ $vacancy->title }}</h2>
        <form action="{{ route('vacancy_update', ['vacancy' => $vacancy->slug]) }}" method="POST">
            <div class="form-group {{ $errors->first('title') != "" ? "has-error" : "" }}">
            	<label class="control-label" for="title"><strong>Title<span class="required">*</span>:</strong></label>
                <input type="text" class="form-control input-lg" name="title" value="{{ old('title') ? : $vacancy->title }}" placeholder="Vacancy's Title" required>
                {!! $errors->first('title', '<p class="text-danger">:message</p>') !!}
            </div>

            <div class="form-group">
                
                <div id="vacancy-detail" role="tabpanel">
                    <ul class="nav nav-tabs pull-right" role="tablist">
                        <li role="presentation" class="active"><a href="#vacancy-detail-text" id="text-tab" role="tab" data-toggle="tab" aria-controls="" aria-expanded="true">Text</a></li>
                        <li role="presentation"><a href="#vacancy-detail-preview" id="preview-tab" role="tab" data-toggle="tab" aria-controls="">Preview</a></li>
                    </ul>
                
                    <label class="control-label" for="vacancy-detail-text"><strong>Detail:</strong></label>

                    <div class="tab-content">
                        <textarea class="tab-pane active" id="vacancy-detail-text" rows="17" name="detail" role="tabpanel" aria-labelledBy="text-tab" style="width:100%;">{{ old('detail') ? : $vacancy->detail }}</textarea>
                        <div class="tab-pane" id="vacancy-detail-preview" role="tabpanel" aria-labelledBy="preview-tab" style="min-height:500px"></div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <a href="{{ route('vacancy_index') }}" title=""><button type="button" class="btn btn-danger pull-right">Cancel</button></a>
            <input id="save" class="btn btn-primary" name="save" type="submit" value="Save As Draft">
            <input id="submit" class="btn btn-success" name="publish" type="submit" value="PUBLISH"/>   
        </form>
    </div>
</div>
@stop

@section('content_vacancy_js')
    <script src="/assets/js/commonmark.js"></script>
    <script type="text/javascript">
        function marker() {
            var text = "## "+ $('input[name=title]').val() +" ## \n"+ $("#vacancy-detail-text").val();
            var reader = new commonmark.Parser();
            var writer = new commonmark.HtmlRenderer();
            var parsed = reader.parse(text);
            var result = writer.render(parsed);
            $("#vacancy-detail-preview").html(result);
        }
        $("#preview-tab").on('click', function(){
            marker();
        });
    </script>
@stop