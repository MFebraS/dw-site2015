@extends('admin/vacancies/layout_vacancy')

@section('vacancy_title')
	<title>Durenworks - Admin/Vacancies</title>
@stop

@section('content_vacancy')
	<div class="col-sm-12">
		<div id="page_title">
			<h1>Vacancies <a class="new-post" href="{{ route('vacancy_create') }}">New Vacancy</a></h1>
		</div>
		
		
		
		@if(session('success') != null)
			<p class="bg-success text-center"><strong>{{ $success = session('success') }}</strong></p>
		@endif
		<table class="table table-hover">
			<thead>
				<th>No</th>
				<th>Title</th>
				<th>Status</th>
				<th>Last Modify</th>
				<th>Action</th>
			</thead>
			<tbody>
				@forelse($vacancies as $key => $vacancy)
					<tr data-href="{{ route('vacancy_show', ['vacancy' => $vacancy->slug]) }}">
						<td>{{ $key+1 }}</td>
						<td>{{ $vacancy->title }}</td>
						<td {{ $vacancy->status == 'published' ? "class=text-success" : "class=text-danger" }}>{{ $vacancy->status }}</td>
						<td>{{ date("d F Y", strtotime($vacancy->updated_at)) }}</td>
						<td>
							<a href="{{ route('vacancy_edit', ['vacancy' => $vacancy->slug]) }}"><button class="btn btn-primary btn-sm">Edit</button> </a> 
							<button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#delete-confirmation" data-delete-target="{{ route('vacancy_delete', ['vacancy' => $vacancy->slug]) }}" data-delete-title="{{ $vacancy->title }}">Delete</button>
						</td>
					</tr>
				@empty
					<tr>
						<td class="text-center" colspan="5">No Record Found</td>
					</tr>
				@endforelse
				@foreach($empties as $idx => $empty)
					<tr data-href="{{ route('vacancy_show', ['vacancy' => $empty->slug]) }}">
						<td>{{ $key+1+$idx+1 }}</td>
						<td>{{ $empty->title }}  <span class="label label-warning">Empty</span></td>
						<td {{ $empty->status == 'published' ? "class=text-success" : "class=text-danger" }}>{{ $empty->status }}</td>
						<td>{{ date("d F Y", strtotime($empty->updated_at)) }}</td>
						<td>
							<a href="{{ route('vacancy_edit', ['vacancy' => $empty->slug]) }}"><button class="btn btn-primary btn-sm">Edit</button> </a> 
							<button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#delete-confirmation" data-delete-target="{{ route('vacancy_delete', ['vacancy' => $empty->slug]) }}" data-delete-title="{{ $empty->title }}">Delete</button>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	
@stop

@section('content_vacancy_js')
<script src="/{{ Config::get('custom_path.js') }}/delete-confirmation.js" type="text/javascript"></script>
<script type="text/javascript">
	$('tr[data-href]').on('click', function(e){

		if(!$(e.target).hasClass('btn')){
			window.location = $(this).attr('data-href');
		}
	});
</script>
@stop