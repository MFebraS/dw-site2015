@extends('admin/layout')

@section('content_title')
    @yield('vacancy_title')
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,500,700,600,800" rel="stylesheet" type="text/css">
    <style type="text/css">
        #vacancy-detail-preview{
            font-family: 'Raleway', sans-serif;
            font-size: 10pt;
            color: #838383;
            padding: 25px;
            background-color: #FFFFFF;
        }
        #vacancy-detail-preview h2{
            margin-bottom: 10px;
            padding: 0;
            font-size: 16pt;
            font-weight: 500;
            color: #464646;
            margin-top: 0px;
        }
        #vacancy-detail-preview ul{
            padding-left: 15px;
        }
        #vacancy-detail-preview ul li{
            margin-top: 5px;
        }
        #vacancy-detail-preview h3{
            margin-top: 20px;
            margin-bottom: 10px;
            font-size: 12pt;
            font-weight: 500;
            color: #464646
        }
    </style>
@stop

@section('layout_2')
<div class="user">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3 col-md-2 sidebar">
                <ul class="nav nav-sidebar">
                    <li id="new-user" {{ $active[1] == 'new' ? "class=active" : "" }}><a href="{{ route('vacancy_create') }}">New Vacancy</a></li>
                    <li id="all-users" {{ $active[1] == 'all' ? "class=active" : "" }}><a href="{{ route('vacancy_index') }}">All Vacancies</a></li>
                    <!-- <li id="employees" {{ $active[1] == '' ? "class=active" : "" }}><a href="{{ route('employees_index') }}">Employees</a></li> -->
                </ul>
            </div>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                @yield('content_vacancy')
            </div>
        </div>
    </div>
</div>
@stop

@section('content_js')
    @yield('content_vacancy_js')
@stop