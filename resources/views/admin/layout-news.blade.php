@extends('admin/layout')
@section('layout_2')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <ul class="nav nav-sidebar">
                <li {{ $active[1] == 'new' ? "class=active" : "" }}><a href="{{ action('Admin\NewsController@create') }}">New Post</a></li>
                <li {{ $active[1] == 'all' ? "class=active" : "" }}><a href="{{ action('Admin\NewsController@index') }}">All Post</a></li>
                <li {{ $active[1] == 'category' ? "class=active" : "" }}><a href="{{ action('Admin\CategoriesController@index') }}">Categories</a></li>
            </ul>
        </div>
@yield('content');
@endsection