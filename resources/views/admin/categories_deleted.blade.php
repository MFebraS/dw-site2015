@extends('admin/layout-news')

@section('content_title')
    <title>Durenworks - Admin/News/Delete Category</title>
@endsection

@section('content')
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <div id="news-admin">
                <div id="categories">
                    <h1 class="page-header">Delete Category</h1>

                    <div class="col-sm-12" id="categories-content">
                        <div class="row">
							<p class="attention">You have deleted <em>{{ $category_name }}</em> category.</p>
                            <br><br>
                            <a class="btn btn-primary" href="{{ action('Admin\CategoriesController@index') }}"> Back </a>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content_js')
    <script>
        $(function(){
            $('.nav.main-menu li:eq(1)').addClass('active');
            $('.nav.nav-sidebar li:eq(2)').addClass('active');
        });
    </script>
@endsection