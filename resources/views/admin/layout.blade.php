<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!--<link rel="icon" href="../../favicon.ico">-->

        <!-- Bootstrap core CSS -->
        <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
        

        <!-- Custom styles for this template -->
        <link href="/assets/css/dashboard.css" rel="stylesheet">
        <link href="/assets/css/admin.css" rel="stylesheet">
        @yield('content_title')
        <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
    </head>

    <body>

        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{ action('HomeController@index') }}">Durenworks</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav main-menu">
                        <li {{ $active[0] == 'dashboard' ? "class=active" : "" }}><a href="{{ action('Admin\DashboardController@index') }}">Dashboard</a></li>
                        <li {{ $active[0] == 'news' ? "class=active" : "" }}><a href="{{ action('Admin\NewsController@index') }}">News</a></li>
                        <li {{ $active[0] == 'works' ? "class=active" : "" }}><a href="{{ action('Admin\WorksController@index') }}">Works</a></li>
                        @if(Auth::user()->level == 1)
                            <li {{ $active[0] == 'users' ? "class=active" : "" }}><a href="{{ action('Admin\UserController@index') }}">Users</a></li>
                            <li {{ $active[0] == 'vacancies' ? "class=active" : "" }}><a href="{{ route('vacancy_index') }}">Vacancies</a></li>
                        @endif
                        <!-- <li><a href="#">Messages</a></li> -->
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="{{ route('user_show', ['user' => Auth::user()->id]) }}">{{ Auth::user()->name }}</a>
                            <ul class="nav second-level">
                                <li> <a href="{{ route('user_changePassword', ['user' => Auth::user()->id]) }}"> Change Password </a> </li>
                                <li> <a href="{{ action('Admin\UserController@edit', ['user' => Auth::user()->id]) }}"> Edit Profile </a> </li>
                                <li> <a href="{{ action('Admin\UserController@logout') }}"> Log Out </a> </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        @yield('layout_2')
        <footer>
        </footer>

        <!-- Bootstrap core JavaScript
================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="/assets/js/jquery-2.1.3.min.js"></script>
        <script src="/assets/js/bootstrap.min.js"></script>
        <!--<script src="../../assets/js/docs.min.js"></script>-->
        @yield('content_js')
    </body>
</html>