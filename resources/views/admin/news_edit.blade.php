@extends('admin/layout-news')

@section('content_title')
    <title>Durenworks - Admin/News/Edit News</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection

@section('content')
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <div id="news-admin">
                <div id="edit-post">
                    <h1 class="page-header">Edit Post</h1>

                    <div class="col-sm-9 no-padding">
                        <div class="table-responsive">
                            <form action="{{ action('Admin\NewsController@update', [$edit_news->id]) }}" method="POST" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group {{ $errors->first('post_title') != "" ? "has-error" : "" }}">
                                    <input type="text" id="post-title" name="post_title" placeholder="Enter news title here" value="{{ old('post_title') ? : $edit_news->title }}" autofocus required />
                                    {!! $errors->first('post_title', '<p class="text-danger">:message</p>') !!}
                                </div>
                                <br>
                                <div id="permalink" class="form-inline {{ $errors->first('permalink') != "" ? "has-error" : "" }}">
                                    <label for="permalink" class="control-label"><b style="margin-right:7px;">Permalink :</b></label>
                                    <span id="permalink-result" {{ $errors->first('permalink') != "" ? "style=color:red" : "" }}> {{ old('permalink') ? : $edit_news->permalink }}
                                    </span>
                                    <input type="hidden" id="duplicate-permalink" name="permalink" value="{{ old('permalink') ? : $edit_news->permalink }}"/>
                                    <input class="mybtn" id="edit-permalink" type="button" name="edit-permalink" value="Edit"/>
                                    <input class="btn btn-info" id="btn-permalink-save" type="button" name="btn-permalink-save" value="Save Changes">
                                    {!! $errors->first('permalink', '<p class="text-danger">:message</p>') !!}
                                </div>
                                
                                <div id="news_sidebar">
                                    <div id="news-categories" class="form-group {{ $errors->first('category_id') != "" ? "has-error" : "" }}">
                                        <label for="category-id" class="control-label"> Categories<span class="required">*</span>: </label>
                                        <div>
                                            <select id="category-id" class="form-control" name="category_id">
                                                <option value="{{ $default_category_id }}"> {{ $default_category_name }} </option>
                                                @foreach($other_categories as $category)
                                                    <option value="{{ $category->id }}"> {{ $category->category_name }} </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        {!! $errors->first('category_id', '<p class="text-danger">:message</p>') !!}
                                    </div>

                                    <div class="form-group {{ $errors->first('contributed_by') != "" ? "has-error" : "" }}">
                                        <label for="contributed_by" class="control-label"> Contributed By: </label>
                                        <div>
                                            <select id="contributed_by" class="form-control" name="contributed_by">
                                                <option>-- Select Contributor --</option>
                                                @foreach($users as $user)
                                                    <option value="{{ $category->id }}" {{ $user->id == old('contributed_by') ? "selected" : ($edit_news->author_id == $user->id ? "selected" : "") }}> {{ $user->name }} </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        {!! $errors->first('contributed_by', '<p class="text-danger">:message</p>') !!}
                                    </div>

                                    <div id="news-status" class="form-group  {{ $errors->first('news_status') != "" ? "has-error" : "" }}">
                                        <label for="news_status" class="control-label"> Status<span class="required">*</span>: </label>
                                        <div>
                                            {!! Form::select('news_status', ["Published"=>"Published", "Draft"=>"Draft"], $edit_news_status, ["class" => "form-control"]) !!}
                                        </div>
                                    </div>
                                    <div id="feature-pic">
                                        <label> Feature Picture </label>
                                        <div class="img-thumbnail" id="feature-picture-frame">
                                            <div id="feature-picture-bg">
                                                @if ($feature_picture != null)
                                                    <img class="feature" id="feature-picture" src="/assets/uploads/upload-news-pictures/{{ $edit_news->id }}/{{ $feature_picture }}" />
                                                @endif
                                            </div>
                                        </div>
                                        <div id="btn-feature">
                                            <span class="btn btn-picture">Feature Picture</span>
                                            <input type="file" class="btn browse-btn" id="feature" name="feature_picture" value="Feature Picture"/>
                                        </div>
                                    </div>
                                </div>

                                <input type="button" class="btn btn-picture" id="btn-add-picture" data-toggle="modal" data-target="#news-change-picture" value="Media"/>
                                <br>
                                <!-- Modal Change Picture -->
                                <div class="modal fade" id="news-change-picture" tabindex="-1" role="dialog" aria-labelledby="add-picture" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">

                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title">Media</h4>
                                                </div>

                                                <div class="modal-body">
                                                    <!-- Tab Contents -->
                                                    @foreach($old_pictures as $old_picture)
                                                        <div class="news-picture-frame media-object" id="{{$old_picture->id}}">
                                                            <div class="vertical-align">
                                                                <img class="edit-pictures" src="/assets/uploads/upload-news-pictures/{{ $edit_news->id }}/{{ $old_picture->file_name }}" />
                                                                <button type="button" class="delete-picture close">
                                                                    <span class="glyphicon glyphicon-trash"></span>
                                                                </button>
                                                                <button type="button" class="insert-picture close">
                                                                    <span class="glyphicon glyphicon-circle-arrow-down"></span>
                                                                </button>
                                                                <span> {{ $old_picture->file_name }} </span> <!-- nama gambar -->
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>

                                                <div class="modal-footer">
                                                    <button type="button" id="finish" class="btn btn-primary" data-dismiss="modal">Finish</button>
                                                    <div id="btn-insert">
                                                        <span class="btn btn-picture">Select File</span>
                                                        <input type="file" class="btn browse-btn file" id="upload-pictures" name="upload_pictures[]" multiple value="Add Picture"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> <!-- News Modal Picture -->

                                <!-- Menampung nama pictures yang di-upload -->
                                <input type="hidden" id="temp-pictures" name="temp_pictures"/>

                                <!-- News Content -->
                                <div id="news-content" role="tabpanel">
                                    <ul class="nav nav-tabs" role="tablist">
                                      <li role="presentation" class="active"><a href="#news-content-text" id="text-tab" role="tab" data-toggle="tab" aria-controls="" aria-expanded="true">Text</a></li>
                                      <li role="presentation"><a href="#news-content-preview" id="preview-tab" role="tab" data-toggle="tab" aria-controls="">Preview</a></li>
                                    </ul>
                                    <!-- News Content -->
                                    <div class="tab-content">
                                        <textarea class="tab-pane active" id="news-content-text" rows="17" cols="80" name="news_content_text" role="tabpanel" aria-labelledBy="text-tab">{{ old('news_content_text') ? : $edit_news->content }}</textarea>
                                        <div class="tab-pane" id="news-content-preview" role="tabpanel" aria-labelledBy="preview-tab"></div>
                                    </div>
                                </div>
                                {!! $errors->first('news_content_text', '<p class="text-danger">:message</p>') !!}
                                <br>
                                <div id="post-btn">
                                    <input class="btn btn-primary" type="submit" name="save_changes" value="Save Changes" />
                                    <button class="btn btn-danger" id="reset" type="reset" value="Reset" > Reset </button>
                                </div>
                            </form>
                        </div>
                        

                    </div>

                    <div class="col-sm-3">
                        <!-- Place for Categories -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content_js')
    <script src="/assets/js/commonmark.js"></script>
    <script type="text/javascript">
        /* Form validation */
        $('#post-title').on('change', function(){
            $(this).val($.trim($(this).val()));
        });
        // ketika submit
        $('#post-btn :submit').on('click', function(event){
            if($("#post-title").val() == '') {
                event.preventDefault();
                $("#post-title").after('<div class="error"><span>This field is may not be empty.</span></div>');
            }
            if($("#permalink-result").text() == '') {
                event.preventDefault();
                $("#edit-permalink").after('<div class="error"><span>This field is may not be empty.</span></div>');
            }
        });
        // Hapus pesan error terdekat ketika field input diklik
        $('#post-title, #edit-permalink').on('click', function(){
            $(this).nextAll('.error').remove();
        });

        /* Konvesi Teks ke Slug */
        function text_slug(str){
            str = str.trim(); // trim
            str = str.toLowerCase();
            
            str = str.replace(/[^a-z0-9 -]/g, '-') // remove invalid chars
            .replace(/\s+/g, '-') // collapse whitespace and replace by -
            .replace(/-+/g, '-') // collapse dashes
            .replace(/^-+/g, '')  // collapse characters in the start
            .replace(/-+$/g, '');  // collapse characters in the end
            return str;
        }
        /* Fungsi untuk Permalink */
        function permalink(){
            var text = $("#post-title").val();
            var slug = text_slug(text);
            $("#permalink-result").text(slug);
            $("#duplicate-permalink").attr('value', slug);
        }
        $("#post-title").on('change keyup paste click', function(){
            permalink();
        });
        /* Menampilkan Tombol Edit */
        $("#post-title").on('change keyup paste click', function(){
            $("#edit-permalink").css("display", "inline-block");
        });
        /* Edit Permalink */
        $("#edit-permalink").on('click', function(){
            var text = $("#permalink-result").text();
            $("#permalink-result").replaceWith("<input id='field-permalink' class='form-control' type='text' name='field-permalink' required value='" + text + "' autofocus />" );
            $("#edit-permalink").css("display", "none");
            $("#btn-permalink-save").css("display", "block");
        });
        /* Save Permalink */
        $("#btn-permalink-save, #post-title").on('click', function(){
            var text = $("#field-permalink").val();
            var slug = text_slug(text);
            $("#field-permalink").replaceWith("<span id='permalink-result'>" + slug + "</span>");
            $("#btn-permalink-save").css("display", "none");
            $("#edit-permalink").css("display", "inline-block");
            $("#duplicate-permalink").attr('value', slug);
        });
        /* Reset Form: Permalink & Title */
        $("#reset").on('click', function(){
            // $("#post-title").empty();
            $("#edit-permalink").css("display", "none");
            $("#permalink-result").empty();
            $("#duplicate-permalink").empty();
            $('#news-content-text').empty();
            var pictures = $("#upload-pictures");
            var feature_pic = $("#feature");
            pictures.replaceWith( pictures = pictures.clone( true ) );
            $(".modal-body").empty();
            feature_pic.replaceWith( feature_pic = feature_pic.clone( true ) );
            $("#feature-picture-bg").empty();
        });
        /* Commonmark */
        function marker() {
            var text = $("#news-content-text").val();
            var reader = new commonmark.Parser();
            var writer = new commonmark.HtmlRenderer();
            var parsed = reader.parse(text);
            var result = writer.render(parsed);
            $("#news-content-preview").html(result);
        }
        $("#preview-tab").on('click', function(){
            marker();
        });

        /*Fungsi untuk Upload and Preview Images - Edit News*/
        function uploadTemp(files, status) {
            $.each(files, function(key,file){
                var type = file.type.toString();
                if(type.indexOf("image/") != -1){
                    var data = new FormData();
                    data.append('0', file);
                    data.append('news_id', {{ $edit_news->id }});
                    if (status == 'Feature') {
                        data.append('picture_status', 'Feature');
                    }
                    $.ajax({
                        url: '/admin/news/uploadTemp',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type: 'POST',
                        data: data,
                        cache: false,
                        dataType: 'json',
                        processData: false, // Don't process the files
                        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                        success: function(data, textStatus, jqXHR)
                        {
                            if(data.status == 'ok'){
                                // Jika yang diisi adalah gambar feature
                                if (status == 'Feature') {
                                    $("#feature-picture-bg").html('<img class="feature" id="feature-picture" src="/assets/uploads/upload-news-pictures/'+{{$edit_news->id}}+'/'+data.filename+'?'+Math.random()*Math.random()+ '"/>');
                                }
                                else {
                                    // upload gambar di modal
                                    $(".modal-body").append('<div class="news-picture-frame media-object"><div class="vertical-align"><img class="edit-pictures" src="/assets/uploads/upload-news-pictures/'+{{$edit_news->id}}+'/'+data.filename+'?'+Math.random()*Math.random()+ '"/>  <button type="button" class="delete-picture close"><span class="glyphicon glyphicon-trash"></span></button> <button type="button" class="insert-picture close"><span class="glyphicon glyphicon-circle-arrow-down"></span></button><span>'+data.filename+'</span> </div> </div>');
                                }
                                var picture_name = data.filename;
                                // menambahkan nama gambar feature ke form input hidden
                                name_array.push(picture_name);
                                $("#temp-pictures").val(name_array);
                            }
                            
                        },
                        error: function(jqXHR, textStatus, errorThrown)
                        {
                            
                        }
                    });
                }
            })
        }

        /*Fungsi untuk Delete gambar di Modal dan folder temp - Edit News*/
        function deletePictures(file_delete, element_delete, status) {
            var data = new FormData();
            data.append('file_delete', file_delete);
            data.append('news_id', {{ $edit_news->id }});
            if (status == 'Feature') {
                data.append('picture_status', 'Feature');
            }
            $.ajax({
                url: '/admin/news/removeTemp',
                headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                type: 'POST',
                data: data,
                cache: false,
                dataType: 'json',
                processData: false, // Don't process the files
                contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                success: function(data, textStatus, jqXHR)
                {
                    
                    if(data.status == 'ok'){
                        // menghilangkan nama gambar yang di-close pada modal
                        // pada form input hidden
                        name_array = $.grep(name_array, function(value) {
                                          return value != file_delete;
                                        });
                        // menambahkan nama file ke form input hidden
                        $("#temp-pictures").val(name_array);
                        element_delete.remove();
                    }
                    
                },
                error: function(jqXHR, textStatus, errorThrown)
                {

                }
            });
        }

        var name_array = [];
        // Upload gambar feature
        $("#feature").on('change', function(){
            var files = $(this)[0].files;
            var status = "Feature";
            uploadTemp(files, status);

            // Jika sudah ada gambar feature, maka ketika memilih gambar feature baru,
            // gambar yang lama akan dihapus
            if($("#feature-picture").length > 0) {
                // mengambil nama gambar yang akan dihapus
                var file_delete = $("#feature-picture-bg").children().attr('src').split('/').pop();
                var element_delete = $("#feature-picture-bg").children();
                deletePictures(file_delete, element_delete, status);
            }
        });
        // Upload gambar modal
        $('#upload-pictures').on('change', function(){
            var files = $(this)[0].files;
            var status = "null";
            uploadTemp(files, status);
        });
        // Hapus gambar di modal
        $('.modal-body').on('click', '.delete-picture', function(){
            var status = "";
            var file_delete = $(this).prev().attr('src').split('/').pop().split('?')[0];
            var element_delete = $(this).parent().parent();
            deletePictures(file_delete, element_delete, status);
            // hapus gambar di textarea
            var content = $("#news-content-text").val();
            var regex = new RegExp("..image.+" + file_delete + ".", "g");
            $("#news-content-text").val(content.replace(regex,''));
        });
        /* Add image link in commonmark */
        $('.modal-body').on('click', '.insert-picture', function(){
            var picture_name = $(this).prev().prev().attr('src').split('/').pop().split('?')[0];
            $("#news-content-text").val($("#news-content-text").val()+'![image](/assets/uploads/upload-news-pictures/'+{{$edit_news->id}}+'/'+ picture_name +')');
            // Mengisikan nama gambar ke form input hidden untuk diproses ketika submit
            /*name_array.push(picture_name);
            $("#temp-pictures").val(name_array);*/
        });
        
    </script>
@endsection
