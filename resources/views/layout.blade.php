<!DOCTYPE html>
<html  class="no-js" lang="en">
	<!--------------------------------------------------------------------------

	Hello, are you interested in HTML, CSS, JS, and PHP?
	We are currently hiring now.
	Contact us on contact@durenworks.com with your CV and project documentation.

	---------------------------------------------------------------------------->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="shortcut icon" href="/{{ Config::get('custom_path.favicon') }}">
		@yield('content_title')

		<!-- Bootstrap -->
		<link href="/assets/css/bootstrap.min.css" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Raleway:400,500,700,600,800" rel="stylesheet" type="text/css">
		<link href="/assets/css/durenworks.css" rel="stylesheet">
		<link href="/assets/css/responsive.css" rel="stylesheet">

		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-52844782-1', 'auto');
			ga('send', 'pageview');
		</script>

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<!-- Content START -->
		
		<!-- Static navbar -->
		<nav class="navbar navbar-default navbar-static-top">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<!-- Logo Brand-->
					<a class="navbar-brand" href="{{ action('HomeController@index') }}"><img src="/assets/images/logo_durenworks.png"/></a>
				</div>
				
				<div id="navbar" class="collapse">
					<ul class="nav navbar-nav"> <!--navbar-right-->
						<li><a href="{{ action('AboutUsController@index') }}" {{ $active[0] == 'about-us' ? "class=active" : "" }}>About Us</a></li>
						<li><a href="{{ action('ServicesController@index') }}" {{ $active[0] == 'services' ? "class=active" : "" }}>Services</a></li>
						<li><a href="{{ action('ProductsController@index') }}" {{ $active[0] == 'products' ? "class=active" : "" }}>Products</a></li>
						<li><a href="{{ action('WorksController@index') }}" {{ $active[0] == 'works' ? "class=active" : "" }}>Our Works</a></li>
						<li><a href="{{ action('NewsController@index') }}" {{ $active[0] == 'news' ? "class=active" : "" }}>News</a></li>
						<li><a href="{{ action('CareersController@index') }}" {{ $active[0] == 'careers' ? "class=active" : "" }}>Careers</a></li>
						<li><a href="{{ action('ContactUsController@index') }}" {{ $active[0] == 'contact-us' ? "class=active" : "" }}>Contact Us</a></li>
					</ul>
				</div><!--/.nav-collapse -->
			</div>
		</nav>
		@yield('content')

		<footer>
			<hr class="footer-line">
			<div class="footer-top hidden-xs">
				<div class="container">
					<div class="row footer-top-head">
						<div class="col-md-3 col-sm-3 hidden-979 hidden-xs">
							<img src="/assets/images/logo_durenworks_gray.png" height="100%">
						</div>
						<div class="col-md-3 col-sm-3">
							<h4>Services</h4>
						</div>
						<div class="col-md-3 col-sm-3">
							<h4>Products</h4>
						</div>
						<div class="col-md-3 col-sm-3">
							<h4>Contact Us</h4>
						</div>
					</div>
					<div class="row footer-top-content">
						<div class="col-md-3 col-sm-3 hidden-979 hidden-xs">
							<p>Digital Utilities, advertising, and engineering works. Founded in Semarang Central Java. Indonesia by Andrie Widyastama and Airlangga Cahya Utama that have experienced over 5 years in IT solution.</p>
							<a href="{{ action('AboutUsController@index') }}">Read more <span class="glyphicon glyphicon-triangle-right"></span></a>
						</div>
						
						<div class="col-md-3 col-sm-3">
							<div class="row">
								<a href="{{ action('ServicesController@index') }}">
									<div class="col-md-10 col-sm-10">
										<p>Web Design &amp; Development</p>
									</div>
									<div class="col-md-2 col-sm-2">
										<span class="glyphicon glyphicon-triangle-right"></span>
									</div>
								</a>
							</div>
							<div class="row">
								<hr width="100%"></hr>
							</div>
							<div class="row" >
								<a href="{{ action('ServicesController@index') }}#business-application">
									<div class="col-md-10 col-sm-10">
										<p>Business Application</p>
									</div>
									<div class="col-md-2 col-sm-2">
										<span class="glyphicon glyphicon-triangle-right"></span>
									</div>
								</a>
							</div>
							<div class="row">
								<hr width="100%"></hr>
							</div>
							<div class="row">
								<a href="{{ action('ServicesController@index') }}#mobile-application">
									<div class="col-md-10 col-sm-10">
										<p>Mobile Application</p>
									</div>
									<div class="col-md-2 col-sm-2">
										<span class="glyphicon glyphicon-triangle-right"></span>
									</div>
								</a>
							</div>
							<div class="row">
								<hr width="100%"></hr>
							</div>
							<div class="row">
								<a href="{{ action('ServicesController@index') }}#digital-marketing">
									<div class="col-md-10 col-sm-10">
										<p>Digital Marketing</p>
									</div>
									<div class="col-md-2 col-sm-2">
										<span class="glyphicon glyphicon-triangle-right"></span>
									</div>
								</a>
							</div>
							
						</div>
						<div class="col-md-3 col-sm-3">
							<div class="row">
								<a href="{{ action('ProductsController@index') }}#pawons">
									<div class="col-md-10 col-sm-10">
										<p>Pawons</p>
									</div>
									<div class="col-md-2 col-sm-2">
										<span class="glyphicon glyphicon-triangle-right"></span>
									</div>
								</a>
							</div>
							<div class="row">
								<hr width="100%"/>
							</div>
							<div class="row">
								<a href="{{ action('ProductsController@index') }}">
									<div class="col-md-10 col-sm-10">
										<p>Mondopad by Infocus</p>
									</div>
									<div class="col-md-2 col-sm-2">
										<span class="glyphicon glyphicon-triangle-right"></span>
									</div>
								</a>
							</div>
							<div class="row">
								<hr width="100%"/>
							</div>
							<div class="row">
								<a href="{{ action('ProductsController@index') }}#avaya">
									<div class="col-md-10 col-sm-10">
										<p>Avaya</p>
									</div>
									<div class="col-md-2 col-sm-2">
										<span class="glyphicon glyphicon-triangle-right"></span>
									</div>
								</a>
							</div>
						</div>
						<div class="col-md-3 col-sm-3">
							<table class="contact-us">
								<tr>
									<td class="contact-us-icon"><span class="glyphicon glyphicon-map-marker"></span></td>
									<td class="contact-us-attribute">Address:</td>
									<td class="contact-us-content">Jl. Jatisari II/4, Tembalang</td>
								</tr>
								<tr>
									<td class="contact-us-icon"><span class="glyphicon glyphicon-earphone"></span></td>
									<td class="contact-us-attribute">Phone:</td>
									<td class="contact-us-content">+6224-7462189</td>
								</tr>
								<tr>
									<td class="contact-us-icon"><span class="glyphicon glyphicon-send"></span></td>
									<td class="contact-us-attribute">Email:</td>
									<td class="contact-us-content">contact@durenworks.com</td>
								</tr>
							</table>
							<div class="row">
								<div class="col-md-2 col-sm-2">
									<a href="https://www.facebook.com/pages/Durenworks/669509426431580" target="_blank">
										<div class="sprite facebook-icon"></div>
									</a>
								</div>
								<div class="col-md-2 col-sm-2">
									<a href="https://twitter.com/durenworks" target="_blank">
										<div class="sprite twitter-icon"></div>
									</a>
								</div>
								<div class="col-md-2 col-sm-2">
									<a href="https://plus.google.com/101815098485465188722" target="_blank">
										<div class="sprite google-icon"></div>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="footer-bottom">
				<div class="container">
					<div class="footer-bottom-left">
						Copyright &copy; 2014
					</div>
					<div class="footer-bottom-right hidden-xs">
						Designed by <b>Durenworks</b>
					</div>
				</div>
			</div>

		</footer>
		<!-- Content END -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
	<script src="/assets/js/jquery-1.11.2.min.js"></script>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/assets/js/bootstrap.min.js"></script>

    @yield('content_js')
    </body>
</html>