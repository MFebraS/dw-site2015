@extends('layout')

@section('content_title')
	<title>Durenworks</title>
	<style type="text/css">
		
	</style>
@stop

@section('content')
<!-- SLIDER -->
		<div class="row slider">
			<div id="slider" class="carousel slide" data-ride="carousel">
				<ol class="carousel-indicators">
					@foreach($sliders as $key => $slider)
						<li data-target="#slider" data-slide-to="{{ $key }}" {{ $key == 0 ? "class=active" : "" }}></li>
					@endforeach
				</ol>

				<div class="carousel-inner" >
					@foreach($sliders as $key => $slider)
						<div class="item {{ $key == 0 ? "active" : "" }}">
							<div class="bg-slider" style="background-image:url(/assets/uploads/sliders/{{ $slider->image }})">
								<img class="bg-slider2" src="/assets/images/index/slider/bg-slider.png">
								<div class="text-slider">
									<div class="container">
										<div class="row">
											@if($slider->description_position == 'right' && $slider->subimage != null)
												<div class="col-sm-6" style="height: 100%;">
													<div class="vertical-center" style="margin-left: auto">
														<div class="subimage-content">
															<img src="{{ Config::get('custom_path.sliders') }}/{{ $slider->subimage }}" alt="">
														</div>
													</div>
												</div>
											@endif
											<div class="{{ $slider->description_position == 'center' ? "col-sm-offset-3" : ($slider->description_position == 'right' && $slider->subimage == null ? "col-sm-offset-6" : "") }} col-sm-6 vertical-center">
												<div class="text-content">
													{!! html_entity_decode($slider->description) !!}
												</div>
												
											</div>
											@if($slider->description_position == 'left' && $slider->subimage != null)
												<div class="col-sm-6 vertical-center">
													<div class="subimage-content">
														<img src="{{ Config::get('custom_path.sliders') }}/{{ $slider->subimage }}" alt="">
													</div>
												</div>
											@endif
										</div>
									</div>
								</div>
							</div>
						</div>
					@endforeach
				</div>

				<!-- Slider Controls -->
				<a class="left carousel-control" href="#slider" data-slide="prev">
					<span><img src="/assets/images/index/slider/arrow_left.png"></span>
				</a>
				<a class="right carousel-control" href="#slider" data-slide="next">
					<span><img src="/assets/images/index/slider/arrow_right.png"></span>
				</a>
			</div>
			<div class="visible-xs xs-carousel">
				<img src="/assets/images/index/slider/small-display.png" width="100%">
			</div>
		</div>
		<!-- END SLIDER -->
		
		<div class="welcome">
			<div class="container">
				<div class="row">
					<div class="col-sm-7">
						<h3 class="text-left">Welcome to Durenworks</h3>
						<p>Durenworks, Digital Utilities, Advertising, and Engineering Works, is a full service IT Consultant &amp; Solution expertised in Webside design &amp; development, Business Solution Provider, Mobile Application Developer, and Digital Marketing Strategy.</p>
						<p>We Established on October 2013 in Semarang, Indonesia. As affiliated company of PT. Widya Solusi Utama for IT Services / Consulting in 2015, strive to fulfils all your business needs under one roof. providing innovative, unique, and high quality services, based on requirements of clients.</p><br/>
						<div class="button-danger-place">
							<a href="{{ action('AboutUsController@index') }}"><button type="button" class="btn btn-danger">LEARN MORE</button></a>
						</div>
					</div>
					<div class="col-sm-5 hidden-xs">
						<img src="/assets/images/index/welcome/mobile.jpg" width="95%">
					</div>
				</div>
			</div>
		</div>
		
		
		<div class="what-we-do">
			<div class="container">
				<h3>What we do</h3>
				<p class="subtitle-h3">Our best skills and capabilities to deliver effectiveness and achieving your business goals</p>
				<div class="row">
					<div class="col-xs-6 col-sm-3 web">
						<a href="{{ action('ServicesController@index') }}">
							<div class="title">
								<div class="icon-place">
                                    <div class="index-sprite icon"></div>
                                </div>
                                <h4>Web &amp; App Design</h4>
							</div>
							<p>The most fundamental needs to introduce your business to customers clean and neat design</p>
							<div class="read-more">
								Read more <span class="glyphicon glyphicon-triangle-right"></span>
							</div>
						</a>
					</div>
					<div class="col-xs-6 col-sm-3 marketing">
						<a href="{{ action('ServicesController@index') }}#digital-marketing">
							<div class="title">
								<div class="icon-place">
                                    <div class="index-sprite icon"></div>
                                </div>
                                <h4>Digital Marketing Strategy</h4>
							</div>
							<p>Reach your potential customer with most targeted, calculated, and result base media in the world</p>
							<div class="read-more">
								Read more <span class="glyphicon glyphicon-triangle-right"></span>
							</div>
						</a>
					</div>
					<div class="col-xs-6 col-sm-3 development">
						<a href="{{ action('ServicesController@index') }}#business-application">
							<div class="title">
								<div class="icon-place">
                                    <div class="index-sprite icon"></div>
                                </div>
                                <h4>Development</h4>
							</div>
							<p>Our experienced Engineer with the most advance programming technology, strive to fulfill your business needs</p>
							<div class="read-more">
								Read more <span class="glyphicon glyphicon-triangle-right"></span>
							</div>
						</a>
					</div>
					<div class="col-xs-6 col-sm-3 collaboration">
						<a href="{{ action('ProductsController@index') }}">
							<div class="title">
								<div class="icon-place">
                                    <div class="index-sprite icon"></div>
                                </div>
                                <h4>Collaborate Sollution</h4>
							</div>
							<p>Delivering latest teleco technology base on data communication video / call conference, IP PABX, and call center</p>
							<div class="read-more">
								Read more <span class="glyphicon glyphicon-triangle-right"></span>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
		
		<div class="some-work">
			<div class="container">
				<h3 class="text-center">Some of Our Works</h3>
				<p class="text-center">The gallery of our team work an overview how we spend our day all this time, giving you visually what we capable of.</p><br/>
				
				<div class="row">
					@forelse($our_works as $key => $work)
						<div class="some-work-img col-sm-6 col-md-4">
							<div class="img-wrap">
								<a href="{{ action('WorksController@index') }}#{{ $work['slug'] }}">
									<img src="/{{ Config::get('custom_path.works') }}/{{ $work['picture'] }}" alt="{{ $work['project_title'] }}'s image">
								</a>
							</div>
							<div class="caption-place">
								<div class="caption-arrow"></div>
								<div class="caption">
									<h5>{{ str_limit($work['project_title'], 40) }}</h5>
									<p>{{ $work['tags'] }}</p>
								</div>
							</div>
						</div>
					@empty
						<h5 class="text-center">No Record Found</h5>
					@endforelse
				</div>
			</div>
			<div class="row see-all {{ $count >= 6 ? "" : "hide" }}">
				<p class="text-center"><a  href="{{ action('WorksController@index') }}">SEE ALL OUR WORK</a></p>
			</div>
		</div>
		
		
		<div class="some-client" >
			<div class="container">
				<div class="row text-center">
					<h3>Some of Our Client</h3>
					<p class="subtitle-h3">We love seeing our satisfied client and we expect your business logo show on this section</p>
					<br/><br>
					
 
					<div class="row">
						@forelse($clients as $key => $client)
							<div class="col-sm-3 col-md-3 col-xs-12 clients" style="padding-bottom:15px">
								{!! $client->count > 0 ? "<a href=".action('WorksController@index', ['client' => $client->slug]).">" : "" !!}
								<img src="/{{ Config::get('custom_path.clients') }}/{{$client->logo}}" width="100%" {{ $client->count > 0 ? "style=cursor:pointer" : "" }}>
								{!! $client->count > 0 ? "</a>" : "" !!}
							</div>
						@empty
							<h5 class="text-center">No Record Found</h5>
						@endforelse
					</div>
				</div>
			</div>
		</div>
@endsection
@section('content_js')

		<script type="text/javascript">
			var window_width = $(window).width();
			$("#slider").bind('slide.bs.carousel', function (e) {
				$('.text-slider').css('opacity', '0');
    			$('.text-slider').delay(400).animate({
                    'opacity':'1'
                }, 800);
			});
	        if(window_width < 755){
				$('.some-work-img a').on('click', function(event) {
					event.preventDefault();
				});
			

			
		</script>
@endsection
